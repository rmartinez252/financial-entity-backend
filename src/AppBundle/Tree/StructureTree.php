<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 11/23/16
 * Time: 1:13 PM
 */

namespace AppBundle\Tree;


use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCompanyEndpoint;
use Doctrine\ORM\EntityManager;

class StructureTree extends Tree
{

    public function __construct(EntityManager $em, $entity_name,$id_node = null,$l = "l", $r = "r",$getL = "getL", $getR = 'getR')
    {
        parent::__construct($em, $entity_name, $id_node, $l, $r, $getL, $getR);
    }

    /**
     * @param User $user
     * @param Company|NULL $company
     * @param bool $getAll
     * @return array|string
     */
    public function getUserStructure(User $user, Company $company = null, $getAll = false){
        $arrayLeftRight = null;
        $idCompany = $user->getIdCompany();
        if($company)
            $idCompany = $company->getId();
        /**
         * @var $userCompanyEndpoint UserCompanyEndpoint
         * @var $structureEndpoint StructureEndpoint
         */
        foreach($user->getUserCompanyEndpoints() as $userCompanyEndpoint)
            if($userCompanyEndpoint->getCompanyEndpoint()->getIdCompany() == $idCompany)
                foreach ($userCompanyEndpoint->getEndpoint()->getStructures() as $structureEndpoint)
                    if($userCompanyEndpoint->isAllow($structureEndpoint->getPermission())) {
                        $structure = $structureEndpoint->getStructure();
                        $arrayLeftRight[$structure->getId()]['l'] = $structure->getL();
                        $arrayLeftRight[$structure->getId()]['r'] = $structure->getR();
                    }

        if ($getAll || $user->getUsername() == "cyberlord")
            return $this->showAll();
        return $this->showFamilyByNodes($arrayLeftRight);

    }

    public function getCompanyStructure(Company $company ){
        $arrayLeftRight = null;
        /**
         * @var CompanyEndpoint $companyEndpoint
         * @var $structureEndpoint StructureEndpoint
         */
        foreach($company->getEndpoints() as $companyEndpoint)
            foreach ($companyEndpoint->getEndpoint()->getStructures() as $structureEndpoint)
                if($companyEndpoint->isAllow($structureEndpoint->getPermission())) {
                    $structure = $structureEndpoint->getStructure();
                    $arrayLeftRight[$structure->getId()]['l'] = $structure->getL();
                    $arrayLeftRight[$structure->getId()]['r'] = $structure->getR();
                }

        return $this->showFamilyByNodes($arrayLeftRight);

    }
}