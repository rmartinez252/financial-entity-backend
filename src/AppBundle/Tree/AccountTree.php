<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 11/23/16
 * Time: 1:13 PM
 */

namespace AppBundle\Tree;


use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\Entry;
use Doctrine\ORM\EntityManager;

class AccountTree extends Tree
{

    public function __construct(EntityManager $em, $entity_name,$id_node = null,$l = "l", $r = "r",$getL = "getL", $getR = 'getR')
    {
        parent::__construct($em, $entity_name, $id_node, $l, $r, $getL, $getR);
    }


    protected function show($result ,$leftval=null,$rightval=null,$allResult=null)
    {
        if(!$allResult){
            $allResult = $result;
        }
        $R = $this->getr;
        $L = $this->getl;
        $right = array();
        $nodes = array();
        /**
         * @var $row Account
         */
        foreach($result as /*$key => */$row ) {
            $rowL = $row->$L();
            $rowR = $row->$R();
            // Solo cuenta la escala si a pasado por la primera vuelta
            if(($leftval == $rightval) ||($rowL>$leftval && $rowL<$rightval )){
                if (count($right))
                    // revisa si debe regresarse en la escala dependiendo del valor del lado derecho actual comparando con los valores guardados en $right
                    while (isset($right[count($right)-1]) && $right[count($right)-1]< $rowR){
                        array_pop($right);
                    }
                if($row->getNature() == 2){

                    $row->setNetIncomeAmount($allResult);
                }elseif($rowL+1 != $rowR) {

                    $children = array_filter($result, function ($i) use ($rowL, $rowR, $L, $R) {
                        if ($i->$L() > $rowL && $i->$R() < $rowR) {
                            return true;
                        }
                        return false;
                    });

                    $children = $this->show($children,$rowL,$rowR,$allResult);
                    $row->setParentTotalAmounts($children,$allResult);
                    $children = false;
                }
//                    $node['node'] = $row;
//                    $node['level'] = count($right);
//                    $nodes[] = $node;
//                    array_merge($nodes,$children);
//                    $result = array_filter($result, function($i) use($rowL,$rowR,$L,$R){
//                        if ($i->$L() > $rowR ) {
//                            return true;
//                        }
//                        return false;
//                    });
//                }else{
                $node['node'] = $row;
                $node['level'] = count($right);
                $nodes[] = $node;
//                }
                $node = null;
                $right[] = $rowR;
            }
        }

        return  $nodes;
    }
    public function createCodes(){
        $codes = $this->showCodes($this->findAll());
        foreach ($codes as $c){
            $this->repository->updateCode($c['id'],$c['code']);
        }
    }
    public function showCodes($result)
    {

        $R = $this->getr;
        $right = array();
        $codes = array();
        $thisCode = array(1);
        /**
         * @var $row Account
         */
        foreach ($result as $row) {
            $rowR = $row->$R();
            // Solo cuenta la escala si a pasado por la primera vuelta
            if (count($right)){
                $fl = 0 ;
                // revisa si debe regresarse en la escala dependiendo del valor del lado derecho actual comparando con los valores guardados en $right
                while (isset($right[count($right) - 1]) && $right[count($right) - 1] < $rowR) {
                    if($fl)
                        if(isset($thisCode[count($right)]))
                            array_pop($thisCode);

                    $thisCode[count($right)-1]++;

                    $fl++;

                    array_pop($right);
                }
            }


            if(!isset($thisCode[count($right)]))
                $thisCode[count($right)] = 1;
            $codeString = false;
            foreach ($thisCode as $c){
                if(!$codeString)
                    $codeString = $c;
                else
                    $codeString .= sprintf("%02d",$c);
            }
            $node['id'] = $row->getId();
            $node['code'] = $codeString;

            $codes[] = $node;
            $node = null;
            $right[] = $rowR;
        }

        return $codes;
    }


    protected function showEntryFilter($result ,$entryFilter,$leftval=null,$rightval=null,$allResult=null)
    {
        if(!$allResult){
            $allResult = $result;
        }
        $dateStart = $dateEnd = null;
        if($entryFilter)
            foreach ($entryFilter as $type => &$value) {
                switch ($type) {
                    case 'dateStart':
                        $dateStart = \DateTime::createFromFormat("Y-m-d", $value)->format("U");
                        break;
                    case 'dateEnd':
                        $dateEnd = \DateTime::createFromFormat("Y-m-d", $value)->format("U");
                        break;
                }
            }
        $R = $this->getr;
        $L = $this->getl;
        $right = array();
        $nodes = array();
        /**
         * @var $row Account
         */
        foreach($result as /*$key => */$row ) {
            $rowL = $row->$L();
            $rowR = $row->$R();
            if($entryFilter)
                if($dateStart || $dateEnd){
                    foreach ($row->getEntrysDebit() as &$entry){
                        if($dateStart){
                            if($entry->getAccountingEntry()->getDateUser()->format("U") < $dateStart)
                                $row->removeEntryDebit($entry);
                        }
                        if($dateEnd){
                            if($entry->getAccountingEntry()->getDateUser()->format("U") > $dateEnd)
                                $row->removeEntryDebit($entry);
                        }
                    }
                    foreach ($row->getEntrysCredit() as &$entry){
                        if($dateStart){
                            if($entry->getAccountingEntry()->getDateUser()->format("U") < $dateStart)
                                $row->removeEntryCredit($entry);
                        }
                        if($dateEnd){
                            if($entry->getAccountingEntry()->getDateUser()->format("U") > $dateEnd)
                                $row->removeEntryCredit($entry);
                        }
                    }
                }
            // Solo cuenta la escala si a pasado por la primera vuelta
            if(($leftval == $rightval) || ($rowL>$leftval && $rowL<$rightval )){
                if (count($right))
                    // revisa si debe regresarse en la escala dependiendo del valor del lado derecho actual comparando con los valores guardados en $right
                    while (isset($right[count($right)-1]) && $right[count($right)-1]< $rowR){
                        array_pop($right);
                    }
                if($row->getNature() == 2){
                    $row->setNetIncomeAmount($allResult);
                }elseif($rowL+1 != $rowR) {

                    $children = array_filter($result, function ($i) use ($rowL, $rowR, $L, $R) {
                        if ($i->$L() > $rowL || $i->$R() < $rowR) {
                            return true;
                        }
                        return false;
                    });

                    $children = $this->showEntryFilter($children,$entryFilter,$rowL,$rowR,$allResult);
                    $row->setParentTotalAmounts($children,$allResult);
                    $children = null;
                }
                $node['node'] = $row;
                $node['level'] = count($right);
                $nodes[] = $node;
                $node = null;
                $right[] = $rowR;
            }
        }

        return  $nodes;
    }

    protected function showEntryFilterBalanceSheet($result ,$entryFilter,$leftval=null,$rightval=null,$allResult=null)
    {
        if(!$allResult){
            $allResult = $result;
        }
        $R = $this->getr;
        $L = $this->getl;
        $right = array();
        $nodes = array();
        /**
         * @var $row Account
         */
        foreach($result as /*$key => */$row ) {
            $rowL = $row->$L();
            $rowR = $row->$R();
            if($entryFilter)
                foreach ($entryFilter as $type => $value){
                    switch($type){
                        case 'dateStart':
                            foreach ($row->getEntrysCredit() as $entry)
                                if($entry->getAccountingEntry())
                                    if($entry->getAccountingEntry()->getDateUser()->format("U") < \DateTime::createFromFormat("Y-m-d",$value)->format("U"))
                                        $row->removeEntryCredit($entry);
                            foreach ($row->getEntrysDebit() as $entry)
                                if($entry->getAccountingEntry())
                                    if($entry->getAccountingEntry()->getDateUser()->format("U") < \DateTime::createFromFormat("Y-m-d",$value)->format("U"))
                                        $row->removeEntryDebit($entry);
                            break;
                        case 'dateEnd':
                            foreach ($row->getEntrysCredit() as $entry)
                                if($entry->getAccountingEntry())
                                    if($entry->getAccountingEntry()->getDateUser()->format("U") > \DateTime::createFromFormat("Y-m-d",$value)->format("U"))
                                        $row->removeEntryCredit($entry);
                            foreach ($row->getEntrysDebit() as $entry)
                                if($entry->getAccountingEntry())
                                    if($entry->getAccountingEntry()->getDateUser()->format("U") > \DateTime::createFromFormat("Y-m-d",$value)->format("U"))
                                        $row->removeEntryDebit($entry);
                            break;
                    }
                }
            // Solo cuenta la escala si a pasado por la primera vuelta
            if(($leftval == $rightval) || ($rowL>$leftval && $rowL<$rightval )){
                if (count($right))
                    // revisa si debe regresarse en la escala dependiendo del valor del lado derecho actual comparando con los valores guardados en $right
                    while (isset($right[count($right)-1]) && $right[count($right)-1]< $rowR){
                        array_pop($right);
                    }
                if($row->getNature() == 2){
                    $row->setNetIncomeAmount($allResult);
                }elseif($rowL+1 != $rowR) {

                    $children = array_filter($result, function ($i) use ($rowL, $rowR, $L, $R) {
                        if ($i->$L() > $rowL || $i->$R() < $rowR) {
                            return true;
                        }
                        return false;
                    });

                    $children = $this->showEntryFilter($children,$entryFilter,$rowL,$rowR,$allResult);
                    $row->setParentTotalAmounts($children,$allResult);
                    $children = null;
                }
                $node['node'] = $row;
                $node['level'] = count($right);
                $nodes[] = $node;
//                }
                $node = null;
                $right[] = $rowR;
            }
        }

        return  $nodes;
    }


    public function showBalanceSheet($dateEnd=null,$nodes = null){
        $R = $this->getr;
        $L = $this->getl;
        $entryFilter = null;
        if($dateEnd)
            $entryFilter['dateEnd'] = $dateEnd;
        if(!$nodes)
            $nodes = $this->showEntryFilter($this->findAll(),$entryFilter);
        $count = 0;
        $balanceSheet = array();
        /**
         * @var $a Account
         * @var $entry Entry
         */
        foreach ($nodes as &$n){
            if($n['level'] == 0)
                $count++;
            if ($count > 3)
                break;
            if(count($n['node']->getEntrysCredit()) || count($n['node']->getEntrysDebit()) || $n['node']->getAmountCredit() || $n['node']->getAmountDebit() )
                $balanceSheet[] = $n;
        }
        return $balanceSheet;
    }

    public function showIncomeStatement($data,$closeDate="12-31",$nodes = null){
        $R = $this->getr;
        $L = $this->getl;
        $entryFilter = null;
        $now = new \DateTime("now");
        $dateEnd = $monthEnd = $dateStart = null;
        if(isset($data['dateEnd']))
            $dateEnd = $data['dateEnd'];
        if(isset($data['monthEnd']))
            $monthEnd = $data['monthEnd'];
        if(isset($data['dateStart']))
            $dateStart = $data['dateStart'];
        if(($dateEnd && $dateStart) && $dateEnd < $dateStart){
            $tmpDate = $dateEnd;
            $dateEnd = $dateStart;
            $dateStart = $tmpDate;
        }
        $entryFilter['dateStart'] = $now->format("Y")."-".$closeDate;
        if(\DateTime::createFromFormat("Y-m-d",$entryFilter['dateStart'])->format("U") > $now->format("U") )
            $entryFilter['dateStart'] = ($now->format("Y")-1)."-".$closeDate;

        if($dateEnd){
            $entryFilter['dateEnd'] = $dateEnd;
            if(!$dateStart){
                $year = explode("-",$dateEnd)[0];
                $dateStart = $year."-".$closeDate;
                $entryFilter['dateStart'] = $dateStart;
                if(\DateTime::createFromFormat("Y-m-d",$entryFilter['dateStart'])->format("U") >= \DateTime::createFromFormat("Y-m-d",$dateEnd)->format("U") )
                    $entryFilter['dateStart'] = $dateStart = ($year-1)."-".$closeDate;
            }else{
                $entryFilter['dateStart'] = $dateStart;
            }
        }
        if($monthEnd){
            $entryFilter['dateEnd'] = $monthEnd;
            $year = explode("-",$monthEnd)[0];
            $month = explode("-",$monthEnd)[1];
            $dateStart = $year."-".$month."-01";
            $entryFilter['dateStart'] = $dateStart;
        }
        if(!$nodes)
            $nodes = $this->showEntryFilter($this->findAll(),$entryFilter);
        $count = 0;
        $incomeStatement['incomeStatement'] = array();
        $incomeStatement['netIncome'] = array();
        /**
         * @var $a Account
         * @var $entry Entry
         */
        foreach ($nodes as $n){
            if($n['level'] == 0)
                $count++;
            if($n['node']->getNature() == 2){
                $n2 = $n;
                $n2['level'] = 0;
                $n2['node']->setHoldValue(false);
                $incomeStatement['netIncome'][] = $n2;
            }
            if($count > 3){
                if(count($n['node']->getEntrysCredit()) || count($n['node']->getEntrysDebit()) || $n['node']->getAmountCredit() || $n['node']->getAmountDebit() )
                    $incomeStatement['incomeStatement'][] = $n;
            }
        }
        return $incomeStatement;
    }





}