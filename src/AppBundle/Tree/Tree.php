<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/10/16
 * Time: 11:42 AM
 */

namespace AppBundle\Tree;


use AppBundle\ResponseModel\treeResponseModel;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class Tree
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var string
     */
    private $entity_name;
    /**
     * @var integer
     */
    private $id_node;
    /**
     * @var EntityRepository
     */
    protected  $repository;
    /**
     * @var string
     */
    private $l;
    /**
     * @var string
     */
    private $r;
    /**
     * @var string
     */
    protected $getl;
    /**
     * @var string
     */
    protected $getr;
    /**
     * @var bool
     */
    private $hidrate;
    /**
     *
     */
    private $resultAll;
    /**
     *
     */
    private $node;
    /**
     * Tree constructor.
     * @param EntityManager $em
     * @param string $entity_name
     * @param integer $id_node
     * @param string $l
     * @param string $r
     * @param string $getL
     * @param string $getR
     *
     */
    public function __construct($em,$entity_name,$id_node = null,$l = "l", $r = "r",$getL = "getL", $getR = 'getR'){
        $this->em = $em;
        $this->repository = $this->em->getRepository($entity_name);
        $this->entity_name = $entity_name;
        if($id_node)
            $node = $this->getNode($id_node);
        $this->l = $l;
        $this->r = $r;
        $this->getl = $getL;
        $this->getr = $getR;
        $this->resultAll = false;
        $this->hidrate = false;
//        if(strpos($this->entity_name , "Account"))
//            $this->hidrate = true;
    }

    /**
     * @param EntityManager $em
     * @return $this
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository($this->entity_name);
        return $this;
    }

    protected function findAll(){
//        if(!$this->resultAll)
        $this->resultAll = $this->repository->findAllOrderByLeft($this->l,$this->hidrate);
        return $this->resultAll;
    }
    private function findFamilyByNodes($arrayLeftRight){
        return  $this->repository->findFamilyForNodes($arrayLeftRight);
    }


    protected function show($result ,$leftval=null,$rightval=null)
    {
        $R = $this->getr;
        $L = $this->getl;
        $right = array();
        $nodes = array();
        foreach($result as /*$key => */$row ) {
            if($this->hidrate){
                $rowL = $row[$this->l];
                $rowR = $row[$this->r];
            }else{
                $rowL = $row->$L();
                $rowR = $row->$R();
            }
            // Solo cuenta la escala si a pasado por la primera vuelta
            if(($leftval == $rightval) ||($rowL>$leftval && $rowL<$rightval )){
                if (count($right))
                    // revisa si debe regresarse en la escala dependiendo del valor del lado derecho actual comparando con los valores guardados en $right
                    while (isset($right[count($right)-1]) && $right[count($right)-1]< $rowR){
                        array_pop($right);
                    }
                $node['node'] = $row;
                if($rowL+1 != $rowR){
                    $children = array_filter($result, function($i) use($rowL,$rowR,$L,$R){
                        if ($i->$L() > $rowL ||$i->$R() < $rowR) {
                            return true;
                        }
                        return false;
                    });
//                    if($keyF = array_search($rowR + 1, array_column($result, 'l')))
//                        $node['children'] = $this->show(array_slice($result,$key,$keyF),$rowL,$rowR);
//                    else
//                        $node['children'] = $this->show(array_slice($result,$key),$rowL,$rowR);
                    $node['children'] = $this->show($children,$rowL,$rowR);
                    $result = array_filter($result, function($i) use($rowL,$rowR,$L,$R){
                        if ($i->$L() > $rowR ) {
                            return true;
                        }
                        return false;
                    });
                }
                if(!count($right))
                    $nodes[$rowL] = $node;
                $node = null;
                $right[] = $rowR;
            }
        }
        return  $nodes;
    }

    public function getNode($id_node){
        return $this->repository->find($id_node);
    }

    // get all tree using left and right parameters to organize
    public function showAll($main = false){
        $result = $this->findAll();
        $show['all'] = $this->show($result);
        if($main){
            $show['main'] = $this->showMain($result);
            return  $show;
        }
        return  $show['all'];
    }

    public function showOneByName($name){
        $result = $this->repository->findBy(array('name'=>$name));
        return  $this->show($result);
    }
    public function showOneById($id){
        $result = $this->repository->findBy(array("id"=>$id));
        return  $this->show($result);
    }
    public function showMain($result = false){
        if(!$result)
            $result = $this->findAll();
        $R = $this->getr;
        $L = $this->getl;
        $right = array();
        $nodes = array();
        foreach($result as $row ) {
            if($this->hidrate){
                $rowL = $row[$this->l];
                $rowR = $row[$this->r];
            }else{
                $rowL = $row->$L();
                $rowR = $row->$R();
            }
            // Solo cuenta la escala si a pasado por la primera vuelta
            if (count($right))
                // revisa si debe regresarse en la escala dependiendo del valor del lado derecho actual comparando con los valores guardados en $right
                while (isset($right[count($right)-1]) && $right[count($right)-1]< $rowR){
                    array_pop($right);
                }
            if(count($right))
                continue;
            //Guarda valores del nodo actual
            $node['node'] = $row;
            if(!count($right))
                $nodes[$rowL] = $node;
            $node = null;
            $right[] = $rowR;
        }
        return  $nodes;
    }


    public function showSonsById($id){
        $R = $this->getr;
        $L = $this->getl;
        if($result = $this->repository->find($id))
            return $this->show($this->repository->findSons($result->$L(),$result->$R()));
        else
            return false;
    }

    public function showParentsById($id)
    {
        $R = $this->getr;
        $L = $this->getl;
        if($result = $this->repository->find($id))
            return $this->show($this->repository->findParents($result->$L(),$result->$R()));
        else
            return false;
    }
    public function getParentsByLOrR($value)
    {
        $R = $this->getr;
        $L = $this->getl;
        return $this->repository->findParentByLOrR($value);

    }
    public function showFamilyById($id)
    {
        $R = $this->getr;
        $L = $this->getl;
        if($result = $this->repository->find($id))
            return $this->show($this->repository->findFamily($result->$L(),$result->$R()));
        else
            return false;
    }
    public function showFamilyByNodes($arrayLeftRight,$main = false){

        if($arrayLeftRight){
            $result = $this->findFamilyByNodes($arrayLeftRight);
            $show['all'] = $this->show($result);
            if($main){
                $show['main'] = $this->showMain($result);
                return  $show;
            }
            return  $show['all'];
        }
        if($main){
            $show['all'] = '';
            $show['main'] = '';
            return  $show;
        }
        return  '';
    }


    public function addOneById($id,$dir = 'subl'){
        $R = $this->getr;
        $L = $this->getl;
        if($result = $this->repository->find($id))
            return $this->addOne($result->$L(),$result->$R(),$dir);
        else
            return false;
    }


    public function addOne($l,$r,$dir = 'subl'){
        switch ($dir)
        {
            case 'left':
                $newnode = $this->repository->updateAddLeft($l);
                break;
            case 'right':
                $newnode = $this->repository->updateAddRight($r);
                break;
            case 'subr':
                $newnode = $this->repository->updateAddSubRight($r);
                break;
            case 'subl':
            default:
                $newnode = $this->repository->updateAddSubLeft($l);
                break;
        }
        return $newnode;
    }
    public function deleteById($id)
    {
        $R = $this->getr;
        $L = $this->getl;
        if($result = $this->repository->find($id)){
            $this->repository->updateBeforeDelete($result->$L(),$result->$R());
            $this->em->remove($result);
            $this->em->flush();
            return $this->showAll();
        }else
            return false;
    }

    /**
     * @return boolean
     */
    public function isHidrate()
    {
        return $this->hidrate;
    }

    /**
     * @param boolean $hidrate
     * @return Tree
     */
    public function setHidrate($hidrate)
    {
        $this->hidrate = $hidrate;
        return $this;
    }


}