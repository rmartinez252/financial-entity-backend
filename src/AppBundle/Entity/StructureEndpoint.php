<?php

namespace AppBundle\Entity;

use AppBundle\Security\PermissionsManager;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * StructureEndpoint
 *
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Table(name="structure_endpoint")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StructureEndpointRepository")
 */
class StructureEndpoint
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="showStructure", type="boolean", nullable=true)
     */
    private $showStructure;

    /**
     * @var Structure
     *
     * @ORM\ManyToOne(targetEntity="Structure", inversedBy="endpoints")
     * @ORM\JoinColumn(name="idStructure", referencedColumnName="id")
     */
    private $structure;
    /**
     * @var Endpoint
     *
     * @ORM\ManyToOne(targetEntity="Endpoint", inversedBy="structures")
     * @ORM\JoinColumn(name="idEndpoint", referencedColumnName="id")
     */
    private $endpoint;

    /**
     * @var int
     *
     * @ORM\Column(name="permission", type="integer")
     */
    private $permission;
    /**
     * @var array
     */
    private $frontEndPermission;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }



    /**
     * Get idStructure
     *
     * @return int
     */
    public function getIdStructure(){
        return $this->structure->getId();
    }



    /**
     * Get idEndpoint
     *
     * @return int
     */
    public function getIdEndpoint(){
        return $this->endpoint->getId();
    }

    /**
     * Set showStructure
     *
     * @param boolean $showStructure
     *
     * @return StructureEndpoint
     */
    public function setShowStructure($showStructure){
        $this->showStructure = $showStructure;
        return $this;
    }

    /**
     * Get showStructure
     *
     * @return bool
     */
    public function getShowStructure(){
        return $this->showStructure;
    }

    /**
     * @return Endpoint
     */
    public function getEndpoint(){
        return $this->endpoint;
    }

    /**
     * @param Endpoint $endpoint
     * @return StructureEndpoint
     */
    public function setEndpoint(Endpoint $endpoint){
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @return Structure
     */
    public function getStructure(){
        return $this->structure;
    }

    /**
     * @param Structure $structure
     * @return StructureEndpoint
     */
    public function setStructure(Structure $structure){
        $this->structure = $structure;
        return $this;
    }

    /**
     * @return int
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @param int $permission
     * @return StructureEndpoint
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;
        return $this;
    }
    /**
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("permissions")
     * @Serializer\Type("array")
     * @Serializer\Groups("CompanyPermissions")
     * @return array
     */
    public function frondEndPermissions(){
        if($this->frontEndPermission)
            return $this->frontEndPermission;
        $permissionManager =  new PermissionsManager();
        return $permissionManager->prosesPermissions($this->permission,"frondend");
    }
    /**
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("name")
     * @Serializer\Type("string")
     * @Serializer\Groups("CompanyPermissions")
     * @return string
     */
    public function getEndpointName()
    {
        if(!$this->endpoint)
            return null;
        return $this->endpoint->getUriName();
    }

    public function noLogSystem(){
        return false;
    }




}

