<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Bank
 *
 * @ORM\Table(name="bank")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BankRepository")
 */
class Bank
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Address
     *
     * @ORM\ManyToOne(targetEntity="Address", inversedBy="banks", cascade={"persist"})
     * @ORM\JoinColumn(name="idAddress", referencedColumnName="id")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     */
    private $phone;


    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="banks", cascade={"persist"})
     * @ORM\JoinColumn(name="idContactPerson", referencedColumnName="id")
     */
    private $contactPerson;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bank
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     * @return Bank
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }



    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Bank
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
    /**
     * @return Person
     */
    public function getPerson(){
        return $this->contactPerson;
    }

    /**
     * @param Person $contactPerson
     * @return Bank
     */
    public function setPerson(Person $contactPerson){
        $this->contactPerson = $contactPerson;
        return $this;
    }
//
//    /**
//     * @return ArrayCollection
//     */
//    public function getBankAccounts()
//    {
//        return $this->bankAccounts;
//    }

}

