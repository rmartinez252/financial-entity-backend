<?php

namespace AppBundle\Entity;

use AppBundle\Security\PermissionsManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CompanyEndpoint
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="company_endpoint")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyEndpointRepository")
 */
class CompanyEndpoint
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var int
     *
     * @ORM\Column(name="permission", type="integer")
     */
    private $permission;
    /**
     * @var array
     *
     */
    private $frontEndPermission;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="endpoints")
     * @ORM\JoinColumn(name="idCompany", referencedColumnName="id")
     */
    private $company;

    /**
     * @var Endpoint
     *
     * @ORM\ManyToOne(targetEntity="Endpoint", inversedBy="companys")
     * @ORM\JoinColumn(name="idEndpoint", referencedColumnName="id")
     */
    private $endpoint;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserCompanyEndpoint", mappedBy="companyEndpoint")
     *
     */
    private $users;

    /**
     * CompanyEndpoint constructor.
     */
    public function __construct(){
        $this->users = new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Get idCompany
     *
     * @return int
     */
    public function getIdCompany()
    {
        if(!$this->company)
            return null;
        return $this->company->getId();
    }


    /**
     * Get idEndpoint
     *
     * @return int
     */
    public function getIdEndpoint()
    {
        if(!$this->endpoint)
            return null;
        return $this->endpoint->getId();
    }

    /**
     * Set permission
     *
     * @param integer $permission
     *
     * @return CompanyEndpoint
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;

        return $this;
    }

    /**
     * Get permission
     *
     * @return int
     */
    public function getPermission(){
        return $this->permission;
    }

    /**
     * @return Company
     */
    public function getCompany(){
        return $this->company;
    }

    /**
     * @param Company $company
     * @return CompanyEndpoint
     */
    public function setCompany(Company $company){
        $this->company = $company;
        return $this;
    }

    /**
     * @return Endpoint
     */
    public function getEndpoint(){
        return $this->endpoint;
    }

    /**
     * @param Endpoint $endpoint
     * @return CompanyEndpoint
     */
    public function setEndpoint(Endpoint $endpoint){
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers(){
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     * @return CompanyEndpoint
     */
    public function setUsers($users){
        $this->users = $users;
        return $this;
    }

    /**
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("permissions")
     * @Serializer\Type("array")
     * @Serializer\Groups({"CompanyPermissions","FullUserPermissions"})
     * @return array
     */
    public function frondEndPermissions(){
        if($this->frontEndPermission)
            return $this->frontEndPermission;
        $permissionManager =  new PermissionsManager();
        return $permissionManager->prosesPermissions($this->permission,"frondend");

    }
    /**
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("name")
     * @Serializer\Type("string")
     * @Serializer\Groups({"CompanyPermissions","FullUserPermissions"})
     * @return string
     */
    public function getEndpointName()
    {
        if(!$this->endpoint)
            return null;
        return $this->endpoint->getUriName();
    }

    /**
     * @param $permission
     * @return bool
     */
    public function isAllow($permission){
        $permissionManager = new PermissionsManager();
        return $permissionManager->isAllow($permission,$this->getPermission());
    }

}

