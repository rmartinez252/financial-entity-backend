<?php

namespace AppBundle\Entity;

use AppBundle\Security\PermissionsManager;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserCompany
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="user_company")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserCompanyRepository")
 */
class UserCompany
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @Serializer\Expose()
     * @ORM\Column(name="`default`", type="boolean")
     */
    private $default;

    /**
     * @var Company
     * @Serializer\Expose()
     *
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(name="idCompany", referencedColumnName="id")
     */
    private $company;
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="companies", cascade={"persist"}))
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $user;
    /**
     * @var int
     *
     * @ORM\Column(name="idClient", type="integer",nullable=true)
     */
    private $idClient;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return boolean
     */
    public function isDefault()
    {
        return $this->default;
    }

    /**
     * @param boolean $default
     * @return UserCompany
     */
    public function setDefault($default)
    {
        $this->default = $default;
        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return UserCompany
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserCompany
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * @param int $idClient
     * @return UserCompany
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;
        return $this;
    }






}

