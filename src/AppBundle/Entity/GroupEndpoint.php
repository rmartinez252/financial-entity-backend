<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupEndpoint
 *
 * @ORM\Table(name="group_endpoint")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupEndpointRepository")
 */
class GroupEndpoint
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idUserGroup", type="integer")
     */
    private $idUserGroup;

    /**
     * @var int
     *
     * @ORM\Column(name="idEndpoint", type="integer")
     */
    private $idEndpoint;

    /**
     * @var int
     *
     * @ORM\Column(name="permission", type="integer")
     */
    private $permission;

    /**
     * @var Endpoint
     *
     * @ORM\ManyToOne(targetEntity="Endpoint", inversedBy="user_groups")
     * @ORM\JoinColumn(name="idEndpoint", referencedColumnName="id")
     */
    private $endpoint;

    /**
     * @var UserGroup
     *
     * @ORM\ManyToOne(targetEntity="UserGroup", inversedBy="endpoints")
     * @ORM\JoinColumn(name="idUserGroup", referencedColumnName="id")
     */
    private $user_group;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set idUserGroup
     *
     * @param integer $idUserGroup
     *
     * @return GroupEndpoint
     */
    public function setIdUserGroup($idUserGroup){
        $this->idUserGroup = $idUserGroup;
        return $this;
    }

    /**
     * Get idUserGroup
     *
     * @return int
     */
    public function getIdUserGroup(){
        return $this->idUserGroup;
    }

    /**
     * Set idEndpoint
     *
     * @param integer $idEndpoint
     *
     * @return GroupEndpoint
     */
    public function setIdEndpoint($idEndpoint){
        $this->idEndpoint = $idEndpoint;
        return $this;
    }

    /**
     * Get idEndpoint
     *
     * @return int
     */
    public function getIdEndpoint(){
        return $this->idEndpoint;
    }

    /**
     * Set permission
     *
     * @param integer $permission
     *
     * @return GroupEndpoint
     */
    public function setPermission($permission){
        $this->permission = $permission;
        return $this;
    }

    /**
     * Get permission
     *
     * @return int
     */
    public function getPermission(){
        return $this->permission;
    }

    /**
     * @return Endpoint
     */
    public function getEndpoint(){
        return $this->endpoint;
    }

    /**
     * @param Endpoint $endpoint
     * @return GroupEndpoint
     */
    public function setEndpoint($endpoint){
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @return UserGroup
     */
    public function getUserGroup(){
        return $this->user_group;
    }

    /**
     * @param UserGroup $user_group
     * @return GroupEndpoint
     */
    public function setUserGroup($user_group){
        $this->user_group = $user_group;
        return $this;
    }




}

