<?php

namespace AppBundle\Entity;

use AppBundle\Security\PermissionsManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * User
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @Hateoas\Relation(
 *      "self",
 *      href=@Hateoas\Route(
 *          "uri_get_one_users",
 *          parameters = {
 *              "id" = "expr(object.getId())"
 *          }
 *      )
 * )
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * String 5-200
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 5,
     *      max = 200,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     *
     * @var string
     *
     * @Serializer\Expose()
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;



    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"FullUserPermissions","FullUserPerson"})
     *
     * @ORM\OneToMany(targetEntity="UserCompany", mappedBy="user", cascade={"persist"}))
     *
     */
    private $companies;

    /**
     * @var UserGroup
     *
     * @ORM\ManyToOne(targetEntity="UserGroup", inversedBy="users")
     * @ORM\JoinColumn(name="idGroup", referencedColumnName="id")
     */
    private $user_group;

    /**
     * @var Person
     * @Serializer\Expose()
     * @Serializer\Groups({"Full","loginResponse","FullUserPermissions","FullUserPerson","UserPerson"})
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(name="idPerson", referencedColumnName="id",nullable=true)
     */
    private $person;

    /**
     * @var Structure
     * @Serializer\Groups({"Full","loginResponse","FullUserPermissions","FullUserPerson","UserPerson"})
     *
     * @ORM\ManyToOne(targetEntity="Structure", inversedBy="usersDefault", cascade={"persist"})
     * @ORM\JoinColumn(name="idStructure", referencedColumnName="id")
     */
    private $structure;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"FullUserPermissions","FullUserPerson"})
     *
     * @ORM\OneToMany(targetEntity="UserCompanyEndpoint", mappedBy="user", cascade={"persist"})
     *
     */
    private $userCompanyEndpoints;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups("UserLog")
     *
     * @ORM\OneToMany(targetEntity="SystemLog", mappedBy="user")
     *
     */
    private $userLog;


    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups("UserLog")
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\SystemLog", mappedBy="user")
     *
     */
    private $userLogCompanies;

    private $roles;

    /**
     * User constructor.
     */
    public function __construct(){
        $this->userCompanyEndpoints = new ArrayCollection();

    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }



    /**
     * Get idPerson
     *
     * @return int
     */
    public function getIdPerson(){
        if(!$this->person)
            return null;
        return $this->person->getId();
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username){
        $this->username = $username;
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername(){
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password){
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword(){
        return $this->password;
    }


    /**
     * Get idCompany
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("idDefaultCompany")
     * @Serializer\Type("integer")
     * @Serializer\Groups({"loginResponse","FullUserPerson"})
     *
     *
     * @return int
     */
    public function getIdCompany(){
        $def = null;
        if(!$this->companies)
            return $def;
        foreach ($this->companies as $userCompany){
            $def = $userCompany->getCompany()->getId();
            if($userCompany->isDefault())
                return $def;
        }
        return $def;
    }
    /**
     * Get idCompany
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("nameDefaultCompany")
     * @Serializer\Type("string")
     * @Serializer\Groups({"loginResponse","FullUserPerson"})
     *
     *
     * @return int
     */
    public function getNameCompany(){
        $def = null;
        if(!$this->companies)
            return $def;
        foreach ($this->companies as $userCompany){
            $def = $userCompany->getCompany()->getName();
            if($userCompany->isDefault())
                return $def;
        }
        return $def;

    }

    /**
     * Get idGroup
     *
     * @return int
     */
    public function getIdGroup(){
        if(!$this->user_group)
            return null;
        return $this->user_group->getId();
    }

    /**
     * @param bool $idCompany
     * @return Company
     */
    public function getCompany($idCompany = false){
        $def = null;
        if(!$this->companies)
            return $def;
        foreach ($this->companies as $userCompany){
            $def = $userCompany->getCompany();
            if($userCompany->getCompany()->getId() == $idCompany)
                return $def;
            if($userCompany->isDefault() && !$idCompany)
                return $def;
        }
        if ($idCompany)
            return null;
        return $def;
    }
    public function getIdClientCompany($idCompany){
        if(!$this->companies)
            return false;
        foreach ($this->companies as $userCompany){
            if($userCompany->getCompany()->getId() == $idCompany)
                return $userCompany->getIdClient();
            if($userCompany->isDefault() && !$idCompany)
                return $userCompany->getIdClient();
        }
        if ($idCompany)
            return false;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @param ArrayCollection $companies
     */
    public function setCompanies($companies)
    {
        $this->companies = $companies;
    }



    /**
     * @return UserGroup
     */
    public function getUserGroup(){
        return $this->user_group;
    }

    /**
     * @param UserGroup $user_group
     * @return User
     */
    public function setUserGroup($user_group){
        $this->user_group = $user_group;
        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson(){
        return $this->person;
    }

    /**
     * @param Person $person
     * @return User
     */
    public function setPerson($person){
        $this->person = $person;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUserCompanyEndpoints(){
        return $this->userCompanyEndpoints;
    }

    /**
     * @param ArrayCollection $userCompanyEndpoints
     * @return User
     */
    public function setUserCompanyEndpoints($userCompanyEndpoints){
        $this->userCompanyEndpoints = $userCompanyEndpoints;
        return $this;
    }

    /**
     * Add attribute
     *
     * @param UserCompanyEndpoint $userCompanyEndpoints
     *
     * @return User
     */
    public function addPermission(UserCompanyEndpoint $userCompanyEndpoints){

        $this->userCompanyEndpoints->add($userCompanyEndpoints);
        $userCompanyEndpoints->setUser($this);
        return $this;
    }

    /**
     * Remove attribute
     *
     * @param   UserCompanyEndpoint $userCompanyEndpoints
     */
    public function removePermission(UserCompanyEndpoint $userCompanyEndpoints){
        $this->userCompanyEndpoints->removeElement($userCompanyEndpoints);
    }
    /**
     * Array with all permissions for this company
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("roles")
     * @Serializer\Type("array")
     * @Serializer\Groups({"loginResponse","FullUserPerson"})
     * @return array
     */
    public function getRoles()
    {

        return $this->roles;
    }
    public function setRoles($idCompany = false){
        if(!$idCompany = $this->getCompany($idCompany)->getId())
            return;
        $roles = array();
//        $roles[] = 'ROLE_ADMIN';
        $permissionManager = new PermissionsManager();
        foreach($this->userCompanyEndpoints as $endpoint){
            /**
             * @var $endpoint UserCompanyEndpoint
             */
            if(!($idCompany == $endpoint->getCompanyEndpoint()->getCompany()->getId()))
                continue;
            $permissionManager->setName(strtoupper($endpoint->getEndpoint()->getUriName()).'_');
            $roles = array_merge($roles,$permissionManager->prosesPermissions($endpoint->getPermission()));
        }
        $this->roles = $roles;
    }

    /**
     * @return ArrayCollection
     */
    public function getUserLog()
    {
        return $this->userLog;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
        return;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Get Default Node left value
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("defaultModule")
     * @Serializer\Type("integer")
     * @Serializer\Groups({"Full","loginResponse","UserPerson"})
     *
     *
     * @return int
     */
    public function getLeftDefaultNode(){

        if(!$this->getStructure())
            return 1;
        return $this->getStructure()->getL();
    }

    public function getIdClient(Company $company){
        foreach ($this->getCompanies() as $c){
            if($c->getCompany() === $company)
                return $c->getIdClient();
        }
        return false;
    }

    /**
     * Functions to use in ApiLog to return the data to safe in log
     * @return array
     */
    public function toSystemLog(){
        return array(
            "id" => $this->getId(),
            "username" => $this->getUsername()
        );
    }

    /**
     * @return array
     */
    public function toSystemLogNew(){
        return array(
            "username" => $this->getUsername()
        );
    }

    /**
     * @return Structure
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @param Structure $structure
     * @return User
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;
        return $this;
    }




}

