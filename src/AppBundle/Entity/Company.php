<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="direction", type="string", length=255)
     */
    private $direction;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;
    /**
     * @var string
     *
     * @ORM\Column(name="closeDate", type="string", length=5)
     */
    private $closeDate;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;
    /**
     * @var string
     *
     * @ORM\Column(name="connection", type="string", length=255)
     */
    private $connection;



    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="companies", cascade={"persist"})
     * @ORM\JoinColumn(name="idContactPerson", referencedColumnName="id")
     */
    private $contactPerson;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CompanyEndpoint", mappedBy="company")
     *
     */
    private $endpoints;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserCompany", mappedBy="company")
     *
     */
    private $users;


    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups("CompanyLog")
     *
     * @ORM\OneToMany(targetEntity="SystemLog", mappedBy="company")
     *
     */
    private $companyLog;
    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups("CompanyLog")
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\SystemLog", mappedBy="company")
     *
     */
    private $companyLogCompanies;
    /**
     * Company constructor.
     */
    public function __construct(){
        $this->endpoints = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set direction
     *
     * @param string $direction
     *
     * @return Company
     */
    public function setDirection($direction){
        $this->direction = $direction;
        return $this;
    }

    /**
     * Get direction
     *
     * @return string
     */
    public function getDirection(){
        return $this->direction;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Company
     */
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Company
     */
    public function setPhone($phone){
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone(){
        return $this->phone;
    }

    /**
     * @return Person
     */
    public function getContactPerson(){
        return $this->contactPerson;
    }

    /**
     * @param Person $contactPerson
     * @return Company
     */
    public function setContactPerson(Person $contactPerson){
        $this->contactPerson = $contactPerson;
        return $this;
    }

    /**
     * Get idContactPerson
     *
     * @return int
     */
    public function getIdContactPerson(){
        return $this->contactPerson->getId();
    }
    /**
     * @return ArrayCollection
     */
    public function getEndpoints(){
        return $this->endpoints;
    }

    /**
     * @param ArrayCollection $endpoints
     * @return Company
     */
    public function setEndpoints($endpoints){
        $this->endpoints = $endpoints;
        return $this;
    }
    /**
     * Add endpoint
     *
     * @param CompanyEndpoint $endpoint
     *
     * @return Company
     */
    public function addEndpoint(CompanyEndpoint $endpoint){
        $this->endpoints[] = $endpoint;
        return $this;
    }

    /**
     * Remove endpoint
     *
     * @param CompanyEndpoint $endpoint
     */
    public function removeEndpoint(CompanyEndpoint $endpoint){
        $this->endpoints->removeElement($endpoint);
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     * @return Company
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * Add user
     *
     * @param User $user
     *
     * @return Company
     */
    public function addUser(User $user){
        $this->users[] = $user;
        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user){
        $this->users->removeElement($user);
    }



    /**
     * @return ArrayCollection
     */
    public function getCompanyLog()
    {
        return $this->companyLog;
    }

    /**
     * @return string
     */
    public function getCloseDate()
    {
        return $this->closeDate;
    }

    /**
     * @param string $closeDate
     * @return Company
     */
    public function setCloseDate($closeDate)
    {
        $this->closeDate = $closeDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param string $connection
     * @return Company
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
        return $this;
    }




}

