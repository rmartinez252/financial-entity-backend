<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Hateoas\Configuration\Annotation as Hateoas;



/**
 * Structure
 * * @Serializer\ExclusionPolicy("all")

 * @ORM\Table(name="structure")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StructureRepository")
 * @Hateoas\Relation(
 *      "frontendRout",
 *      href = "expr(object.getUriName())"
 * )
 */
class Structure
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"Full","loginResponse","Basic","CompanyPermissions"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 5,
     *      max = 200,
     *      minMessage = "Structure name must be at least {{ limit }} characters long",
     *      maxMessage = "Structure name cannot be longer than {{ limit }} characters"
     * )
     *
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"Full","loginResponse","Basic","CompanyPermissions"})
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"Full","loginResponse","Basic"})
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="structure")
     *
     */
    private $usersDefault;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="uriName", type="string", length=255, nullable=true)
     */
    private $uriName;

    /**
     * @Assert\NotBlank()
     * @Serializer\Expose()
     * @Serializer\Groups("Full")
     * @var int
     *
     *
     * @ORM\Column(name="l", type="integer")
     */
    private $l;

    /**
     * @Assert\NotBlank()
     * @Serializer\Expose()
     * @Serializer\Groups("Full")
     * @var int
     *
     *
     * @ORM\Column(name="r", type="integer")
     */
    private $r;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="StructureEndpoint", mappedBy="structure", cascade={"persist"})
     *
     */
    private $endpoints;

    /**
     * Structure constructor.
     */
    public function __construct(){
        $this->endpoints = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Structure
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Structure
     */
    public function setDescription($description){
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * Set l
     *
     * @param integer $l
     *
     * @return Structure
     */
    public function setL($l){
        $this->l = $l;
        return $this;
    }

    /**
     * Get l
     *
     * @return int
     */
    public function getL(){
        return $this->l;
    }

    /**
     * Set r
     *
     * @param integer $r
     *
     * @return Structure
     */
    public function setR($r){
        $this->r = $r;
        return $this;
    }

    /**
     * Get r
     *
     * @return int
     */
    public function getR(){
        return $this->r;
    }

    /**
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("permissions")
     * @Serializer\Type("array")
     * @Serializer\Groups({"CompanyPermissions"})
     *
     * @return ArrayCollection
     */
    public function getEndpoints(){
        return $this->endpoints;
    }

    /**
     * @param ArrayCollection $endpoints
     * @return Structure
     */
    public function setEndpoints(ArrayCollection $endpoints){
        $this->endpoints = $endpoints;
        return $this;
    }
    /**
     * Add endpoint
     *
     * @param Endpoint $endpoint
     *
     * @return Structure
     */
    public function addEndpoint(Endpoint $endpoint){
        $this->endpoints[] = $endpoint;
        return $this;
    }

    /**
     * Remove endpoint
     *
     * @param Endpoint $endpoint
     */
    public function removeEndpoint(Endpoint $endpoint){
        $this->endpoints->removeElement($endpoint);
    }

    /**
     * @return string
     */
    public function getUriName()
    {
        return $this->uriName;
    }

    /**
     * @param string $uriName
     * @return Structure
     */
    public function setUriName($uriName)
    {
        $this->uriName = $uriName;
        return $this;
    }
//
//    /**
//     * children
//     * @Serializer\VirtualProperty
//     * @Serializer\SerializedName("children")
//     * @Serializer\Type("array")
//     * @Serializer\Groups({"loginResponse"})
//     *
//     *
//     * @return array
//     */
//    public function getChildren(){
//        //todo
//
//    }

}

