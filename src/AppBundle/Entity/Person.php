<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Person
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 */
class Person
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"Full","loginResponse","FullBank","FullUserPermissions","FullUserPerson","UserPerson"})
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"Full","loginResponse","FullBank","FullUserPermissions","FullUserPerson","UserPerson"})
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;
    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"FullBank","FullUserPermissions","FullUserPerson","UserPerson"})
     *
     * @ORM\OneToMany(targetEntity="PersonAttribute", mappedBy="person", cascade={"persist"})
     *
     */
    private $attributes;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="person")
     *
     */
    private $users;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Company", mappedBy="contactPerson")
     *
     */
    private $companies;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Bank", mappedBy="contactPerson")
     *
     */
    private $banks;


    /**
     * Endpoint constructor.
     */
    public function __construct()
    {
        $this->attributes= new ArrayCollection();
        $this->users = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Person
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Person
     */
    public function setLastname($lastname){
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname(){
        return $this->lastname;
    }

    /**
     * @return ArrayCollection
     */
    public function getAttributes(){
        return $this->attributes;
    }

    /**
     * @param ArrayCollection $attributes
     * @return Person
     */
    public function setAttributes($attributes){
        $this->attributes = $attributes;
        return $this;
    }
    /**
     * Add attribute
     *
     * @param PersonAttribute $attribute
     *
     * @return Person
     */
    public function addAttribute(PersonAttribute $attribute){

        $this->attributes->add($attribute);
        $attribute->setPerson($this);
        return $this;
    }

    /**
     * Remove attribute
     *
     * @param PersonAttribute $attribute
     */
    public function removeAttribute(PersonAttribute $attribute){
        $this->attributes->removeElement($attribute);
    }
    /**
     * @return ArrayCollection
     */
    public function getUsers(){
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     * @return Person
     */
    public function setUsers($users){
        $this->users = $users;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanies(){
        return $this->companies;
    }

    /**
     * @param ArrayCollection $companies
     * @return Person
     */
    public function setCompanies($companies){
        $this->companies = $companies;
        return $this;
    }
    /**
     * @return ArrayCollection
     */
    public function getBanks(){
        return $this->banks;
    }

    /**
     * @param ArrayCollection $banks
     * @return Person
     */
    public function setBanks($banks){
        $this->banks = $banks;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getClients(){
        return $this->clients;
    }

    /**
     * @param ArrayCollection $clients
     * @return Person
     */
    public function setClients($clients){
        $this->clients = $clients;
        return $this;
    }


    /**
     * Get name to string(From)
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

}

