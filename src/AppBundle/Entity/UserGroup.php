<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserGroup
 *
 * @ORM\Table(name="user_group")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserGroupRepository")
 */
class UserGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="GroupEndpoint", mappedBy="user_group")
     *
     */
    private $endpoints;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="user_group")
     *
     */
    private $users;

    /**
     * UserGroup constructor.
     */
    public function __construct(){
        $this->endpoints = new ArrayCollection();
        $this->users = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UserGroup
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return UserGroup
     */
    public function setDescription($description){
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * @return ArrayCollection
     */
    public function getEndpoints(){
        return $this->endpoints;
    }

    /**
     * @param ArrayCollection $endpoints
     * @return UserGroup
     */
    public function setEndpoints($endpoints){
        $this->endpoints = $endpoints;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers(){
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     * @return UserGroup
     */
    public function setUsers($users){
        $this->users = $users;
        return $this;
    }


}

