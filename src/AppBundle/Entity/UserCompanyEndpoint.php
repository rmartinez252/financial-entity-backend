<?php

namespace AppBundle\Entity;

use AppBundle\Security\PermissionsManager;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserCompanyEndpoint
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="user_company_endpoint")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserCompanyEndpointRepository")
 */
class UserCompanyEndpoint
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @ORM\Column(name="permission", type="integer")
     */
    private $permission;

    /**
     * @var CompanyEndpoint
     *
     * @ORM\ManyToOne(targetEntity="CompanyEndpoint", inversedBy="users")
     * @ORM\JoinColumn(name="idCompanyEndpoint", referencedColumnName="id")
     */
    private $companyEndpoint;
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCompanyEndpoints")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        if($this->user)
            return $this->user->getId();
        else
            return false;
    }


    /**
     * Get idCompanyEndpoint
     *
     * @return int
     */
    public function getIdCompanyEndpoint()
    {
        if(!$this->companyEndpoint)
            return null;
        return $this->companyEndpoint->getId();
    }

    /**
     * Set permission
     *
     * @param integer $permission
     *
     * @return UserCompanyEndpoint
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;

        return $this;
    }

    /**
     * Get permission
     *
     * @return int
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * get permissions value 0-15 and
     * returns a boolean, if this permissions
     * is valid for this endpoint
     *
     * @param int $permission
     * @return boolean
     */
    public function isAllow($permission){
        $permissionManager =  new PermissionsManager();
        return $permissionManager->isAllow($permission,$this->getPermission());
    }

    /**
     * @return CompanyEndpoint
     */
    public function getCompanyEndpoint()
    {
        return $this->companyEndpoint;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("endpoint")
     *

     * @return Endpoint
     */
    public function getEndpoint()
    {
        return $this->companyEndpoint->getEndpoint();
    }

    /**
     * @param CompanyEndpoint $companyEndpoint
     * @return UserCompanyEndpoint
     */
    public function setCompanyEndpoint($companyEndpoint)
    {
        $this->companyEndpoint = $companyEndpoint;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(){
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserCompanyEndpoint
     */
    public function setUser($user){
        $this->user = $user;
        return $this;
    }


}

