<?php

namespace AppBundle\Entity;

use AppBundle\Security\PermissionsManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * EndPoint
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="endpoint")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EndpointRepository")
 */
class Endpoint
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="uriName", type="string", length=255, unique=true)
     */
    private $uriName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="permission", type="integer")
     */
    private $permission;


    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     *
     * @ORM\OneToMany(targetEntity="StructureEndpoint", mappedBy="endpoint")
     *
     */
    private $structures;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CompanyEndpoint", mappedBy="endpoint")
     *
     */
    private $companys;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="GroupEndpoint", mappedBy="endpoint")
     *
     */
    private $user_groups;

    private $frontEndPermission;

    /**
     * Endpoint constructor.
     */
    public function __construct(){
        $this->structures = new ArrayCollection();
        $this->companys = new ArrayCollection();
        $this->user_groups= new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set uriName
     *
     * @param string $uriName
     *
     * @return EndPoint
     */
    public function setUriName($uriName){
        $this->uriName = $uriName;
        return $this;
    }

    /**
     * Get uriName
     *
     * @return string
     */
    public function getUriName(){
        return $this->uriName;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return EndPoint
     */
    public function setDescription($description){
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * Set permission
     *
     * @param integer $permission
     *
     * @return EndPoint
     */
    public function setPermission($permission){
        $this->permission = $permission;
        return $this;
    }

    /**
     * Get permission
     *
     * @return int
     */
    public function getPermission(){
        return $this->permission;
    }

    /**
     * @return ArrayCollection
     */
    public function getStructures(){
        return $this->structures;
    }

    /**
     * @param ArrayCollection $structures
     * @return Endpoint
     */
    public function setStructures($structures){
        $this->structures = $structures;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanys(){
        return $this->companys;
    }

    /**
     * @param ArrayCollection $companys
     * @return Endpoint
     */
    public function setCompanys($companys){
        $this->companys = $companys;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUserGroups(){
        return $this->user_groups;
    }

    /**
     * @param ArrayCollection $user_groups
     * @return Endpoint
     */
    public function setUserGroups($user_groups){
        $this->user_groups = $user_groups;
        return $this;
    }


}

