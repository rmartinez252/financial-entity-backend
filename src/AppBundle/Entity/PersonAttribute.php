<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonAttribute
 *
 * @ORM\Table(name="person_attribute")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonAttributeRepository")
 */
class PersonAttribute
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idPerson", type="integer")
     */
    private $idPerson;

    /**
     * @var int
     *
     * @ORM\Column(name="idAttribute", type="integer")
     */
    private $idAttribute;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * @var Attribute
     *
     * @ORM\ManyToOne(targetEntity="Attribute", inversedBy="personAttribute")
     * @ORM\JoinColumn(name="idAttribute", referencedColumnName="id")
     */
    private $attribute;
    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="attributes")
     * @ORM\JoinColumn(name="idPerson", referencedColumnName="id")
     */
    private $person;


    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set idPerson
     *
     * @param integer $idPerson
     *
     * @return PersonAttribute
     */
    public function setIdPerson($idPerson){
        $this->idPerson = $idPerson;
        return $this;
    }

    /**
     * Get idPerson
     *
     * @return int
     */
    public function getIdPerson(){
        return $this->idPerson;
    }

    /**
     * Set idAttribute
     *
     * @param integer $idAttribute
     *
     * @return PersonAttribute
     */
    public function setIdAttribute($idAttribute){
        $this->idAttribute = $idAttribute;
        return $this;
    }

    /**
     * Get idAttribute
     *
     * @return int
     */
    public function getIdAttribute(){
        return $this->idAttribute;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return PersonAttribute
     */
    public function setValue($value){
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue(){
        return $this->value;
    }

    /**
     * @return Attribute
     */
    public function getAttribute(){
        return $this->attribute;
    }

    /**
     * @param Attribute $attribute
     * @return PersonAttribute
     */
    public function setAttribute($attribute){
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson(){
        return $this->person;
    }

    /**
     * @param Person $person
     * @return PersonAttribute
     */
    public function setPerson($person){
        $this->person = $person;
        return $this;
    }

}

