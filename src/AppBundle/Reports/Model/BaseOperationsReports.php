<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 3/30/17
 * Time: 2:42 PM
 */

namespace AppBundle\Reports\Model;


use AppBundle\EntityCompanies\Operation;
use JMS\Serializer\Annotation as Serializer;

abstract class BaseOperationsReports
{
    /**
     * @var int
     * @Serializer\Expose()
     */
    protected $id;
    /**
     * @var string
     * @Serializer\Expose()
     */
    protected $description;
    /**
     * @var string
     * @Serializer\Expose()
     */
    protected $productId;

    /**
     * @var string
     * @Serializer\Expose()
     */
    protected $productName;

    /**
     * @var float
     * @Serializer\Expose()
     */
    protected $initialAmount;
    /**
     * @var float
     * @Serializer\Expose()
     */
    protected $todayAmount;

    /**
     * @var string
     * @Serializer\Expose()
     */
    protected $clientName;
    /**
     * @var array
     * @Serializer\Expose()
     */
    protected $clientInfo;

    /**
     * @var \DateTime
     * @Serializer\Expose()
     */
    protected $effectiveDate;

    /**
     * @var \DateTime
     * @Serializer\Expose()
     */
    protected $maturityDate;

    /**
     * @var int
     * @Serializer\Expose()
     */
    protected $tenor;

    /**
     * @var array
     * @Serializer\Expose()
     */
    protected $attributes;

    /**
     * @var array
     * @Serializer\Expose()
     */
    protected $payments;
    /**
     * @var array
     * @Serializer\Expose()
     */
    protected $entries;

    /**
     * Funding01 constructor.
     * @param Operation $operation
     */
    public function __construct(Operation $operation)
    {
        $operation->preSerialize();

        $client = $operation->getClient();
        $this->id = $operation->getId();
        $this->description = $operation->getDescription();
        $this->productId = $operation->getProduct()->getId();
        $this->productName = $operation->getProduct()->getName();
        $this->clientName = $client->getName();
        $clientInfo['id'] = $client->getId();
        $clientInfo['email'] = $client->getEmail();
        $clientInfo['phone'] = $client->getPhone();
        $clientInfo['contactPerson'] = $client->getContactPerson()->getName();
        $clientInfo['contactPerson'] .= " ".$client->getContactPerson()->getLastname();
        $clientInfo['address']['address'] = $client->getAddress()->getAddress();
        $clientInfo['address']['city'] = $client->getAddress()->getCity();
        $clientInfo['address']['country'] = $client->getAddress()->getCountry();
        $clientInfo['address']['zipCode'] = $client->getAddress()->getZipCode();
        $clientInfo['address']['state'] = $client->getAddress()->getState();
        $this->clientInfo = $clientInfo;
        $this->effectiveDate = $operation->getEffectiveDateFront();
        $this->maturityDate = $operation->getMaturityDate();
        $this->tenor = $operation->getTenor();
        $this->initialAmount = floatval($operation->getInitialAmount());
        $this->entries = $operation->getEntries();


    }

    protected function amountDays($dateStart,$dateEnd){
        if(!$this->validateDate($dateEnd))
            return 0;
        if(is_string($dateStart))
            if(!$this->validateDate($dateStart))
                return 0;
            else
                $dateStart = \DateTime::createFromFormat('Y-m-d', $dateStart);
        $dateEnd = \DateTime::createFromFormat('Y-m-d', $dateEnd);
        if($dateStart <= $dateEnd)
            return date_diff($dateEnd,$dateStart)->days ;
        return 0;
    }

    protected function validateDate($date)
    {
        $d = \DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

}