<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 12/16/16
 * Time: 3:40 PM
 */

namespace AppBundle\Reports\Model\Operations;


use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Entry;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\OperationAttribute;
use AppBundle\EntityCompanies\OperationInvoice;
use AppBundle\EntityCompanies\Payment;
use AppBundle\Operations\OperationProcess;
use AppBundle\Reports\Model\BaseOperationsReports;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AccountingEntryModel
 * @package AppBundle\Form\Model
 * @Serializer\ExclusionPolicy("all")
 */
class Lending01Model extends BaseOperationsReports
{


    /**
     * @var array
     * @Serializer\Expose()
     */
    private $invoices;

    /**
     * @var float
     * @Serializer\Expose()
     */
    private $netAmount;


    /**
     * Funding01 constructor.
     * @param Operation $operation
     * @param EntityManager $em
     */
    public function __construct(Operation $operation ,EntityManager $em = null)
    {
        parent::__construct($operation);
        $operationProcess = new OperationProcess($operation,$em);
        /**
         * @var OperationAttribute $attribute
         * @var Payment $payment
         * @var OperationInvoice $invoice
         */
        foreach ($operation->getAttributes() as $attribute){
            $this->attributes[$attribute->getAttributeProduct()->getProductAttribute()->getSystemName()] = $attribute->getValue();
        }
        if($payments = $operation->getPayments())
            foreach ($payments as $payment){
                $v['id'] = $payment->getId();

                if($payment->getEntry())
                    $v["date"] = $payment->getEntry()->getDateUser()->format("Y-m-d");
                else
                    $v["date"] = $payment->getOperation()->getDateSystem()->format("Y-m-d");

                $v["debtor"]["id"] = $payment->getDebtor()->getId();
                $v["debtor"]["name"] = $payment->getDebtor()->getName();
                $v["value"] = $payment->getValue();
                $this->payments[] = $v;
            }
        if($invoices = $operation->getInvoices())
            foreach ($invoices as $invoice){
                $this->invoices[$invoice->getId()]["debtor"]["id"] = $invoice->getDebtor()->getId();
                $this->invoices[$invoice->getId()]["debtor"]["name"] = $invoice->getDebtor()->getName();
                $this->invoices[$invoice->getId()]["number"] = $invoice->getReference();
                $this->invoices[$invoice->getId()]["value"] = $invoice->getValue();
                $this->invoices[$invoice->getId()]["dateInvoice"] = $invoice->getDateInvoice();
                $this->invoices[$invoice->getId()]["maturityDate"] = $invoice->invoiceMaturityDate();
            }
        $this->todayAmount = $operationProcess->getTotalAmount(true);
        /**
         * @var $accountingEntry AccountingEntry
         * @var $entry Entry
         */

        $this->netAmount = $operationProcess->getNetAmount();

    }




}