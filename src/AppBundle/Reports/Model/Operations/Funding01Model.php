<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 12/16/16
 * Time: 3:40 PM
 */

namespace AppBundle\Reports\Model\Operations;


use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Entry;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\OperationAttribute;
use AppBundle\EntityCompanies\Payment;
use AppBundle\Operations\OperationProcess;
use AppBundle\Reports\Model\BaseOperationsReports;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AccountingEntryModel
 * @package AppBundle\Form\Model
 * @Serializer\ExclusionPolicy("all")
 */
class Funding01Model extends BaseOperationsReports
{

    /**
     * @var array
     * @Serializer\Expose()
     */
    private $paymentDays;
    /**
     * @var array
     * @Serializer\Expose()
     */
    private $dailyAccruals;
    /**
     * @var array
     * @Serializer\Expose()
     */
    private $totalAccruals;


    /**
     * Funding01 constructor.
     * @param Operation $operation
     * @param EntityManager $em
     */
    public function __construct(Operation $operation ,EntityManager $em = null)

    {
        parent::__construct($operation);
        $initialDate = $this->effectiveDate;
        $operationProcess = new OperationProcess($operation,$em);

        /**
         * @var OperationAttribute $attribute
         * @var Payment $payment
         */
        foreach ($operation->getAttributes() as $attribute) {
            if (substr_count($attribute->getAttributeProduct()->getProductAttribute()->getSystemName(), "payday")) {
                $payDate = $attribute->getValue();
                $days = $this->amountDays($initialDate, $payDate);
                $initialDate = $payDate;
                $this->paymentDays[$attribute->getAttributeProduct()->getProductAttribute()->getSystemName()]["name"] = $attribute->getAttributeProduct()->getProductAttribute()->getShowName();
                $this->paymentDays[$attribute->getAttributeProduct()->getProductAttribute()->getSystemName()]["date"] = $payDate;
                $this->paymentDays[$attribute->getAttributeProduct()->getProductAttribute()->getSystemName()]["estimateValue"] = $operationProcess->calculateAccrual($days);

            } else {
                $this->attributes[$attribute->getAttributeProduct()->getProductAttribute()->getSystemName()] = $attribute->getValue();
            }
        }
        if ($payments = $operation->getPayments())
            foreach ($payments as $payment) {
                $v['id'] = $payment->getId();
                $v["date"] = $payment->getEntry()->getDateUserFront();
                $v["value"] = round(floatval($payment->getValue()),2);
                $this->payments[] = $v;
            }
        /**
         * @var $accountingEntry AccountingEntry
         * @var $entry Entry
         */
        $this->totalAccruals["value"] = 0;
        $this->totalAccruals["date"] = null;
        if ($entries = $operation->getEntries())
            foreach ($entries as $accountingEntry) {
                if ($entries = $accountingEntry->getEntrys())
                    if ($accountingEntry->getType()->getName() == OperationProcess::TYPE_ACCRUAL)
                        foreach ($entries as $entry){
                            $this->dailyAccruals[$entry->getId()]["date"] = $accountingEntry->getDateUserFront();
                            $this->dailyAccruals[$entry->getId()]["value"] = $entry->getValue();
                            if($this->totalAccruals["date"] < $accountingEntry->getDateUserFront())
                                $this->totalAccruals["date"] = $accountingEntry->getDateUserFront();
                            $this->totalAccruals["value"] += $entry->getValue();
                        }
            }

        $this->todayAmount = floatval($operationProcess->getTotalAmount(true));

    }
}