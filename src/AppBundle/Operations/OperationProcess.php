<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 1/31/17
 * Time: 2:20 PM
 */

namespace AppBundle\Operations;


use AppBundle\Api\ApiProblem;
use AppBundle\Api\ApiProblemException;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\AccountingEntryType;
use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Client;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\OperationInvoice;
use AppBundle\Operations\Factoring01Process;
use AppBundle\Repository\OperationRepository;
use Doctrine\ORM\EntityManager;

class OperationProcess
{
    const TYPE_ACCRUAL = "Accruals";
    const TYPE_PAYMENT = "Payment";
    const TYPE_LATE_FEES = "LateFees";
    const TYPE_SYSTEM = "System";

    /**
     * @var $operation Operation
     */
    private $operation;
    /**
     * @var $process Process
     */
    private $process;
    /**
     * @var $em EntityManager
     */
    private $em;
    /**
     * @var $user User
     */
    private $user;

    /**
     * MainProcess constructor.
     * @param Operation $operation
     * @param EntityManager $em
     * @param User $user
     */
    public function __construct(Operation $operation,EntityManager $em = null,User $user = null)
    {
        $this->operation = $operation;
        $this->em = $em;
        $this->user = $user;
        $this->setProcessType();

    }

    public function createOperation(){
        $this->process->createOp();
        $this->operation = $this->process->getOperation();
    }

    public function addPayment(BankMove $bankMove,$value = null,Client $debtor = null,$options = false){
        $payments = $this->process->addPayment($bankMove,$value,$debtor,$options);
        $this->operation = $this->process->getOperation();
        return $payments;
    }

    public function getBeforeClose(){

        return $this->process->getBeforeClose();
    }

    /**
     * @param bool $refresh
     * @return Operation
     */
    public function getOperation($refresh = false)
    {
        if($this->em && $refresh){
            $this->em->refresh($this->operation);
            if($this->operation->getInvoices())
                foreach ($this->operation->getInvoices() as &$i)
                    $this->em->refresh($i);
        }
        return $this->operation;
    }

    public function changeStatus($newStatus){
        return $this->process->changeStatus($newStatus);
    }

    /**
     * @param Operation $operation
     * @return $this
     */
    public function setOperation(Operation $operation)
    {
        $this->operation = $operation;
        $this->setProcessType();
        return $this;
    }

    private  function setProcessType(){

        switch ($this->operation->getProduct()->getProcess()->getName()){
            case "factoring01":
                $this->process = new Factoring01Process($this->operation,$this->em,$this->user);
                break;
            case "factoringHistoric":
                $this->process = new FactoringHistoricProcess($this->operation,$this->em,$this->user);
                break;
            case "reverseFactoring01":
                $this->process = new ReverseFactoring01Process($this->operation,$this->em,$this->user);
                break;
            case "assetBaseLoad01":
                $this->process = new AssetBaseLoadProcess($this->operation,$this->em,$this->user);
                break;
        }
    }

    public function createAccrual(){

        $newEntries = $this->process->createAccrual();
        $this->operation = $this->process->getOperation();
        return $newEntries;

    }

    public function getLastAccrualDate(){
        return $this->process->lastAccrualDate();
    }

    public function createLateFee(){
        $newEntries = $this->process->createLateFee();
        $this->operation = $this->process->getOperation();
        return $newEntries;

    }

    public function getTotalAmount($options = false){
        return $this->process->getTotalAmount($options);
    }

    public function getNetAmount(){
        return $this->process->getNetAmount();
    }

    public function getMaturityDate(){
        return $this->process->getMaturityDate();
    }

    public function calculateAccrual($days){
        return $this->process->calculateAccrual($days);
    }

    public function getInvoiceCommissionP(OperationInvoice $invoice){
        return $this->process->getInvoiceCommissionP($invoice);

    }

    public function getProcessAmounts(){
        return $this->process->getProcessAmounts();
    }

    /**
     * @param EntityManager $em
     * @return OperationRepository
     */
    private function getOperationRepository(EntityManager $em){
        return $em->getRepository('AppBundle\\EntityCompanies\\Operation');
    }

    /**
     * @param int $statusCode
     * @param string $type
     */
    private function createApiProblem($statusCode = 400, $type = ApiProblem::TYPE_OPERATION_PROCESS_ERROR){
        $apiProblem = new ApiProblem($statusCode, $type);
        throw new ApiProblemException($apiProblem);
    }
}