<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 2/15/17
 * Time: 10:38 AM
 */

namespace AppBundle\Operations;



use AppBundle\Api\ApiProblem;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Client;
use AppBundle\EntityCompanies\Entry;
use AppBundle\EntityCompanies\InvoicePayment;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\OperationInvoice;
use AppBundle\EntityCompanies\Payment;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

abstract class Lending extends Process
{



    public function createOp(){
        $conn = $this->em->getConnection();
        $conn->beginTransaction();
        try{
            $this->operation->setStatus($this->operationStatusCurrentValid);
            $this->operation->setDateSystem(new \DateTime("now"));
//            $this->operation->getInvoices()
            $this->em->persist($this->operation);
            $ats= new ArrayCollection();
            foreach ($this->operation->getAttributes() as $at)
                $ats[] =$at->setOperation($this->operation);

            $invoices = new ArrayCollection();
            foreach ($this->operation->getInvoices() as $invo)
                $invoices[] = $invo->setOperation($this->operation);
            $this->operation->setInvoices($invoices);
            $this->em->flush();
//            var_dump($this->operation->getAttributes()[0]->getOperation()->getId());
//            exit;
            if($this->accountingSet)
                $this->createOp_Accounting();

            $conn->commit();
        }catch (\Exception $e){
            var_dump($e->getMessage());

            $conn->rollBack();
            exit;
            $this->createApiProblem();
        }
        $this->em->refresh($this->operation);
    }

    public function createLateFee(){
        parent::createLateFee();
        if(!$this->accountingSet)
            return false;
        //TODO CHANGE ACCOUNTS
        /**
         * @var $factoringLateFee Account
         * @var $factoringLateFeeReceivable Account
         * @var $ac ProductAccountingConfigurationProduct
         * @var $invoice OperationInvoice
         */
        foreach($this->accountingSet as $ac) {
            $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
            $$systemName = $ac->getAccount();
        }
        $dailyLateFeeRate = $this->getDailyLateFeeRate();
        $createEntries = new ArrayCollection();
        foreach ($this->operation->getInvoices() as $invoice){
            if($lastLateFeeDate = $this->lastLateFeeDate($invoice)){
                if($lateFeeAmount = $this->lateFeeAmount($invoice)){
                    if($days = $this->getDaysAmount($lastLateFeeDate)) {

                        $value = ($lateFeeAmount * $dailyLateFeeRate) * $days;
                        $lateFeeEntry = new Entry();
                        $lateFeeEntry->setAccountCredit($factoringLateFeeReceivable);
                        $lateFeeEntry->setAccountDebit($factoringLateFee);
                        $lateFeeEntry->setValue($value);

                        $createEntry = new AccountingEntry();
                        $createEntry->setIdUser($this->user->getId());
                        $createEntry->setDateUser($this->today);
                        $createEntry->addEntry($lateFeeEntry);
                        $createEntry->setType($this->entryTypeLateFee);

                        $createEntry->setOperation($this->operation);
                        $createEntry->setOperationInvoice($invoice);
                        $this->em->persist($createEntry);
                        $this->em->flush();
                        $createEntries[] = $createEntry;
                    }
                }
            }
        }

        return $createEntries;
    }

    protected function getDailyLateFeeRate(){
        $commissions = $this->attributes['commissions'];
        return (($commissions/100 * 1.18 ) / 30);
    }

    public function closeOp()
    {

    }

    /**
     * @param BankMove $bankMove
     * @param float $value
     * @param Client $debtor
     * @param array $options
     * @return array|ArrayCollection
     */
    public function addPayment(BankMove $bankMove, $value, Client $debtor, $options){
        /**
         * @var $ac ProductAccountingConfigurationProduct
         * @var $initialFactoring Account
         * @var $factoringLateFee Account
         * @var $bankAccount Account
         * @var $invoice OperationInvoice
         */
        foreach($this->accountingSet as $ac) {
            $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
            $$systemName = $ac->getAccount();
        }

        $bankMove->setBankAccount($bankAccount->getBankAccount());
        $bankMove->setDateUser($this->today);
        $entries =  new ArrayCollection();
        $newInvoicePayments =  new ArrayCollection();

        $payment = new Payment();
        $payment->setDebtor($debtor);
        $payment->setOperation($this->operation);
        $paymentEntry = new AccountingEntry();
        $paymentEntry->setIdUser($this->user->getId());
        $paymentEntry->setDateUser(new \DateTime("now"));
        $paymentEntry->setOperation($this->operation);
        $paymentEntry->setDescription("Payment");
        $paymentEntry->setType($this->entryTypePayment);
        foreach ($this->getOperationInvoiceRepository()->findDebtorInvoice($this->operation->getId(),$debtor->getId()) as $invoice){
            $invoiceValue = $invoice->getValue();
            $lateFeeValue = 0;
            if($lateFees = $invoice->getLateFeeEntries())
                foreach ($lateFees as $lateFee)
                    foreach ($lateFee->getEntrys() as $lf)
                        $lateFeeValue += $lf->getValue();

            if($oldInvoicePayments = $invoice->getInvoicePayments())
                foreach ($oldInvoicePayments as $oldInvoicePayment)
                    if($oldInvoicePayment->isLateFee())
                        $lateFeeValue -= $oldInvoicePayment->getValue();
                    else
                        $invoiceValue -= $oldInvoicePayment->getValue();

            if($options['lateFeeFirst']){
                if($lateFeeValue) {
                    $returnLateFee = $this->createInvoicePayment($value,$lateFeeValue,$bankMove, $paymentEntry, $invoice, $payment,true);
                    $value = $returnLateFee['value'];
                    $entries[] = $returnLateFee['entry'];
                    $newInvoicePayments[] = $returnLateFee['newInvoicePayment'];
                }
                if(!$options['strict'] && $value && $invoiceValue){
                    $returnInvoicePayment = $this->createInvoicePayment($value,$invoiceValue,$bankMove, $paymentEntry, $invoice, $payment,false);

                    $value = $returnInvoicePayment['value'];
                    $entries[] = $returnInvoicePayment['entry'];
                    $newInvoicePayments[] = $returnInvoicePayment['newInvoicePayment'];
                }
            }else{

                if($invoiceValue){

                    $returnInvoicePayment = $this->createInvoicePayment($value,$invoiceValue,$bankMove, $paymentEntry, $invoice, $payment,false);
                    $value = $returnInvoicePayment['value'];
                    $entries[] = $returnInvoicePayment['entry'];
                    $newInvoicePayments[] = $returnInvoicePayment['newInvoicePayment'];
                }
                if(!$options['strict'] && $value && $lateFeeValue) {
                    $returnLateFee = $this->createInvoicePayment($value,$lateFeeValue,$bankMove, $paymentEntry, $invoice, $payment,true);
                    $value = $returnLateFee['value'];
                    $entries[] = $returnLateFee['entry'];
                    $newInvoicePayments[] = $returnLateFee['newInvoicePayment'];
                }
            }
            if($value == 0)
                break;
            elseif ($value < 0){
                $this->createApiProblem(400,ApiProblem::TYPE_PAYMENT_ERROR);
            }
        }
        if($value){
            $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_OVERPAY);
        }

        $paymentEntry->setEntrys($entries);

        $payment->setEntry($paymentEntry);
        $payment->setInvoicePayments($newInvoicePayments);
        $this->em->persist($payment);
        $this->em->flush();

        return $newInvoicePayments;
    }

    public function createInvoicePayment($value,$paymentValue,BankMove $bankMove,AccountingEntry $paymentEntry,OperationInvoice $invoice,Payment $payment,$isLateFee){
        /**
         * @var $ac ProductAccountingConfigurationProduct
         * @var $initialFactoring Account
         * @var $factoringLateFeeReceivable Account
         * @var $bankAccount Account
         * @var $invoice OperationInvoice
         */
        foreach($this->accountingSet as $ac) {
            $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
            $$systemName = $ac->getAccount();
        }
        $entryValue = $paymentValue;
        if($value < $paymentValue){
            $entryValue = $value;
            $value = 0;
        }else{
            $value -= $paymentValue;
        }
        $entry = new Entry();
        $entry->setAccountDebit($bankAccount);
        $entry->setValue($entryValue);
        $entry->setBankMoveCredit($bankMove);
        $entry->setAccountingEntry($paymentEntry);

        if($isLateFee){
            $entry->setAccountCredit($factoringLateFeeReceivable);
        }else{
            $entry->setAccountCredit($initialFactoring);
        }
        $newInvoicePayment = new InvoicePayment();
        $newInvoicePayment->setInvoice($invoice);
        $newInvoicePayment->setPayment($payment);
        $newInvoicePayment->setIsLateFee($isLateFee);
        $newInvoicePayment->setValue($entryValue);

        $return['newInvoicePayment'] = $newInvoicePayment;
        $return['entry'] = $entry;
        $return['value'] =$value;
        return $return ;
    }

    protected function invoiceExpire(OperationInvoice $invoice){
        $invoiceMaturityDate = $this->invoiceMaturityDateLateFee($invoice);

        if(!($this->maturityDate < $this->today || $invoiceMaturityDate < $this->today))
            return false;
        if($this->maturityDate <= $invoiceMaturityDate)
            return $this->maturityDate;
        return $invoiceMaturityDate;
    }

    protected function lastLateFeeDate(OperationInvoice $invoice){
        /**
         * @var AccountingEntry $entry
         */
        if(!$lastLateFeeDate = $this->invoiceExpire($invoice))
            return false;

        foreach ($invoice->getLateFeeEntries() as $entry){
            if($entry->getDateSystem() > $lastLateFeeDate)
                $lastLateFeeDate = $entry->getDateSystem();
        }

        return $lastLateFeeDate;
    }

    protected function lateFeeAmount(OperationInvoice $invoice){
        $total = $invoice->getValue();
        foreach ($invoice->getInvoicePayments() as $invoicePayment){
            $total -= $invoicePayment->getValue();
        }
        return $total;
    }

    protected function invoiceMaturityDate(OperationInvoice $invoice){
        $date = new \DateTimeImmutable($invoice->getDateInvoice()->format("Y-m-d"));
        return $date->add(new \DateInterval("P".$invoice->getTenor()."D"));
    }

    protected function invoiceMaturityDateLateFee(OperationInvoice $invoice){
        $date = new \DateTimeImmutable($invoice->getDateInvoice()->format("Y-m-d"));
        return $date->add(new \DateInterval("P".$this->getInvoiceDays($invoice)."D"));
    }

    public function getInvoiceDays(OperationInvoice $invoice){
        $periods = $this->getInvoicePeriods($invoice);
        return $periods * 30;

    }

    public function getInvoicePeriods(OperationInvoice $invoice){
        $maturityDate = \DateTime::createFromFormat('Y-m-d', $invoice->invoiceMaturityDate());
        $days = $this->getDaysAmount($this->operation->getEffectiveDate(),$maturityDate);
        $periods = 1;
        if($days <= 30)
            return $periods;
        $days -= 30;
        while ($days>0){
            $days -=15;
            $periods += 0.5;
        }
        return $periods ;
    }

    public function getOperationPeriods(){
        $maturityDate = $this->getMaturityDate();
        $days = $this->getDaysAmount($this->operation->getEffectiveDate(),$maturityDate);
        $periods = 1;
        if($days <= 30)
            return $periods;
        $days -= 30;
        while ($days>0){
            $days -=15;
            $periods += 0.5;
        }
        return $periods ;
    }


}