<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 2/14/17
 * Time: 10:06 AM
 */

namespace AppBundle\Operations;


use AppBundle\Api\ApiProblem;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Client;
use AppBundle\EntityCompanies\Entry;
use AppBundle\EntityCompanies\InvoicePayment;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\OperationInvoice;
use AppBundle\EntityCompanies\Payment;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

class FactoringHistoricProcess extends Lending
{


    /**
     * @var $invoices array
     */
    protected $invoices;



    /**
     * Factoring constructor.
     * @param Operation $operation
     * @param EntityManager $em
     * @param User $user
     */
    public function __construct(Operation $operation, EntityManager $em = null,User $user = null)
    {
        parent::__construct($operation, $em,$user);
        $this->invoices = $this->operation->getInvoices();
        $this->setTotalAmount();
    }


    public function setTotalAmount()
    {
        $this->totalAmount  = 0;
        foreach ($this->invoices as $invoice)
            $this->totalAmount  += $invoice->getValue();
    }

    public function getTotalAmount($susPayments = false){

        $totalPay = parent::getTotalAmount();
        if($susPayments){
            /**
             * @var $accountingEntry AccountingEntry
             * @var $entry Entry
             * @var $payment InvoicePayment
             */
            foreach ($this->operation->getPayments() as $payment){
                $totalPay -= $payment->getValue();
            }
            foreach ($this->operation->getEntries() as $accountingEntry){
                if($entries = $accountingEntry->getEntrys() )
                    if($accountingEntry->getType()->getName() == OperationProcess::TYPE_LATE_FEES)
                        foreach ($entries as $entry)
                            $totalPay += $entry->getValue();

            }
            if(isset($this->attributes['historicLateFee']))
                $totalPay += $this->attributes['historicLateFee'];
        }
        return $totalPay;
    }
    /**
     * @return float
     */
    public function getNetAmount(){
        return $this->getProcessAmounts()['t']['totalTransitValue'];
    }
    /**
     * @param BankMove $bankMove
     * @param float $value
     * @param Client $debtor
     * @param array $options
     * @return array|ArrayCollection
     */
    public function addPayment(BankMove $bankMove, $value, Client $debtor, $options){
        /**
         * @var $ac ProductAccountingConfigurationProduct
         * @var $initialFactoring Account
         * @var $factoringLateFee Account
         * @var $factoringLateFeeReceivable Account
         * @var $bankAccount Account
         * @var $invoice OperationInvoice
         */
        foreach($this->accountingSet as $ac) {
            $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
            $$systemName = $ac->getAccount();
        }

        $bankMove->setBankAccount($bankAccount->getBankAccount());
        $entries =  new ArrayCollection();
        $newInvoicePayments =  new ArrayCollection();

        $payment = new Payment();
        $payment->setDebtor($debtor);
        $payment->setOperation($this->operation);
        $paymentEntry = new AccountingEntry();
        $paymentEntry->setIdUser($this->user->getId());
        $paymentEntry->setDateUser($bankMove->getDateUser());
        $paymentEntry->setOperation($this->operation);
        $paymentEntry->setDescription("Payment");
        $paymentEntry->setType($this->entryTypePayment);

        $lateFeeValue = 0;
        if(isset($this->attributes['historicLateFee']))
            $lateFeeValue = $this->attributes['historicLateFee'];
        foreach ($this->getOperationInvoiceRepository()->findDebtorInvoice($this->operation->getId(),$debtor->getId()) as $invoice){
            $invoiceValue = $invoice->getValue();

            if($oldInvoicePayments = $invoice->getInvoicePayments())
                foreach ($oldInvoicePayments as $oldInvoicePayment)
                    if($oldInvoicePayment->isLateFee())
                        $lateFeeValue -= $oldInvoicePayment->getValue();
                    else
                        $invoiceValue -= $oldInvoicePayment->getValue();

            if($options['lateFeeFirst']){
                if($lateFeeValue) {
                    $returnLateFee = $this->createInvoicePayment($value,$lateFeeValue,$bankMove, $paymentEntry, $invoice, $payment,true);
                    $value = $returnLateFee['value'];
                    $entries[] = $returnLateFee['entry'];
                    $newInvoicePayments[] = $returnLateFee['newInvoicePayment'];
                }
                if(!$options['strict'] && $value && $invoiceValue){
                    $returnInvoicePayment = $this->createInvoicePayment($value,$invoiceValue,$bankMove, $paymentEntry, $invoice, $payment,false);
                    $value = $returnInvoicePayment['value'];
                    $entries[] = $returnInvoicePayment['entry'];
                    $newInvoicePayments[] = $returnInvoicePayment['newInvoicePayment'];
                }
            }else{
                if($invoiceValue){
                    $returnInvoicePayment = $this->createInvoicePayment($value,$invoiceValue,$bankMove, $paymentEntry, $invoice, $payment,false);
                    $value = $returnInvoicePayment['value'];
                    $entries[] = $returnInvoicePayment['entry'];
                    $newInvoicePayments[] = $returnInvoicePayment['newInvoicePayment'];
                }
                if(!$options['strict'] && $value && $lateFeeValue) {
                    $returnLateFee = $this->createInvoicePayment($value,$lateFeeValue,$bankMove, $paymentEntry, $invoice, $payment,true);
                    $value = $returnLateFee['value'];
                    $entries[] = $returnLateFee['entry'];
                    $newInvoicePayments[] = $returnLateFee['newInvoicePayment'];
                }
            }
            if($value == 0)
                break;
            elseif ($value < 0){
                $this->createApiProblem(400,ApiProblem::TYPE_PAYMENT_ERROR);
            }
        }
        if($value){
            $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_OVERPAY);
        }

        $paymentEntry->setEntrys($entries);

        $payment->setEntry($paymentEntry);
        $payment->setInvoicePayments($newInvoicePayments);
        $this->em->persist($payment);
        $this->em->flush();

        return $newInvoicePayments;
    }

    private function getCommission(OperationInvoice $invoice = null){
        $commitV = 0;
        if($invoice)
            $invoices[] = $invoice;
        else
            $invoices = $this->invoices;
        foreach ($invoices as $invo){
            /**
             * @var $invo OperationInvoice
             */
            $value = $invo->getValue();
            $invoiceCommission = $this->getInvoiceCommissionP($invo);
            $commitV += $value * $invoiceCommission;
        }
        return $commitV;
    }

    public function getInvoiceCommissionP(OperationInvoice $invoice){
        $commitP = $this->attributes['commissions'];
        $commitP = $commitP/100;
        $periods = $this->getInvoicePeriods($invoice);
        return $commitP * $periods;
    }

    private function getReserve(){
        $reserveP = $this->attributes['reserve'];
        $reserveP = $reserveP/100;
        $reserveV = $this->totalAmount * $reserveP;
        return $reserveV;
    }

    private function getTotalReserve(){
        /** @var $reserve Account
         * @var $payment Payment
         * @var $entry Entry
         */
        foreach($this->accountingSet as $ac) {
            if($ac->getProductAccountingConfiguration()->getSystemName() == "reserve")
                $reserve = $ac->getAccount();
        }
        $totalReserve = $this->getReserve();
        foreach ($this->operation->getPayments() as $payment){
            if($accountingEntry = $payment->getEntry())
                foreach($accountingEntry->getEntrys() as $entry)
                    if($entry->getAccountDebit() == $reserve)
                        $totalReserve -= $entry->getValue();
        }
    }

    private function getAttributeValue($att,$type = ""){
        if(!isset($this->attributes[$att]))
            return 0 ;
        switch ($type){
            case '%':
                $attP = $this->attributes[$att];
                $attP = $attP/100;
                $attV = $this->totalAmount * $attP;
                break;
            default:
                $attV = $this->attributes[$att];
        }

        return $attV;
    }

    private function getDiscount(){
        return $this->getAttributeValue("discount","%");
    }

    private function getInsurance(){
        return $this->getAttributeValue("insurance","%");
    }

    private function getAnalysis(){
        return $this->getAttributeValue("analysis");
    }

    private function getWire(){
        return $this->getAttributeValue("wire");
    }

    public function createOp()
    {
        parent::createOp(); // TODO: Change the autogenerated stub
    }

    public function getOp()
    {
        parent::getOp(); // TODO: Change the autogenerated stub
    }

    public function closeOp()
    {
        parent::closeOp(); // TODO: Change the autogenerated stub
    }

    public function createOp_Accounting()
    {
        $createOpEntry = parent::createOp_Accounting();
        /**
         * @var $initialFactoring Account
         * @var $commissions Account
         * @var $commissionsDeferred Account
         * @var $transit Account
         * @var $reserve Account
         * @var $bankAccount Account
         * @var $factoringLateFee Account
         *
         * @var $discounts Account
         * @var $insurance Account
         * @var $clientAnalysis Account
         * @var $bankFee Account

         * @var $ac ProductAccountingConfigurationProduct
         */
        /*
         *              Declare all Accounts from DataBase
         *
         * the names of variables is in product_accounting_configuration
         * and we get all accounts related in product_accounting_configuration_product
         *
         * */
        foreach($this->accountingSet as $ac) {
            $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
            $$systemName = $ac->getAccount();
        }
        // entries

        $proA = $this->getProcessAmounts();

        $transitValue = $proA['t']['obligatoryTransitValue'];

        $attEntry = array();

        if($proA['discountValue']){
            $transitValue -=  $proA['discountValue'];
            $attEntry[] = $this->newEntry($initialFactoring,$discounts,$proA['discountValue']);
        }
        if($proA['insuranceValue']){
            $transitValue -=  $proA['insuranceValue'];
            $attEntry[] = $this->newEntry($initialFactoring,$insurance,$proA['insuranceValue']);
        }
        if($proA['analysisValue']){
            $transitValue -=  $proA['analysisValue'];
            $attEntry[] = $this->newEntry($initialFactoring,$clientAnalysis,$proA['analysisValue']);
        }
        if($proA['wireValue']){
            $transitValue -=  $proA['wireValue'];
            $attEntry[] = $this->newEntry($initialFactoring,$bankFee,$proA['wireValue']);
        }

        $commissionEntry = $this->newEntry($initialFactoring,$commissionsDeferred,$proA['commissionsValue']);
        $reserveEntry = $this->newEntry($initialFactoring,$reserve,$proA['reserveValue']);
        $transitEntry = $this->newEntry($initialFactoring,$transit,$transitValue);

        $createOpEntry->addEntry($commissionEntry);
        $createOpEntry->addEntry($reserveEntry);
        $createOpEntry->addEntry($transitEntry);
        foreach ($attEntry as &$et)
            $createOpEntry->addEntry($et);

        $this->em->persist($createOpEntry);
        $this->em->flush();
    }

    private function newEntry(Account $debit, Account $credit, $value,$descriptionDebit = "",$descriptionCredit = ""){
        $newEntry= new Entry();
        $newEntry->setAccountCredit($credit);
        $newEntry->setAccountDebit($debit);
        $newEntry->setValue($value);
        $newEntry->setDescriptionCredit($descriptionCredit);
        $newEntry->setDescriptionDebit($descriptionDebit);
        return $newEntry;
    }

    public function getProcessAmounts(){
        $amounts['commissionsValue'] = $this->getCommission();
        $amounts['reserveValue'] = $this->getReserve();
        $amounts['discountValue'] = $this->getDiscount();
        $amounts['insuranceValue'] = $this->getInsurance();
        $amounts['analysisValue'] = $this->getAnalysis();
        $amounts['wireValue'] = $this->getWire();

        $amounts['t']['obligatoryTransitValue'] = $this->totalAmount - $amounts['commissionsValue'] - $amounts['reserveValue'];

        $amounts['t']['totalTransitValue'] = $this->totalAmount;
        foreach($amounts as $att=>$val){
//            if($att == 'p') {
//                foreach ($val as $attP => $valP) {
//                    $amounts['t']['totalTransitValue'] = $valP;
//                    $amounts[$attP] = $valP;
//                }
//            }else
            if($att != 't')
                $amounts['t']['totalTransitValue'] -= $val;
        }
        return $amounts;
    }

    public function createAccrual(){
//        parent::createAccrual();
//        if(($days = $this->getDaysAmount($this->lastAccrualDate())) && $this->accountingSet){
//            /**
//             * @var $commissions Account
//             * @var $commissionsDeferred Account
//             * @var $ac ProductAccountingConfigurationProduct
//             */
//            foreach($this->accountingSet as $ac) {
//                $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
//                $$systemName = $ac->getAccount();
//            }
//
//            $createEntry = new AccountingEntry();
//            $createEntry->setIdUser($this->user->getId());
//            $createEntry->setDateUser($this->today);
//            $createEntry->setType($this->entryTypeAccrual);
//
//            $createEntry->setOperation($this->operation);
//            foreach ($this->getOperation()->getInvoices() as $invoice){
//                $value = $this->calculateAccrualInvoice($days,$invoice);
//                $commissionEntry = new Entry();
//                $commissionEntry->setAccountCredit($commissions);
//                $commissionEntry->setAccountDebit($commissionsDeferred);
//                $commissionEntry->setValue($value);
//                $createEntry->addEntry($commissionEntry);
//            }
//
//            $this->em->persist($createEntry);
//            $this->em->flush();
//            return $createEntry;
//        }
//        else
            return false;
    }

    protected function calculateAccrualInvoice($days,OperationInvoice $invoice){
        $invoiceDays = $this->getInvoiceDays($invoice);
        $invoiceTotalCommission = $this->getInvoiceCommissionP($invoice) * $invoice->getValue();

        return ($invoiceTotalCommission / $invoiceDays) * $days;
    }


    public function getPendingPayments(){
        $this->pendingPayments['incomes']['invoices'] = $this->getPendingPaymentsInvoices();
        $this->pendingPayments['expenses']['reserve'] = $this->getTotalReserve();
        return $this->pendingPayments;
    }



    protected function getPendingPaymentsInvoices(){
        /**
         * @var $accountingEntry AccountingEntry
         * @var $entry Entry
         * @var $payment InvoicePayment
         * @var $invoice OperationInvoice
         */
        $payments = array();
        foreach ($this->invoices as $invoice){
            $pp['value'] = $invoice->getValue();
            $pp['valueLateFee'] = 0;
            $pp['idInvoice'] = $invoice->getId();
            $pp['invoice'] = $invoice;

            foreach ($invoice->getInvoicePayments() as $payment){
                if(!$payment->isLateFee())
                    $pp['value'] -= $payment->getValue();
                else
                    $pp['valueLateFee'] -=$payment->getValue();

            }
            foreach ($invoice->getLateFeeEntries() as $accountingEntry){
                if($entries = $accountingEntry->getEntrys() )
                    if($accountingEntry->getType()->getName() == OperationProcess::TYPE_LATE_FEES)
                        foreach ($entries as $entry)
                            $pp['valueLateFee']  += $entry->getValue();

            }
            if($pp['value'] || $pp['valueLateFee'])
                $payments[] = $pp;
        }
        if(isset($this->attributes['historicLateFee'])){
            $payments['historicLateFee']  = $this->attributes['historicLateFee'];
        }
        return $payments;
    }



}