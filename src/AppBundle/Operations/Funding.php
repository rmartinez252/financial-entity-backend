<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 2/15/17
 * Time: 10:38 AM
 */

namespace AppBundle\Operations;


use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\AccountingEntryType;
use AppBundle\EntityCompanies\Entry;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use Doctrine\ORM\EntityManager;

abstract class Funding extends Process
{


    /**
     * Funding constructor.
     * @param Operation $operation
     * @param EntityManager $em
     * @param User $user
     */
    public function __construct(Operation $operation, EntityManager $em = null,User $user = null)
    {
        parent::__construct($operation, $em,$user);

    }

    public function createAccrual(){
        parent::createAccrual();
        if(($days = $this->getDaysAmount($this->lastAccrualDate())) && $this->accountingSet){
            /**
             * @var $loanInterestExpense Account
             * @var $interestLoanPayable Account
             * @var $ac ProductAccountingConfigurationProduct
             */
            foreach($this->accountingSet as $ac) {
                $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
                $$systemName = $ac->getAccount();
            }
            $value = $this->calculateAccrual($days);
            $initialEntry = new Entry();
            $initialEntry->setAccountCredit($interestLoanPayable);
            $initialEntry->setAccountDebit($loanInterestExpense);
            $initialEntry->setValue($value);

            $createEntry = new AccountingEntry();
            $createEntry->setIdUser($this->user->getId());
            $createEntry->setDateUser($this->today);
            $createEntry->addEntry($initialEntry);
            $createEntry->setType($this->entryTypeAccrual);

            $createEntry->setOperation($this->operation);
            $this->em->persist($createEntry);
            $this->em->flush();
            return $createEntry;
        }
        else
            return false;
    }

    protected function accrualAmount($date = false){
        if(!$date)
            $date = $this->today;
        $value = 0;
        if($entries = $this->getOperation()->getEntries()){
            foreach ($entries as $Aentry)
                if($Aentry->getType() )
                    if($Aentry->getType() == $this->entryTypeAccrual)
                        foreach ($Aentry as $entry)
                            $value+=$entry->getValue();
        }
        $days = $this->getDaysAmount($this->getOperation()->getEffectiveDate(),$date);
        $value += $this->calculateAccrual($days);
        return $value;
    }

    public function calculateAccrual($days){
        $loadType = $this->attributes['loanType'];
        $interest = $this->attributes['interest'];
        $totalAmount = floatval($this->getTotalAmount());
        // entries
        $dailyInterest = $interest/$loadType/100;
        return round(($totalAmount * $dailyInterest) * $days,2);
    }


}