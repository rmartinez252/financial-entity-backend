<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 2/15/17
 * Time: 10:33 AM
 */

namespace AppBundle\Operations;


use AppBundle\Api\ApiProblem;
use AppBundle\Api\ApiProblemException;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\AccountingEntryType;
use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Client;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\OperationAttribute;
use AppBundle\EntityCompanies\OperationInvoice;
use AppBundle\EntityCompanies\OperationStatus;
use AppBundle\EntityCompanies\Payment;
use AppBundle\EntityCompanies\ProductAccountingConfiguration;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use AppBundle\EntityCompanies\ProductAttributeProduct;
use AppBundle\Repository\AccountingEntryTypeRepository;
use AppBundle\Repository\OperationInvoiceRepository;
use AppBundle\Repository\OperationStatusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

abstract class Process
{
    /**
     * @var $operation Operation
     */
    protected  $operation;
    /**
     * @var $isAccountingSet boolean|ProductAccountingConfiguration
     */
    protected   $accountingSet;
    /**
     * @var $em EntityManager
     */
    protected $em;
    /**
     * @var $user User
     */
    protected $user;
    /**
     * @var $attributes array
     */
    protected $attributes;

    /**
     * @var $totalAmount float
     */
    protected $totalAmount;
    
    /**
     * @var AccountingEntryType
     */
    protected $entryTypeNewOperation;
    
    /**
     * @var AccountingEntryType
     */
    protected $entryTypeAccrual;
    
    /**
     * @var AccountingEntryType
     */
    protected $entryTypeSystem;
    
    /**
     * @var AccountingEntryType
     */
    protected $entryTypeLateFee;
    
    /**
     * @var OperationStatus
     */
    protected $operationStatusCurrentValid;
    
    /**
     * @var OperationStatus
     */
    protected $operationStatusExpiredValid;
    
    /**
     * @var OperationStatus
     */
    protected $operationStatusExpired;
    
    /**
     * @var OperationStatus
     */
    protected $operationStatusClose;
    /**
     * @var array
     */    
    protected $pendingPayments;
    
    /**
     * @var \DateTime
     */
    protected $today;

    protected $maturityDate;

    /**
     * Process constructor.
     * @param Operation $operation
     * @param EntityManager $em
     * @param User $user
     */
    public function __construct(Operation $operation,EntityManager $em = null,User $user = null)
    {
        $this->operation = $operation;
        $this->em = $em;
        $this->user = $user;

        if(!$this->validateAttributes())
            $this->createApiProblem();
        $this->accountingSet = $this->validateAccounting();
        if($em){
            $entryTypeRepo = $this->getAccountingEntryTypeRepository();
            $this->entryTypeNewOperation = $entryTypeRepo->findNewOperationType();
            $this->entryTypeAccrual = $entryTypeRepo->findAccrualType();
            $this->entryTypeLateFee = $entryTypeRepo->findLateFeeType();
            $this->entryTypeSystem = $entryTypeRepo->findSystemType();
            $this->entryTypePayment = $entryTypeRepo->findPaymentType();

            $operationStatusRepo = $this->getOperationStatusRepository();
            $this->operationStatusCurrentValid = $operationStatusRepo->findCurrentValidType();
            $this->operationStatusExpiredValid = $operationStatusRepo->findExpiredValidType();
            $this->operationStatusExpired = $operationStatusRepo->findExpiredType();
            $this->operationStatusClose = $operationStatusRepo->findCloseType();
        }
        $this->setMaturityDate();
        $this->today = new \DateTime("now");
//        $this->today = \DateTime::createFromFormat('Y-m-d', '2016-11-30');

        $this->pendingPayments['incomes'] = '';
        $this->pendingPayments['expenses'] = '';
    }
////////////////////////////////////////////////////////////////////////
//                          OPERATION FLOW                            //
////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function createOp(){
        $conn = $this->em->getConnection();
        $conn->beginTransaction();
        try{
            $this->operation->setStatus($this->operationStatusCurrentValid);
            $this->operation->setDateSystem(new \DateTime("now"));

            $this->em->persist($this->operation);
            $ats= new ArrayCollection();
            foreach ($this->operation->getAttributes() as $at)
                $ats[] =$at->setOperation($this->operation);
            $this->operation->setAttributes($ats);
            $this->em->flush();

            if($this->accountingSet)
                $this->createOp_Accounting();

            $conn->commit();
        }catch (\Exception $e){

            $conn->rollBack();
//            var_dump($e->getMessage());
//            exit;
            $this->createApiProblem();
        }
        $this->em->refresh($this->operation);
    }

    /**
     * @return AccountingEntry
     */
    public function createOp_Accounting(){
        $createOpEntry = new AccountingEntry();
        $createOpEntry->setIdUser($this->user->getId());
        $createOpEntry->setDateUser($this->operation->getEffectiveDate());
        $createOpEntry->setOperation($this->operation);
        $createOpEntry->setDescription("New Operation Fist Entry");
        $createOpEntry->setType($this->entryTypeNewOperation);
        return $createOpEntry;
    }

    /**
     * @param BankMove $bankMove
     * @param float $value
     * @param Client $debtor
     */
    public function addPayment(BankMove $bankMove, $value, Client $debtor,$options){
        $this->createApiProblem(400,ApiProblem::TYPE_OPERATION_PROCESS_ERROR);
    }

    public function createAccrual(){
        $this->evaluateStatus();
    }

    public function createLateFee(){
        $this->evaluateStatus();

    }

    public function getBeforeClose(){
        $beforeClosePayments = $this->getPendingPayments();
        return $beforeClosePayments;
    }

    public function changeStatus(OperationStatus $newStatus){
        if($newStatus != $this->operationStatusClose){
            $this->operation->setStatus($newStatus);
            $this->em->persist($this->operation);
            $this->em->flush();
        }else{
            return $this->closeOp();
        }

        return $this->operation;
    }

    public function closeOp(){
        if(!$this->getTotalAmount(true)){
            $this->operation->setStatus($this->operationStatusClose);
            $this->em->persist($this->operation);
            $this->em->flush();
            return $this->operation;
        }
        return false;
    }




///////////////////////////////////////////////////////////////////////
//                              TOOLS                                //
///////////////////////////////////////////////////////////////////////
    /**
     * @return bool
     */
    private function validateAttributes(){
        $productAttributes = $this->operation->getProduct()->getAttributes();
        $operationAttributes = $this->operation->getAttributes();

        foreach ($operationAttributes as $oat){
            $this->attributes[$oat->getAttributeProduct()->getProductAttribute()->getSystemName()] = $oat->getValue();
        }
        foreach ($productAttributes as $at){
            /**
             * @var $at ProductAttributeProduct
             */
            if(!isset($this->attributes[$at->getProductAttribute()->getSystemName()]) && !$at->getIsNullable() )
                return false;
            if(!isset($this->attributes[$at->getProductAttribute()->getSystemName()]))
                $this->attributes[$at->getProductAttribute()->getSystemName()] = $at->getProductAttribute()->getDefaultValue();


        }
        return true;
    }

    /**
     * @return bool|ArrayCollection
     */
    private function validateAccounting(){
        $productAccounting = $this->operation->getProduct()->getAccountingConfigurations();
        foreach ($productAccounting as $ac){
            /**
             * @var $ac ProductAccountingConfigurationProduct
             */

            if(!$ac->getAccount())
                return false;
        }
        return $productAccounting;
    }



    /**
     * @param \DateTime|null $date
     */
    protected function setMaturityDate(\DateTime $date = null){
        if($date)
            $this->maturityDate = $date;
        else{
            $date = new \DateTimeImmutable($this->operation->getEffectiveDate()->format("Y-m-d"));
            $this->maturityDate = $date->add(new \DateInterval("P".$this->operation->getTenor()."D"));
        }
    }

    protected function getDaysAmount($date,$date2 = false){
        if(!$date2)
            $date2 =  $this->today;
        if($date <= $date2)
            return date_diff($date2,$date)->days ;
        return 0;
    }

    /**
     * @return \DateTime
     */
    public function lastAccrualDate(){
        /**
         * @var AccountingEntry $entry
         */
        $lastAccrual = $this->operation->getEffectiveDate();
        if($entries = $this->operation->getEntries()){
            foreach ($entries as $entry){
                if($entry->getType() )
                    if($entry->getType() == $this->entryTypeAccrual && $entry->getDateSystem() > $lastAccrual)
                        $lastAccrual = $entry->getDateSystem();
            }
        }
        return $lastAccrual;
    }


    /**
     * @return \DateTime
     */
    public function getMaturityDate(){
        return $this->maturityDate;
    }

    public function calculateAccrual($days){
        return false;
    }

    public function getInvoiceCommissionP(OperationInvoice $invoice){
        return false;
    }

    public function getProcessAmounts(){
        return false;
    }

    /**
     * @param bool $option
     * @return float
     */
    public function getTotalAmount($option = false){
        return $this->totalAmount;
    }

    /**
     * @return float
     */
    public function getNetAmount(){
        return $this->totalAmount;
    }

    /**
     * @return Operation
     */
    public function getOperation()
    {
        return $this->operation;
    }



    public function getPendingPayments(){
        return $this->pendingPayments;
    }

    private function evaluateStatus(){
        if($status = $this->operation->getStatus())
            if(!$status->getType())
                return false;

    }

    /**
     * @param int $statusCode
     * @param string $type
     */
    protected function createApiProblem($statusCode = 400, $type = ApiProblem::TYPE_ATTRIBUTES_DATA){
        $apiProblem = new ApiProblem($statusCode, $type);
        throw new ApiProblemException($apiProblem);
    }


////////////////////////////////////////////////////////////////
//                      REPOSITORIES                          //
////////////////////////////////////////////////////////////////

    /**
     * @return AccountingEntryTypeRepository
     */
    protected function getAccountingEntryTypeRepository(){
        return $this->em->getRepository('AppBundle\\EntityCompanies\\AccountingEntryType');
    }

    /**
     * @return OperationStatusRepository
     */
    protected function getOperationStatusRepository(){
        return $this->em->getRepository('AppBundle\\EntityCompanies\\OperationStatus');
    }

    /**
     * @return OperationInvoiceRepository
     */
    protected function getOperationInvoiceRepository(){
        return $this->em->getRepository('AppBundle\\EntityCompanies\\OperationInvoice');
    }



}

