<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Company;
use AppBundle\Entity\SystemLog;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use JMS\Serializer\SerializationContext;
use JMS\SerializerBundle\JMSSerializerBundle;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ApiLogListener
{
    /**
     * @var array
     */
    private $actions = array();
    /**
     * @var int
     */
    private $position = 0;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Company
     */
    private $company;
    /**
     * @var TokenStorage
     */
    private $tokenSt;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ApiLogListener constructor.
     * @param TokenStorage $tokenSt
     * @param ContainerInterface $container
     */
    public function __construct(TokenStorage $tokenSt,ContainerInterface $container)
    {
        $this->tokenSt = $tokenSt;
        $this->container = $container;
    }
    protected function getUser()
    {
        $token  = $this->tokenSt->getToken();
        if (null === $token ) {
            return ;
        }
        $user = $token->getUser();
        return $user;
    }
    protected function getCompany()
    {
        $token  = $this->tokenSt->getToken();
        if (null === $token ) {
            return ;
        }
        if(!$company = $token->getAttribute("company"))
            return ;
        return $company;
    }
    protected function getUsername(){
        if(!$this->user)
            return "No user logged";
        return $this->user->getUsername();
    }
    protected function getCompanyName(){
        if(!$this->company)
            return "No company Selected";
        return $this->company->getName();
    }
    /**
     * @param $data
     * @param string $moreGroups
     * @param string $format
     * @return mixed
     */
    protected function serialize($data, $moreGroups = '' ,$format = 'json'){
        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $groups = array('Default');
        if ($moreGroups) {
            if(is_array($moreGroups)){
                foreach ($moreGroups as $gr){
                    $groups[] = $gr;
                }
            }else
                $groups[] = $moreGroups;
        }
        $context->setGroups($groups);

        return $this->container->get('jms_serializer')
            ->serialize($data, $format, $context);
    }
    private function createLog($type,EntityManager $em,$entity){


        $name = explode('\\',get_class($entity))[2];
        if($name  == "SystemLog" )
            exit;

        $positionName = strtoupper($type);
        $functionName = 'toSystemLog'.ucfirst(strtolower($type));
        $refClass = $em->getClassMetadata(get_class($entity))->getReflectionClass();


        $this->actions[$positionName][$this->position]['name'] = $name;
        $this->actions[$positionName][$this->position]['data'] = "define toSystemLog() or ".$functionName."() in '".$this->actions['NEW'][$this->position]['name']."' Entity to fill this field";
        if($refClass->hasMethod($functionName)){
            $this->actions[$positionName][$this->position]['data'] = $entity->$functionName();
        }elseif($refClass->hasMethod('toSystemLog'))
            $this->actions[$positionName][$this->position]['data'] = $entity->toSystemLog();
        else{
            $this->actions[$positionName][$this->position]['data'] .= $this->serialize($entity);
        }
        if(!$this->actions[$positionName][$this->position]['data'])
            unset($this->actions[$positionName][$this->position]);
        $this->position++;
    }

    public function preFlush(PreFlushEventArgs $args)
    {
        $this->user = $this->getUser();
        $this->company = $this->getCompany();
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            $this->createLog('new',$em,$entity);
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $this->createLog('update',$em,$entity);
        }
//
//        foreach ($uow->getScheduledEntityDeletions() as $entity) {
//            var_dump('DELETE'.$em->getClassMetadata(get_class($entity))->getName());
//
//        }
//
//        foreach ($uow->getScheduledCollectionDeletions() as $col) {
//            var_dump('DELETE'.$col);
//
//        }
//
//        foreach ($uow->getScheduledCollectionUpdates() as $col) {
//            var_dump('UPDATE');
//        }
        if(empty($this->actions['NEW']))
            unset($this->actions['NEW']);
        if(empty($this->actions['UPDATE']))
            unset($this->actions['UPDATE']);
        foreach($this->actions as $act => $v){
            foreach($v as $type => $log){
//                if($this->company)
//                var_dump($this->company->getId());
                if($em->getConnection()->getDatabase() == "no hacer log"/*"tcg2"*/){
                    $sysLog = new SystemLog($act,$this->getUsername(),$this->user,$this->company);
                    $sysLog->setInfo(json_encode($log));
                    $em->merge($sysLog);
                }
            }
        }

    }


}
