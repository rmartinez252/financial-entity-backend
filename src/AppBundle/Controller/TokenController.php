<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class TokenController extends BaseController
{

    /**
     * ApiDoc(
     *  description="Get a Token System",
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="HTTP Basic Authorization (username and password)",
     *             "required" ="true"
     *          }
     *     },
     * output={
     *    "class" = "token",
     *    "name" = "Token"
     *    }
     * )
     * @Route("/tokens" , name="uri_post_token")
     * @Method("POST")
     */
    public function tokenPostAction(Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $request->getUser()]);

        if (!$user) {
            throw $this->createNotFoundException($user);
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $request->getPassword());

        if (!$isValid) {
            throw new BadCredentialsException();
        }

        $token = $this->get('token_manager')->getNew($user);

        return new JsonResponse(['token' => $token]);
    }
}