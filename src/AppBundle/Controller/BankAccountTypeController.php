<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\Entity\BankAccountType;
use AppBundle\Form\BankAccountTypeType;
use AppBundle\Form\BankType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class BankAccountTypeController extends BaseController
{
//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                          BANK ACCOUNT TYPE
//******************************************************************//
//////////////////////////////////////////////////////////////////////
    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create new AccountType (Bank) ",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Create a new AccountType (Bank)",
     *  input="AppBundle\Form\BankAccountTypeType",
     *  output={
     *     "class" = "AppBundle\Entity\BankAccountType"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_POST')")
     * @Route("/bankAccountTypes" , name="uri_post_bank_account_type")
     * @Method("POST")
     */
    public function postActionAccountType(Request $request){
        $accountType = new BankAccountType();
        $form = $this->createForm(BankAccountTypeType::class, $accountType);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($accountType);
        $em->flush();
        $response = $this->createValidApiResponse($accountType, 201,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update  AccountType (Bank) ",
     *         404={
     *           "Returned when the  AccountType (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Update a AccountType (Bank) ",
     *  input="AppBundle\Form\BankType",
     *  output={
     *     "class" = "AppBundle\Entity\Bank"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_PATH')")
     * @Route("/bankAccountTypes/{id}" , name="uri_path_bank_account_type")
     * @Method("PATCH")
     * @param BankAccountType $accountType
     * @param Request $request
     * @return Response|void
     */
    public function patchActionAccountType(BankAccountType $accountType,Request $request){
        $form = $this->createForm(BankAccountTypeType::class, $accountType);
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($accountType);
        $em->flush();
        $response = $this->createValidApiResponse($accountType, 200,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get  AccountType (Bank) ",
     *         404={
     *           "Returned when the  AccountType (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Show All AccountType (Bank) in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\Entity\BankAccountType",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankAccountTypes" , name="uri_get_bank_account_type")
     * @Method("GET")
     */
    public function getActionAccountType(){
        $accountTypeRepository = $this->getBankAccountTypeRepository();
        return $this->createValidApiResponse(array('bankAccountType'=>$accountTypeRepository->findAll()),200,404);
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get AccountType (Bank) ",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Get All info from one AccountType (Bank)",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     *  output="AppBundle\Entity\Bank"
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankAccountTypes/{id}" , name="uri_get_one_bank_account_type")
     * @Method("GET")
     * @param BankAccountType $accountType
     * @return Response
     */
    public function getOneActionAccountType(BankAccountType $accountType){

        return $this->createValidApiResponse($accountType,200,404,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete  AccountType (Bank) ",
     *         404={
     *           "Returned when the AccountType (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="delete a AccountType (Bank)",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     * )
     * @Security("is_granted('ROLE_BANK_DELETE')")
     * @Route("/bankAccountTypes/{id}" , name="uri_delete_bank_account_type")
     * @Method("DELETE")
     * @param BankAccountType $accountType
     * @return Response|void
     */
    public function deleteActionAccountType(BankAccountType $accountType){
        if($accountType){
            $em = $this->getDoctrine()->getManager();
            $em->remove($accountType);
            $em->flush();
            return $this->createApiResponse(true, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }
//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                    BANK ACCOUNT TYPE END
//******************************************************************//
//////////////////////////////////////////////////////////////////////


}