<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/31/16
 * Time: 9:13 AM
 */

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class FormController extends BaseController
{
    /**
     * @ApiDoc(
     *  section = "_notReadyForms",
     *  description="Show users form",
     *  output="AppBundle\Form\Api\FormResponse"
     * )
     * @Route("/form/{formName}" , name="uri_get_form")
     * @Method("GET")
     */
    public function getFormAction($formName){

        return $this->createApiResponse($this->createFormResponse($formName));
    }

    /**
     * @ApiDoc(
     *  section = "_notReadyForms",
     *  description="Show users form for Edit User",
     *  output="AppBundle\Form\UserType"
     * )
     * @Security("is_granted('ROLE_USER_GET')")
     * @Route("/form/{formName}/{id}" , name="uri_get_users_form")
     * @Method("GET")
     */
    public function getFormUpdateAction($id){
        /**
         * @var $user User
         */
        if($user = $this->getUserRepository()->find($id)){
            $user->setPassword(null);
            return $this->createApiResponse(array('form'=>$this->createForm(UserType::class,$user)->all(),'form2'=>$this->createForm(UserType::class,$user)->isValid(),'data'=>$user),200,'Full');
        }
    }
}