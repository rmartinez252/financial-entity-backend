<?php

namespace AppBundle\Controller;

use AppBundle\Api\ApiProblem;
use AppBundle\Api\ApiProblemException;
use AppBundle\Form\Api\FormResponse;
use AppBundle\Form\UserType;
use AppBundle\Repository\AccountingEntryRepository;
use AppBundle\Repository\AccountingEntrySavedRepository;
use AppBundle\Repository\AttributeRepository;
use AppBundle\Repository\BankAccountTypeRepository;
use AppBundle\Repository\BankMoveRepository;
use AppBundle\Repository\BankMoveTypeRepository;
use AppBundle\Repository\BankRepository;
use AppBundle\Repository\ClientRepository;
use AppBundle\Repository\CompanyEndpointRepository;
use AppBundle\Repository\OperationAttributeRepository;
use AppBundle\Repository\OperationRepository;
use AppBundle\Repository\OperationStatusRepository;
use AppBundle\Repository\PaymentRepository;
use AppBundle\Repository\ProgrammerRepository;
use AppBundle\Repository\StructureRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\BattleRepository;
use AppBundle\Repository\ApiTokenRepository;
use AppBundle\Tree\AccountTree;
use AppBundle\Tree\StructureTree;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Tests\Encoder\PasswordEncoder;

abstract class BaseController extends Controller
{

    /**
     * @return UserRepository
     */
    protected function getUserRepository(){
        return $this->getDoctrine()->getRepository('AppBundle:User');
    }
    /**
     * @return BankRepository
     */
    protected function getBankRepository(){
        return $this->getDoctrine()->getRepository('AppBundle:Bank');
    }
    /**
     * @return BankAccountTypeRepository
     */
    protected function getBankAccountTypeRepository(){
        return $this->getDoctrine()->getRepository('AppBundle:BankAccountType');
    }
    /**
     * @return BankMoveTypeRepository
     */
    protected function getBankMoveTypeRepository(){
        return $this->getDoctrine()->getRepository('AppBundle:BankMoveType');
    }
    /**
     * @return AttributeRepository
     *
     */
    protected function getAttributeRepository(){
        return $this->getDoctrine()->getRepository('AppBundle:Attribute');
    }

    /**
     * @return CompanyEndpointRepository
     */
    protected function getCompanyEndpointRepository(){
        return $this->getDoctrine()->getRepository('AppBundle:CompanyEndpoint');
    }
    /**
     * @return StructureRepository
     */
    protected function getStructureRepository(){

        return $this->getDoctrine()->getRepository('AppBundle:Structure');
    }

    /**
     * @param bool $company|
     * @return ClientRepository
     */
    protected function getClientRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\Client');
    }

    /**
     * @return StructureTree
     */
    protected function getStructureTreeManager(){
        return $this->container->get('appbundle.tree.structure');
    }

    /**
     * @param bool|string $company
     * @param EntityManager $em
     * @return AccountTree
     */
    protected function getAccountTreeManager($company = false,EntityManager $em=null){
        if(!$company)
            $company = $this->getCompany(true);
        $accountingTreeManager = $this->container->get('appbundle.tree.account.'.$company);
        if($em){
            $accountingTreeManager->setEm($em);
        }
        return $accountingTreeManager;
    }

    /**
     * @param bool|string $company
     * @return bool|EntityManager
     */
    protected function getEntityManager($company = false){
        if(!$company)
            $company = $this->getCompany(true);
        return $this->container->get('doctrine.orm.'.$company.'_entity_manager');
    }
    /**
     * @param bool $company
     * @return bool|\Doctrine\ORM\EntityRepository
     */
    protected function getAttributesCompanyRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\Attribute');
    }

    /**
     * @param bool $company
     * @return AccountingEntryRepository
     */
    protected function getAccountingEntryRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\AccountingEntry');
    }
    /**
     * @param bool $company
     * @return AccountingEntrySavedRepository
     */
    protected function getAccountingEntrySavedRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\AccountingEntrySaved');
    }
    /**
     * @param bool $company
     * @return bool|\Doctrine\ORM\EntityRepository
     */
    protected function getAccountingEntryTypeRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\AccountingEntryType');
    }
    /**
     * @param bool $company
     * @return bool|\Doctrine\ORM\EntityRepository
     */
    protected function getProductRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\Product');
    }
    /**
     * @param bool $company
     * @return OperationRepository
     */
    protected function getOperationRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\Operation');
    }
    /**
     * @param bool $company
     * @return OperationStatusRepository
     */
    protected function getOperationStatusRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\OperationStatus');
    }
    /**
     * @param bool $company
     * @return PaymentRepository
     */
    protected function getOperationPaymentRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\Payment');
    }
    /**
     * @param bool $company
     * @return OperationAttributeRepository
     */
    protected function getOperationAttributeRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\OperationAttribute');
    }
    /**
     * @param bool $company
     * @return bool|\Doctrine\ORM\EntityRepository
     */
    protected function getAccountRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\Account');
    }
    /**
     * @param bool $company
     * @return bool|\Doctrine\ORM\EntityRepository
     */
    protected function getBankAccountRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\BankAccount');
    }
    /**
     * @param bool $company
     * @return BankMoveRepository
     */
    protected function getBankMoveRepository($company = false){
        return $this->getEntityManager($company)->getRepository('AppBundle\\EntityCompanies\\BankMove');
    }


    /**
     * @return PasswordEncoder
     */
    protected function getSecurityEncoder(){
        return $this->container->get('security.password_encoder');
    }

    /**
     * @param $data
     * @param int $statusCode
     * @param string $groups
     * @param bool $token
     * @return Response
     */
    protected function createApiResponse($data,$statusCode = 200, $groups = '' , $token = false ){
//        var_dump($this->getUser()->getUsername());die;
        $json = $this->serialize($data,$groups);
        $response = new Response($json, $statusCode, array(
            'Content-Type' => 'application/hateoas+json'
        ));
        if(!$token)
            $response->headers->set('AuthorizationTk',$this->refreshToken());
        else
            $response->headers->set('AuthorizationTk',$token);

        return $response;
    }

    /**
     * @param int $statusCode
     * @param string $type
     */
    protected function createApiProblem($statusCode = 400, $type = ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT){
        $apiProblem = new ApiProblem($statusCode, $type);
        throw new ApiProblemException($apiProblem);
    }


    /**
     * @param string $name
     * @param array $data
     * @return FormResponse|bool|\Symfony\Component\Form\Form
     */


    protected function createFormResponse($name,$data = null){
        $form = false;
        switch ($name){
            case 'user':
                $form =  $this->createForm(UserType::class);
                break;
        }
        if(!$form)
            return $form;
        $formResponse = new FormResponse($form,$data);
        return $formResponse;
    }

    /**
     * @param $data
     * @param int $statusCodeTrue
     * @param int $statusCodeFalse
     * @param string $groups
     * @param string $type
     * @param bool $token
     * @return Response|void
     */
    protected function createValidApiResponse($data,$statusCodeTrue = 200,$statusCodeFalse = 400, $groups = '', $type = ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT, $token = false){
        if($data){
            return $this->createApiResponse($data,$statusCodeTrue,$groups,$token);
        }
        return $this->createApiProblem($statusCodeFalse,$type);
    }

    /**
     * @param $data
     * @param string $moreGroups
     * @param string $format
     * @return mixed
     */
    protected function serialize($data, $moreGroups = '' ,$format = 'json'){
        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $groups = array('Default');
        if ($moreGroups) {
            if(is_array($moreGroups)){
                foreach ($moreGroups as $gr){
                    $groups[] = $gr;
                }
            }else
                $groups[] = $moreGroups;
        }
        $context->setGroups($groups);

        return $this->container->get('jms_serializer')
            ->serialize($data, $format, $context);
    }



    /**
     * @param Request $request
     * @param FormInterface $form
     */
    protected function processForm(Request $request, FormInterface $form){
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            $apiProblem = new ApiProblem(400, ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);

            throw new ApiProblemException($apiProblem);
        }

        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getErrorsFromForm(FormInterface $form){
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }

    /**
     * @param FormInterface $form
     */
    protected function throwApiProblemValidationException(FormInterface $form){
        $errors = $this->getErrorsFromForm($form);

        $apiProblem = new ApiProblem(
            400,
            ApiProblem::TYPE_VALIDATION_ERROR2
        );
        $apiProblem->set('errorForm', $errors);
        $apiProblem->set('postData', $form->getData());

        throw new ApiProblemException($apiProblem);
    }
}
