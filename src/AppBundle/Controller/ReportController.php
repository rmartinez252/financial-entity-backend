<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\Form\AccountingEntryType;
use AppBundle\Form\AccountType;
use AppBundle\Form\Model\AccountingEntryModel;
use AppBundle\Form\Model\Reports\GeneralLedgerModel;
use AppBundle\Reports\Model\Operations\Funding01;
use AppBundle\Reports\Model\Operations\Funding01Model;
use AppBundle\Reports\Model\Operations\Lending01Model;
use AppBundle\Repository\AccountRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class AccountController
 * @package AppBundle\Controller
 */

/**
 *
 * @Security("is_granted('ROLE_USER')")
 */
class ReportController extends BaseController
{
/////////////////////////////////////////////////////////////////
//              ACCOUNTING REPORTS                             //
/////////////////////////////////////////////////////////////////
    /**
     * @ApiDoc(
     *  section = "05) Accounting - Reports",
     *  description="general ledger",
     *  parameters = {
     *         { "name" = "dateStart", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "dateEnd", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "accountStart", "dataType" = "id", "format" = "integer", "required" = false },
     *         { "name" = "accountEnd", "dataType" = "id", "format" = "integer", "required" = false }
     *  },
     *  output={
     *       "class"="AppBundle\EntityCompanies\AccountingEntry"
     *     }
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accounting/reports/generalLedger" , name="uri_get_report_general_ledger")
     * @Method("GET")
     * @return Response
     */
    public function getReportGeneralLedger(Request $request){
        $data = $request->query->all() ;
        $dateStart = $dateEnd = $startL = $endL = null;
        /**
         * @var $accountRepository AccountRepository
         */
        $accountRepository = $this->getAccountRepository();
        if(isset($data['accountStart'])) {
            if (!$accountStart = $accountRepository->findOneBy(array("id" => $data['accountStart'])))
                $this->createApiProblem();
            else
                $startL = $accountStart->getL();
        }
        if(isset($data['accountEnd'])){
            if(!$accountEnd = $accountRepository->findOneBy(array("id"=>$data['accountEnd'])))
                $this->createApiProblem();
            else
                $endL = $accountEnd->getL();
        }
        $accounts = $accountRepository->findInterval($startL,$endL);
        if(isset($data['dateStart']))
            $dateStart = $data['dateStart'];
        if(isset($data['dateEnd']))
            $dateEnd = $data['dateEnd'];
        $generalLedgerModels = array();
        foreach ($accounts as $account){
            $model = new GeneralLedgerModel($account,$dateStart,$dateEnd);
            if($model->getEntries())
                $generalLedgerModels[] = $model;
        }
        if(!$generalLedgerModels)
            $this->createApiProblem(404,ApiProblem::TYPE_NO_CONTENT_IN_THIS_SEARCH);
        return $this->createValidApiResponse($generalLedgerModels,200,400,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Reports",
     *  description="balance sheet - Get ",
     *  parameters = {
     *         { "name" = "dateEnd", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false }
     *  },
     *  output={
     *       "class"="AppBundle\Entity\Account"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/accounting/reports/balanceSheet" , name="uri_get_report_balance_sheet")
     * @Method("GET")
     * @return Response
     */
    public function getReportBalanceSheet(Request $request){
        $data = $request->query->all() ;
        $dateEnd = null;
        if(isset($data['dateEnd']))
            $dateEnd = $data['dateEnd'];
        $am = $this->getAccountTreeManager();
        $accounts = $am->showBalanceSheet($dateEnd);
        $response = $this->createApiResponse($accounts,200);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Reports",
     *  description="Income Statement - Get ",
     *  parameters = {
     *         { "name" = "dateEnd", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "dateStart", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "monthEnd", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false }
     *  },
     *  output={
     *       "class"="AppBundle\Entity\Account"
     *     }
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accounting/reports/incomeStatement" , name="uri_get_report_income_statement")
     * @Method("GET")
     * @return Response
     */
    public function getReportIncomeStatement(Request $request){
        $data = $request->query->all() ;
        $am = $this->getAccountTreeManager();
        $accounts = $am->showIncomeStatement($data,$this->getCompany()->getCloseDate());
        $response = $this->createApiResponse($accounts,200);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Reports",
     *  description="Journal Entry - Get ",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get accounting entry",
     *         404="Returned when something else is not found"
     *     },
     *  description="Get All info Entries in a date range",
     *  parameters = {
     *         { "name" = "day", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "dateStart", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "dateEnd", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false }
     *  },
     *  output="AppBundle\Form\Model\AccountingEntryModel"
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accounting/reports/journalEntry" , name="uri_get_report_journal_entry")
     * @Method("GET")
     * @param Request $request
     * @return Response|void
     * @internal param AccountingEntry $accountEntry
     */
    public function getReportJournalEntryAction(Request $request){
        //SAME AS GET ACCOUNTING ENTRY BY DATE
        $accountEntryRepository = $this->getAccountingEntryRepository();
        $data = $request->query->all() ;
        $q = $accountEntryRepository->createQueryBuilder("a");
        if(isset($data['day'])){
            $dateStart = $data['day'];
            $dateEnd = $data['day'];
        }elseif(isset($data['dateStart']) && isset($data['dateEnd'])){
            $dateStart = $data['dateStart'];
            $dateEnd = $data['dateEnd'];
            if($dateEnd < $dateStart){
                $safeDate = $dateEnd;
                $dateEnd = $dateStart;
                $dateStart = $safeDate;
            }
        }
        if (isset($dateStart))
            $dateStart = $dateStart." 00:00:00";

        if (isset($dateEnd))
            $dateEnd = $dateEnd." 23:59:59";
//var_dump($dateEnd);
//        exit;
        if(isset($dateStart) && isset($dateEnd)) {
            $q->andWhere("a.dateUser >= :dateStart")->setParameter('dateStart', $dateStart);
            $q->andWhere("a.dateUser <= :dateEnd")->setParameter('dateEnd', $dateEnd);
        }else{
            if(isset($dateStart))
                $q->andWhere("a.dateUser >= :dateStart")->setParameter('dateStart', $dateStart);
            if(isset($dateEnd)){
                $q->andWhere("a.dateUser <= :dateEnd")->setParameter('dateEnd', $dateEnd);
            }
        }
        $q->orderBy("a.dateUser");
        $accountEntries = $q->getQuery()->execute();
        $accountEntryModels = array();
        foreach ($accountEntries as $accountEntry){
            if($accountEntry)
                $accountEntryModels[] = new AccountingEntryModel($accountEntry);
        }
        return $this->createValidApiResponse(array("entrys"=>$accountEntryModels),200,400,'Full');
    }

/////////////////////////////////////////////////////////////////
//              OPERATIONS REPORTS                             //
/////////////////////////////////////////////////////////////////

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations - Reports",
     *  description="Funding Operation Report - Get ",
     *  output={
     *       "class"="AppBundle\Reports\Model\Operations\Funding01"
     *     }
     * )
     * @Security("is_granted('ROLE_PRODUCT_GET')")
     * @Route("/operations/reports/funding/{idOperation}" , name="uri_get_report_funding01")
     * @Method("GET")
     * @param $idOperation
     * @return Response
     */
    public function getReportFunding01($idOperation){
        if(!$operation = $this->getOperationRepository()->findOneByPortfolio($idOperation,"f"))
            $this->createApiProblem(400,ApiProblem::TYPE_NO_OPERATION_ID_PORTFOLIO);
        $em = $this->getEntityManager();
        $report = new Funding01Model($operation,$em);
        $response = $this->createApiResponse($report,200);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations - Reports",
     *  description="Funding Operation Report - Get ",
     *  output={
     *       "class"="AppBundle\Reports\Model\Operations\Lending01"
     *     }
     * )
     * @Security("is_granted('ROLE_PRODUCT_GET')")
     * @Route("/operations/reports/lending/{idOperation}" , name="uri_get_report_lending01")
     * @Method("GET")
     * @param $idOperation
     * @return Response
     */
    public function getReportLending01($idOperation){
        if(!$operation = $this->getOperationRepository()->findOneByPortfolio($idOperation,"l"))
            $this->createApiProblem(400,ApiProblem::TYPE_NO_OPERATION_ID_PORTFOLIO);
        $em = $this->getEntityManager();
        $report = new Lending01Model($operation,$em);
        $response = $this->createApiResponse($report,200);
        return $response;
    }

/////////////////////////////////////////////////////////////////
//              CLIENTS REPORTS                                //
/////////////////////////////////////////////////////////////////


    /**
     * @ApiDoc(
     *  section = "04) Clients - Reports",
     *  description="Funding Operation Report - Get ",
     *  output={
     *       "class"="AppBundle\Reports\Model\Operations\Funding01"
     *     }
     * )
     * @Security("is_granted('ROLE_PRODUCT_GET')")
     * @Route("/clients/{idClient}/reports/operations/funding" , name="uri_get_client_report_funding01")
     * @Method("GET")
     * @param $idClient
     * @return Response
     */
    public function getReportClientFunding01($idClient){
        if(!$operations = $this->getOperationRepository()->findAllByClient($idClient,"f"))
            $this->createApiProblem(404,ApiProblem::TYPE_NO_OPERATION);
        $em = $this->getEntityManager();
        $report = array();
        foreach ($operations as $operation)
            $report[] = new Funding01Model($operation,$em);
        $response = $this->createApiResponse($report,200);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "04) Clients - Reports",
     *  description="Lending Operation Report - Get ",
     *  output={
     *       "class"="AppBundle\Reports\Model\Operations\Lending01"
     *     }
     * )
     * @Security("is_granted('ROLE_PRODUCT_GET')")
     * @Route("/clients/{idClient}/reports/operations/lending" , name="uri_get_client_report_lending01")
     * @Method("GET")
     * @param $idClient
     * @return Response
     */
    public function getReportClientLending01($idClient){
        if(!$operations = $this->getOperationRepository()->findAllByClient($idClient,"l"))
            $this->createApiProblem(404,ApiProblem::TYPE_NO_OPERATION);
        $em = $this->getEntityManager();
        $report = array();
        foreach ($operations as $operation)
            $report[] = new Lending01Model($operation,$em);
        $response = $this->createApiResponse($report,200);
        return $response;
    }

}