<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Entry;
use AppBundle\Form\AccountingEntryType;
use AppBundle\Form\Model\AccountingEntryModel;
use AppBundle\Form\Model\Reports\GeneralLedgerModel;
use AppBundle\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class AccountController
 * @package AppBundle\Controller
 */

/**
 *
 * @Security("is_granted('ROLE_USER')")
 */
class AccountingEntryController extends BaseController
{

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry",
     *  description="Create a new Accounting Entry",
     *  requirements={},

     *  input="AppBundle\Form\AccountingEntryType",
     *  output={
     *       "class"="AppBundle\EntityCompanies\AccountingEntry"
     *     }
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_POST')")
     * @Route("/accountingEntries" , name="uri_post_accounting_entry")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postAction(Request $request){
        //create AccountingEntry object
        $entry = new AccountingEntry();
        //call Entity Manager, this EM is From the Company Data Base
        $em = $this->getEntityManager();
        //create from for validation
        $form = $this->createForm(AccountingEntryType::class, $entry,array('em' => $em));
        //populate from fields with POST info
        $this->processForm($request, $form);
        //validate form
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        $postEntities = new ArrayCollection();
        /**
         * @var $et Entry
         */
        //start foreach of Entries for validate Bank Account type
        //and start bankMove validation process
        foreach ($entry->getEntrys() as $et){
            /**
             * @var $bankMoveCredit BankMove
             * @var $bankMoveDebit BankMove
             */
            //if the account using as Account Credit in this entity is a bank account
            if($et->getAccountCredit()->isBankAccount()){
                //validate if this account move is not empty
                if(!$bankMoveCredit = $et->getBankMoveCredit())
                    return $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_ERROR2);
                //validate if this move type is a credit move type
                if($bankMoveCredit->getBankMoveType()->isNature())
                    return $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_ERROR2);
                //populate bankMove
                $bankMoveCredit->setBankAccount($et->getAccountCredit()->getBankAccount());
                $bankMoveCredit->setDateUser($entry->getDateUser());
                //set bankMove with new data in the entry
                $et->setBankMoveCredit($bankMoveCredit);

            }
            //if the account using as Account Debit in this entity is a bank account
            if($et->getAccountDebit()->isBankAccount()){
                //validate if this account move is not empty
                if(!$bankMoveDebit = $et->getBankMoveDebit())
                    return $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_ERROR2);
                //validate if this move type is a debit move type
                if(!$bankMoveDebit->getBankMoveType()->isNature())
                    return $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_ERROR2);
                //populate bankMove
                $bankMoveDebit->setBankAccount($et->getAccountDebit()->getBankAccount());
                $bankMoveDebit->setDateUser($entry->getDateUser());
                //set bankMove with new data in the entry
                $et->setBankMoveDebit($bankMoveDebit);
            }
            //list all entries in one ArrayCollection
            $postEntities[] = $et;
        }
        //set new entries collection in the accountingEntry
        $entry->setEntrys($postEntities);
        //add current user in the accountingEntry
        $entry->setIdUser($this->getUser()->getId());
        //save accountingEntry in company data base
        $em->persist($entry);
        $em->flush();
        $em->clear();

        $response = $this->createApiResponse($entry, 201);
//TODO Review this header link
        $headerUrl = $this->generateUrl(
            'uri_get_one_accounting_entry',
            ['id' => $entry->getId()]
        );
        $response->headers->set('Location', $headerUrl );

        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry",
     *  description="Edit existent Accounting Entry",
     *  requirements={},
     *  input="AppBundle\Form\AccountingEntryType",
     *  output={
     *       "class"="AppBundle\EntityCompanies\AccountingEntry"
     *     }
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_POST')")
     * @Route("/accountingEntries/{id}" , name="uri_patch_accounting_entry")
     * @Method("PATCH")
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function patchAction($id,Request $request){

        //get AccountingEntry object

        /** @var AccountingEntry $accountEntry */

        if(!$accountEntry = $this->getAccountingEntryRepository()->findOneBy(array('id'=>$id)))
            return $this->createApiProblem();
        if($accountEntry->getType()->isSystem())
            return $this->createApiProblem(412,ApiProblem::TYPE_VALIDATION_SYSTEM_TYPE);
        //call Entity Manager, this EM is From the Company Data Base
        $em = $this->getEntityManager();
        $em->beginTransaction();
        $conn = $em->getConnection();
        try{
            foreach ($accountEntry->getEntrys() as $e)
                $em->remove($e);
//            $accountEntry->removeAllEntry();
            $em->flush();
            //create from for validation
            $form = $this->createForm(AccountingEntryType::class, $accountEntry,array('em' => $em));
            //populate from fields with POST info
            //validate form

            $this->processForm($request, $form);
            if (!$form->isValid()) {
                $this->throwApiProblemValidationException($form);
            }

            $postEntities = new ArrayCollection();
            /**
             * @var $et Entry
             */
            //start foreach of Entries for validate Bank Account type
            //and start bankMove validation process
            foreach ($accountEntry->getEntrys() as $et){
                /**
                 * @var $bankMoveCredit BankMove
                 * @var $bankMoveDebit BankMove
                 */
                //if the account using as Account Credit in this entity is a bank account
                if($et->getAccountCredit()->isBankAccount()){
                    //validate if this account move is not empty
                    if(!$bankMoveCredit = $et->getBankMoveCredit())
                        return $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_ERROR2);
                    //validate if this move type is a credit move type
                    if($bankMoveCredit->getBankMoveType()->isNature())
                        return $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_ERROR2);
                    //populate bankMove
                    $bankMoveCredit->setBankAccount($et->getAccountCredit()->getBankAccount());
                    $bankMoveCredit->setDateUser($accountEntry->getDateUser());
                    //set bankMove with new data in the entry
                    $et->setBankMoveCredit($bankMoveCredit);

                }
                //if the account using as Account Debit in this entity is a bank account
                if($et->getAccountDebit()->isBankAccount()){
                    //validate if this account move is not empty
                    if(!$bankMoveDebit = $et->getBankMoveDebit())
                        return $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_ERROR2);
                    //validate if this move type is a debit move type
                    if(!$bankMoveDebit->getBankMoveType()->isNature())
                        return $this->createApiProblem(400,ApiProblem::TYPE_VALIDATION_ERROR2);
                    //populate bankMove
                    $bankMoveDebit->setBankAccount($et->getAccountDebit()->getBankAccount());
                    $bankMoveDebit->setDateUser($accountEntry->getDateUser());
                    //set bankMove with new data in the entry
                    $et->setBankMoveDebit($bankMoveDebit);
                }
                //list all entries in one ArrayCollection
                $postEntities[] = $et;
            }
            //set new entries collection in the accountingEntry
            $accountEntry->setEntrys($postEntities);
            //add current user in the accountingEntry
            $accountEntry->setIdUser($this->getUser()->getId());
            //save accountingEntry in company data base
            $em->persist($accountEntry);
            $em->flush();
            $conn->commit();
        }catch (\Exception $e){
            $conn->rollback();
        }

        $response = $this->createApiResponse($accountEntry, 201);
//TODO Review this header link
        $headerUrl = $this->generateUrl(
            'uri_get_one_accounting_entry',
            ['id' => $accountEntry->getId()]
        );
        $response->headers->set('Location', $headerUrl );

        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get accounting entry",
     *         404="Returned when something else is not found"
     *     },
     *  description="Get All info from one Entry",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id accounting entry"},},
     *  output="AppBundle\Form\Model\AccountingEntryModel"
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accountingEntries/byId/{id}" , name="uri_get_one_accounting_entry")
     * @Method("GET")
     * @param $id
     * @return Response|void
     * @internal param AccountingEntry $accountEntry
     */
    public function getOneAction($id){

        /** @var AccountingEntry $accountEntry */

        if(!$accountEntry = $this->getAccountingEntryRepository()->findOneBy(array('id'=>$id)))
            return $this->createApiProblem();

        $accountEntryModel = new AccountingEntryModel($accountEntry);
        return $this->createValidApiResponse($accountEntryModel,200,400,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get accounting entry",
     *         404="Returned when something else is not found"
     *     },
     *  description="Get All info from one Entry",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id accounting entry"},},
     *  output="AppBundle\Form\Model\AccountingEntryModel"
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accountingEntries/byEntryId/{id}" , name="uri_get_one_by_entry_accounting_entry")
     * @Method("GET")
     * @param $id
     * @return Response|void
     * @internal param AccountingEntry $accountEntry
     */
    public function getOneByEntryIdAction($id){

        if(!$accountEntry = $this->getAccountingEntryRepository()->findOneByEntryId($id))
            return $this->createApiProblem();

        $accountEntryModel = new AccountingEntryModel($accountEntry);
        return $this->createValidApiResponse($accountEntryModel,200,400,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get accounting entry",
     *         404="Returned when something else is not found"
     *     },
     *  description="Get All info Entries in a date range",
     *  parameters = {
     *         { "name" = "day", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "dateStart", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "dateEnd", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false }
     *  },
     *  output="AppBundle\Form\Model\AccountingEntryModel"
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accountingEntries/byDate" , name="uri_get_accounting_entry_bydate")
     * @Method("GET")
     * @param Request $request
     * @return Response|void
     * @internal param AccountingEntry $accountEntry
     */
    public function getByDateAction(Request $request){
        //SAME AS JOURNAL ENTRY REPORT
        $accountEntryRepository = $this->getAccountingEntryRepository();
        $data = $request->query->all() ;
        $q = $accountEntryRepository->createQueryBuilder("a");
        if(isset($data['day']))
            $q->andWhere("a.dateUser = :day")->setParameter('day', $data['day']);

        elseif(isset($data['dateStart']) && isset($data['dateEnd'])) {
            if($data['dateStart'] < $data['dateEnd']){
                $q->andWhere("a.dateUser >= :dateStart")->setParameter('dateStart', $data['dateStart']);
                $q->andWhere("a.dateUser <= :dateEnd")->setParameter('dateEnd', $data['dateEnd']);
            }elseif($data['dateStart'] > $data['dateEnd']){
                $q->andWhere("a.dateUser <= :dateStart")->setParameter('dateStart', $data['dateStart']);
                $q->andWhere("a.dateUser >= :dateEnd")->setParameter('dateEnd', $data['dateEnd']);
            }else
                $q->andWhere("a.dateUser = :dateStart")->setParameter('dateStart', $data['dateStart']);
        }else{
            if(isset($data['dateStart']))
                $q->andWhere("a.dateUser >= :dateStart")->setParameter('dateStart', $data['dateStart']);
            if(isset($data['dateEnd']))
                $q->andWhere("a.dateUser <= :dateEnd")->setParameter('dateEnd', $data['dateEnd']);
        }
        $q->orderBy("a.dateUser");
        $accountEntrys = $q->getQuery()->execute();
        $accountEntryModels = array();
        foreach ($accountEntrys as $accountEntry){
            if($accountEntry)
                $accountEntryModels[] = new AccountingEntryModel($accountEntry);
        }
        return $this->createValidApiResponse(array("entrys"=>$accountEntryModels),200,400,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry",
     *  description="general ledger",
     *  parameters = {
     *         { "name" = "dateStart", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false },
     *         { "name" = "dateEnd", "dataType" = "date", "format" = "yyyy-mm-dd", "required" = false }
     *  },
     *  output={
     *       "class"="AppBundle\EntityCompanies\AccountingEntry"
     *     }
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accountingEntries/byAccount/{id}" , name="uri_get_accounting_entry_byaccount")
     * @Method("GET")
     * @param $id
     * @param Request $request
     * @return Response
     * @internal param Account $account
     */
    public function getByAccountAction($id,Request $request){
        $data = $request->query->all() ;
        $dateStart = $dateEnd = null;

        $account = $this->getAccountRepository()->findOneBy(array('id'=>$id));
        if(isset($data['dateStart']))
            $dateStart = $data['dateStart'];
        if(isset($data['dateEnd']))
            $dateEnd = $data['dateEnd'];
        $GeneralLedgerModels = array();
        $model = new GeneralLedgerModel($account,$dateStart,$dateEnd);
        if($model->getEntries())
            $GeneralLedgerModels[] = $model;

        return $this->createValidApiResponse($GeneralLedgerModels,200,400,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get accounting entry",
     *         404="Returned when something else is not found"
     *     },
     *  description="Get All info from one Entry",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id accounting entry"},},
     *  output="AppBundle\Form\Model\AccountingEntryModel"
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accountingEntryTypes" , name="uri_get_accounting_entry_type")
     * @Method("GET")
     * @return Response|void
     * @internal param AccountingEntry $accountEntry
     */
    public function getEntryTypeAction(){
        $accountEntryType = $this->getAccountingEntryTypeRepository()->findAll();
        /** @var \AppBundle\EntityCompanies\AccountingEntryType $accountEntryType */

        if(!$accountEntryType)
            return $this->createApiProblem();

        return $this->createValidApiResponse($accountEntryType,200,400,'Full');
    }


//    /**
//     * @ApiDoc(
//     *  section = "05) Accounts",
//     *  description="Show all accounts from one company in json array ",
//     *  output={
//     *       "class"="AppBundle\Entity\Account"
//     *     }
//     * )
//     * @Security("is_granted('ROLE_STRUCTURE_GET')")
//     * @Route("/accounts" , name="uri_get_all_account")
//     * @Method("GET")
//     * @return Response
//     */
//    public function getAllAction(){
//        $companyName = $this->getCompany(true);
//
//        $am = $this->getAccountTreeManager($companyName);
//        $structure = $am->showAll();
//
//        $response = $this->createApiResponse($structure, 200);
//        return $response;
//    }
//
//    /**
//     * @ApiDoc(
//     *  section = "05) Accounts",
//     *  description="delete a account and Show all others accounts in a json array",
//     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id node "},},
//     *     output={
//     *       "class"="AppBundle\Entity\Structure"
//     *     }
//     * )
//     * @Security("is_granted('ROLE_STRUCTURE_GET')")
//     * @Route("/structures/{id}" , name="uri_delete_structures")
//     * @Method("DELETE")
//     */
//    public function deleteAction($id){
//        $companyName = $this->getCompany(true);
//
//        $am = $this->getAccountTreeManager($companyName);
//        $accounts = $am->deleteById($id);
//        $response = $this->createApiResponse($accounts, 200);
////        $nodeLink  = $this->generateUrl(
////            'uri_get_family_structures',
////            ['id' => $id]
////        );
////        $response->headers->set('Location', $nodeLink);
//        return $response;
//    }


}