<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\Entity\Bank;
use AppBundle\Form\BankType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class BankController extends BaseController
{
//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                              BANK
//******************************************************************//
//////////////////////////////////////////////////////////////////////
    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create new Bank",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Create a new Bank",
     *  input="AppBundle\Form\BankType",
     *  output={
     *     "class" = "AppBundle\Entity\Bank"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_POST')")
     * @Route("/banks" , name="uri_post_banks")
     * @Method("POST")
     */
    public function postAction(Request $request){
        $bank = new Bank();
        $form = $this->createForm(BankType::class, $bank);
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($bank);
        $em->flush();
        $response = $this->createValidApiResponse($bank, 201,404,"FullBank");
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update banks",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Update a Bank",
     *  input="AppBundle\Form\BankType",
     *  output={
     *     "class" = "AppBundle\Entity\Bank"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_PATH')")
     * @Route("/banks/{id}" , name="uri_path_banks")
     * @Method("PATCH")
     * @param Bank $bank
     * @param Request $request
     * @return Response|void
     */
    public function patchAction(Bank $bank,Request $request){
        $form = $this->createForm(BankType::class, $bank);
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($bank);
        $em->flush();
        $response = $this->createValidApiResponse($bank, 200,404,"FullBank");
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get banks",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Show All banks in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\Entity\Bank",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/banks" , name="uri_get_banks")
     * @Method("GET")
     */
    public function getAction(){
        $bankRepository = $this->getBankRepository();
        return $this->createValidApiResponse(array('bank'=>$bankRepository->findAll()),200,404,"FullBank");
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get banks",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Get All info from one bank",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     *  output="AppBundle\Entity\Bank"
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/banks/{id}" , name="uri_get_one_banks")
     * @Method("GET")
     * @param Bank $bank
     * @return Response
     */
    public function getOneAction(Bank $bank){

        return $this->createValidApiResponse($bank,200,404,'FullBank');
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete banks",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="delete a bank and Show ",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     * )
     * @Security("is_granted('ROLE_BANK_DELETE')")
     * @Route("/banks/{id}" , name="uri_delete_banks")
     * @Method("DELETE")
     * @param Bank $bank
     * @return Response|void
     */
    public function deleteAction(Bank $bank){
        if($bank){
            $em = $this->getDoctrine()->getManager();
            $em->remove($bank);
            $em->flush();
            return $this->createApiResponse(true, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }
//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                              BANK END
//******************************************************************//
//////////////////////////////////////////////////////////////////////

}