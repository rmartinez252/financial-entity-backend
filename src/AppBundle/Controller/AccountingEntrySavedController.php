<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\AccountingEntrySaved;
use AppBundle\Form\AccountingEntrySavedType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class AccountController
 * @package AppBundle\Controller
 */

/**
 *
 * @Security("is_granted('ROLE_USER')")
 */
class AccountingEntrySavedController extends BaseController
{

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry Save",
     *  description="Create a new Accounting Entry",
     *  requirements={},

     *  input="AppBundle\Form\AccountingEntrySavedType",
     *  output={
     *       "class"="AppBundle\EntityCompanies\AccountingEntrySaved"
     *     }
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_POST')")
     * @Route("/accountingEntries/save" , name="uri_post_accounting_entry_saved")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postAction(Request $request){
        //create AccountingEntry object
        $accountingEntrySaved = new AccountingEntrySaved();
        //call Entity Manager, this EM is From the Company Data Base
        $em = $this->getEntityManager();
        //create from for validation
        $form = $this->createForm(AccountingEntrySavedType::class, $accountingEntrySaved,array('em' => $em));
        //populate from fields with POST info
        $this->processForm($request, $form);
        //validate form
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em->persist($accountingEntrySaved);
        $em->flush();
        $em->clear();
        $response = $this->createApiResponse($accountingEntrySaved, 201,"FullAccountingEntrySave");
        $headerUrl = "link";
//TODO link header
//        $headerUrl = $this->generateUrl(
//            'api_programmers_show',
//            ['nickname' => $programmer->getNickname()]
//        );
        $response->headers->set('Location', $headerUrl );

        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry Save",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get accounting entry",
     *         404="Returned when something else is not found"
     *     },
     *  description="Get All info from one Accounting Entry Saved",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id accounting entry saved"},},
     *  output="AppBundle\EntityCompanies\AccountingEntrySaved"
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accountingEntries/save/{id}" , name="uri_get_one_accounting_entry_saved")
     * @Method("GET")
     * @param $id
     * @return Response|void
     * @internal param AccountingEntry $accountEntry
     */
    public function getOneAction($id){
        $accountEntrySaved = $this->getAccountingEntrySavedRepository()->findOneBy(array('id'=>$id));
        /** @var AccountingEntrySaved $accountEntrySaved  */

        if(!$accountEntrySaved )
            return $this->createApiProblem(404,ApiProblem::TYPE_NO_CONTENT_IN_THIS_SEARCH);

        return $this->createValidApiResponse($accountEntrySaved,200,400,'FullAccountingEntrySave');
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounting Entry Save",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get accounting entry saved",
     *         404="Returned when something else is not found"
     *     },
     *  description="Get All info from one Entry",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id accounting entry saved"},},
     *  output="AppBundle\EntityCompanies\AccountingEntrySaved"
     * )
     * @Security("is_granted('ROLE_ACCOUNTING_GET')")
     * @Route("/accountingEntries/save" , name="uri_get_all_accounting_entry_saved")
     * @Method("GET")
     * @return Response|void
     * @internal param AccountingEntry $accountEntry
     */
    public function getAllAction(){
        $accountEntrySaved = $this->getAccountingEntrySavedRepository()->findAll();
        if(!$accountEntrySaved)
            return $this->createApiProblem(404,ApiProblem::TYPE_NO_CONTENT_IN_THIS_SEARCH);

        return $this->createValidApiResponse($accountEntrySaved,200,400,'FullAccountingEntrySave');
    }



}