<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/20/16
 * Time: 8:59 AM
 */

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Company;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCompanyEndpoint;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Nelmio\ApiDocBundle\Parser\JmsMetadataParser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class LoginController extends BaseController
{
    /**
     * @ApiDoc(
     *  section = "01) Login",
     *  description="Get a Token System",
     *  headers={
     *         {
     *             "name" = "Authorization",
     *             "description" = "HTTP Basic Authorization (username and password)",
     *             "required" = "true"
     *          }
     *     },
     *  output={
     *       "class" = "AppBundle\ResponseModel\loginResponseModel",
     *       "groups" = {"loginResponse"},
     *     "parsers"={"Nelmio\ApiDocBundle\Parser\JmsMetadataParser"},
     *     }
     * )
     * @Route("/login" , name = "uri_post_login")
     * @Method({"POST"})
     */
    public function loginPostAction(Request $request)
    {
        /**
         * @var $user User
         * @var $userCompanyEndpoint UserCompanyEndpoint
         * @var $structureEndpoint StructureEndpoint
         */
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $request->getUser()]);

        if (!$user) {
            throw $this->createNotFoundException($user);
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $request->getPassword());

        if (!$isValid) {
            throw new BadCredentialsException();
        }
        $body = $this->loginBody($user);
        $token = $this->get('token_manager')->getNew($user);
        $response = $this->createApiResponse($body,201,'loginResponse',$token);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "01) Login",
     *  description="change a user to other company",
     *  output={
     *       "class"="AppBundle\Form\Model\AccountStructureModel"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/changeCompany/{idCompany}" , name="uri_change_company")
     * @Method("GET")
     * @param $idCompany
     * @return Response
     */
    public function getChangeCompanyAction($idCompany){
        $user = $this->getUser();

        if (!$company = $user->getCompany($idCompany))
            return $this->createApiProblem(400);
        $body = $this->loginBody($user,$company);
        $token = $this->get('token_manager')->getNew($user,$company);
        $response = $this->createApiResponse($body,201,'loginResponse',$token);
        return $response;
    }

    private function loginBody(User $user,Company $company=null){
        if(!$company)
            $company = $user->getCompany();
        $user->setRoles($company->getId());
        $body['structure'] = $this->getStructureTreeManager()->getUserStructure($user,$company);
        $body['user'] = $user;

        $body['isClient'] = false;
        if($user->getIdClientCompany($company->getId()))
            $body['isClient'] = true;
        $body['currentCompany']['id'] = $company->getId();
        $body['currentCompany']['name'] = $company->getName();
        $body['companies'] = $user->getCompanies();
        return $body;
    }
}