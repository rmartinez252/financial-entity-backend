<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\Product;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use AppBundle\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class ProductConfigurationController extends BaseController
{

//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                              Products
//******************************************************************//
//////////////////////////////////////////////////////////////////////
    /**
     * @ApiDoc(
     *  section = "07) Products - Configuration",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get products",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all product, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Product",
     *     "groups" = "FullProduct"
     * }
     * )
     * @Security("is_granted('ROLE_PRODUCT_GET')")
     * @Route("/products" , name="uri_get_products")
     * @Method("GET")
     * @return Response|void
     */
    public function getProductsAction(){
        $product = $this->getProductRepository()->findAll();
        return $this->createValidApiResponse($product,200,404,"FullProduct");
    }


//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                              Attributes Products
//******************************************************************//
//////////////////////////////////////////////////////////////////////

    /**
     * @ApiDoc(
     *  section = "07) Products - Configuration",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get banks",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all attributes needed for one product, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\ProductAttributeProduct",
     *     "groups" = "FullAttributeProduct"
     * }
     * )
     * @Security("is_granted('ROLE_PRODUCT_GET')")
     * @Route("/products/attributes/{id}" , name="uri_get_product_attributes")
     * @Method("GET")
     * @param $id
     * @return Response|void
     */
    public function getAttributesAction($id){
        $product = $this->getProductRepository()->findOneBy(array('id'=>$id));
        return $this->createValidApiResponse(array("product"=>$product,"productAttributes"=>$product->getAttributes()),200,404,"FullAttributeProduct");
    }

//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                              Accounting Products
//******************************************************************//
//////////////////////////////////////////////////////////////////////

    /**
     * @ApiDoc(
     *  section = "07) Products - Configuration",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get banks",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all accounting configuration needed for one product, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\ProductAccountingConfigurationProduct",
     *     "groups" = "FullAccountingProduct"
     * }
     * )
     * @Security("is_granted('ROLE_PRODUCT_GET')")
     * @Route("/products/accounting/{id}" , name="uri_get_product_accounting")
     * @Method("GET")
     * @param $id
     * @return Response|void
     * @internal param Product $product
     */
    public function getAccountingAction($id){
        $product = $this->getProductRepository()->findOneBy(array('id'=>$id));
        return $this->createValidApiResponse(array("product"=>$product,"accountingConfiguration"=>$product->getAccountingConfigurations()),200,404,"FullAccountingProduct");
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Configuration",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get banks",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="update accounts to be use in one product, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\ProductAccountingConfigurationProduct",
     *     "groups" = "FullAccountingProduct"
     * }
     * )
     * @Security("is_granted('ROLE_PRODUCT_POST')")
     * @Route("/products/accounting/{id}" , name="uri_patch_product_accounting")
     * @Method("PATCH")
     * @param $id
     * @param Request $request
     * @return Response|void
     * @internal param Product $product
     */
    public function patchAccountingAction($id,Request $request){
        $product = $this->getProductRepository()->findOneBy(array('id'=>$id));
        $data = json_decode($request->getContent(),true );
        $accountingConfigurations = $product->getAccountingConfigurations();
        /**
         * @var $accountRepository AccountRepository
         * @var $ac ProductAccountingConfigurationProduct
         *
         */
        $accountRepository = $this->getAccountRepository();
        $newAccountingConfigurations = new ArrayCollection();
        foreach ($accountingConfigurations as $ac){
            $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
            if(array_key_exists($systemName,$data)){
                if($newAccount = $accountRepository->findOneById($data[$systemName]))
                    $ac->setAccount($newAccount);
            }
            $newAccountingConfigurations[] = $ac;
        }
        $product->setAccountingConfigurations($newAccountingConfigurations);
        $em = $this->getEntityManager();
        $em->persist($product);
        $em->flush();
        return $this->createValidApiResponse($product->getAccountingConfigurations(),200,404,"FullAccountingProduct");
    }

}