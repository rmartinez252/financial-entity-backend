<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\Entity\Attribute;
use AppBundle\Form\AttributeCompanyType;
use AppBundle\Form\AttributeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Security("is_granted('ROLE_USER')")
 */
class AttributeController extends BaseController
{


    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create attribute type",
     *         404="Returned when something else is not found"
     *     },
     *  description="Create a new Attribute Type  ",
     *  input="AppBundle\Form\AttributeType",
     *  output="AppBundle\Entity\Attribute"
     * )
     * @Security("is_granted('ROLE_SYSTEM_POST_ATTRIBUTE_TYPE')")
     * @Route("/attributes/user" , name="uri_post_attributes")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postAction(Request $request){
        $attribute = new Attribute();
        $form = $this->createForm(AttributeType::class, $attribute);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($attribute);
        $em->flush();
        $response = $this->createValidApiResponse($attribute, 201,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update attribute type",
     *         404="Returned when something else is not found"
     *     },
     *  description="Update a Attribute",
     *  input="AppBundle\Form\AttributeType",
     *  output="AppBundle\Entity\Attribute"
     * )
     * @Security("is_granted('ROLE_SYSTEM_PATH_ATTRIBUTE_TYPE')")
     * @Route("/attributes/user/{id}" , name="uri_path_attributes")
     * @Method("PATCH")
     * @param Attribute $attribute
     * @param Request $request
     * @return Response
     * @internal param $id
     */
    public function patchAction(Attribute $attribute,Request $request){
        $form = $this->createForm(AttributeType::class, $attribute);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($attribute);
        $em->flush();
        $response = $this->createValidApiResponse($attribute, 200,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get attributes",
     *         404="Returned when something else is not found"
     *     },
     *  description="Show All attribute Type in a json array",
     *  output="AppBundle\Entity\Attribute"
     * )
     * @Security("is_granted('ROLE_SYSTEM_GET')")
     * @Route("/attributes/user" , name="uri_get_attributes")
     * @Method("GET")
     */
    public function getAttributes(){
        $attributeRepository = $this->getAttributeRepository();
        return $this->createValidApiResponse(array('Attributes'=>$attributeRepository->findAll()),200,404);
    }

    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get attribute",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Get All info from one attribute",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id user"},},
     *  output="AppBundle\Entity\Attribute"
     * )
     * @Security("is_granted('ROLE_SYSTEM_GET')")
     * @Route("/attributes/user/{id}" , name="uri_get_one_attribute")
     * @Method("GET")
     * @param Attribute $attribute
     * @return Response
     */
    public function getOneAction(Attribute $attribute){
        return $this->createValidApiResponse($attribute,200,404,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete attributes",
     *         404={
     *           "Returned when the attribute is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="delete a attribute and return boolean from this action",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id user"},},
     * )
     * @Security("is_granted('ROLE_SYSTEM_DELETE_ATTRIBUTE_TYPE')")
     * @Route("/attributes/user/{id}" , name="uri_delete_attribute")
     * @Method("DELETE")
     * @param $attribute Attribute
     * @return Response|void
     */
    public function deleteAction(Attribute $attribute){

        if($attribute){
            $em = $this->getDoctrine()->getManager();
            $em->remove($attribute);
            $em->flush();
            return $this->createApiResponse($attribute, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }


    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create attribute type",
     *         404="Returned when something else is not found"
     *     },
     *  description="Create a new Attribute Type  ",
     *  input="AppBundle\Form\AttributeType",
     *  output="AppBundle\Entity\Attribute"
     * )
     * @Security("is_granted('ROLE_SYSTEM_POST_ATTRIBUTE_TYPE')")
     * @Route("/attributes/company" , name="uri_post_attributes_company")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postActionCompany(Request $request){
        $attribute = new Attribute();
        $form = $this->createForm(AttributeCompanyType::class, $attribute);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getEntityManager();
        $em->persist($attribute);
        $em->flush();
        $response = $this->createValidApiResponse($attribute, 201,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update attribute type",
     *         404="Returned when something else is not found"
     *     },
     *  description="Update a Attribute",
     *  input="AppBundle\Form\AttributeType",
     *  output="AppBundle\Entity\Attribute"
     * )
     * @Security("is_granted('ROLE_SYSTEM_PATH_ATTRIBUTE_TYPE')")
     * @Route("/attributes/company/{id}" , name="uri_path_attributes_company")
     * @Method("PATCH")
     * @param $id
     * @param Request $request
     * @return Response
     * @internal param Attribute $attribute
     * @internal param $id
     */
    public function patchActionCompany($id,Request $request){
        $em = $this->getEntityManager();
        $attribute = $this->getAttributesCompanyRepository()->findOneBy(array("id"=>$id));

        $form = $this->createForm(AttributeCompanyType::class, $attribute);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em->persist($attribute);
        $em->flush();
        $response = $this->createValidApiResponse($attribute, 200,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get attributes",
     *         404="Returned when something else is not found"
     *     },
     *  description="Show All attribute Type in a json array",
     *  output="AppBundle\Entity\Attribute"
     * )
     * @Security("is_granted('ROLE_SYSTEM_GET')")
     * @Route("/attributes/company" , name="uri_get_attributes_company")
     * @Method("GET")
     */
    public function getAttributesCompany(){
        $attributeRepository = $this->getAttributesCompanyRepository();
        return $this->createValidApiResponse(array('Attributes'=>$attributeRepository->findAll()),200,404);
    }

    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get attribute",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Get All info from one attribute",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id user"},},
     *  output="AppBundle\Entity\Attribute"
     * )
     * @Security("is_granted('ROLE_SYSTEM_GET')")
     * @Route("/attributes/company/{id}" , name="uri_get_one_attribute_company")
     * @Method("GET")
     * @param $id
     * @return Response
     * @internal param Attribute $attribute
     */
    public function getOneActionCompany($id){
        $attribute = $this->getAttributesCompanyRepository()->findOneBy(array("id"=>$id));
        return $this->createValidApiResponse($attribute,200,404,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "03) Person Attributes",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete attributes",
     *         404={
     *           "Returned when the attribute is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="delete a attribute and return boolean from this action",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id user"},},
     * )
     * @Security("is_granted('ROLE_SYSTEM_DELETE_ATTRIBUTE_TYPE')")
     * @Route("/attributes/company/{id}" , name="uri_delete_attribute_company")
     * @Method("DELETE")
     * @param $id
     * @return Response|void
     * @internal param Attribute $attribute
     */
    public function deleteActionCompany($id){

        $em = $this->getEntityManager();
        $attribute = $this->getAttributesCompanyRepository()->findOneBy(array("id"=>$id));
        if($attribute){
            $em->remove($attribute);
            $em->flush();
            return $this->createApiResponse($attribute, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }





}