<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\BankAccount;
use AppBundle\Form\BankAccountType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class BankAccountController extends BaseController
{
    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create new Account (Bank) ",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Create a new Account (Bank)",
     *  input="AppBundle\Form\BankAccountType",
     *  output={
     *     "class" = "AppBundle\EntityCompanies\BankAccount"
     * }
     * )
     *
     * @Security("is_granted('ROLE_BANK_POST')")
     * @Route("/bankAccounts" , name="uri_post_bank_account")
     * @Method("POST")
     * @param Request $request
     * @return Response|void
     */
    public function postActionAccount(Request $request){
        $account = new BankAccount();
        $em = $this->getEntityManager();

        $form = $this->createForm(BankAccountType::class, $account,array("em"=>$em));
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em->persist($account);
        $em->flush();
        $response = $this->createValidApiResponse($account, 201,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update Account (Bank) ",
     *         404={
     *           "Returned when the  Account (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Update a Account (Bank) ",
     *  input="AppBundle\Form\BankAccountType",
     *  output={
     *     "class" = "AppBundle\Entity\BankAccount"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_PATH')")
     * @Route("/bankAccounts/{id}" , name="uri_path_bank_account")
     * @Method("PATCH")
     * @param $id
     * @param Request $request
     * @return Response|void
     * @internal param BankAccount $account
     */
    public function patchActionAccount($id,Request $request){
        $account = $this->getBankAccountRepository()->findOneBy(array("id"=>$id));
        $em = $this->getEntityManager();
        if(!$account)
            return $this->createApiProblem(404,ApiProblem::TYPE_INVALID_REQUEST_DATA);

        $form = $this->createForm(BankAccountType::class, $account,array("em"=>$em));
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em->persist($account);
        $em->flush();
        $response = $this->createValidApiResponse($account, 200,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get  Account (Bank) ",
     *         404={
     *           "Returned when the  Account (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Show All Account (Bank) in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\Entity\BankAccount",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankAccounts" , name="uri_get_bank_account")
     * @Method("GET")
     */
    public function getActionAccount(){
        $accountRepository = $this->getBankAccountRepository();
        return $this->createValidApiResponse(array('bankAccount'=>$accountRepository->findAll()),200,404);
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get Account (Bank) ",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Get All info from one Account (Bank)",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     *  output="AppBundle\Entity\Bank"
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankAccounts/{id}" , name="uri_get_one_bank_account")
     * @Method("GET")
     * @param $id
     * @return Response
     * @internal param BankAccount $account
     */
    public function getOneActionAccount($id){

        $account = $this->getBankAccountRepository()->findOneBy(array("id"=>$id));
        return $this->createValidApiResponse($account,200,404,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete  Account (Bank) ",
     *         404={
     *           "Returned when the Account (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="delete a Account (Bank)",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     * )
     * @Security("is_granted('ROLE_BANK_DELETE')")
     * @Route("/bankAccounts/{id}" , name="uri_delete_bank_account")
     * @Method("DELETE")
     * @param $id
     * @return Response|void
     * @internal param BankAccount $account
     */
    public function deleteActionAccount($id){

        $account = $this->getBankAccountRepository()->findOneBy(array("id"=>$id));
        if($account){
            $em = $this->getEntityManager();
            $em->remove($account);
            $em->flush();
            return $this->createApiResponse(true, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }


}