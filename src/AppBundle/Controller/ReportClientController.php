<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\Form\AccountingEntryType;
use AppBundle\Form\AccountType;
use AppBundle\Form\Model\AccountingEntryModel;
use AppBundle\Form\Model\Reports\GeneralLedgerModel;
use AppBundle\Reports\Model\Operations\Funding01;
use AppBundle\Reports\Model\Operations\Funding01Model;
use AppBundle\Reports\Model\Operations\Lending01Model;
use AppBundle\Repository\AccountRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class AccountController
 * @package AppBundle\Controller
 */

/**
 *
 * @Security("is_granted('ROLE_USER')")
 */
class ReportClientController extends BaseController
{

/////////////////////////////////////////////////////////////////
//              OPERATIONS REPORTS                             //
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//              CLIENTS REPORTS                                //
/////////////////////////////////////////////////////////////////


    /**
     * @ApiDoc(
     *  section = "04) Clients - Reports",
     *  description="Funding Operation Report - Get ",
     *  output={
     *       "class"="AppBundle\Reports\Model\Operations\Funding01"
     *     }
     * )
     * @Security("is_granted('ROLE_CLIENT_REPORT_1_GET')")
     * @Route("/clients/reports/operations/funding" , name="client_report_uri_get_client_report_funding01")
     * @Method("GET")
     * @return Response
     */
    public function getReportClientFunding01(){
        $user = $this->getUser();
        $company = $this->getCompany();
        if(!$idClient = $user->getIdClient($company))
            $this->createApiProblem(400,ApiProblem::TYPE_NO_CLIENT_FOUND);
        if(!$operations = $this->getOperationRepository()->findAllByClient($idClient,"f"))
            $this->createApiProblem(400,ApiProblem::TYPE_NO_OPERATION);
        $em = $this->getEntityManager();
        $report = array();
        foreach ($operations as $operation)
            $report[] = new Funding01Model($operation,$em);
        $response = $this->createApiResponse($report,200);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "04) Clients - Reports",
     *  description="Lending Operation Report - Get ",
     *  output={
     *       "class"="AppBundle\Reports\Model\Operations\Lending01"
     *     }
     * )
     * @Security("is_granted('ROLE_CLIENT_REPORT_1_GET')")
     * @Route("/clients/reports/operations/lending" , name="client_report_uri_get_client_report_lending01")
     * @Method("GET")
     * @return Response
     */
    public function getReportClientLending01(){
        $user = $this->getUser();
        $company = $this->getCompany();
        if(!$idClient = $user->getIdClient($company))
            $this->createApiProblem(400,ApiProblem::TYPE_NO_CLIENT_FOUND);
        if(!$operations = $this->getOperationRepository()->findAllByClient($idClient,"l"))
            $this->createApiProblem(400,ApiProblem::TYPE_NO_OPERATION);
        $em = $this->getEntityManager();
        $report = array();
        foreach ($operations as $operation)
            $report[] = new Lending01Model($operation,$em);
        $response = $this->createApiResponse($report,200);
        return $response;
    }
    /**
     * @ApiDoc(
     *  section = "04) Clients - Reports",
     *  description="Lending Operation Report - Get ",
     *  output={
     *       "class"="AppBundle\Reports\Model\Operations\Lending01"
     *     }
     * )
     * @Security("is_granted('ROLE_CLIENT_REPORT_1_GET')")
     * @Route("/clients/reports/operations" , name="client_report_uri_get_client_report_operations")
     * @Method("GET")
     * @return Response
     */
    public function getReportClientOperations(){
        $user = $this->getUser();
        $company = $this->getCompany();
        if(!$idClient = $user->getIdClient($company))
            $this->createApiProblem(400,ApiProblem::TYPE_NO_CLIENT_FOUND);
        if(!$operationsLending = $this->getOperationRepository()->findAllByClient($idClient,"l"))
            $operationsLending = null;
        if(!$operationsFunding = $this->getOperationRepository()->findAllByClient($idClient,"f"))
            $operationsFunding = null;
        $em = $this->getEntityManager();
        $report['Funding'] = array();
        $report['Lending'] = array();
        if($operationsFunding)
            foreach ($operationsFunding as $operation)
                $report['Funding'][] = new Funding01Model($operation,$em);
        if($operationsLending)
            foreach ($operationsLending as $operation)
                $report['Lending'][] = new Lending01Model($operation,$em);
        $response = $this->createApiResponse($report,200);
        return $response;
    }

}