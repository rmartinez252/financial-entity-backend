<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\Entity\BankMoveType;
use AppBundle\Form\BankMoveTypeType;
use AppBundle\Form\BankType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class BankMoveTypeController extends BaseController
{
//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                          BANK MOVE TYPE
//******************************************************************//
//////////////////////////////////////////////////////////////////////
    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create new MoveType (Bank) ",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Create a new MoveType (Bank)",
     *  input="AppBundle\Form\BankMoveTypeType",
     *  output={
     *     "class" = "AppBundle\Entity\BankMoveType"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_POST')")
     * @Route("/bankMoveTypes" , name="uri_post_bank_move_type")
     * @Method("POST")
     */
    public function postActionMoveType(Request $request){
        $moveType = new BankMoveType();
        $form = $this->createForm(BankMoveTypeType::class, $moveType);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($moveType);
        $em->flush();
        $response = $this->createValidApiResponse($moveType, 201,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update  MoveType (Bank) ",
     *         404={
     *           "Returned when the  MoveType (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Update a MoveType (Bank) ",
     *  input="AppBundle\Form\BankType",
     *  output={
     *     "class" = "AppBundle\Entity\Bank"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_PATH')")
     * @Route("/bankMoveTypes/{id}" , name="uri_path_bank_move_type")
     * @Method("PATCH")
     * @param BankMoveType $moveType
     * @param Request $request
     * @return Response|void
     */
    public function patchActionMoveType(BankMoveType $moveType,Request $request){
        $form = $this->createForm(BankMoveTypeType::class, $moveType);
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($moveType);
        $em->flush();
        $response = $this->createValidApiResponse($moveType, 200,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get  MoveType (Bank) ",
     *         404={
     *           "Returned when the  MoveType (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Show All MoveType (Bank) in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\Entity\Bank",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankMoveTypes" , name="uri_get_bank_move_type")
     * @Method("GET")
     */
    public function getActionMoveType(){
        $moveTypeRepository = $this->getBankMoveTypeRepository();
        return $this->createValidApiResponse(array('moveType'=>$moveTypeRepository->findAll()),200,404);
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get MoveType (Bank) ",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Get All info from one MoveType (Bank)",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     *  output="AppBundle\Entity\BankMoveType"
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankMoveTypes/{id}" , name="uri_get_one_bank_move_type")
     * @Method("GET")
     * @param BankMoveType $moveType
     * @return Response
     */
    public function getOneActionMoveType(BankMoveType $moveType){

        return $this->createValidApiResponse($moveType,200,404,'Full');
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete  MoveType (Bank) ",
     *         404={
     *           "Returned when the MoveType (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="delete a MoveType (Bank)",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     * )
     * @Security("is_granted('ROLE_BANK_DELETE')")
     * @Route("/bankMoveTypes/{id}" , name="uri_delete_bank_move_type")
     * @Method("DELETE")
     * @param BankMoveType $moveType
     * @return Response|void
     */
    public function deleteActionMoveType(BankMoveType $moveType){
        if($moveType){
            $em = $this->getDoctrine()->getManager();
            $em->remove($moveType);
            $em->flush();
            return $this->createApiResponse(true, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }
//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                    BANK MOVE TYPE END
//******************************************************************//
//////////////////////////////////////////////////////////////////////


}