<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\Entity\Person;
use AppBundle\Entity\PersonAttribute;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCompanyEndpoint;
use AppBundle\Form\ArrayPermissionType;
use AppBundle\Form\PersonAttributeType;
use AppBundle\Form\PersonType;
use AppBundle\Form\UserType;
use AppBundle\Form\UserTypeTEST;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Security("is_granted('ROLE_USER')")
 */
class UserController extends BaseController
{
    ////////////////////////////////////
    //                                //
    //               TEST             //
    //                                //
    ////////////////////////////////////
    /**
     * ApiDoc(
     *  description="Create a new User TEST Function ",
     *  input="AppBundle\Entity\User",
     *  output="AppBundle\Entity\User"
     * )
     * @Security("is_granted('ROLE_USER_POST')")
     * @Route("/userstest" , name="uri_post_usersTEST")
     * @Method("POST")
     */
    public function postTESTAction(Request $request){
        $user = new User();
        $form = $this->createForm(UserTypeTEST::class, $user);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        //$programmer->setUser($this->getUser());
        $user->setPassword(123);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $response = $this->createApiResponse($user, 201);
        $programmerUrl = "link";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * ApiDoc(
     *  description="Get TEST Function ",
     *  output="AppBundle\Entity\User"
     * )
     * @Security("is_granted('ROLE_USER_POST')")
     * @Route("/userstest/{name}" , name="uri_get_one_usersTEST")
     * @Method("GET")
     */
    public function getOneTestAction($name){

        $userRepository = $this->getUserRepository();
        $users = $userRepository->findBy(array("username"=>$name));
        $response = $this->createApiResponse($users, 200);
        return $response;
    }

    ////////////////////////////////////
    //                                //
    //           END TEST             //
    //                                //
    ////////////////////////////////////

    /**
     * @ApiDoc(
     *  section = "02) Users",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create new user",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Create a new User  ",
     *  input="AppBundle\Form\UserType",
     *  output={
     *     "class" = "AppBundle\Entity\User",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_USER_POST')")
     * @Route("/users" , name="uri_post_users")
     * @Method("POST")
     */
    public function postAction(Request $request){
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        //$programmer->setUser($this->getUser());
        $password = $this->getSecurityEncoder()
            ->encodePassword($user, $user->getPassword());
        $user->setPassword($password);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $response = $this->createValidApiResponse($user, 201,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
        /*
            $user->setUsername($username);
            $password = $this->getService('security.password_encoder')
                ->encodePassword($user, $plainPassword);*/
    }

    /**
     * @ApiDoc(
     *  section = "02) Users",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update users",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Update a User",
     *  input="AppBundle\Form\UserType",
     *  output={
     *     "class" = "AppBundle\Entity\User",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_USER_POST')")
     * @Route("/users/{id}" , name="uri_path_users")
     * @Method("PATCH")
     * @param User $user
     * @param Request $request
     * @return Response|void
     */
    public function patchAction(User $user,Request $request){
        $content = json_decode($request->getContent(),true);
        $form = $this->createForm(UserType::class, $user);

        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        //$programmer->setUser($this->getUser());
        if(isset($content['password'])){
            $password = $this->getSecurityEncoder()
                ->encodePassword($user, $content['password']);
            $user->setPassword($password);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $response = $this->createValidApiResponse($user, 200,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "02) Users",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get users",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Show All users in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\Entity\User",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_USER_GET')")
     * @Route("/users" , name="uri_get_users")
     * @Method("GET")
     */
    public function getAction(){
        $userRepository = $this->getUserRepository();
        return $this->createValidApiResponse(array('User'=>$userRepository->findAll()),200,404,"UserPerson");
    }

    /**
     * @ApiDoc(
     *  section = "02) Users",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get users",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Get All info from one user",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id user"},},
     *  output="AppBundle\Entity\User"
     * )
     * @Security("is_granted('ROLE_USER_GET')")
     * @Route("/users/{id}" , name="uri_get_one_users")
     * @Method("GET")
     * @param User $user
     * @return Response
     */
    public function getOneAction(User $user){

        return $this->createValidApiResponse($user,200,404,array('FullUserPerson','FullPerson','Default'));
    }

    /**
     * @ApiDoc(
     *  section = "02) Users",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete users",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="delete a node and Show all node in a json array",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id user"},},
     * )
     * @Security("is_granted('ROLE_USER_DELETE')")
     * @Route("/users/{id}" , name="uri_delete_users")
     * @Method("DELETE")
     */
    public function deleteAction(User $user){
        if($user){
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            return $this->createApiResponse(true, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }

    /**
     * @ApiDoc(
     *  section = "02) Users",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to get users",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Show All permission from your actual company in a json array and all Structure",
     *  output={
     *     "code" = "201",
     *     "class" = "AppBundle\Entity\Endpoints",
     *     "groups" = "CompanyPermissions"
     * }
     * )
     * @Security("is_granted('ROLE_USER_GET')")
     * @Route("/permissions" , name="uri_get_permissions")
     * @Method("GET")
     */
    public function getPermissionsAction(){
        $sm =  $this->getStructureTreeManager();
        $permissions = $this->getCompany()->getEndpoints();
        $structure = $sm->getCompanyStructure($this->getCompany());
        return $this->createValidApiResponse(array('Structure'=>$structure,'Permissions'=>$permissions),200,404,array("CompanyPermissions"));
    }

    /**
     * @ApiDoc(
     *  section = "02) Users",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to get users",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="get an array with all permissions to set for one user",
     *  input="AppBundle\Form\ArrayPermissionType",
     *  output={
     *     "code" = "201",
     *     "class" = "AppBundle\Entity\User",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_USER_GET')")
     * @Route("/permissions/{id}" , name="uri_post_permissions")
     * @Method("POST")
     * @param User $user
     * @param Request $request
     * @return Response|void
     */
    public function postPermissionsAction(User $user,Request $request){

//        foreach(json_encode($request->getContent() )as $p){
//            var_dump($p);
//        }
        if(!$user)
            exit; //todo return api problem... user not found
        $form = $this->createForm(ArrayPermissionType::class);
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $company = $this->getCompany();
        foreach ($user->getUserCompanyEndpoints() as $userCompanyEndpoint){
            if($userCompanyEndpoint->getCompanyEndpoint()->getCompany()->getId() == $company->getId())
                $user->removePermission($userCompanyEndpoint);
        }
        $companyEndpointRepository = $this->getCompanyEndpointRepository();

        foreach($form->getData()["Permissions"] as $p){
            $permission =  new UserCompanyEndpoint();
            $permission->setCompanyEndpoint($companyEndpointRepository->findOneById($p->getId()));
            $permission->setPermission($p->getPermissions());
            $user->addPermission($permission);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return $this->createValidApiResponse($user, 201,404,'FullUSerPermissions');

    }


}