<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Entry;
use AppBundle\Form\BankMoveType;
use AppBundle\Form\BankType;
use AppBundle\Form\EntryType;
use AppBundle\Form\Model\BankMoveModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class BankMoveController extends BaseController
{
    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create new Move (Bank) ",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Create a new Move (Bank)",
     *  input="AppBundle\Form\BankMoveType",
     *  output={
     *     "class" = "AppBundle\Entity\BankMove"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_POST')")
     * @Route("/bankMoves" , name="uri_post_bank_move")
     * @Method("POST")
     * @param Request $request
     * @return Response|void
     */
    public function postActionMove(Request $request){
        $moveModel = new BankMoveModel($this->getUser());
        $em = $this->getEntityManager();
        $form = $this->createForm(BankMoveType::class, $moveModel,array('em' => $em));
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
//        $move->setAccountingEntry($this->getUser()->getId());

        $move = $moveModel->createMove();
        $em->persist($move);
        $em->flush();

        $response = $this->createValidApiResponse($move, 201,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update  Move (Bank) ",
     *         404={
     *           "Returned when the  Move (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Update a Move (Bank) ",
     *  input="AppBundle\Form\BankType",
     *  output={
     *     "class" = "AppBundle\Entity\Bank"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_PATH')")
     * @Route("/bankMoves/{id}" , name="uri_path_bank_move")
     * @Method("PATCH")
     * @param $id
     * @param Request $request
     * @return Response|void
     * @internal param BankMove $move
     */
    public function patchActionMove($id,Request $request){

        $move = $this->getBankMoveRepository()->findOneBy(array("id"=>$id));
        $moveModel = new BankMoveModel($this->getUser());
        $em = $this->getEntityManager();
        $form = $this->createForm(BankMoveType::class, $moveModel,array('em' => $em));
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        $move = $moveModel->updateMove($move);
        $em->persist($move);
        $em->flush();
        $response = $this->createValidApiResponse($move, 200,404,'Full');
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get  Move (Bank) ",
     *         404={
     *           "Returned when the  Move (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Show All Move (Bank) in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\Entity\Bank",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankMoves" , name="uri_get_bank_move")
     * @Method("GET")
     */
    public function getActionMove(){
        $moveRepository = $this->getBankMoveRepository();
        return $this->createValidApiResponse(array('bankMoves'=>$moveRepository->findAll()),200,404,"BankMove1");
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get  Move (Bank) ",
     *         404={
     *           "Returned when the  Move (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Show All Move (Bank) for one account in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\Entity\Bank",
     *     "groups" = "Full"
     * }
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankMoves/byAccount/{id}" , name="uri_get_bank_move_by_account")
     * @Method("GET")
     * @param $id
     * @return Response|void
     */
    public function getActionMoveByAccount($id){
        $moveRepository = $this->getBankMoveRepository();
        return $this->createValidApiResponse(array('bankMoves'=>$moveRepository->findAllByAccount($id)),200,404,"BankMove1");
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get Move (Bank) ",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="Get All info from one Move (Bank)",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     *  output="AppBundle\Entity\Bank"
     * )
     * @Security("is_granted('ROLE_BANK_GET')")
     * @Route("/bankMoves/{id}" , name="uri_get_one_bank_move")
     * @Method("GET")
     * @param $id
     * @return Response
     * @internal param BankMove $move
     */
    public function getOneActionMove($id){
        $moveRepository = $this->getBankMoveRepository();

        return $this->createValidApiResponse($moveRepository->find($id),200,404,array('BankMove1','BankMove2'));
    }

    /**
     * @ApiDoc(
     *  section = "06) Banks",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete  Move (Bank) ",
     *         404={
     *           "Returned when the Move (Bank)  is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="delete a Move (Bank)",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id bank"},},
     * )
     * @Security("is_granted('ROLE_BANK_DELETE')")
     * @Route("/bankMoves/{id}" , name="uri_delete_bank_move")
     * @Method("DELETE")
     * @param $id
     * @return Response|void
     * @internal param BankMove $move
     */
    public function deleteActionMove($id){
        $move = $this->getBankMoveRepository()->findOneBy(array("id"=>$id));

        if($move){
            $em = $this->getEntityManager();
            $em->remove($move);
            $em->flush();
            return $this->createApiResponse(true, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }



}