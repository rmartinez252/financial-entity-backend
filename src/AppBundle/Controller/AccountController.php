<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\EntityCompanies\Account;
use AppBundle\Form\AccountType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class AccountController
 * @package AppBundle\Controller
 */

/**
 *
 * @Security("is_granted('ROLE_USER')")
 */
class AccountController extends BaseController
{

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounts",
     *  description="Create a new Account node",
     *  requirements={},
     *  parameters={
     *      {"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id for reference account"},
     *      {"name"="name", "dataType"="string","format"="{not blank}, {length: min: 5, max: 200}", "required"=true, "description"="Name for the new account"},
     *      {"name"="description", "dataType"="string","format"="{length: max: 200}" , "required"=false, "description"="Description for the new account"},
     *      {"name"="dir", "dataType"="string","format"="{subl,subr,right,left},default = subl" , "required"=false, "description"="Directrix of direction to put the new node"},
     *      {"name"="nature", "dataType"="boolean", "required"=false, "description"="it's true if the account have a debtor nature ( increase the total amount when receive a debit) -> Assets , and false if the account have a creditor nature ( increase the total amount when receive a credit) -> Liabilities"}
     *  },
     *  output={
     *       "class"="AppBundle\EntityCompanies\Account"
     *     }
     * )
     * @Security("is_granted('ROLE_ACCOUNT_POST')")
     * @Route("/accounts" , name="uri_post_account")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function postAction(Request $request){
//TODO CREATE A MODEL AND FORM TYPE FOR THIS POST
        //get data from body
        $data = json_decode($request->getContent(),true );
        //get entity manager from actual company
        $em = $this->getEntityManager();
        //start transaction and get connection to allow rollback
        $em->beginTransaction();
        $conn = $em->getConnection();
        //get account manager, using this entity manager to allow rollback
        $sm = $this->getAccountTreeManager(false,$em);
        //get direction to be used in the addOneById function
        $dir = null;
        if(isset($data['dir']))
            $dir = $data['dir'];
        //start try to catch add account errors and rollback
        // the function addOneById change all the Left and Right values
        // and we need to put it as before if something go wrong
        try{
            //addOneById re-order left and right before insert the new account
            //return the new left and right values to be used in the new account
            if(!$accountData = $sm->addOneById($data['id'],$dir))
                return $this->createApiProblem();
            //set nature with user value or if not, find parent value and use it
            if(isset($data['nature'])){
                $accountData['nature'] = $data['nature'];
            }else{
                $parentAccount = $sm->getParentsByLOrR($accountData["l"]);
                $accountData['nature'] = $parentAccount->getNature();
            }
            //get others account values
            $accountData['name'] = $data['name'];
            $accountData['description'] = $data['description'];
            //use all data pre-set and send it to form to be validate as an Account
            $account = new Account();
            $form = $this->createForm(AccountType::class, $account);
            $form->submit($accountData);

            if (!$form->isValid()) {
                $this->throwApiProblemValidationException($form);
            }
            //persiste account
            $em->persist($account);
            //update account codes
            $sm->createCodes();

            $em->flush();
            $conn->commit();
        }catch (\Exception $e){
            //if something go wrong rollback
            $conn->rollBack();
            return $this->createApiResponse($e->getTraceAsString(), 400);
        }
        $response = $this->createApiResponse($account, 201);
//        $headerUrl = "link";
//        $response->headers->set('Location', $headerUrl );

        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounts",
     *  description="Show all accounts from one company in json array ",
     *  output={
     *       "class"="AppBundle\Form\Model\AccountStructureModel"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/accounts" , name="uri_get_all_account")
     * @Method("GET")
     * @return Response
     */
    public function getAllAction(){
        $am = $this->getAccountTreeManager();
        $accounts = $am->showAll();
        $response = $this->createApiResponse($accounts,200);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounts",
     *  description="Show all about one accounts in a json array",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id node "},},
     *     output={
     *       "class"="AppBundle\Entity\Account"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/accounts/{id}" , name="uri_get_one_account")
     * @Method("GET")
     * @param $id
     * @return Response|void
     */
    public function getOneAction($id){

        $account = $this->getAccountRepository()->findOneBy(array('id'=>$id));
        return $this->createValidApiResponse($account,200,400,'FullAccount');
    }

    /**
     * @ApiDoc(
     *  section = "05) Accounting - Accounts",
     *  description="delete a account and Show all others accounts in a json array",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id node "},},
     *     output={
     *       "class"="AppBundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/accounts/{id}" , name="uri_delete_account")
     * @Method("DELETE")
     * @param $id
     * @return Response
     */
    public function deleteAction($id){
        $am = $this->getAccountTreeManager();
        $accounts = $am->deleteById($id);
        $response = $this->createApiResponse($accounts, 200);
        return $response;
    }


}