<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Structure;
use AppBundle\Entity\User;
use AppBundle\Form\StructureType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class StructureController
 * @package AppBundle\Controller
 */

/**
 *
 * @Security("is_granted('ROLE_USER')")
 */
class StructureController extends BaseController
{
    /**
     * ApiDoc(
     *  description="function to be used just in test. DON'T USE IN PROD MODE. it's gonna be deleted",
     *  input="AppBundle\Entity\Structure",
     *  output="AppBundle\Entity\Structure"
     * )
     * @Security("is_granted('ROLE_STRUCTURE_POST')")
     * @Route("/structurestest" , name="uri_poststructuresTEST")
     * @Method("POST")
     */
    public function postTESTTAction(Request $request){

        $structure = new Structure();
        $form = $this->createForm(StructureType::class, $structure);

        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($structure);
        $em->flush();
        $response = $this->createApiResponse($structure,201,'Basic');
        $programmerUrl = "link";
//        $programmerUrl = $this->generateUrl(
//            'uri_poststructuresTEST',
//            ['nickname' => $programmer->getNickname()]
//        );
        $response->headers->set('Location', $programmerUrl);

        return $response;
    }

    /**
     * ApiDoc(
     *  description="function to be used just in test. DON'T USE IN PROD MODE. it's gonna be deleted",
     *  input="AppBundle\Entity\Structure",
     *  output="AppBundle\Entity\Structure"
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/structurestest/{name}" , name="uri_get_structuresTEST")
     * @Method("GET")
     */
    public function getTESTAction($name){

        $sm = $this->getStructureTreeManager();
        switch ($name){
            case 'showMain':
                $structure = $sm->showMain();
                break;
            default:
                $structure = $sm->showOneByName($name);
                break;
        }
        $response = $this->createApiResponse($structure, 200,'Basic');
        $programmerUrl = "link";
//        $programmerUrl = $this->generateUrl(
//            'api_programmers_show',
//            ['nickname' => $programmer->getNickname()]
//        );
        $response->headers->set('Location', $programmerUrl);

        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "_no) Structure",
     *  description="Create a new structure node",
     *  requirements={},
     *  parameters={
     *      {"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id for reference node"},
     *      {"name"="name", "dataType"="string","format"="{not blank}, {length: min: 5, max: 200}", "required"=true, "description"="Name for the new node"},
     *      {"name"="description", "dataType"="string","format"="{length: max: 200}" , "required"=false, "description"="Description for the new node"},
     *      {"name"="dir", "dataType"="string","format"="{subl,subr,right,left},default = subl" , "required"=false, "description"="Directrix of direction to put the new node"}
     *  },
     *  output={
     *       "class"="AppBundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_POST')")
     * @Route("/structures" , name="uri_post_structures")
     * @Method("POST")
     */
    public function postAction(Request $request){

        $data = json_decode($request->getContent(),true );
//        var_dump($data["id"]);
//        exit;
        $sm = $this->getStructureTreeManager();
        $dir = null;
        if(isset($data['dir']))
            $dir = $data['dir'];
        $structureData = $sm->addOneById($data['id'],$dir);
        $structureData['name'] = $data['name'];
        $structure = new Structure();
        $form = $this->createForm(StructureType::class, $structure);
        $form->submit($structureData);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        //$programmer->setUser($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $em->persist($structure);
        $em->flush();

        $response = $this->createApiResponse($structure, 201,'Basic');
        $programmerUrl = "link";
//        $programmerUrl = $this->generateUrl(
//            'api_programmers_show',
//            ['nickname' => $programmer->getNickname()]
//        );
        $response->headers->set('Location', $programmerUrl);

        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "_no) Structure",
     *  description="Show all structure nodes in json array ",
     *  output={
     *       "class"="AppB$rowLundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/structures" , name="uri_get_all_structures")
     * @Method("GET")
     */
    public function getAllAction(){

        $sm = $this->getStructureTreeManager();
        $structure = $sm->showAll();

        $response = $this->createApiResponse($structure, 200,'Full');
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "_no) Structure",
     *  description="Show All info about one node",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id node"},},
     *     output={
     *       "class"="AppBundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/structures/{id}" , name="uri_get_structures",defaults={"id" = ""})
     * @Method("GET")
     */
    public function getAction($id){

        $sm = $this->getStructureTreeManager();
        switch ($id){
            case 'showMain':
                $structure = $sm->showMain();
                break;
            case '':
                $structure = null;
                break;
            default:
                $structure = $sm->getNode($id);
                break;
        }
        $response = $this->createApiResponse($structure, 200,'Full');
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "_no) Structure",
     *  description="Show a node with his sons in a json array",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id for reference node"},},
     *     output={
     *       "class"="AppBundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/structures/{id}/sons" , name="uri_get_sons_structures")
     * @Method("GET")
     */
    public function getSonsAction($id){
        $sm = $this->getStructureTreeManager();
        $structure = $sm->showSonsById($id);
        $response = $this->createApiResponse($structure, 200,'Basic');
        $nodeLink  = $this->generateUrl(
            'uri_get_sons_structures',
            ['id' => $id]
        );
        $response->headers->set('Location', $nodeLink);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "_no) Structure",
     *  description="Show a node with his parents in a json array",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id for reference node"},},
     *     output={
     *       "class"="AppBundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/structures/{id}/parents" , name="uri_get_parents_structures")
     * @Method("GET")
     */
    public function getParentsAction($id){
        $sm = $this->getStructureTreeManager();
        if($structure = $sm->showParentsById($id)){
            $response = $this->createApiResponse($structure, 200,'Basic');
            $nodeLink  = $this->generateUrl(
                'uri_get_parents_structures',
                ['id' => $id]
            );
            $response->headers->set('Location', $nodeLink);
        }else{
            $response = $this->createApiResponse($structure, 200,'Basic');

        }
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "_no) Structure",
     *  description="Show a node with his family(Sons and Parents) in a json array",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id for reference node"},},
     *     output={
     *       "class"="AppBundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/structures/{id}/family" , name="uri_get_family_structures")
     * @Method("GET")
     */
    public function getFamilyAction($id){
        $sm = $this->getStructureTreeManager();
        $structure = $sm->showFamilyById($id);
        $response = $this->createApiResponse($structure, 200,'Basic');
        $nodeLink  = $this->generateUrl(
            'uri_get_family_structures',
            ['id' => $id]
        );
        $response->headers->set('Location', $nodeLink);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "_no) Structure",
     *  description="Show a User structure for specific Company ",
     *  requirements={
     *     {"name"="idUser", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id user"},
     *     {"name"="idCompany", "dataType"="integer","format"="{not blank}", "required"=false, "description"="id company"},
     * },
     *     output={
     *       "class"="AppBundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/structures/{idUser}/{idCompany}" , name="uri_get_user_structures",defaults={"idCompany"=""})
     * @Method("GET")
     */
    public function getUserStructureAction(User $idUser,$idCompany){
        $sm = $this->getStructureTreeManager();
        if(!$idCompany)
            $structure = $sm->showFamilyById(1);
        $response = $this->createApiResponse($structure, 200,'Basic');
        $nodeLink  = $this->generateUrl(
            'uri_get_family_structures',
            ['id' => $idUser->getId()]
        );
        $response->headers->set('Location', $nodeLink);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "_no) Structure",
     *  description="delete a node and Show all node in a json array",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id node "},},
     *     output={
     *       "class"="AppBundle\Entity\Structure"
     *     }
     * )
     * @Security("is_granted('ROLE_STRUCTURE_GET')")
     * @Route("/structures/{id}" , name="uri_delete_structures")
     * @Method("DELETE")
     */
    public function deleteAction($id){
        $sm = $this->getStructureTreeManager();
        $structure = $sm->deleteById($id);
        $response = $this->createApiResponse($structure, 200,'Basic');
//        $nodeLink  = $this->generateUrl(
//            'uri_get_family_structures',
//            ['id' => $id]
//        );
//        $response->headers->set('Location', $nodeLink);
        return $response;
    }



}