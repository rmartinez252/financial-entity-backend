<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\Client;
use AppBundle\Form\ClientType;
use Symfony\Component\HttpFoundation\Request;
/**
 * use for annotations
 */
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class ClientController
 * @package AppBundle\Controller
 */
class ClientController extends BaseController
{
    /**
     * @ApiDoc(
     *  section = "04) Clients",
     *  statusCodes={
     *         201="Returned when successful",
     *         401="Returned when the user is not authorized to create clients",
     *         404="Returned when something else is not found"
     *     },
     *  description="Create a new Client ",
     *  input="AppBundle\Form\ClientType",
     *  output="AppBundle\Form\ClientType"
     * )
     * @Security("is_granted('ROLE_CLIENT_POST')")
     * @Route("/clients" , name="uri_post_clients")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function postAction(Request $request){
        $client = new Client();
        $em = $this->getEntityManager();
        $form = $this->createForm(ClientType::class, $client, array('em' => $em));

        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $em->persist($client);
        $em->flush();
        $response = $this->createValidApiResponse($client, 201,400);
        /*TODO*/
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;

    }

    /**
     * @ApiDoc(
     *  section = "04) Clients",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to update clients",
     *         404="Returned when something else is not found"
     *     },
     *  description="Update a Client",
     *  input="AppBundle\Form\ClientType",
     *  output="AppBundle\Form\ClientType"
     * )
     * @Security("is_granted('ROLE_CLIENT_POST')")
     * @Route("/clients/{id}" , name="uri_path_clients")
     * @Method("PATCH")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @internal param Client $client
     */
    public function patchAction($id,Request $request){
        $client = $this->getClientRepository()->findOneBy(array("id"=>$id));
        $em = $this->getEntityManager();
        $form = $this->createForm(ClientType::class, $client, array('em' => $em));
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        $em->persist($client);
        $em->flush();
        $response = $this->createValidApiResponse($client, 200,400);
        $programmerUrl = "link-TODO";
        $response->headers->set('Location', $programmerUrl);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section = "04) Clients",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get clients",
     *         404="Returned when something else is not found"
     *     },
     *  description="Show All Clients in a json array",
     *  output="AppBundle\Form\ClientType"
     * )
     * @Security("is_granted('ROLE_CLIENT_GET')")
     * @Route("/clients" , name="uri_get_clients")
     * @Method("GET")
     */
    public function getAction(){
        $clientRepository = $this->getClientRepository();
        return $this->createValidApiResponse(array("clients"=>$clientRepository->findAll()),200,400,"ClientPersonNames");
    }

    /**
     * @ApiDoc(
     *  section = "04) Clients",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get clients",
     *         404="Returned when something else is not found"
     *     },
     *  description="Get All info from one Client",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id client"},},
     * output={
     *      "class"="AppBundle\EntityCompanies\Client",
     *      "groups"={"FullClient","Default"}
     *   }
     * )
     * @Security("is_granted('ROLE_CLIENT_GET')")
     * @Route("/clients/{id}" , name="uri_get_one_clients")
     * @Method("GET")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @internal param Client $client
     */
    public function getOneAction($id){
        $clientRepository = $this->getClientRepository();
        $client = $clientRepository->findOneBy(array("id"=>$id));
        return $this->createValidApiResponse($client,200,400,array('ClientPerson','FullClient'));
    }

    /**
     * @ApiDoc(
     *  section = "04) Clients",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to delete clients",
     *         404="Returned when something else is not found"
     *     },
     *  description="delete a client and return true if its deleted",
     *  requirements={{"name"="id", "dataType"="integer","format"="{not blank}", "required"=true, "description"="id client"},},
     * )
     * @Security("is_granted('ROLE_CLIENT_DELETE')")
     * @Route("/clients/{id}" , name="uri_delete_clients")
     * @Method("DELETE")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @internal param Client $client
     */
    public function deleteAction($id){
        $clientRepository = $this->getClientRepository();
        $client = $clientRepository->findOneBy(array("id"=>$id));
        /**
         * @var $client Client
         */
        if($client){
            $em = $this->getEntityManager();
            if($client->getOperationInvoices()->count() || $client->getOperations()->count())
                return $this->createApiProblem(400,ApiProblem::TYPE_CLIENT_WITH_OPERATION);

            $em->remove($client);
            $em->flush();
            return $this->createApiResponse(true, 200);
        }
        return $this->createApiProblem(400,ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);
    }


}