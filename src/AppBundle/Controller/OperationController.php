<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/4/16
 * Time: 6:53 PM
 */

namespace AppBundle\Controller;


use AppBundle\Api\ApiProblem;
use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\OperationAttribute;
use AppBundle\EntityCompanies\Payment;
use AppBundle\EntityCompanies\Product;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use AppBundle\EntityCompanies\ProductAttributeProduct;
use AppBundle\Form\BankMoveOperationPaymentType;
use AppBundle\Form\Model\AblDateModel;
use AppBundle\Form\Model\OperationPaymentAblModel;
use AppBundle\Form\Model\OperationPaymentModel;
use AppBundle\Form\OperationPaymentAblType;
use AppBundle\Form\OperationPaymentType;
use AppBundle\Form\OperationType;
use AppBundle\Operations\OperationProcess;
use AppBundle\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Operations\Factoring01Process;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class OperationController extends BaseController
{

//////////////////////////////////////////////////////////////////////
//******************************************************************//
//                              Operations                          //
//******************************************************************//
//////////////////////////////////////////////////////////////////////
    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to create new operations",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all operations in your actual company, in a json array",
     *  input="AppBundle\Form\OperationType",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Operation",
     *     "groups" = "FullAttributeProduct"
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_POST')")
     * @Route("/operations" , name="uri_post_operation")
     * @Method("POST")
     * @param Request $request
     * @internal $operation Operation
     * @return Response|void
     */
    public function postOperationAction(Request $request){
        $operation = new Operation();
        $em = $this->getEntityManager();
        $form = $this->createForm(OperationType::class, $operation,array('em' => $em));
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $operationProcess = New OperationProcess($operation,$em,$this->getUser());
        $operationProcess->createOperation();
        return $this->createValidApiResponse(array("operation"=>$operationProcess->getOperation()),200,404,"AllOperation");
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get operations",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all operations in your actual company, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Operation",
     *     "groups" = "FullAttributeProduct"
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_GET')")
     * @Route("/operations" , name="uri_get_operations")
     * @Method("GET")
     * @return Response|void
     */
    public function getOperationsAction(){
        $operations = $this->getOperationRepository()->findAll();
        return $this->createValidApiResponse(array("operations"=>$operations),200,404,"AllOperation");
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get operations",
     *         404="Returned when something is not found"
     *     },
     *  description="return the 3 dates of payments ",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\Form\Model\AblDateModel"
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_GET')")
     * @Route("/operations/abl/paymentDays/{date}" , name="uri_get_operations_abl_dates")
     * @Method("GET")
     * @param $date
     * @return Response|void
     */
    public function getAblPaymentDaysAction($date){
        $paymentDays = new AblDateModel($date);
        return $this->createValidApiResponse($paymentDays,200,404);
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get operations",
     *         404="Returned when something is not found"
     *     },
     *  description="return the paydays of one existent operation ",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\OperationsAttribute"
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_GET')")
     * @Route("/operations/{id}/paymentDays" , name="uri_get_operations_payments_dates")
     * @Method("GET")
     * @param $id
     * @return Response|void
     */
    public function getOperationPaymentDaysAction($id){

        $paymentDays = $this->getOperationAttributeRepository()->findPayments($id);
        return $this->createValidApiResponse($paymentDays,200,404,"FullOperationAttribute");
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to create new payments",
     *         404={
     *           "Returned when the Operation is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="make new payment for one operation ",
     *  input="AppBundle\Form\OperationPaymentType",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Payment",
     *     "groups" = "FullAttributeProduct"
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_POST')")
     * @Route("/operations/lending/{id}/payments" , name="uri_post_operation_lending_payment")
     * @Method("POST")
     * @param Request $request
     * @param $id
     * @internal $operation Operation
     * @return Response|void
     */
    public function postOperationLendingPaymentAction(Request $request,$id){
        if(!$operation = $this->getOperationRepository()->findOneByPortfolio($id,"l"))
            return $this->createApiProblem(400,ApiProblem::TYPE_NO_OPERATION_ID_PORTFOLIO);
        $em = $this->getEntityManager();
        $paymentModel = new OperationPaymentModel();
        $form = $this->createForm(OperationPaymentType::class, $paymentModel,array('em' => $em,'idOperation'=>$id));
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }
        $operationProcess = New OperationProcess($operation,$em,$this->getUser());
        $payments = $operationProcess->addPayment($paymentModel->getBankMove(),$paymentModel->getValue(),$paymentModel->getDebtor(),$paymentModel->getOptions());
        return $this->createValidApiResponse(array("operation"=>$operationProcess->getOperation(),"payments" => $payments),200,404,array("FullOperation","AllOperation","FullPayment"));
    }
    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to create new payments",
     *         404={
     *           "Returned when the Operation is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="make new payment for one operation ",
     *  input="AppBundle\Form\OperationPaymentAblType",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Payment",
     *     "groups" = "FullAttributeProduct"
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_POST')")
     * @Route("/operations/funding/{id}/payments" , name="uri_post_operation_funding_payment")
     * @Method("POST")
     * @param Request $request
     * @param $id
     * @internal $operation Operation
     * @return Response|void
     */
    public function postOperationFundingPaymentAction(Request $request,$id){
        if(!$operation = $this->getOperationRepository()->findOneByPortfolio($id,"funding"))
            return $this->createApiProblem(400,ApiProblem::TYPE_NO_OPERATION_ID_PORTFOLIO);
        $em = $this->getEntityManager();
        $operationPaymentModel = new OperationPaymentAblModel();
        $form = $this->createForm(OperationPaymentAblType::class, $operationPaymentModel,array('em' => $em));

        $this->processForm($request, $form);

        if (!$form->isValid()) {
//            foreach ($form->getErrors() as $err)
//            var_dump($err->getMessage());
//            exit;
            $this->throwApiProblemValidationException($form);
        }
        $operationProcess = New OperationProcess($operation,$em,$this->getUser());
        $operationProcess->addPayment($operationPaymentModel->getBankMove(),$operationPaymentModel->getValue());
        return $this->createValidApiResponse(array("operation"=>$operationProcess->getOperation()),200,404,array("FullOperation","AllOperation","FullPayment"));
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get payments",
     *         404={
     *           "Returned when the Operation is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="get all payments from one operation ",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Payment",
     *     "groups" = "FullAttributeProduct"
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_GET')")
     * @Route("/operations/{id}/payments" , name="uri_get_operation_payment")
     * @Method("GET")
     * @param $id
     * @return Response|void
     */
    public function getOperationPaymentAction($id){
        if(!$payments = $this->getOperationPaymentRepository()->findOperationPayment($id))
            return $this->createApiProblem();
        return $this->createValidApiResponse(array("payments"=>$payments),200,404,"AllOperation");
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get operations",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all payments needed to be done before this operation can be close, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Operation",
     *     "groups" = {"AllOperation","FullOperation"}
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_GET')")
     * @Route("/operations/{idOperation}/beforeClose" , name="uri_get_operations_before_close")
     * @Method("GET")
     * @param $idOperation

     * @return Response|void
     */
    public function getOperationsBeforeCloseAction($idOperation){
        if(!$operations = $this->getOperationRepository()->findOneBy(array("id"=>$idOperation)))
            return $this->createApiProblem(404);
        $operationProcess =  new OperationProcess($operations);
        $beforeClose = $operationProcess->getBeforeClose();
        return $this->createValidApiResponse(array("beforeClose"=>$beforeClose),200,404,array("FullOperation","AllOperation"));
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get operations",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all operations status, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\OperationsStatus",
     *     "groups" = "FullAttributeProduct"
     * }
     * )
     * @Security("is_granted('ROLE_PRODUCT_GET')")
     * @Route("/operations/status" , name="uri_get_operations_status")
     * @Method("GET")
     * @return Response|void
     */
    public function getOperationsStatusAction(){
        $operationsStatus = $this->getOperationStatusRepository()->findAll();
        return $this->createValidApiResponse(array("status"=>$operationsStatus),200,404);
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get operations",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all operations in your actual company, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Operation",
     *     "groups" = {"AllOperation","FullOperation"}
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_GET')")
     * @Route("/operations/{id}" , name="uri_get_one_operations")
     * @Method("GET")
     * @param $id

     * @return Response|void
     */
    public function getOneOperationsAction($id){
        $operations = $this->getOperationRepository()->findOneBy(array("id"=>$id));
        return $this->createValidApiResponse(array("operations"=>$operations),200,404,array("FullOperation","AllOperation"));
    }

    /**
     * @ApiDoc(
     *  section = "07) Products - Operations",
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to get operations",
     *         404={
     *           "Returned when the bank is not found",
     *           "Returned when something else is not found"
     *         }
     *     },
     *  description="show all operations in your actual company, in a json array",
     *  output={
     *     "code" = "200",
     *     "class" = "AppBundle\EntityCompanies\Operation",
     *     "groups" = {"AllOperation","FullOperation"}
     * }
     * )
     * @Security("is_granted('ROLE_OPERATION_POST')")
     * @Route("/operations/{id}/status/{idNewStatus}" , name="uri_patch_one_operations")
     * @Method("PATCH")
     * @param $id
     * @param $idNewStatus
     * @return Response|void
     */
    public function getChangeStatusOperationsAction($id,$idNewStatus){
        if(!$operation = $this->getOperationRepository()->findOneBy(array("id"=>$id)))
            return $this->createApiProblem(404);
        if(!$operationsStatus = $this->getOperationStatusRepository()->findOneBy(array("id"=>$idNewStatus)))
            return $this->createApiProblem(404);
        $em = $this->getEntityManager();
        $operationProcess = new OperationProcess($operation,$em);
        if(!$op = $operationProcess->changeStatus($operationsStatus))
            return $this->createApiProblem(412);

        return $this->createValidApiResponse(array("operations"=>$op),200,404,array("FullOperation","AllOperation"));
    }
}