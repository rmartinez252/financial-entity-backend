<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 11/8/16
 * Time: 12:57 PM
 */

namespace AppBundle\ResponseModel;


use AppBundle\Entity\Person;
use AppBundle\Entity\Structure;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;

class loginResponseModel
{

    /**
     * login User information
     * @JMS\Type("AppBundle\Entity\User")
     * @JMS\Groups("loginResponse")
     * @var User $user
     */
    private $user;
    /**
     * Structure for default company and module
     * @JMS\Type("AppBundle\Entity\Structure")
     * @JMS\Groups("loginResponse")
     * @var array $structure
     */
    private $structure;




}