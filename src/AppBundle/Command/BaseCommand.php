<?php
namespace AppBundle\Command;
use AppBundle\Entity\User;
use AppBundle\Repository\AccountingEntryTypeRepository;
use AppBundle\Repository\CompanyRepository;
use AppBundle\Repository\OperationRepository;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
//use Symfony\Component\Console\Command\Command;

/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 3/16/17
 * Time: 12:44 PM
 */
abstract class  BaseCommand extends ContainerAwareCommand
{
    /**
     * @var string
     */
    private $connection;
    /**
     * @var User
     */
    private $user;

    /**
     * BaseCommand constructor.
     * @param null $name
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(){
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }
    /**
     * @param string $company
     * @return EntityManager
     */
    protected function getEntityManagerCompany($company = null){
        if(!$company)
            $company = $this->connection;
        return $this->getContainer()->get('doctrine.orm.'.$company.'_entity_manager');
    }

    /**
     * @return UserRepository
     */
    protected function getUserRepository(){
        return $this->getEntityManager()->getRepository('AppBundle:User');
    }
    /**
     * @return CompanyRepository
     */
    protected function getCompanyRepository(){
        return $this->getEntityManager()->getRepository('AppBundle:Company');
    }

    /**
     * @param $company
     * @return OperationRepository
     */
    protected function getOperationRepository($company = null){
        if(!$company)
            $company = $this->connection;
        return $this->getEntityManagerCompany($company)->getRepository('AppBundle\\EntityCompanies\\Operation');
    }
    /**
     * @param $company
     * @return AccountingEntryTypeRepository
     */
    protected function getAccountingEntryTypeRepository($company = null){
        if(!$company)
            $company = $this->connection;
        return $this->getEntityManagerCompany($company)->getRepository('AppBundle\\EntityCompanies\\AccountingEntryType');
    }
    /**
     * @param $company
     * @return ProductRepository
     */
    protected function getProductRepository($company = null){
        if(!$company)
            $company = $this->connection;
        return $this->getEntityManagerCompany($company)->getRepository('AppBundle\\EntityCompanies\\Product');
    }

    /**
     * @return string
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param mixed string
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if(isset($this->user))
            return $this->user;
        return $this->user = $this->getUserRepository()->findSystemUser();
    }

}