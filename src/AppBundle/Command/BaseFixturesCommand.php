<?php
namespace AppBundle\Command;
use AppBundle\Entity\Company;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\User;
use AppBundle\Repository\AccountingEntryRepository;
use AppBundle\Repository\AccountingEntryTypeRepository;
use AppBundle\Repository\AccountRepository;
use AppBundle\Repository\ClientRepository;
use AppBundle\Repository\CompanyRepository;
use AppBundle\Repository\OperationRepository;
use AppBundle\Repository\OperationStatusRepository;
use AppBundle\Repository\ProcessRepository;
use AppBundle\Repository\ProductAccountingConfigurationRepository;
use AppBundle\Repository\ProductAttributeProductRepository;
use AppBundle\Repository\ProductAttributeRepository;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
//use Symfony\Component\Console\Command\Command;

/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 3/16/17
 * Time: 12:44 PM
 */
abstract class  BaseFixturesCommand extends ContainerAwareCommand
{
    /**
     * @var string
     */
    private $connection;
    /**
     * @var User
     */
    private $user;

    /**
     * BaseCommand constructor.
     * @param null $name
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(){
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }
    /**
     * @param string $company
     * @return EntityManager
     */
    protected function getEntityManagerCompany($company = null){
        if(!$company)
            $company = $this->connection;
        return $this->getContainer()->get('doctrine.orm.'.$company.'_entity_manager');
    }
    protected function getAccountManager(){

        return $this->getContainer()->get('appbundle.tree.account.'.$this->getConnection());
    }

    /**
     * @return UserRepository
     */
    protected function getEndpointRepository(){
        return $this->getEntityManager()->getRepository('AppBundle:Endpoint');
    }
    /**
     * @return CompanyRepository
     */
    protected function getCompanyRepository(){
        return $this->getEntityManager()->getRepository('AppBundle:Company');
    }
    /**
     * @return UserRepository
     */
    protected function getUserRepository(){
        return $this->getEntityManager()->getRepository('AppBundle:User');
    }

    /**
     * @return AccountRepository
     */
    protected function getAccountRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\Account");
    }


    /**
     * @return AccountingEntryRepository
     */
    protected function getAccountingEntryRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\AccountingEntry");
    }

    /**
     * @return OperationRepository
     */
    protected function getOperationRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\Operation");
    }
    /**
     * @return ProductRepository
     */
    protected function getProductRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\Product");
    }
    /**
     * @return ProductAttributeProductRepository
     */
    protected function getProductAttributeProductRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\ProductAttributeProduct");
    }
    /**
     * @return OperationStatusRepository
     */
    protected function getStatusRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\OperationStatus");
    }
    /**
     * @return ProcessRepository
     */
    protected function getProcessRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\Process");
    }
    /**
     * @return ClientRepository
     *
     */
    protected function getClientRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\Client");
    }
    /**
     * @return AccountingEntryTypeRepository
     *
     */
    protected function getEntryTypeRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\AccountingEntryType");
    }
    /**
     * @return ProductAttributeRepository
     */
    protected function getProductAttributeRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\ProductAttribute");
    }
    /**
     * @return ProductAccountingConfigurationRepository
     */
    protected function getProductAccountingConfigurationRepository(){
        return $this->getEntityManagerCompany()->getRepository("AppBundle\\EntityCompanies\\ProductAccountingConfiguration");
    }




    /**
     * @return string
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param mixed string
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if(isset($this->user))
            return $this->user;
        return $this->user = $this->getUserRepository()->findSystemUser();
    }
    /**
     * @return array
     */
    protected function getAttributes(){
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Start/startFiles/attributes.csv', 'r');
        $attributes = array();
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if ($line[0] == "attributeName")
                continue;
            $attributes[$c]['name'] = $line[0];
            $attributes[$c]['type'] = $line[1];
        }
        return $attributes;
    }
    protected function createCompanyAttributes(Company $company,$attributeInfo)
    {
        $attribute = new \AppBundle\EntityCompanies\Attribute();
        $attribute->setName($attributeInfo["name"]);
        $attribute->setType($attributeInfo["type"]);
        $em = $this->getEntityManagerCompany($company->getConnection());
        $em->persist($attribute);
        $em->flush();
        var_dump($company->getName()."/".$attributeInfo["name"]);
        return $attribute;
    }
    protected function getCompanies(){
        return $this->getCompanyRepository()->findAll();
    }

    /**
     * @param $id
     * @return null|Company
     */
    protected function getCompanyById($id){
        return $this->getCompanyRepository()->find($id);
    }


}