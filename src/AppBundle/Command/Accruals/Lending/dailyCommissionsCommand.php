<?php

namespace AppBundle\Command\Accruals\Lending;

use AppBundle\Command\BaseCommand;
use AppBundle\Entity\Company;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\Product;
use AppBundle\Operations\OperationProcess;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class dailyCommissionsCommand extends BaseCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("dailyCommissions")

            // the short description shown while running "php bin/console list"
            ->setDescription('Review and create all the  daily accruals of commissions .')

            // the full command description  shown when running the command with
            // the "--help" option
        ;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $now = new \DateTime("now");
        $output->writeln([
            "",
            "",
            "",
            "",
            "===========================================================================",
            "                      ".$now->format("y-m-d H:i:s"),
            "===========================================================================",
        ]);
        /**
         * @var Company $company
         * @var Operation $operation
         * @var Product $product
         */
        //find all companies to be accrualiceted
        $companies = $this->getCompanyRepository()->findAll();
        foreach($companies as $company){
            $this->setConnection($company->getConnection());
            //log
            $output->writeln([
                "",
                "",
                "================= Start ".$company->getName()." =================",
                "",
                "",
            ]);
            //get Entity manager from this company
            $emc = $this->getEntityManagerCompany();
            //get the operation repository and find all operations for this company
            $products = $this->getProductRepository()->findByPortFolio("l");
            $opNoAcc = array();
            foreach ($products as $product){
                $operations = $this->getOperationRepository()->findByProduct($product->getId());
                foreach ($operations as $operation){
                    if(!$operation->getStatus()->getType()){
                        $opNoAcc[] = "   Id:(".$operation->getId().")/Type: ".$operation->getProduct()->getName()." /EffectiveDate: ".$operation->getEffectiveDate()->format(" y-m-d ");
                        continue;
                    }
                    $operationProcess = New OperationProcess($operation,$emc,$this->getUser());
                    //tag
                    $output->writeln([
                        "Operation Info".
                        "   Id:(".$operation->getId().")/Type: ".$operation->getProduct()->getName()." /EffectiveDate: ".$operation->getEffectiveDate()->format(" y-m-d ")
                    ]);
                    if($newEntry = $operationProcess->createAccrual()){
                        $output->writeln('   New Commission Accrual');
                        $output->writeln('      Accrual id : '.$newEntry->getId().'/ EntryDate: '.$newEntry->getDateSystem()->format("y-m-d"));
                        foreach ($newEntry->getEntrys() as $et)
                            $output->writeln('      Value: '.$et->getValue());

                    }else{
                        $output->writeln([
                            "   This Operation don't have new Accruals",
                        ]);
                    }

                    $output->writeln([
                        "------------------------------",
                    ]);
                }
            }
            if(count($opNoAcc)){
                $output->writeln([
                    "No Accrual op",
                    "   Operations Info",
                ]);
                $output->writeln($opNoAcc);
            }
            //log
            $output->writeln([
                "",
                "",
                "================= End ".$company->getName()." =================",
                "",
                "",
            ]);
        }
        $output->writeln([
            "===========================================================================",
            "END                    ". $now->format("y-m-d H:i:s"),
            "===========================================================================",
        ]);
    }


}