<?php

namespace AppBundle\Command\System\Companies;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\AccountingEntryType;
use AppBundle\EntityCompanies\Address;
use AppBundle\EntityCompanies\Client;
use AppBundle\EntityCompanies\Entry;
use AppBundle\EntityCompanies\InvoicePayment;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\OperationAttribute;
use AppBundle\EntityCompanies\OperationInvoice;
use AppBundle\EntityCompanies\Payment;
use AppBundle\EntityCompanies\Person;
use AppBundle\EntityCompanies\Product;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use AppBundle\Operations\OperationProcess;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class PopulateOperationsHistoricLendingCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("PopulateOperationsHistoricLending")
            // the short description shown while running "php bin/console list"
            ->setDescription('Create All data from one company using files')
            ->addArgument("companyId",InputArgument::REQUIRED,"Id of one company already create in the main database")
            ->addArgument("typeFile",InputArgument::OPTIONAL,"type of the files to process : findTrade (default),firstFactory","findTrade")
            ->addArgument("deleteBefore",InputArgument::OPTIONAL,"",false)


        ;
    }


    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "POPULATE COMPANY OPERATIONS";
        $output->writeln([
            "===========================================================================",
            "                      ".$name,
            "===========================================================================",
        ]);
        $companyId = $input->getArgument("companyId");
        $type = $input->getArgument("typeFile");
        $deleteBefore = $input->getArgument("deleteBefore");
        if(!$company = $this->getCompanyById($companyId)){
            $output->writeln([
                "<error>===========================================================================",
                "This is not a valid Company Id : ".$companyId,
                "===========================================================================</error>",
            ]);
            return false;
        }
        $output->writeln("<error>You Are about to Charge lots of operations in ". $company->getName());
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion("Are your sure you want to continue?(Y/N)</error>", false);

        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                "===========================================================================",
                "END WITHOUT DO NOTHING ",
                "===========================================================================",
            ]);
            return false;

        }
        $this->setConnection($company->getConnection());
        if($deleteBefore){
            $em = $this->getEntityManagerCompany();
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\AccountingEntry WHERE idOperation <> null OR idOperationInvoice <> null')->execute();
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\OperationInvoice')->execute();
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\InvoicePayment')->execute();
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\OperationAttribute')->execute();
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\Operation')->execute();
        }
        $this->createOperations($company,$this->getUser(),$type);

        $output->writeln([
            "===========================================================================",
            "END                    ".$name,
            "===========================================================================",
        ]);
    }

    /**
     * @param Company $company
     * @param User $user
     */
    private function createOperations(Company $company,User $user,$type){
        //TODO search in company entity to define what company manager and tree to use
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        $operationRepository = $this->getOperationRepository();
        $productRepository = $this->getProductRepository();
        $statusRepository = $this->getStatusRepository();
        $clientRepository = $this->getClientRepository();
        $entryTypeRepository = $this->getEntryTypeRepository();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/'.$this->getConnection().'/operationsHistoricLending.csv', 'r');
        $description = $accountCode = $account = '';
        $operations = array();
        $dateSystem = \DateTime::createFromFormat('d/m/y', "07/01/16");
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {

            $invoice = array();
            switch ($type){
                case 'firstFactory':

                    if(
                        $line[0] == 0 ||
                        $line[0] == "Contrato " ||
                        !$line[0] ||
                        !$line[1] ||
                        !$line[5] ||
                        !$line[7] ||
                        !$line[10]
                    ){

                        var_dump("---------------");
                        var_dump($line[0]);
                        var_dump($line[1]);
                        var_dump($line[5]);
                        var_dump($line[7]);
                        var_dump($line[10]);
                        var_dump($line[12]);
                        var_dump($line[14]);
                        var_dump($line[16]);
                        var_dump("---------------");
                        var_dump("---------------");

                        continue;
                    }

                    $invoice['debtorName'] = $line[7];
                    $invoice['reference'] = $line[2];
                    $invoice['invoiceAmount'] = floatval($line[12]);
                    $invoice['paymentAmount'] = floatval($line[14]);
                    $invoice['lateFeeAmount'] = floatval($line[16]);
//
//                    $invoice['invoiceAmount'] = $line[12];
//                    $invoice['paymentAmount'] = $line[14];
//                    $invoice['lateFeeAmount'] = $line[16];
                    $op = $line[0];
                    $operations[$op]['description'] = "Numero de contrato :".$op;
                    $operations[$op]['clientName'] = $line[5];
                    $operations[$op]['dateStart'] = $line[1];
                    $operations[$op]['dateEnd'] = $line[10];
                    $operations[$op]['invoices'][] = $invoice;

                    break;
                default :
                    break;
            }
        }
        fclose($file);
        $product = $productRepository->findOneById(4);

        /**
         * @var $initialFactoring Account
         * @var $commissions Account
         * @var $commissionsDeferred Account
         * @var $transit Account
         * @var $reserve Account
         * @var $bankAccount Account
         * @var $factoringLateFee Account
         * @var $factoringLateFee Account
         * @var $factoringLateFeeReceivable Account
         *
         * @var $discounts Account
         * @var $insurance Account
         * @var $clientAnalysis Account
         * @var $bankFee Account
         * @var $product Product

         * @var $ac ProductAccountingConfigurationProduct
         */
        /*
         *              Declare all Accounts from DataBase
         *
         * the names of variables is in product_accounting_configuration
         * and we get all accounts related in product_accounting_configuration_product
         *
         * */
        foreach($product->getAccountingConfigurations() as $ac) {
            $systemName = $ac->getProductAccountingConfiguration()->getSystemName();
            $$systemName = $ac->getAccount();
        }

        foreach ($operations as $opCode => $o) {

//            var_dump($entryCode."->".$e['description']);
            $lateFee = 0;
            $operation = new Operation();
            $operation->setProduct($product);
            $atts =  new ArrayCollection();
            $lateFeeAtt = null;
            foreach ($product->getAttributes() as $att){
                $at = new OperationAttribute();
                $at->setValue(0);
                $at->setAttributeProduct($att);
                $at->setOperation($operation);
                if($att->getProductAttribute()->getId()!= 13)
                    $atts->add($at);
                else
                    $lateFeeAtt = $att;
            }
            $operation->setAttributes($atts);
            $operation->setStatus($statusRepository->findExpiredType());

            $effectiveDate = \DateTime::createFromFormat('d/m/y', $o['dateStart']);

            $endDate = \DateTime::createFromFormat('d/m/y', $o['dateEnd']);

            $tenor = 0 ;
            if($effectiveDate <= $endDate)
                $tenor = date_diff($endDate,$effectiveDate)->days ;

            if(!$client  = $clientRepository->findOneBy(array("name"=>$o['clientName']))){

                var_dump("-------------BAD CLIENT---------");
                var_dump($o);
                var_dump("--------------BAD CLIENT-----------");
                continue;
            }
            $operation->setClient($client);


            $operation->setEffectiveDate($effectiveDate);
            $operation->setTenor($tenor);
            $operation->setDateSystem($dateSystem);
            $operation->setDescription($o['description']);
            $operationAmount = 0;
            var_dump($o['invoices']);
            $invoices = new ArrayCollection();
            $em->persist($operation);
            foreach ($o['invoices'] as $i){
                $operationAmount += $i['invoiceAmount'];
                $invoice =  new OperationInvoice();
                $invoice->setTenor($tenor);
                $invoice->setDateInvoice($effectiveDate);
                $invoice->setOperation($operation);
                if(!$debtor  = $clientRepository->findOneBy(array("name"=>$i['debtorName']))){
                    $debtor =  new Client();
                    $debtor->setName($i['debtorName']);
                    $contactPerson = new Person();
                    $address = new Address();

                    $contactPerson->setName("No Contact Person");
                    $contactPerson->setLastname(".");
                    $debtor->setContactPerson($contactPerson);
                    $debtor->setPhone("no Phone");
                    $address->setAddress("no address");
                    $debtor->setAddress($address);
                    $debtor->setRuc("no ruc");
                    $debtor->setEmail("no@email.com");
                    $em->persist($debtor);
                    $em->flush();

                }
                $invoice->setDebtor($debtor);
                $invoice->setReference($i['reference']);
                $invoice->setValue($i['invoiceAmount']);
                if($i['paymentAmount'] > 0 ){
                    $payment = new Payment();
                    $payment->setDebtor($debtor);
                    $payment->setOperation($operation);
//                    $paymentAccountingEntry =  new AccountingEntry();
//                    $paymentAccountingEntry->setDateUser($dateSystem);
//                    $paymentAccountingEntry->setIdUser($user->getId());
//                    $paymentAccountingEntry->setDateSystem($dateSystem);
//                    $paymentAccountingEntry->setOperation($operation);
//                    $paymentAccountingEntry->setOperationInvoice($invoice);
//                    $paymentAccountingEntry->setType($entryTypeRepository->findPaymentType());
//                    $paymentEntry = $this->newEntry($bankAccount,$initialFactoring,$i['paymentAmount']);
//                    $paymentAccountingEntry->addEntry($paymentEntry);
//                    $payment->setEntry($paymentAccountingEntry);

                    $invoicePayment = new InvoicePayment();
                    $invoicePayment->setValue($i['paymentAmount']);
//                    $invoicePayment->setInvoice($invoice);
                    $invoicePayment->setIsLateFee(false);
                    $invoicePayment->setPayment($payment);
                    $payment->addInvoicePayments($invoicePayment);
                    $invoice->addInvoicePayments($invoicePayment);
//                    $em->persist($invoicePayment);
//                    $em->persist($invoice);
//                    $em->persist($payment);
                    $operation->addPayment($payment);

                }
                $operation->addInvoice($invoice);
                if($i['lateFeeAmount'] > 0){
//                    $lateFeeEntry = $this->newEntry($factoringLateFee,$factoringLateFeeReceivable,$i['lateFeeAmount']);

//                    $lateFeeAccountingEntry = new AccountingEntry();
//                    $lateFeeAccountingEntry->setIdUser($user->getId());
//                    $lateFeeAccountingEntry->setDateUser($dateSystem);
//                    $lateFeeAccountingEntry->setDateSystem($dateSystem);
//                    $lateFeeAccountingEntry->addEntry($lateFeeEntry);
//                    $lateFeeAccountingEntry->setType($entryTypeRepository->findLateFeeType());
//
//                    $lateFeeAccountingEntry->setOperation($operation);
//                    $lateFeeAccountingEntry->setOperationInvoice($invoice);
//                    $invoice->addLateFeeEntry($lateFeeAccountingEntry);
//                    $em->persist($lateFeeAccountingEntry);

//                    $entries[]= $lateFeeAccountingEntry;
                    $lateFee +=$i['lateFeeAmount'];
                }

                $invoices->add($invoice);
            }
            if($lateFee){
                $at = new OperationAttribute();
                $at->setValue($lateFee);
                $at->setAttributeProduct($lateFeeAtt);
                $at->setOperation($operation);
                $atts->add($at);
                $operation->setAttributes($atts);
            }
//            $createOpEntry = new AccountingEntry();
//            $createOpEntry->setIdUser($user->getId());
//            $createOpEntry->setDateUser($dateSystem);
//            $createOpEntry->setDateSystem($dateSystem);
//            $createOpEntry->setOperation($operation);
//            $createOpEntry->setDescription("New Operation Fist Entry");
//            $createOpEntry->setType($entryTypeRepository->findNewOperationType());
//
//            $createOpEntry->addEntry($this->newEntry($initialFactoring,$transit,$operationAmount));
//            $ent = new ArrayCollection();
//            $ent->add($createOpEntry);
//            if(isset($entries))
//                foreach ($entries as $e){
//                    $ent->add($e);
//                }
//            $operation->setEntries($ent);

            $em->flush();
            var_dump("________________________________________________________________________");
            var_dump("________________________________________________________________________");


        }
    }
    private function newEntry(Account $debit, Account $credit, $value,$descriptionDebit = "",$descriptionCredit = ""){
        $newEntry= new Entry();
        $newEntry->setAccountCredit($credit);
        $newEntry->setAccountDebit($debit);
        $newEntry->setValue($value);
        $newEntry->setDescriptionCredit($descriptionCredit);
        $newEntry->setDescriptionDebit($descriptionDebit);
        return $newEntry;
    }





}