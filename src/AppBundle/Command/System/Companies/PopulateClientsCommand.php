<?php

namespace AppBundle\Command\System\Companies;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Address;
use AppBundle\EntityCompanies\Client;
use AppBundle\EntityCompanies\Entry;
use AppBundle\EntityCompanies\Person;
use AppBundle\EntityCompanies\Process;
use AppBundle\EntityCompanies\Product;
use AppBundle\EntityCompanies\ProductAccountingConfiguration;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use AppBundle\EntityCompanies\ProductAttribute;
use AppBundle\EntityCompanies\ProductAttributeProduct;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class PopulateClientsCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("PopulateClientsCommand")
            // the short description shown while running "php bin/console list"
            ->setDescription('Create All clients from one company using a file')
            ->addArgument("companyId",InputArgument::REQUIRED,"Id of one company already create in the main database")
            ->addArgument("typeFile",InputArgument::OPTIONAL,"type of the files to process : findTrade (default),firstFactory","findTrade")

        ;
    }


    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "CLIENTS";
        $output->writeln([
            "===========================================================================",
            "                      ".$name,
            "===========================================================================",
        ]);
        $companyId = $input->getArgument("companyId");
        $type = $input->getArgument("typeFile");
        if(!$company = $this->getCompanyById($companyId)){
            $output->writeln([
                "<error>===========================================================================",
                "This is not a valid Company Id : ".$companyId,
                "===========================================================================</error>",
            ]);
            return false;
        }

        $this->setConnection($company->getConnection());

        $this->createClients($type);


        $output->writeln([
            "===========================================================================",
            "END                    ".$name,
            "===========================================================================",
        ]);
    }

    private function createClients($type){
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/'.$this->getConnection().'/clients.csv', 'r');
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if(!$line[0] || !$line[8] ||  $line[8] == "." || !$line[10] || $line[10] == "LAS TABLAS")
                continue;
            $client = new Client();
            $contactPerson = new Person();
            $address = new Address();
            switch($type){
                case 'firstFactory':
                    $client->setCuentaFF($line[0]);
                    $client->setName($line[1]);
                    $contactPerson->setName($line[5]);
                    $contactPerson->setLastname(".");
                    $client->setContactPerson($contactPerson);
                    $client->setPhone($line[8]);
                    $address->setAddress($line[10]);
                    $client->setAddress($address);
                    $client->setRuc($line[14]);
                    $client->setEmail("no@email.com");
                    $em->persist($client);
                    $em->flush();
                    break;
                default :
                    var_dump("no type");
                    break;
            }
        }
        fclose($file);
    }

}