<?php

namespace AppBundle\Command\System\Companies;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\AccountingEntryType;
use AppBundle\EntityCompanies\Entry;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class PopulateEntriesCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("PopulateEntriesCommand")
            // the short description shown while running "php bin/console list"
            ->setDescription('Create All data from one company using files')
            ->addArgument("companyId",InputArgument::REQUIRED,"Id of one company already create in the main database")
            ->addArgument("typeFile",InputArgument::OPTIONAL,"type of the files to process : findTrade (default),firstFactory","findTrade")
            ->addArgument("deleteBefore",InputArgument::OPTIONAL,"",false)


        ;
    }


    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "POPULATE COMPANY ENTRIES";
        $output->writeln([
            "===========================================================================",
            "                      ".$name,
            "===========================================================================",
        ]);
        $companyId = $input->getArgument("companyId");
        $type = $input->getArgument("typeFile");
        $deleteBefore = $input->getArgument("deleteBefore");
        if(!$company = $this->getCompanyById($companyId)){
            $output->writeln([
                "<error>===========================================================================",
                "This is not a valid Company Id : ".$companyId,
                "===========================================================================</error>",
            ]);
            return false;
        }
        $output->writeln("<error>You Are about to Charge lots of entries in ". $company->getName());
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion("Are your sure you want to continue?(Y/N)</error>", false);

        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                "===========================================================================",
                "END WITHOUT DO NOTHING ",
                "===========================================================================",
            ]);
            return false;

        }
        $this->setConnection($company->getConnection());
        if($deleteBefore){
            $em = $this->getEntityManagerCompany();
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\Entry')->execute();
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\AccountingEntry')->execute();
        }
        $this->createAccountEntries($company,$this->getUser(),$type);

        $output->writeln([
            "===========================================================================",
            "END                    ".$name,
            "===========================================================================",
        ]);
    }

    /**
     * @param Company $company
     * @param User $user
     */
    private function createAccountEntries(Company $company,User $user,$type){
        //TODO search in company entity to define what company manager and tree to use
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        $accountEntryRepo = $this->getAccountingEntryRepository();
        $accountRepo = $this->getAccountRepository();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/'.$this->getConnection().'/accountentries.csv', 'r');
        $entriesArray = array();
        $description = $accountCode = $account = '';
        $anulado = false;
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            switch ($type){
                case 'firstFactory':

                    if(strpos($line[4],"*ANULADO*") || strpos($line[4],"*Anulado*")){
                        $anulado = true;
                        continue;
                    }
                    if(!$line[3] && $line[3] !== "0"){
                        $anulado = false;
                        $accountCode = '';
                        $description = $line[4];
                        if($line[6])
                            $description .= " ".$line[6];
                        $description = preg_replace('/\s+/', ' ', $description);
                        continue;
                    }
                    if($anulado)
                        continue;

                    $accountCode = $line[5];
                    if(!$account = $accountRepo->findOneBy(array("historicCode"=>$accountCode))){
                        $accountCode = "";
                        continue;
                    }
                    $f = $line[4];
                    $entriesArray[$f][$line[0]]['date'] = $line[4];
                    $entriesArray[$f][$line[0]]['description'] = $description;
                    if($line[9])
                        $entriesArray[$f][$line[0]]['debit'][$account->getId()] = $line[9];
                    if($line[10])
                        $entriesArray[$f][$line[0]]['credit'][$account->getId()] = $line[10];

                    print_r($c.") accountId :'".$account->getId()."\n");
                    var_dump($entriesArray[$f][$line[0]]);
                    break;
                default :
                    if($line[0] == "fecha" || $line[0] == "" ){
                        $accountCode = '';
                        continue;
                    }
                    if(!$accountCode){
                        $accountCode = $line[0];
                        if(!$account = $accountRepo->findOneBy(array("code"=>$accountCode)))
                            $accountCode = "";
                        continue;
                    }
                    if(!$line[4] && !$line[5])
                        continue;
                    $entriesArray[$line[1]]['date'] = str_replace(array("ene","abr","ago","dic"),array("jan","apr","aug","dec"),$line[0]);
                    $entriesArray[$line[1]]['description'] = $line[3];

                    if($line[4]!="0.00")
                        if(isset($entriesArray[$line[1]]['debit'][$account->getId()]))
                            $entriesArray[$line[1]]['debit'][$account->getId()] += $line[4];
                        else
                            $entriesArray[$line[1]]['debit'][$account->getId()] = $line[4];
                    if($line[5]!="0.00")
                        if(isset($entriesArray[$line[1]]['credit'][$account->getId()]))
                            $entriesArray[$line[1]]['credit'][$account->getId()] += $line[5];
                        else
                            $entriesArray[$line[1]]['credit'][$account->getId()] = $line[5];

                    print_r($c.") accountId :'".$account->getId()."\n");
                    var_dump($entriesArray[$line[1]]);
                    break;
            }
        }
        fclose($file);
        $badEntries = array();
        if($type == "FirstFactory")
            foreach ($entriesArray as $f => $entriesArrays)
                foreach ($entriesArrays as $entryCode => $e){
//            var_dump($entryCode."->".$e['description']);
                    $accountingEntry = new AccountingEntry();
                    switch($type){
                        case 'firstFactory':
                            $date = \DateTime::createFromFormat('d/m/y', $e['date']);
                            break;
                        default :
                            $date = \DateTime::createFromFormat('j-M.-Y', $e['date']);
                            break;
                    }
                    $accountingEntry->setDescription($entryCode." ".$e['description']);
                    $accountingEntry->setDateUser($date);
                    $accountingEntry->setIdUser($user->getId());
                    print_r($entryCode.")->".$date->format("Y-m-j")."\n");
                    $date = null;
                    unset($date);
                    if(!isset($e['debit']) || !isset($e['credit'])){
                        var_dump("--------------------------------------------");
                        var_dump($e);
                        $badEntries[$entryCode]=$e;
                        var_dump("--------------------------------------------");

                    }else{
                        $totalDebit = $totalCredit = 0;
                        foreach ($e['debit'] as $debitAccountId => $debitValue)
                            $totalDebit+=$debitValue;
                        foreach ($e['credit'] as $creditAccountId => $creditValue)
                            $totalCredit+=$creditValue;

                        if(round($totalDebit,2) - round($totalCredit,2)) {
                            var_dump("-----------------totales-----------------------");
                            var_dump($totalDebit);
                            var_dump($totalCredit);
                            var_dump($totalDebit - $totalCredit);
                            var_dump("----------------------------------------");
                            var_dump($e);
                            $badEntries[$entryCode]=$e;

                        }else{
                            foreach ($e['debit'] as $debitAccountId => $debitValue){
                                $debitValue = round($debitValue,2);
                                foreach ($e['credit'] as $creditAccountId => $creditValue){
                                    $creditValue = round($creditValue,2);
                                    $entry =  new Entry();
                                    $entry->setAccountCredit($accountRepo->findOneBy(array("id" => $creditAccountId)));
                                    $entry->setAccountDebit($accountRepo->findOneBy(array("id" => $debitAccountId)));
                                    if($debitValue == $creditValue){
                                        $entry->setValue($debitValue);
                                        $e['credit'][$creditAccountId] = $e['debit'][$debitAccountId] = null;
                                        unset($e['credit'][$creditAccountId]);
                                        unset($e['debit'][$debitAccountId]);
                                    }elseif($debitValue > $creditValue){
                                        $entry->setValue($creditValue);
                                        $e['credit'][$creditAccountId] = null;
                                        unset($e['credit'][$creditAccountId]);
                                        //resto
                                        $e['debit'][$debitAccountId] = $debitValue = round( $debitValue - $creditValue,2);
                                        var_dump("D/in: ".$creditAccountId." out: ".$debitAccountId ." Amount: ".$debitValue);

                                    }elseif($debitValue < $creditValue){
                                        $entry->setValue($debitValue);
                                        $e['debit'][$debitAccountId] = null;
                                        unset($e['debit'][$debitAccountId]);
                                        //resto
                                        $e['credit'][$creditAccountId] = $creditValue = round( $creditValue - $debitValue,2);
                                        var_dump("C/in: ".$creditAccountId." out: ".$debitAccountId ." Amount: ".$creditValue);
                                        $accountingEntry->addEntry($entry);
                                        continue 2;
                                    }
//                        $entry->setAccountingEntry($accountingEntry);
                                    $accountingEntry->addEntry($entry);
                                    $entry = null;
                                    unset($entry);
                                }
                            }
                            if(!$accountingEntry->getEntrys()){
                                var_dump("----------------------------------------");
                                var_dump($entryCode);
                                var_dump("----------------------------------------");
                            }
                            if($entryCode == "CD12-16-23"){
                                var_dump("----------------------------------------");

                                var_dump($e);
                                var_dump("----------------------------------------");
                                var_dump("----------------------------------------");
                                var_dump($entryCode);
                                foreach ($accountingEntry->getEntrys() as $et){
                                    var_dump($et->getValue());
                                }
                                var_dump("----------------------------------------");

                            }

                            $em->persist($accountingEntry);
                            $em->flush();
                            $em->clear();
                            if(!$c % 55 ){
                                $em = null;
                                $em = $this->getEntityManagerCompany();
                                $em->getConnection()->getConfiguration()->setSQLLogger(null);
                            }
                            gc_collect_cycles();
                        }
                    }
                }

        else
            foreach ($entriesArray as $entryCode => $e){
//            var_dump($entryCode."->".$e['description']);
                $accountingEntry = new AccountingEntry();
                switch($type){
                    case 'firstFactory':
                        $date = \DateTime::createFromFormat('d/m/y', $e['date']);
                        break;
                    default :
                        $date = \DateTime::createFromFormat('j-M-Y', $e['date']);
                        break;
                }
                if(!$date){
                    var_dump($e['date']);
                    var_dump("==================================================");
                }
                $accountingEntry->setDescription($entryCode." ".$e['description']);
                $accountingEntry->setDateUser($date);
                $accountingEntry->setIdUser($user->getId());
                print_r($entryCode.")->".$date->format("Y-m-j")."\n");
                $date = null;
                unset($date);
                if(!isset($e['debit']) || !isset($e['credit'])){
                    var_dump("--------------------------------------------");
                    var_dump($e);
                    $badEntries[$entryCode]=$e;
                    var_dump("--------------------------------------------");

                }else{
                    $totalDebit = $totalCredit = 0;
                    foreach ($e['debit'] as $debitAccountId => $debitValue)
                        $totalDebit+=$debitValue;
                    foreach ($e['credit'] as $creditAccountId => $creditValue)
                        $totalCredit+=$creditValue;

                    if(round($totalDebit,2) - round($totalCredit,2)) {
                        var_dump("-----------------totales-----------------------");
                        var_dump($totalDebit);
                        var_dump($totalCredit);
                        var_dump($totalDebit - $totalCredit);
                        var_dump("----------------------------------------");
                        var_dump($e);
                        $badEntries[$entryCode]=$e;

                    }else{
                        foreach ($e['debit'] as $debitAccountId => $debitValue){
                            $debitValue = round($debitValue,2);
                            foreach ($e['credit'] as $creditAccountId => $creditValue){
                                $creditValue = round($creditValue,2);
                                $entry =  new Entry();
                                $entry->setAccountCredit($accountRepo->findOneBy(array("id" => $creditAccountId)));
                                $entry->setAccountDebit($accountRepo->findOneBy(array("id" => $debitAccountId)));
                                if($debitValue == $creditValue){
                                    $entry->setValue($debitValue);
                                    $e['credit'][$creditAccountId] = $e['debit'][$debitAccountId] = null;
                                    unset($e['credit'][$creditAccountId]);
                                    unset($e['debit'][$debitAccountId]);
                                }elseif($debitValue > $creditValue){
                                    $entry->setValue($creditValue);
                                    $e['credit'][$creditAccountId] = null;
                                    unset($e['credit'][$creditAccountId]);
                                    //resto
                                    $e['debit'][$debitAccountId] = $debitValue = round( $debitValue - $creditValue,2);
                                    var_dump("D/in: ".$creditAccountId." out: ".$debitAccountId ." Amount: ".$debitValue);

                                }elseif($debitValue < $creditValue){
                                    $entry->setValue($debitValue);
                                    $e['debit'][$debitAccountId] = null;
                                    unset($e['debit'][$debitAccountId]);
                                    //resto
                                    $e['credit'][$creditAccountId] = $creditValue = round( $creditValue - $debitValue,2);
                                    var_dump("C/in: ".$creditAccountId." out: ".$debitAccountId ." Amount: ".$creditValue);
                                    $accountingEntry->addEntry($entry);
                                    continue 2;
                                }
//                        $entry->setAccountingEntry($accountingEntry);
                                $accountingEntry->addEntry($entry);
                                $entry = null;
                                unset($entry);
                            }
                        }
                        if(!$accountingEntry->getEntrys()){
                            var_dump("----------------------------------------");
                            var_dump($entryCode);
                            var_dump("----------------------------------------");
                        }
                        if($entryCode == "CD12-16-23"){
                            var_dump("----------------------------------------");

                            var_dump($e);
                            var_dump("----------------------------------------");
                            var_dump("----------------------------------------");
                            var_dump($entryCode);
                            foreach ($accountingEntry->getEntrys() as $et){
                                var_dump($et->getValue());
                            }
                            var_dump("----------------------------------------");

                        }

                        $em->persist($accountingEntry);
                        $em->flush();
                        $em->clear();
                        if(!$c % 55 ){
                            $em = null;
                            $em = $this->getEntityManagerCompany();
                            $em->getConnection()->getConfiguration()->setSQLLogger(null);
                        }
                        gc_collect_cycles();
                    }
                }
            }
        var_dump("-----------------BAD Entries-----------------------");
        foreach ($badEntries as $entryCode => $e){
            var_dump($entryCode);
        }
        var_dump("----------------------------------------");
    }





}