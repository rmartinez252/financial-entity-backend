<?php

namespace AppBundle\Command\System\Companies;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Entry;
use AppBundle\EntityCompanies\OperationStatus;
use AppBundle\EntityCompanies\Process;
use AppBundle\EntityCompanies\Product;
use AppBundle\EntityCompanies\ProductAccountingConfiguration;
use AppBundle\EntityCompanies\ProductAccountingConfigurationProduct;
use AppBundle\EntityCompanies\ProductAttribute;
use AppBundle\EntityCompanies\ProductAttributeProduct;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class PopulateProductsCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("PopulateProductsCommand")
            // the short description shown while running "php bin/console list"
            ->setDescription('Create All data from one company using files')
            ->addArgument("companyId",InputArgument::REQUIRED,"Id of one company already create in the main database")
        ;
    }


    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "PRODUCTS";
        $output->writeln([
            "===========================================================================",
            "                      ".$name,
            "===========================================================================",
        ]);
        $companyId = $input->getArgument("companyId");
        if(!$company = $this->getCompanyById($companyId)){
            $output->writeln([
                "<error>===========================================================================",
                "This is not a valid Company Id : ".$companyId,
                "===========================================================================</error>",
            ]);
            return false;
        }

        $this->setConnection($company->getConnection());

        $this->createBeforeProducts();
        $this->createProducts();


        $output->writeln([
            "===========================================================================",
            "END                    ".$name,
            "===========================================================================",
        ]);
    }

    private function createBeforeProducts(){
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        $processRepo = $this->getProcessRepository();
        $productAttributeRepo = $this->getProductAttributeRepository();
        $productAccountingConfigurationRepo = $this->getProductAccountingConfigurationRepository();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/beforeProducts.csv', 'r');
        $entityName = "Att";
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if($line[0] == "" || $line[0] == "productAttributeName"){
                $entityName = "Att";
                var_dump($entityName);
                continue;
            }elseif($line[0] == "processName"){
                $entityName = "Pro";
                var_dump($entityName);
                continue;
            }elseif($line[0] == "AttributeProcess"){
                $entityName = "AttPro";
                var_dump($entityName);
                continue;
            }elseif($line[0] == "productAccountingName"){
                $entityName = "Acc";
                var_dump($entityName);
                continue;

            }elseif($line[0] == "productAccountingProcess"){
                $entityName = "AccPro";
                var_dump($entityName);
                continue;
            }elseif($line[0] == "operationStatus"){
                $entityName = "opeSt";
                var_dump($entityName);
                continue;
            }

            /**
             * @var $process Process
             */
            switch ($entityName){
                case "Pro":
                    $process = new Process();
                    $process->setName($line[0]);
                    $em->persist($process);
                    $em->flush();
                    break;
                case "AttPro":
                    if(!$process = $processRepo->findOneBy(array("name"=>$line[0])))
                        continue;
                    if(!$attribute = $productAttributeRepo->findOneBy(array("systemName"=>$line[1])))
                        continue;
                    var_dump($process->getName()." -> ".$attribute->getSystemName());
                    if($process->getAttributes())
                        $process->addAttribute($attribute);
                    else
                        $process->setAttributes(array($attribute));
                    $em->persist($process);
                    $em->flush();
                    break;

                case "Att":
                    $productAttribute = new ProductAttribute();
                    $productAttribute->setShowName($line[0]);
                    $productAttribute->setSystemName($line[1]);
                    $productAttribute->setType($line[2]);
                    $productAttribute->setDefaultValue($line[3]);

                    $em->persist($productAttribute);
                    $em->flush();
                    break;
                case "Acc":
                    $processAccounting = new ProductAccountingConfiguration();
                    $processAccounting->setShowName($line[1]);
                    $processAccounting->setSystemName($line[0]);
                    $processAccounting->setShowDescription($line[2]);

                    $em->persist($processAccounting);
                    $em->flush();
                    break;
                case "AccPro":
                    if(!$process = $processRepo->findOneBy(array("name"=>$line[0])))
                        continue;
                    if(!$accounting = $productAccountingConfigurationRepo->findOneBy(array("systemName"=>$line[1])))
                        continue;
                    var_dump($accounting->getSystemName());
                    var_dump($process->getName()." -> ".$accounting->getSystemName());
                    $process->addAccountingConfiguration($accounting);
                    $em->persist($process);
                    $em->flush();
                    break;
                case "opeSt":
                    $operationStatus = new OperationStatus();
                    $operationStatus->setName($line[0]);
                    $operationStatus->setType($line[1]);
                    $operationStatus->setSystemName($line[2]);
                    $em->persist($operationStatus);
                    $em->flush();
                    break;
                default:
                    break;
            }
        }
        fclose($file);

    }


    private function createProducts(){
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        $productRepo = $this->getProductRepository();
        $processRepo = $this->getProcessRepository();
        $productAttributeRepo = $this->getProductAttributeRepository();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/'.$this->getConnection().'/products.csv', 'r');
        $entityName = "Pro";
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if($line[0] == "" || $line[0] == "productName"){
                $entityName = "Pro";
                continue;
            }elseif($line[0] == "productAttributeProduct"){
                $entityName = "ProAtt";
                continue;
            }
            switch ($entityName) {
                case "Pro":
                    $product = new Product();
                    $product->setName($line[0]);
                    $product->setDescription($line[1]);
                    $product->setAbbreviation($line[2]);
                    $product->setPortfolio($line[3]);
                    $product->setNeedsInvoice($line[4]);
                    /**
                     * @var $process Process
                     */
                    if(!$process = $processRepo->findOneBy(array("name"=>$line[5])))
                        continue;
                    $product->setProcess($process);
                    foreach ($process->getAccountingConfigurations() as $ac){
                        $pac = new ProductAccountingConfigurationProduct();
                        $pac->setProductAccountingConfiguration($ac);
                        $pac->setNature(0);
                        $pac->setProduct($product);
                        $product->addAccountingConfiguration($pac);
                    }
                    $em->persist($product);
                    $em->flush();
                    var_dump($product->getName());
                    break;
                case "ProAtt":
                    if(!$product = $productRepo->findOneBy(array("abbreviation"=>$line[0])))
                        continue;
                    if(!$attribute = $productAttributeRepo->findOneBy(array("systemName"=>$line[1])))
                        continue;
                    $productAttributeProduct =  new ProductAttributeProduct();
                    $productAttributeProduct->setProduct($product);
                    $productAttributeProduct->setProductAttribute($attribute);
                    $productAttributeProduct->setIsNullable($line[2]);
                    $productAttributeProduct->setIsCalculation($line[3]);
                    $productAttributeProduct->setIsPercentage($line[4]);
                    $productAttributeProduct->setIsPositive($line[5]);
                    $em->persist($productAttributeProduct);
                    $em->flush();
                    break;
                default:
                    break;
            }
        }
        fclose($file);

    }

    /**
     * @param Company $company
     * @param User $user
     */
    private function createAccountEntries(Company $company,User $user,$type){
        //TODO search in company entity to define what company manager and tree to use
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        $accountEntryRepo = $this->getAccountingEntryRepository();
        $accountRepo = $this->getAccountRepository();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/'.$this->getConnection().'/accountentries.csv', 'r');
        $entriesArray = array();
        $description = $accountCode = $account = '';
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {

            switch ($type){
                case 'firstFactory':

                    if(!$line[0]){
                        $accountCode = '';
                        $description = $line[4];
                        if($line[6])
                            $description .= " ".$line[6];
                        $description = preg_replace('/\s+/', ' ', $description);
                        continue;
                    }

                    $accountCode = $line[5];
                    if(!$account = $accountRepo->findOneBy(array("historicCode"=>$accountCode))){
                        $accountCode = "";
                        continue;
                    }
                    $entriesArray[$line[0]]['date'] = $line[4];
                    $entriesArray[$line[0]]['description'] = $description;
                    if($line[9])
                        $entriesArray[$line[0]]['debit'][$account->getId()] = $line[9];
                    if($line[10])
                        $entriesArray[$line[0]]['credit'][$account->getId()] = $line[10];

                    print_r($c.") accountId :'".$account->getId()."\n");
                    var_dump($entriesArray[$line[0]]);
                    break;
                default :
                    if($line[0] == "fecha" || $line[0] == "" ){
                        $accountCode = '';
                        continue;
                    }
                    if(!$accountCode){
                        $accountCode = $line[0];
                        if(!$account = $accountRepo->findOneBy(array("code"=>$accountCode)))
                            $accountCode = "";
                        continue;
                    }
                    $entriesArray[$line[1]]['date'] = str_replace(array("ago","dic"),array("aug","dec"),$line[0]);
                    if($line[4])
                        $entriesArray[$line[1]]['debit'][$account->getId()] = $line[4];
                    if($line[5])
                        $entriesArray[$line[1]]['credit'][$account->getId()] = $line[5];

                    print_r($c.") accountId :'".$account->getId()."\n");
                    var_dump($entriesArray[$line[1]]);
                    break;
            }
        }
//        var_dump($am->showAll());
        fclose($file);
        foreach ($entriesArray as $entryCode => $e){
            $accountingEntry = new AccountingEntry();
            switch($type){
                case 'firstFactory':
                    $date = \DateTime::createFromFormat('d/m/Y', $e['date']);
                    $accountingEntry->setDescription($e['description']);
                    break;
                default :
                    $date = \DateTime::createFromFormat('j-M.-Y', $e['date']);
                    $accountingEntry->setDescription($entryCode);
                    break;
            }
            $accountingEntry->setDateUser($date);
            $accountingEntry->setIdUser($user->getId());
            print_r($entryCode.")->".$date->format("Y-m-j")."\n");
            $date = null;
            unset($date);
            if(!isset($e['debit']) || !isset($e['credit']))
                var_dump($e);
            else{
                foreach ($e['debit'] as $debitAccountId => $debitValue){
                    foreach ($e['credit'] as $creditAccountId => $creditValue){
                        $entry =  new Entry();
                        $entry->setAccountCredit($accountRepo->findOneBy(array("id" => $creditAccountId)));
                        $entry->setAccountDebit($accountRepo->findOneBy(array("id" => $debitAccountId)));
                        if($debitValue == $creditValue){
                            $entry->setValue($debitValue);
                            $e['credit'][$creditAccountId] = $e['debit'][$debitAccountId] = null;
                            unset($e['credit'][$creditAccountId]);
                            unset($e['debit'][$debitAccountId]);
                        }elseif($debitValue > $creditValue){
                            $entry->setValue($creditValue);
                            $e['credit'][$creditAccountId] = null;
                            unset($e['credit'][$creditAccountId]);
                            //resto
                            $e['debit'][$debitAccountId] = $debitValue = $debitValue - $creditValue;

                        }elseif($debitValue < $creditValue){
                            $entry->setValue($debitValue);
                            $e['debit'][$debitAccountId] = null;
                            unset($e['debit'][$debitAccountId]);
                            //resto
                            $e['credit'][$debitAccountId] = $creditValue = $creditValue - $debitValue;
                        }
//                        $entry->setAccountingEntry($accountingEntry);
                        $accountingEntry->addEntry($entry);
                        $entry = null;
                        unset($entry);

                    }
                }
                $em->persist($accountingEntry);
                $em->flush();
                $em->clear();
                if(!$c % 55 ){
                    $em = null;
                    $em = $this->getEntityManagerCompany();
                    $em->getConnection()->getConfiguration()->setSQLLogger(null);
                }
                gc_collect_cycles();
            }
        }

    }




}