<?php

namespace AppBundle\Command\System\Companies;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\AccountingEntryType;
use AppBundle\EntityCompanies\Entry;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class PopulateCompanyCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("PopulateCompanyCommand")
            // the short description shown while running "php bin/console list"
            ->setDescription('Create All data from one company using files')
            ->addArgument("companyId",InputArgument::REQUIRED,"Id of one company already create in the main database")
            ->addArgument("typeFile",InputArgument::OPTIONAL,"type of the files to process : findTrade (default),firstFactory","findTrade")



        ;
    }


    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "FULL RESTART OF COMPANY";
        $output->writeln([
            "===========================================================================",
            "                      ".$name,
            "===========================================================================",
        ]);
        $companyId = $input->getArgument("companyId");
        $type = $input->getArgument("typeFile");
        if(!$company = $this->getCompanyById($companyId)){
            $output->writeln([
                "<error>===========================================================================",
                "This is not a valid Company Id : ".$companyId,
                "===========================================================================</error>",
            ]);
            return false;
        }
        $output->writeln("<error>You Are about to delete all data in ". $company->getName());
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion("Are your sure you want to continue?(Y/N)</error>", false);

        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                "===========================================================================",
                "END WITHOUT DO NOTHING ",
                "===========================================================================",
            ]);
            return false;

        }
        $this->setConnection($company->getConnection());

        $argumentsDrop = array(
            'command' => 'doctrine:schema:drop',
            '--force'    => true,
            '--em'    => $this->getConnection(),
            '--full-database'    => true,
        );
        $dropInput = new ArrayInput($argumentsDrop);
        $this->getApplication()->find('doctrine:schema:drop')->run($dropInput,$output);

        $argumentsCreate = array(
            'command' => 'doctrine:schema:update',
            '--em'    => $this->getConnection(),
            '--force'    => true,
        );
        $createInput = new ArrayInput($argumentsCreate);
        $this->getApplication()->find('doctrine:schema:update')->run($createInput,$output);

        $attributesInfo = $this->getAttributes();
        foreach ($attributesInfo as $attributeInfo){
            $this->createCompanyAttributes($company,$attributeInfo);
        }
        $this->createAccounts($company,$type);
        $this->createAccountEntryTypes();
        $inputCommands = new ArrayInput(array("companyId"=>$company->getId()));
        $inputCommands2 = new ArrayInput(array("companyId"=>$company->getId(),"typeFile"=>$type));

        $this->getApplication()->find("PopulateEntriesCommand")->run($inputCommands2,$output);

        $this->getApplication()->find("PopulateProductsCommand")->run($inputCommands,$output);



        $output->writeln([
            "===========================================================================",
            "END                    ".$name,
            "===========================================================================",
        ]);
    }

    protected function preRestartCompanies(){
        $output = new NullOutput();
        $companies = $this->getCompanies();
        $argumentsCompanies = array(
            'command' => 'createCompaniesCommand',
            'cleanBefore'    => true,
            'createEndpoints'    => false,
        );
        $companiesInput = new ArrayInput($argumentsCompanies);
        $this->getApplication()->find('createCompaniesCommand')->run($companiesInput,$output);

    }


    private function createAccounts(Company $company,$type){
        $from = $to = false;
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        $accountRepo = $this->getAccountRepository();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/'.$this->getConnection().'/account.csv', 'r');
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if($from)
                if($c<$from)
                    continue;
            if($to)
                if($c>$to)
                    continue;
            $account = new Account();
            if($line[0] == "codigo")
                continue;
            $histArray = $line[0];
            $histArray = explode('.',$histArray);
            $rest = strlen(end($histArray));
            if(!$rest)
                $rest = strlen(prev($histArray));
            $histArray = null;
            unset($histArray);
            switch($type){
                case 'firstFactory':
                    $patent = substr($line[4],0,-$rest);
                    $account->setName($line[1]);
                    $account->setHistoricCode($line[3]);
                    $account->setCode($line[4]);
                    $account->setNature($line[6]);
                    break;
                default :
                    $patent = substr($line[2],0,-$rest);
                    $account->setName($line[1]);
                    $account->setHistoricCode($line[3]);
                    $account->setCode($line[2]);
                    $account->setNature($line[6]);
                    break;
            }
//            print_r("parent:'".$patent."'->");
            if($c == 1){
//                print_r("no parent and no record in accout, put left in 1 and right y 2");
                $account->setL(1);
                $account->setR(2);
            }elseif($patent === ""){
                print_r("\n no parent put in right of the main menu: ".$line[2]."\n");
                $am = $this->getAccountManager();
                $main = $am->showMain();
//                foreach ($main as $m)
//                    print_r("n:".$m['node']->getName()."\n");
                $last = end($main);
                $main = null;
                unset($main);
//                var_dump($last->getName());
//                $em->refresh($last['node']);
                $em->clear();
                $newnode = $am->addOne($last['node']->getL(),$last['node']->getR(),'right');
                $account->setL($newnode['l']);
                $account->setR($newnode['r']);
                var_dump($last['node']->getName()." /L:".$last['node']->getL()." /R:".$last['node']->getR());
                var_dump($newnode['l']);
                var_dump($newnode['r']);
                $newnode = null;
                unset($newnode);

            }else{
//                print_r("search parent");
                $am = $this->getAccountManager();
                if(!$parentNode = $accountRepo->findOneBy(array("code"=>$patent))){
                    print_r("\n -> no find any record, the account :'".$line[1]."' try to find '".$patent."' and it doesn't exist \n");
                    continue;
                }else{
//                    print_r("put in subr form the L and R of parent L:".$parentNode->getL()."//R:".$parentNode->getR()."\n");
                    $newnode = $am->
                    addOne($parentNode->getL(),$parentNode->getR(),'subr');
                    $account->setL($newnode['l']);
                    $account->setR($newnode['r']);
                    $em->refresh($parentNode);
                }
            }
            print_r($c.") :'".$account->getName()."'->L:".$account->getL().", R:".$account->getR()."\n");
            $em->persist($account);
            $em->flush();
            $em->clear();
            if(!$c % 55 ){
                $em = null;
                $em = $this->getEntityManagerCompany();
            }
            $accountRepo = $em->getRepository("AppBundle\\EntityCompanies\\Account");

        }
        fclose($file);

    }

    /**
     * @param Company $company
     * @param User $user
     */
    private function createAccountEntries(Company $company,User $user,$type){
        //TODO search in company entity to define what company manager and tree to use
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        $accountEntryRepo = $this->getAccountingEntryRepository();
        $accountRepo = $this->getAccountRepository();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/'.$this->getConnection().'/accountentries.csv', 'r');
        $entriesArray = array();
        $description = $accountCode = $account = '';
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {

            switch ($type){
                case 'firstFactory':

                    if(!$line[0]){
                        $accountCode = '';
                        $description = $line[4];
                        if($line[6])
                            $description .= " ".$line[6];
                        $description = preg_replace('/\s+/', ' ', $description);
                        continue;
                    }

                    $accountCode = $line[5];
                    if(!$account = $accountRepo->findOneBy(array("historicCode"=>$accountCode))){
                        $accountCode = "";
                        continue;
                    }
                    $entriesArray[$line[0]]['date'] = $line[4];
                    $entriesArray[$line[0]]['description'] = $description;
                    if($line[9])
                        $entriesArray[$line[0]]['debit'][$account->getId()] = $line[9];
                    if($line[10])
                        $entriesArray[$line[0]]['credit'][$account->getId()] = $line[10];

                    print_r($c.") accountId :'".$account->getId()."\n");
                    var_dump($entriesArray[$line[0]]);
                    break;
                default :
                    if($line[0] == "fecha" || $line[0] == "" ){
                        $accountCode = '';
                        continue;
                    }
                    if(!$accountCode){
                        $accountCode = $line[0];
                        if(!$account = $accountRepo->findOneBy(array("code"=>$accountCode)))
                            $accountCode = "";
                        continue;
                    }
                    $entriesArray[$line[1]]['date'] = str_replace(array("ago","dic"),array("aug","dec"),$line[0]);
                    if($line[4])
                        $entriesArray[$line[1]]['debit'][$account->getId()] = $line[4];
                    if($line[5])
                        $entriesArray[$line[1]]['credit'][$account->getId()] = $line[5];

                    print_r($c.") accountId :'".$account->getId()."\n");
                    var_dump($entriesArray[$line[1]]);
                    break;
            }
        }
//        var_dump($am->showAll());
        fclose($file);
        foreach ($entriesArray as $entryCode => $e){
            $accountingEntry = new AccountingEntry();
            switch($type){
                case 'firstFactory':
                    $date = \DateTime::createFromFormat('d/m/Y', $e['date']);
                    $accountingEntry->setDescription($e['description']);
                    break;
                default :
                    $date = \DateTime::createFromFormat('j-M.-Y', $e['date']);
                    $accountingEntry->setDescription($entryCode);
                    break;
            }
            $accountingEntry->setDateUser($date);
            $accountingEntry->setIdUser($user->getId());
            print_r($entryCode.")->".$date->format("Y-m-j")."\n");
            $date = null;
            unset($date);
            if(!isset($e['debit']) || !isset($e['credit']))
                var_dump($e);
            else{
                foreach ($e['debit'] as $debitAccountId => $debitValue){
                    foreach ($e['credit'] as $creditAccountId => $creditValue){
                        $entry =  new Entry();
                        $entry->setAccountCredit($accountRepo->findOneBy(array("id" => $creditAccountId)));
                        $entry->setAccountDebit($accountRepo->findOneBy(array("id" => $debitAccountId)));
                        if($debitValue == $creditValue){
                            $entry->setValue($debitValue);
                            $e['credit'][$creditAccountId] = $e['debit'][$debitAccountId] = null;
                            unset($e['credit'][$creditAccountId]);
                            unset($e['debit'][$debitAccountId]);
                        }elseif($debitValue > $creditValue){
                            $entry->setValue($creditValue);
                            $e['credit'][$creditAccountId] = null;
                            unset($e['credit'][$creditAccountId]);
                            //resto
                            $e['debit'][$debitAccountId] = $debitValue = $debitValue - $creditValue;

                        }elseif($debitValue < $creditValue){
                            $entry->setValue($debitValue);
                            $e['debit'][$debitAccountId] = null;
                            unset($e['debit'][$debitAccountId]);
                            //resto
                            $e['credit'][$debitAccountId] = $creditValue = $creditValue - $debitValue;
                        }
//                        $entry->setAccountingEntry($accountingEntry);
                        $accountingEntry->addEntry($entry);
                        $entry = null;
                        unset($entry);

                    }
                }
                $em->persist($accountingEntry);
                $em->flush();
                $em->clear();
                if(!$c % 55 ){
                    $em = null;
                    $em = $this->getEntityManagerCompany();
                    $em->getConnection()->getConfiguration()->setSQLLogger(null);
                }
                gc_collect_cycles();
            }
        }

    }

    private function createAccountEntryTypes(){
        //TODO search in company entity to define what company manager and tree to use
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany();
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Companies/companiesFiles/accountingEntryType.csv', 'r');
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if($line[0] == "accountingEntryType")
                continue;
            $entryType =  new AccountingEntryType();
            $entryType->setName($line[0]);
            $entryType->setSystem($line[1]);
            $entryType->setNumber($line[2]);
            $em->persist($entryType);
            $em->flush();

        }



    }




}