<?php

namespace AppBundle\Command\System\Start;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Attribute;
use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\Product;
use AppBundle\Operations\OperationProcess;
use AppBundle\Repository\StructureRepository;
use AppBundle\Tree\Tree;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

class CleanAllCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("cleanAllCommand")
            ->setAliases(array("startCommand00"))
            // the short description shown while running "php bin/console list"
            ->setDescription('Truncate All Tables')

        ;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "Clear All DataBase";
        $output->writeln([
            "===========================================================================",
            $name,
        ]);

        $this->clearTables($output);
        $output->writeln([
            $name." END",
            "===========================================================================",
        ]);
    }

    /**
     * @return array
     */
    protected function getCompanies(){
        return $this->getCompanyRepository()->findAll();
    }

    protected function clearTables($output){
        $companies = $this->getCompanies();
        $argumentsDrop = array(
            'command' => 'doctrine:schema:drop',
            '--force'    => true,
            '--full-database'    => true,
        );
        $dropInput = new ArrayInput($argumentsDrop);
        $this->getApplication()->find('doctrine:schema:drop')->run($dropInput,$output);

        $argumentsCreate = array(
            'command' => 'doctrine:schema:update',
            '--force'    => true,
        );
        $createInput = new ArrayInput($argumentsCreate);
        $this->getApplication()->find('doctrine:schema:update')->run($createInput,$output);

        /**
         * @var $company Company
         */
        foreach ($companies as $company){
            $argumentsDrop = array(
                'command' => 'doctrine:schema:drop',
                '--force'    => true,
                '--em'    => $company->getConnection(),
                '--full-database'    => true,
            );
            $dropInput = new ArrayInput($argumentsDrop);
            $this->getApplication()->find('doctrine:schema:drop')->run($dropInput,$output);

            $argumentsCreate = array(
                'command' => 'doctrine:schema:update',
                '--em'    => $company->getConnection(),
                '--force'    => true,
            );
            $createInput = new ArrayInput($argumentsCreate);
            $this->getApplication()->find('doctrine:schema:update')->run($createInput,$output);

        }

    }




}