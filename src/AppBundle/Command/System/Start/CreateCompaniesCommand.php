<?php

namespace AppBundle\Command\System\Start;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\Product;
use AppBundle\Operations\OperationProcess;
use AppBundle\Repository\StructureRepository;
use AppBundle\Tree\Tree;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCompaniesCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("createCompaniesCommand")
            ->setAliases(array("startCommand03"))
            // the short description shown while running "php bin/console list"
            ->setDescription('Create companies from file.')

            // Arguments
            ->addArgument("cleanBefore",InputArgument::OPTIONAL,"Define if the command Clean de database Before execute",false)
            ->addArgument("createEndpoints",InputArgument::OPTIONAL,"Define if the command Clean de database Before execute",true)

        ;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "Create Companies";
        $output->writeln([
            "===========================================================================",
            $name,
            "",
        ]);
        if($input->getArgument("cleanBefore")){
            $output->writeln("Clean");
            $this->clearTables();

        }
        $companiesInf = $this->getCompanies();
        foreach($companiesInf as $companyInf ){
            $company = $this->createCompany($companyInf);
            if($input->getArgument("createEndpoints"))
                $this->createCompanyEndpoints($company);
        }
        $output->writeln([
            "",
            $name." END",
            "===========================================================================",
        ]);
    }


    private function createCompany($companyInfo)
    {
        $company = new Company();
        $company->setName($companyInfo["name"]);
        $company->setDirection($companyInfo["direction"]);
        $company->setEmail($companyInfo["email"]);
        $company->setPhone($companyInfo["phone"]);
        $company->setCloseDate($companyInfo["closeDate"]);
        $company->setConnection($companyInfo["connection"]);
        $em = $this->getEntityManager();
        $em->persist($company);
        $em->flush();
        var_dump($companyInfo['name']);

        return $company;
    }
    /**
     * @param string $name
     * @return null|object
     */
    protected function  getEndpointByName($name){
        return $this->getEndpointRepository()->findOneBy(array("uriName"=>$name));
    }
    /**
     * @return array
     */
    protected function getCompanies(){
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Start/startFiles/companies.csv', 'r');
        $companies = array();
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if ($line[0] == "companyName")
                continue;
            $companies[$c]['name'] = $line[0];
            $companies[$c]['direction'] = $line[1];
            $companies[$c]['email'] = $line[2];
            $companies[$c]['phone'] = $line[3];
            $companies[$c]['closeDate'] = $line[4];
            $companies[$c]['connection'] = $line[5];
        }
        return $companies;
    }
    protected function createCompanyEndpoints(Company $company){
        $em = $this->getEntityManager();
        $endpoints = $this->getEndpointRepository()->findAll();
        /**
         * @var $ep Endpoint
         */
        foreach ($endpoints as $ep){
            $company_endpoint = new CompanyEndpoint();
            $company_endpoint->setCompany($company);
            $company_endpoint->setEndpoint($ep);
            $company_endpoint->setPermission($ep->getPermission());
            $company->addEndpoint($company_endpoint);
            $em->persist($company_endpoint);
            $em->flush();
            var_dump("CompanyEndpoint->".$ep->getUriName());

        }
    }

    protected function clearTables(){
        $em = $this->getEntityManager();
        //get the Account manager for selected company
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        //delete all records before start de upload
        $em->createQuery('DELETE FROM AppBundle\\Entity\\UserCompanyEndpoint')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\UserCompany')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\CompanyEndpoint')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\SystemLog')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\User')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\Company')->execute();
    }




}