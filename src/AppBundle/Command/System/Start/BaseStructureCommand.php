<?php

namespace AppBundle\Command\System\Start;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Company;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\Product;
use AppBundle\Operations\OperationProcess;
use AppBundle\Repository\StructureRepository;
use AppBundle\Tree\Tree;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BaseStructureCommand extends BaseFixturesCommand {
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var StructureRepository
     */
    private $repo;

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("BaseStructure")
            ->setAliases(array("startCommand02"))
            // the short description shown while running "php bin/console list"
            ->setDescription('Create All the Structure and permissions for basic system.')

            // Arguments
            ->addArgument("cleanBefore",InputArgument::OPTIONAL,"Define if the command Clean de database Before execute",false)

        ;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "Create Structure";
        $output->writeln([
            "===========================================================================",
            $name,
            "",
        ]);
        if($input->getArgument("cleanBefore")){
            $output->writeln("Clean");
            $this->clearTables();

        }
        $this->em = $this->getEntityManager();
        $this->repo = $this->getStructureRepository();
        $structure = $this->getStructure();
        foreach($structure as $st){
            $this->safeStructure($st);
        }
        $output->writeln([
            "",
            $name." End",
            "===========================================================================",
        ]);
    }
    /**
     * @return StructureRepository
     */
    protected function getStructureRepository(){
        return $this->getEntityManager()->getRepository('AppBundle:Structure');
    }
    /**
     * @param $st
     * @param int $l
     * @param int $r
     */
    private function safeStructure($st, $l = null, $r = null){

        /**
         * @var $sm Tree
         */
        $sm = $this->getContainer()->get('appbundle.tree.structure');
        $em = $this->em;
        $structureRepo = $this->repo;
        $node =  new Structure();
        $node->setDescription($st['description']);
        $node->setName($st['name']);
        if(isset($st["uriName"]))
            $node->setUriName($st["uriName"]);
        if(!$stResponse = $structureRepo->findAll()){
            $node->setL(1);
            $node->setR(2);
        }elseif(!$l || !$r){
            $main = $sm->showMain($stResponse);
            $last = end($main);
            $newnode = $sm->addOne($last['node']->getL(),$last['node']->getR(),'right');
            $node->setL($newnode['l']);
            $node->setR($newnode['r']);
        }else{
            $newnode = $sm->addOne($l,$r,'subr');
            $node->setL($newnode['l']);
            $node->setR($newnode['r']);
        }
        $em->persist($node);
        $em->flush();
        var_dump($node->getName());
        if(isset($st['endpoint'])) {
            if (is_array($st['endpoint'])) {
                foreach ($st['endpoint'] as $ep) {
                    $structure_endpoint = new StructureEndpoint();
                    if(is_array($ep)){
                        $structure_endpoint->setEndpoint($this->getEndpointByName($ep[0]));
                        $structure_endpoint->setPermission($ep[1]);
                    }else{
                        $structure_endpoint->setEndpoint($this->getEndpointByName($ep));
                        $structure_endpoint->setPermission(15);
                    }
                    $structure_endpoint->setStructure($node);
                    $structure_endpoint->setShowStructure(true);
                    $em->persist($structure_endpoint);
                    $em->flush();
                    var_dump("      ".$structure_endpoint->getEndpointName());
                }
            }
        }
        if(is_array($st['children'])){
            foreach($st['children'] as $ch){
                $this->safeStructure($ch,$node->getL(),$node->getR());
                $em->refresh($node);
            }
        }
    }
    /**
     * @param string $name
     * @return null|object
     */
    protected function  getEndpointByName($name){
        return $this->getEndpointRepository()->findOneBy(array("uriName"=>$name));
    }
    /**
     * @return array
     */
    protected function getStructure(){
        $structure = array(
            array(
                "name"=> "Accounting",
                "description"=> "accounting module",
                "uriName"=> "/accounting/accounting_entry",
                "children"=>
                    array(
                        array(
                            "name"=> "Accounting Entry",
                            "description"=> "",
                            "uriName"=> "accounting_entry",
                            "endpoint"=>array(
                                array("ACCOUNTING_ENTRY",15)
                            ),
                            "children"=> ""
                        ),
                        array(
                            "name"=> "Accounts",
                            "description"=> "",
                            "children"=> array(
                                array(
                                    "name"=> "Create",
                                    "description"=> "",
                                    "uriName"=> "create",
                                    "endpoint"=>array(
                                        array("ACCOUNT",15)
                                    ),
                                    "children"=> ""
                                ),
                                array(
                                    "name"=> "Chart",
                                    "description"=> "",
                                    "uriName"=> "chart",
                                    "endpoint"=>array(
                                        array("ACCOUNT",15)
                                    ),
                                    "children"=> ""
                                )
                            )
                        ),
                        array(
                            "name"=> "Reports",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "Balance Sheet",
                                        "description"=> "",
                                        "uriName"=> "balanceSheet",
                                        "endpoint"=>array(
                                            array("ACCOUNTING_REPORT",15)
                                        ),"children"=> ""
                                    ),
                                    array(
                                        "name"=> "General Ledger",
                                        "description"=> "",
                                        "uriName"=> "generalLedger",
                                        "endpoint"=>array(
                                            array("ACCOUNTING_REPORT",15)
                                        ),"children"=> ""
                                    ),
                                    array(
                                        "name"=> "Income Statement",
                                        "description"=> "",
                                        "uriName"=> "incomeStatement",
                                        "endpoint"=>array(
                                            array("ACCOUNTING_REPORT",15)
                                        ),"children"=> ""
                                    ),
                                    array(
                                        "name"=> "Journal Entry",
                                        "description"=> "",
                                        "uriName"=> "journalEntry",
                                        "endpoint"=>array(
                                            array("ACCOUNTING_REPORT",15)
                                        ),"children"=> ""
                                    )
                                )
                        )
                    )
            ),
            array(
                "name"=> "System",
                "description"=> "System parametrization of all module",
                "uriName"=> "/system/clients/view",
                "children"=>
                    array(
                        array(
                            "name"=> "Users",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "New",
                                        "description"=> "",
                                        "uriName"=> "new",
                                        "endpoint"=>array(
                                            array("USER",15)
                                        ),
                                        "children"=> ""
                                    ),
                                    array(
                                        "name"=> "View/Edit",
                                        "description"=> "",
                                        "uriName"=> "view",
                                        "endpoint"=>array(
                                            array("USER",15)
                                        ),
                                        "children"=> ""
                                    )
                                )
                        ),

                        array(
                            "name"=> "Clients",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "New",
                                        "description"=> "",
                                        "uriName"=> "new",
                                        "endpoint"=>array(
                                            array("CLIENT",15)
                                        ),
                                        "children"=> ""
                                    ),
                                    array(
                                        "name"=> "View/Edit",
                                        "description"=> "",
                                        "uriName"=> "view",
                                        "endpoint"=>array(
                                            array("CLIENT",15)
                                        ),
                                        "children"=> ""
                                    )
                                )
                        )
                    )
            ),
            array(
                "name"=> "Banking",
                "description"=> "Banking module",
                "uriName"=> "/banking/accounts",
                "children"=>
                    array(
                        array(
                            "name"=> "Banks",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "Create Bank",
                                        "description"=> "",
                                        "uriName"=> "bankCreate",
                                        "endpoint"=>array(
                                            array("BANK",15)
                                        ),
                                        "children"=> ""
                                    ),
                                    array(
                                        "name"=> "Create Account",
                                        "description"=> "",
                                        "uriName"=> "accountCreate",
                                        "endpoint"=>array(
                                            array("BANK",15)
                                        ),
                                        "children"=> ""
                                    )
                                )
                        ),

                        array(
                            "name"=> "Activities",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "Create Memo",
                                        "description"=> "",
                                        "uriName"=> "transactions",
                                        "endpoint"=>array(
                                            array("BANK",15)
                                        ),
                                        "children"=> ""
                                    )
                                )
                        ),
                        array(
                            "name"=> "Accounts",
                            "description"=> "",
                            "uriName"=> "accounts",
                            "children"=>""
                        )
                    )
            ),
            array(
                "name"=> "Operations",
                "description"=> "",
                "uriName"=> "/operations/activities/view",
                "children"=>
                    array(
                        array(
                            "name"=> "Activities",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "Create Operation",
                                        "uriName"=> "create",
                                        "endpoint"=>array(
                                            array("OPERATION",15)
                                        ),
                                        "description"=> "",
                                        "children"=>""
                                    ),
                                    array(
                                        "name"=> "View Operations",
                                        "uriName"=> "view",
                                        "endpoint"=>array(
                                            array("OPERATION",15)
                                        ),
                                        "description"=> "",
                                        "children"=>""
                                    )
                                )
                        ),
                        array(
                            "name"=> "Products",
                            "uriName"=> "products",
                            "endpoint"=>array(
                                array("PRODUCT",15)
                            ),
                            "description"=> "",
                            "children"=>""
                        ),
                        array(
                            "name"=> "Reports",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "Client Statement",
                                        "uriName"=> "clientStatement",
                                        "endpoint"=>array(
                                            array("OPERATION",15)
                                        ),
                                        "description"=> "",
                                        "children"=> ""
                                    ),
                                    array(
                                        "name"=> "Funding Statement",
                                        "uriName"=> "fundingStatement",
                                        "endpoint"=>array(
                                            array("OPERATION",15)
                                        ),
                                        "description"=> "",
                                        "children"=> ""
                                    )

                                )

                        )
                    )
            ),
            array(
                "name"=> "Client Statement",
                "description"=> "Client Reports",
                "uriName"=> "/system/clients/view",
                "children"=>
                    array(
                        array(
                            "name"=> "Client Statement",
                            "description"=> "",
                            "uriName"=> "view",
                            "endpoint"=>array(
                                array("CLIENT_REPORT_1",1)
                            ),
                            "children"=> ""
                        ),
                        array(
                            "name"=> "View/Edit",
                            "description"=> "",
                            "uriName"=> "view",
                            "endpoint"=>array(
                                array("USER",15)
                            ),
                            "children"=> ""
                        )
                    )
            )
        );
        return $structure;
    }

    protected function clearTables(){
        $em = $this->getEntityManager();
        //get the Account manager for selected company
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        //delete all records before start de upload
        $em->createQuery('DELETE FROM AppBundle\\Entity\\StructureEndpoint')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\Structure')->execute();
    }



}