<?php

namespace AppBundle\Command\System\Start;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Address;
use AppBundle\Entity\Attribute;
use AppBundle\Entity\Bank;
use AppBundle\Entity\BankAccountType;
use AppBundle\Entity\BankMoveType;
use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Person;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\Product;
use AppBundle\Operations\OperationProcess;
use AppBundle\Repository\StructureRepository;
use AppBundle\Tree\Tree;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BanksCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("createBanksCommand")
            ->setAliases(array("startCommand05"))
            // the short description shown while running "php bin/console list"
            ->setDescription('Load Banks and BankMove Type from files to both tables.')

            // Arguments
            ->addArgument("cleanBefore",InputArgument::OPTIONAL,"Define if the command Clean de database Before execute",false)

        ;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "Bank and BankMoveType";
        $output->writeln([
            "===========================================================================",
            $name,
            "",
        ]);
        if($input->getArgument("cleanBefore")){
            $output->writeln("Clean");
            $this->clearTables();

        }
        $output->writeln("====== BANKS");
        $banksInfo = $this->getBanks();
        foreach ($banksInfo as $bankInfo){
            $this->createBank($bankInfo);
        }
        $output->writeln("====== MOVE TYPES");
        $bankMoveTypesInfo = $this->getBankMoveTypes();
        foreach ($bankMoveTypesInfo as $bankMoveTypeInfo){
            $this->createBankMoveType($bankMoveTypeInfo);
        }
        $output->writeln("====== ACCOUNT TYPE");
        $accountTypesInfo = $this->getAccountTypes();
        foreach ($accountTypesInfo as $accountTypeInfo){
            $this->createAccountType($accountTypeInfo);
        }

        $output->writeln([
            "",
            $name." END",
            "===========================================================================",
        ]);
    }


    private function createAccountType($accountTypeInfo)
    {
        $accountType = new BankAccountType();
        $accountType->setName($accountTypeInfo["name"]);
        $em = $this->getEntityManager();
        $em->persist($accountType);
        $em->flush();
        var_dump($accountType->getName());
        return $accountType;
    }

    private function createBankMoveType($bankMoveInfo)
    {
        $bankMove = new BankMoveType();
        $bankMove->setName($bankMoveInfo["name"]);
        $bankMove->setDescription($bankMoveInfo["description"]);
        $bankMove->setNature($bankMoveInfo["nature"]);
        $em = $this->getEntityManager();
        $em->persist($bankMove);
        $em->flush();
        var_dump($bankMove->getName());
        return $bankMove;
    }
    private function createBank($bankInfo)
    {
        $bank = new Bank();
        $person = new Person();
        $person->setName($bankInfo["personName"]);
        $person->setLastname($bankInfo["personLastname"]);
        $address = new Address();
        $address->setAddress($bankInfo["direction"]);
        $bank->setName($bankInfo["name"]);
        $bank->setPhone($bankInfo["phone"]);
        $bank->setAddress($address);
        $bank->setPerson($person);
        $em = $this->getEntityManager();
        $em->persist($bank);
        $em->flush();
        var_dump($bank->getName());

        return $bank;
    }
    
    /**
     * @return array
     */
    protected function getBanks(){
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Start/startFiles/banks.csv', 'r');
        $banks = array();
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if ($line[0] == "bankName")
                continue;
            $banks[$c]['name'] = $line[0];
            $banks[$c]['phone'] = $line[1];
            $banks[$c]['direction'] = $line[2];
            $banks[$c]['personName'] = $line[3];
            $banks[$c]['personLastname'] = $line[4];
        }
        return $banks;
    }
    /**
     * @return array
     */
    protected function getBankMoveTypes(){
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Start/startFiles/bankMoveTypes.csv', 'r');
        $banks = array();
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if ($line[0] == "moveName")
                continue;
            $banks[$c]['name'] = $line[0];
            $banks[$c]['description'] = $line[1];
            $banks[$c]['nature'] = $line[2];
        }
        return $banks;
    }
    /**
     * @return array
     */
    protected function getAccountTypes(){
        //open file
        $file = fopen( getcwd().'/src/AppBundle/Command/System/Start/startFiles/bankAccountTypes.csv', 'r');
        $accountTypes = array();
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if ($line[0] == "accountType")
                continue;
            $accountTypes[$c]['name'] = $line[0];
        }
        return $accountTypes;
    }
    
    
    /**
     * @param string $name
     * @return null|object
     */
    protected function  getEndpointByName($name){
        return $this->getEndpointRepository()->findOneBy(array("uriName"=>$name));
    }
    /**
     * @return array
     */
    protected function getCompanies(){
        return $this->getCompanyRepository()->findAll();
    }
    protected function clearTables(){
        $companies = $this->getCompanies();
        /**
         * @var $company Company
         */
        foreach ($companies as $company){
            $em = $this->getEntityManagerCompany($company->getConnection());
            $em->getConnection()->getConfiguration()->setSQLLogger(null);
            //delete all records before start de upload
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\BankMove')->execute();
            $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\BankAccount')->execute();
        }
        $em = $this->getEntityManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $em->createQuery('DELETE FROM AppBundle\\Entity\\Bank')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\BankMoveType')->execute();
    }




}