<?php

namespace AppBundle\Command\System\Start;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Attribute;
use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Person;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCompany;
use AppBundle\Entity\UserCompanyEndpoint;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\Product;
use AppBundle\Operations\OperationProcess;
use AppBundle\Repository\StructureRepository;
use AppBundle\Tree\Tree;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class FullRestartCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("FullRestartCommand")
            // the short description shown while running "php bin/console list"
            ->setDescription('Truncate All Tables')

        ;
    }


    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "FULL RESTART OF THE SYSTEM";
        $output->writeln([
            "===========================================================================",
            "                      ".$name,
            "===========================================================================",
        ]);
        $output->writeln("<error>You Are about to delete all data in All data bases");
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion("Are your sure you want to continue?(Y/N)</error>", false);

        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                "===========================================================================",
                "END WITHOUT DO NOTHING ",
                "===========================================================================",
            ]);
            return false;

        }
        $this->preRestartCompanies();
        $inputCommands = new ArrayInput(array());
        $this->getApplication()->find("startCommand00")->run($inputCommands,$output);
        $this->getApplication()->find("startCommand01")->run($inputCommands,$output);
        $this->getApplication()->find("startCommand02")->run($inputCommands,$output);
        $this->getApplication()->find("startCommand03")->run($inputCommands,$output);
        $this->getApplication()->find("startCommand04")->run($inputCommands,$output);
        $this->getApplication()->find("startCommand05")->run($inputCommands,$output);
        $companies = $this->getCompanyRepository()->findAll();
        $userAdmin = $this->createUser("cyberlord",'$2a$08$Y1J2TZ6KMp7pA4MYmq4oW.S.RN8E9dXqFTUHVQtCWyTTnU37akcrW',$companies);
        $output->writeln("User admin for all companyes : cyberlord");

        $output->writeln([
            "===========================================================================",
            "END                    ".$name,
            "===========================================================================",
        ]);
    }

    protected function getCompanies(){
        return $this->getCompanyRepository()->findAll();
    }

    protected function preRestartCompanies(){
        $output = new NullOutput();
        $argumentsCompanies = array(
            'command' => 'createCompaniesCommand',
            'cleanBefore'    => true,
            'createEndpoints'    => false,
        );
        $companiesInput = new ArrayInput($argumentsCompanies);
        $this->getApplication()->find('createCompaniesCommand')->run($companiesInput,$output);

    }

    /**
     * @param string $name
     * @param string $password
     * @param ArrayCollection $companies
     * @return User
     */
    private function createUser($name, $password = '$2a$08$Y1J2TZ6KMp7pA4MYmq4oW.S.RN8E9dXqFTUHVQtCWyTTnU37akcrW', $companies,$personName = "Ronald",$personLastname = "Martinez"){
        $em = $this->getEntityManager();
        $userAdmin = new User();
        $userAdmin->setUsername($name);
        $userAdmin->setPassword($password);
        $userCompanies = new ArrayCollection();
        $uce =  new ArrayCollection();
        $default = true;
        /**
         * @var $company Company
         */
        foreach ($companies as $company ) {
            $userCompany =  new UserCompany();
            var_dump($company->getName());
            $userCompany->setCompany($company);
            $userCompany->setUser($userAdmin);
            $userCompany->setDefault($default);
            var_dump($userCompany->getCompany()->getId());
//            $em->persist($userCompany);
            $userCompanies[] = $userCompany;
            $default = false;


            foreach($company->getEndpoints() as $endpoint){
                $uc = new UserCompanyEndpoint();
                $uc->setUser($userAdmin);
                $uc->setCompanyEndpoint($endpoint);
                $uc->setPermission($endpoint->getPermission());
                $uce[] = $uc;
                var_dump($userCompany->getCompany()->getId() ." -> ".$endpoint->getEndpoint()->getUriName());


            }
        }
        $userAdmin->setUserCompanyEndpoints($uce);
        $userAdmin->setCompanies($userCompanies);
        $person =  new Person();
        $person->setName($personName);
        $person->setLastname($personLastname);
        $userAdmin->setPerson($person);
        $em->persist($userAdmin);
        $em->flush();
        return $userAdmin;
    }




}