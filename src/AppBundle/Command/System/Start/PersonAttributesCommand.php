<?php

namespace AppBundle\Command\System\Start;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Attribute;
use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\Product;
use AppBundle\Operations\OperationProcess;
use AppBundle\Repository\StructureRepository;
use AppBundle\Tree\Tree;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PersonAttributesCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("createPersonAttributesCommand")
            ->setAliases(array("startCommand04"))
            // the short description shown while running "php bin/console list"
            ->setDescription('Create load person attributes from file to both tables.')

            // Arguments
            ->addArgument("cleanBefore",InputArgument::OPTIONAL,"Define if the command Clean de database Before execute",false)
            ->addArgument("bothTables",InputArgument::OPTIONAL,"Define if the command do both tables or just main one",true)

        ;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "Create Attributes";
        $output->writeln([
            "===========================================================================",
            $name,
            "",
        ]);
        $both = $input->getArgument("bothTables");
        if($input->getArgument("cleanBefore")){
            $output->writeln("Clean");
            $this->clearTables($both);

        }
        $attributesInfo = $this->getAttributes();
        foreach ($attributesInfo as $attributeInfo){
            $this->createAttribute($attributeInfo,$both);
        }

        $output->writeln([
            "",
            $name." END",
            "===========================================================================",
        ]);
    }


    private function createAttribute($attributeInfo,$createAttributeCompanies = false)
    {
        $attribute = new Attribute();
        $attribute->setName($attributeInfo["name"]);
        $attribute->setType($attributeInfo["type"]);
        $em = $this->getEntityManager();
        $em->persist($attribute);
        $em->flush();
        var_dump($attributeInfo["name"]);
        if($createAttributeCompanies){
            $companies = $this->getCompanies();
            foreach($companies as $company )
                $this->createCompanyAttributes($company,$attributeInfo);

        }
        return $attribute;
    }

    /**
     * @param string $name
     * @return null|object
     */
    protected function  getEndpointByName($name){
        return $this->getEndpointRepository()->findOneBy(array("uriName"=>$name));
    }
    /**
     * @return array
     */
    protected function getCompanies(){
        return $this->getCompanyRepository()->findAll();
    }


    protected function clearTables($both){
        $em = $this->getEntityManager();
        //get the Account manager for selected company
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        //delete all records before start de upload
        $em->createQuery('DELETE FROM AppBundle\\Entity\\PersonAttribute')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\Attribute')->execute();
        if($both){
            $companies = $this->getCompanies();
            /**
             * @var $company Company
             */
            foreach ($companies as $company){
                $em = $this->getEntityManagerCompany($company->getConnection());
                $em->getConnection()->getConfiguration()->setSQLLogger(null);
                //delete all records before start de upload
                $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\PersonAttribute')->execute();
                $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\Attribute')->execute();
            }
        }
    }




}