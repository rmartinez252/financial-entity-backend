<?php

namespace AppBundle\Command\System\Start;

use AppBundle\Command\BaseFixturesCommand;
use AppBundle\Entity\Company;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\EntityCompanies\Operation;
use AppBundle\EntityCompanies\Product;
use AppBundle\Operations\OperationProcess;
use AppBundle\Repository\StructureRepository;
use AppBundle\Tree\Tree;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EndpointCommand extends BaseFixturesCommand {
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName("EndpointCommand")
            ->setAliases(array("startCommand01"))

            // the short description shown while running "php bin/console list"
            ->setDescription('Create All the Structure and permissions for basic system.')

            // Arguments
            ->addArgument("cleanBefore",InputArgument::OPTIONAL,"Define if the command Clean de database Before execute",false)

        ;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = "Create Endpoints";
        $output->writeln([
            "===========================================================================",
            $name,
            "",
        ]);

        if($input->getArgument("cleanBefore")) {
            $output->writeln("Clean");
            $this->clearTables();

        }

        $this->createEndpoint('ACCOUNTING_ENTRY',15);
        $this->createEndpoint('STRUCTURE',15);
        $this->createEndpoint('ACCOUNT',15);
        $this->createEndpoint('ACCOUNTING',15);
        $this->createEndpoint('BANK',15);
        $this->createEndpoint('PRODUCT',15);
        //FALTA ESPESIFUCAR UNO POR REPORTE
        $this->createEndpoint('ACCOUNTING_REPORT',1);
        $this->createEndpoint('PARAMETRIZATION_PRODUCT',15);
        $this->createEndpoint('PARAMETRIZATION_ACCOUNTS',15);
        $this->createEndpoint('OPERATION',15);
        $this->createEndpoint('USER',15);
        $this->createEndpoint('CLIENT',15);
        $this->createEndpoint('CLIENT_REPORT_1',1);
        $output->writeln([
            "",
            $name." END",
            "===========================================================================",
        ]);
    }

    /**
     * @param string $name
     * @param int $permission
     * @return Endpoint
     */
    protected function createEndpoint($name, $permission = 15)
    {
        $endpoint = new Endpoint();
        $endpoint->setUriName($name);
        $endpoint->setPermission($permission);
        $endpoint->setDescription('Description '.$name);
        $em = $this->getEntityManager();
        $em->persist($endpoint);
        $em->flush();
        var_dump($name);
        return $endpoint;
    }
    protected function clearTables(){
        $em = $this->getEntityManager();
        //get the Account manager for selected company
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        //delete all records before start de upload
        $em->createQuery('DELETE FROM AppBundle\\Entity\\StructureEndpoint')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\UserCompanyEndpoint')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\CompanyEndpoint')->execute();
        $em->createQuery('DELETE FROM AppBundle\\Entity\\Endpoint')->execute();
    }



}