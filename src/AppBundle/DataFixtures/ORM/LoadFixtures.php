<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\AppBundle;
use AppBundle\Entity\Attribute;
use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Person;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\Entity\UserCompanyEndpoint;
use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Entry;
use AppBundle\Repository\StructureRepository;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadFixtures implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function load(ObjectManager $manager = NULL)
    {

        $this->createEndpoint('ACCOUNTING_ENTRY',15);
        //FALTA ESPESIFUCAR UNO POR REPORTE
        $this->createEndpoint('ACCOUNTING_REPORT',1);
        $this->createEndpoint('PARAMETRIZATION_PRODUCT',15);
        $this->createEndpoint('PARAMETRIZATION_ACCOUNTS',15);
        $this->createEndpoint('OPERATION_LENDING',15);
        $this->createEndpoint('USERS',15);
        $this->createEndpoint('CLIENTS',15);
        $this->createEndpoint('BANK',15);
        $structure = array(
            array(
                "name"=> "Accounting",
                "description"=> "accounting module",
                "children"=>
                    array(
                        array(
                            "name"=> "Accounting Entry",
                            "description"=> "",
                            "uriName"=> "accounting_entry",
                            "endpoint"=>array(
                                array("ACCOUNTING_ENTRY",15)
                            ),
                            "children"=> ""
                        ),
                        array(
                            "name"=> "Accounts",
                            "description"=> "",
                            "children"=> array(
                                array(
                                    "name"=> "Create",
                                    "description"=> "",
                                    "uriName"=> "create",
                                    "endpoint"=>array(
                                        array("ACCOUNTING_ENTRY",15)
                                    ),
                                    "children"=> ""
                                ),
                                array(
                                    "name"=> "Chart",
                                    "description"=> "",
                                    "uriName"=> "chart",
                                    "endpoint"=>array(
                                        array("ACCOUNTING_ENTRY",15)
                                    ),
                                    "children"=> ""
                                )
                            )
                        ),
                        array(
                            "name"=> "Reports",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "Balance Sheet",
                                        "description"=> "",
                                        "uriName"=> "balanceSheet",
                                        "endpoint"=>array(
                                            array("ACCOUNTING_REPORT",15)
                                        ),"children"=> ""
                                    ),
                                    array(
                                        "name"=> "General Ledger",
                                        "description"=> "",
                                        "uriName"=> "generalLedger",
                                        "endpoint"=>array(
                                            array("ACCOUNTING_REPORT",15)
                                        ),"children"=> ""
                                    ),
                                    array(
                                        "name"=> "Income Statement",
                                        "description"=> "",
                                        "uriName"=> "incomeStatement",
                                        "endpoint"=>array(
                                            array("ACCOUNTING_REPORT",15)
                                        ),"children"=> ""
                                    )
                                )
                        )
                    )
            ),
            array(
                "name"=> "System",
                "description"=> "System parameterization of accounting module",
                "children"=>
                    array(
                        array(
                            "name"=> "Users",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "New",
                                        "description"=> "",
                                        "uriName"=> "new",
                                        "endpoint"=>array(
                                            array("USERS",15)
                                        ),
                                        "children"=> ""
                                    ),
                                    array(
                                        "name"=> "View/Edit",
                                        "description"=> "",
                                        "uriName"=> "view",
                                        "endpoint"=>array(
                                            array("USERS",15)
                                        ),
                                        "children"=> ""
                                    )
                                )
                        ),

                        array(
                            "name"=> "Clients",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "New",
                                        "description"=> "",
                                        "uriName"=> "new",
                                        "endpoint"=>array(
                                            array("CLIENTS",15)
                                        ),
                                        "children"=> ""
                                    ),
                                    array(
                                        "name"=> "View/Edit",
                                        "description"=> "",
                                        "uriName"=> "view",
                                        "endpoint"=>array(
                                            array("CLIENTS",15)
                                        ),
                                        "children"=> ""
                                    )
                                )
                        )
                    )
            ),
            array(
                "name"=> "Banking",
                "description"=> "Banking module",
                "children"=>
                    array(
                        array(
                            "name"=> "Banks",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "Create Bank",
                                        "description"=> "",
                                        "uriName"=> "bankCreate",
                                        "endpoint"=>array(
                                            array("BANK",15)
                                        ),
                                        "children"=> ""
                                    ),
                                    array(
                                        "name"=> "Create Account",
                                        "description"=> "",
                                        "uriName"=> "accountCreate",
                                        "endpoint"=>array(
                                            array("BANK",15)
                                        ),
                                        "children"=> ""
                                    )
                                )
                        ),

                        array(
                            "name"=> "Activities",
                            "description"=> "",
                            "children"=>
                                array(
                                    array(
                                        "name"=> "Create Memo",
                                        "description"=> "",
                                        "uriName"=> "",
                                        "endpoint"=>array(
                                            array("BANK",15)
                                        ),
                                        "children"=> ""
                                    )
                                )
                        )
                    )
            )
//            array(
//                "name"=> "Operations",
//                "description"=> "",
//                "children"=>
//                    array(
//                        array(
//                            "name"=> "Activities",
//                            "description"=> "",
//                            "children"=>
//                                array(
//                                    array(
//                                        "name"=> "Start",
//                                        "description"=> "",
//                                        "children"=>
//                                            array(
//                                                array(
//                                                    "name"=> "New",
//                                                    "uriName"=> "lending_new",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "Approve",
//                                                    "uriName"=> "lending_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "Insert payment",
//                                                    "uriName"=> "lending_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "View",
//                                                    "uriName"=> "lending_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "Liquidate",
//                                                    "uriName"=> "lending_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "Void",
//                                                    "uriName"=> "lending_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                )
//                                            )
//                                    ),
//                                    array(
//                                        "name"=> "Funding",
//                                        "description"=> "",
//                                        "children"=>
//                                            array(
//                                                array(
//                                                    "name"=> "New",
//                                                    "uriName"=> "funding_new",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "Approve",
//                                                    "uriName"=> "funding_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "Insert payment",
//                                                    "uriName"=> "funding_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "View",
//                                                    "uriName"=> "funding_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "Liquidate",
//                                                    "uriName"=> "funding_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                ),
//                                                array(
//                                                    "name"=> "Void",
//                                                    "uriName"=> "funding_search",
//                                                    "description"=> "",
//                                                    "children"=> ""
//                                                )
//                                            )
//                                    )
//                                )
//                        ),
//                        array(
//                            "name"=> "Reports",
//                            "description"=> "",
//                            "children"=>
//                                array(
//                                    array(
//                                        "name"=> "Report01",
//                                        "uriName"=> "operations_report",
//                                        "description"=> "",
//                                        "children"=> ""
//                                    ),
//                                    array(
//                                        "name"=> "Report02",
//                                        "uriName"=> "operations_report",
//                                        "description"=> "",
//                                        "children"=> ""
//                                    ),
//                                    array(
//                                        "name"=> "Report03",
//                                        "uriName"=> "operations_report",
//                                        "description"=> "",
//                                        "children"=> ""
//                                    )
//                                )
//
//                        )
//                    )
//            ),
//            array(
//                "name" =>"System",
//                "description"=>"",
//                "children"=>
//                    array(
//                        array(
//                            "name"=> "Users",
//                            "description"=> "",
//                            "children"=>
//                                array(
//                                    array(
//                                        "name"=> "New",
//                                        "description"=> "",
//                                        "uriName"=> "users_form",
//                                        "endpoint"=>array(
//                                            array("USERS",3)
//                                        ),"children"=> ""
//                                    ),
//                                    array(
//                                        "name"=> "View",
//                                        "description"=> "",
//                                        "uriName"=> "users_view",
//                                        "endpoint"=>array(
//                                            array("USERS",1)
//                                        ),"children"=> ""
//                                    ),
//                                    array(
//                                        "name"=> "Edit",
//                                        "description"=> "",
//                                        "uriName"=> "users_view",
//                                        "endpoint"=>array(
//                                            array("USERS",5)
//                                        ),"children"=> ""
//                                    ),
//                                    array(
//                                        "name"=> "Delete",
//                                        "description"=> "",
//                                        "uriName"=> "users_view",
//                                        "endpoint"=>array(
//                                            array("USERS",9)
//                                        ),"children"=> ""
//                                    )
//                                )
//                        ),
//                        array(
//                            "name"=> "Clients",
//                            "description"=> "",
//                            "children"=>
//                                array(
//                                    array(
//                                        "name"=> "New",
//                                        "description"=> "",
//                                        "uriName"=> "clients_form",
//                                        "endpoint"=>array(
//                                            array("CLIENTS",3)
//                                        ),"children"=> ""
//                                    ),
//                                    array(
//                                        "name"=> "View",
//                                        "description"=> "",
//                                        "uriName"=> "clients_view",
//                                        "endpoint"=>array(
//                                            array("CLIENTS",1)
//                                        ),"children"=> ""
//                                    ),
//                                    array(
//                                        "name"=> "Edit",
//                                        "description"=> "",
//                                        "uriName"=> "clients_view",
//                                        "endpoint"=>array(
//                                            array("CLIENTS",5)
//                                        ),"children"=> ""
//                                    ),
//                                    array(
//                                        "name"=> "Delete",
//                                        "description"=> "",
//                                        "uriName"=> "clients_view",
//                                        "endpoint"=>array(
//                                            array("CLIENTS",9)
//                                        ),"children"=> ""
//                                    )
//                                )
//                        )
//                    )
//            )
        );
        foreach($structure as $st){
            $this->safeStructure($st);
        }
        $company = $this->createCompany("TCG Capital");
        $this->createAttribute("Work phone","phone");
        $this->createAttribute("cell phone","phone");
//        $this->createAccounts($company,200);
        $userAdmin = $this->createUser("cyberlord",'$2a$08$Y1J2TZ6KMp7pA4MYmq4oW.S.RN8E9dXqFTUHVQtCWyTTnU37akcrW',$company);
        $userAdmin2 = $this->createUser("jczurita",'$2a$08$Y1J2TZ6KMp7pA4MYmq4oW.S.RN8E9dXqFTUHVQtCWyTTnU37akcrW',$company,"Juan Carlos","Zurita");
        $this->setAllPermissions($userAdmin,$company);
        $this->setAllPermissions($userAdmin2,$company);
        $this->createAccountEntries($company,$userAdmin);

        foreach($company->getEndpoints() as $companyEndpoint){
            if($companyEndpoint->getEndpoint()->getUriName() == "PARAMETRIZATION_PRODUCT")
                $this->setPermission($userAdmin,$companyEndpoint,5);
            if($companyEndpoint->getEndpoint()->getUriName() == "ACCOUNTING_ENTRY")
                $this->setPermission($userAdmin,$companyEndpoint,15);
            if($companyEndpoint->getEndpoint()->getUriName() == "ACCOUNTING_REPORT")
                $this->setPermission($userAdmin,$companyEndpoint,15);
        }
    }

    /**
     * @param $st
     * @param int $l
     * @param int $r
     */
    private function safeStructure($st, $l = null, $r = null){

        /**
         * @var $sm \AppBundle\Tree\Tree
         * @var $structureRepo StructureRepository
         */
        $sm = $this->container->get('appbundle.tree.structure');
        $em = $this->getEntityManager();
        $structureRepo = $em->getRepository('AppBundle:Structure');
        $node =  new Structure();
        $node->setDescription($st['description']);
        $node->setName($st['name']);
        if(isset($st["uriName"]))
            $node->setUriName($st["uriName"]);
        if(!$stResponse = $structureRepo->findAll()){
            $node->setL(1);
            $node->setR(2);
        }elseif(!$l || !$r){
            $main = $sm->showMain($stResponse);
            $last = end($main);
            $newnode = $sm->addOne($last['node']->getL(),$last['node']->getR(),'right');
            $node->setL($newnode['l']);
            $node->setR($newnode['r']);
        }else{
            $newnode = $sm->addOne($l,$r,'subr');
            $node->setL($newnode['l']);
            $node->setR($newnode['r']);
        }

        $em->persist($node);
        $em->flush();
        if(isset($st['endpoint'])) {
            if (is_array($st['endpoint'])) {
                foreach ($st['endpoint'] as $ep) {
                    $structure_endpoint = new StructureEndpoint();
                    if(is_array($ep)){
                        $structure_endpoint->setEndpoint($this->getEndpointByName($ep[0]));
                        $structure_endpoint->setPermission($ep[1]);
                    }else{
                        $structure_endpoint->setEndpoint($this->getEndpointByName($ep));
                        $structure_endpoint->setPermission(15);
                    }
                    $structure_endpoint->setStructure($node);
                    $structure_endpoint->setShowStructure(true);
                    $em->persist($structure_endpoint);
                    $em->flush();
                }
            }
        }
        if(is_array($st['children'])){
            foreach($st['children'] as $ch){
                $this->safeStructure($ch,$node->getL(),$node->getR());
                $em->refresh($node);
            }
        }
    }

    /**
     * @param string $name
     * @param int $permission
     * @return Endpoint
     */
    private function createEndpoint($name, $permission =15)
    {
        $endpoint = new Endpoint();
        $endpoint->setUriName($name);
        $endpoint->setPermission($permission);
        $endpoint->setDescription('Description '.$name);
        $em = $this->getEntityManager();
        $em->persist($endpoint);
        $em->flush();
        return $endpoint;
    }

    /**
     * @param string $companyName
     * @param string $direction
     * @param string $email
     * @param string $phone
     * @return Company
     */
    private function createCompany($companyName, $direction='dir', $email='foo@bar.com', $phone='3056761728')
    {
        $company = new Company();
        $company->setName($companyName);
        $company->setDirection($direction);
        $company->setEmail($email);
        $company->setPhone($phone);
        $company->setCloseDate("12-31");
        $em = $this->getEntityManager();
        $em->persist($company);
        $em->flush();
        $endpoints = $this->getEntityManager()->getRepository('AppBundle:Endpoint')->findAll();
        /**
         * @var $ep Endpoint
         */
        foreach ($endpoints as $ep){
            $company_endpoint = new CompanyEndpoint();
            $company_endpoint->setCompany($company);
            $company_endpoint->setEndpoint($ep);
            $company_endpoint->setPermission($ep->getPermission());
            $company->addEndpoint($company_endpoint);
            $em->persist($company_endpoint);
            $em->flush();
        }
        return $company;
    }

    /**
     * @param string $name
     * @param string $password
     * @param Company $company
     * @return User
     */
    private function createUser($name, $password = '$2a$08$Y1J2TZ6KMp7pA4MYmq4oW.S.RN8E9dXqFTUHVQtCWyTTnU37akcrW', Company $company,$personName = "Ronald",$personLastname = "Martinez"){
        $em = $this->getEntityManager();
        $userAdmin = new User();
        $userAdmin->setUsername($name);
        $userAdmin->setPassword($password);
        $userAdmin->setCompany($company);
        $person =  new Person();
        $person->setName($personName);
        $person->setLastname($personLastname);
        $userAdmin->setPerson($person);
        $em->persist($userAdmin);
        $em->flush();
        return $userAdmin;
    }

    /**
     * @param string $name
     * @param $type
     * @return Attribute
     */
    private function createAttribute($name, $type ){
        $em = $this->getEntityManager();
        $attribute = new Attribute();
        $attribute->setName($name);
        $attribute->setType($type);
        $em->persist($attribute);
        $em->flush();
        return $attribute;
    }

    /**
     * @param User $user
     * @param CompanyEndpoint $endpoint
     * @param null $permission
     */
    private function setPermission(User $user, CompanyEndpoint $endpoint, $permission = null){
        $user_company_endpoint = new UserCompanyEndpoint();
        $user_company_endpoint->setUser($user);
        $user_company_endpoint->setCompanyEndpoint($endpoint);
        if(!$permission)
            $permission = $endpoint->getPermission();

        $user_company_endpoint->setPermission($permission);
        $em = $this->getEntityManager();
        $em->persist($user_company_endpoint);
        $em->flush();
    }
    private function setAllPermissions(User $user,Company $company){
        foreach($company->getEndpoints() as $ce)
            $this->setPermission($user,$ce);
    }

    /**
     * @param Company $company
     */
    private function createAccounts(Company $company,$from = false,$to = false){
        //TODO search in company entity to define what company manager and tree to use
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany01();
        //get the Account manager for selected company

        //delete all records before start de upload
//        $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\Entry')->execute();
//        $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\AccountingEntry')->execute();
//        $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\Account')->execute();
//        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $accountRepo = $em->getRepository("AppBundle\\EntityCompanies\\Account");
        //open file
        $file = fopen( getcwd().'/src/AppBundle/DataFixtures/ORM/account.csv', 'r');
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if($from)
                if($c<$from)
                    continue;
            if($to)
                if($c>$to)
                    continue;
            $account = new Account();
            if($line[0] == "codigo")
                continue;
            $histArray = $line[0];
            $histArray = explode('.',$histArray);
            $rest = strlen(end($histArray));
            if(!$rest)
                $rest = strlen(prev($histArray));
            unset($histArray);
            $patent = substr($line[2],0,-$rest);
            $account->setName($line[1]);
            $account->setHistoricCode($line[2]);
            $account->setCode($line[2]);
            $account->setNature($line[6]);
//            print_r("parent:'".$patent."'->");
            if($c == 1){
//                print_r("no parent and no record in accout, put left in 1 and right y 2");
                $account->setL(1);
                $account->setR(2);
            }elseif($patent === ""){
                print_r("\n no parent put in right of the main menu: ".$line[2]."\n");
                $am = $this->container->get('appbundle.tree.account.company01');
                $main = $am->showMain();
//                foreach ($main as $m)
//                    print_r("n:".$m['node']->getName()."\n");
                $last = end($main);
                unset($main);
//                var_dump($last->getName());
//                $em->refresh($last['node']);
                $em->clear();
                $newnode = $am->addOne($last['node']->getL(),$last['node']->getR(),'right');
                $account->setL($newnode['l']);
                $account->setR($newnode['r']);
                var_dump($last['node']->getName()." /L:".$last['node']->getL()." /R:".$last['node']->getR());
                var_dump($newnode['l']);
                var_dump($newnode['r']);
                unset($newnode);

            }else{
//                print_r("search parent");
                $am = $this->container->get('appbundle.tree.account.company01');
                if(!$parentNode = $accountRepo->findOneBy(array("historicCode"=>$patent))){
                    print_r("\n -> no find any record, the account :'".$line[1]."' try to find '".$patent."' and it doesn't exist \n");
//                    var_dump("##############");
//                    var_dump("######################################################################");
//                    var_dump("##############");
//                    var_dump($line);
//                    var_dump("##############");
//                    var_dump("######################################################################");
//                    var_dump("##############");
                    continue;
                }else{
//                    print_r("put in subr form the L and R of parent L:".$parentNode->getL()."//R:".$parentNode->getR()."\n");
                    $newnode = $am->addOne($parentNode->getL(),$parentNode->getR(),'subr');
                    $account->setL($newnode['l']);
                    $account->setR($newnode['r']);
                    $em->refresh($parentNode);
                }
            }
            print_r($c.") :'".$account->getName()."'->L:".$account->getL().", R:".$account->getR()."\n");
            $em->persist($account);
            $em->flush();
            $em->clear();
            if(!$c % 55 ){
                unset($em);
                $em = $this->getEntityManagerCompany01();
            }
            $accountRepo = $em->getRepository("AppBundle\\EntityCompanies\\Account");

        }
        fclose($file);

    }

    /**
     * @param Company $company
     */
    private function createAccountEntries(Company $company,User $user,$from = false,$to = false){
        //TODO search in company entity to define what company manager and tree to use
        //get the Entity manager for selected company
        $em = $this->getEntityManagerCompany01();
        //get the Account manager for selected company
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        //delete all records before start de upload
        $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\Entry')->execute();
        $em->createQuery('DELETE FROM AppBundle\\EntityCompanies\\AccountingEntry')->execute();
//        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $accountEntryRepo = $em->getRepository("AppBundle\\EntityCompanies\\AccountingEntry");
        $accountRepo = $em->getRepository("AppBundle\\EntityCompanies\\Account");
        //open file
        $file = fopen( getcwd().'/src/AppBundle/DataFixtures/ORM/accountentries.csv', 'r');
        $entriesArray = array();
        $accountCode = $account = '';
        for ($c = 0 ;($line = fgetcsv($file)) !== FALSE; $c++) {
            if($from)
                if($c<$from)
                    continue;
            if($to)
                if($c>$to)
                    continue;
            if($line[0] == "fecha" || $line[0] == "" ){
                $accountCode = '';
                continue;
            }
            if(!$accountCode){
                $accountCode = $line[0];
                if(!$account = $accountRepo->findOneBy(array("code"=>$accountCode)))
                    $accountCode = "";
                continue;
            }
            $entriesArray[$line[1]]['date'] = str_replace(array("ago","dic"),array("aug","dec"),$line[0]);
            if($line[4])
                $entriesArray[$line[1]]['debit'][$account->getId()] = $line[4];
            if($line[5])
                $entriesArray[$line[1]]['credit'][$account->getId()] = $line[5];

            print_r($c.") accountId :'".$account->getId()."\n");
            var_dump($entriesArray[$line[1]]);
        }
//        var_dump($am->showAll());
        fclose($file);
        foreach ($entriesArray as $entryCode => $e){
            $accountingEntry = new AccountingEntry();
            $date = \DateTime::createFromFormat('j-M.-Y', $e['date']);
            $accountingEntry->setDateUser($date);
            $accountingEntry->setDescription($entryCode);
            $accountingEntry->setIdUser($user->getId());
            print_r($entryCode.")->".$date->format("Y-m-j")."\n");
            unset($date);
            if(!isset($e['debit']) || !isset($e['credit']))
                var_dump($e);
            else{
                foreach ($e['debit'] as $debitAccountId => $debitValue){
                    foreach ($e['credit'] as $creditAccountId => $creditValue){
                        $entry =  new Entry();
                        $entry->setAccountCredit($accountRepo->findOneBy(array("id" => $creditAccountId)));
                        $entry->setAccountDebit($accountRepo->findOneBy(array("id" => $debitAccountId)));
                        if($debitValue == $creditValue){
                            $entry->setValue($debitValue);
                            unset($e['credit'][$creditAccountId]);
                            unset($e['debit'][$debitAccountId]);
                            $accountingEntry->addEntry($entry);
                            break;
                        }elseif($debitValue > $creditValue){
                            $entry->setValue($creditValue);
                            unset($e['credit'][$creditAccountId]);
                            //resto
                            $e['debit'][$debitAccountId] = $debitValue = $debitValue - $creditValue;
                            $accountingEntry->addEntry($entry);

                        }elseif($debitValue < $creditValue){
                            $entry->setValue($debitValue);
                            unset($e['debit'][$debitAccountId]);
                            //resto
                            $e['credit'][$debitAccountId] = $creditValue = $creditValue - $debitValue;
                            $accountingEntry->addEntry($entry);
                            break;
                        }
//                        $entry->setAccountingEntry($accountingEntry);
                        $accountingEntry->addEntry($entry);
                        unset($entry);
                    }
                }
                $em->persist($accountingEntry);
                $em->flush();
                $em->clear();
                if(!$c % 55 ){
                    $em = $this->getEntityManagerCompany01();
                    $em->getConnection()->getConfiguration()->setSQLLogger(null);
                }
                gc_collect_cycles();
            }
        }

    }


    /**
     * @return EntityManager
     */
    private function getEntityManager(){
        return $this->container->get('doctrine.orm.default_entity_manager');
    }
    /**
     * @return EntityManager
     */
    private function getEntityManagerCompany01(){
        return $this->container->get('doctrine.orm.company01_entity_manager');
    }

    /**
     * @param string $name
     * @return null|object
     */
    protected function  getEndpointByName($name){
        return $this->getEntityManager()->getRepository('AppBundle:Endpoint')->findOneBy(array("uriName"=>$name));
    }






}