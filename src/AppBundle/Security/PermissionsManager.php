<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 11/22/16
 * Time: 2:39 PM
 */

namespace AppBundle\Security;


class PermissionsManager
{

    /**
     * @var int
     */
    private $permissions;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $sufix;

    /**
     * PermissionsManager constructor.
     * @param $name
     * @param int $permissions
     * @param string $sufix
     */
    public function __construct($name='', $permissions = 0,$sufix = "ROLE_")
    {
        $this->name = $name;
        $this->permissions = $permissions;
        $this->sufix = $sufix;
    }

    /**
     * @return int
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param int $permissions
     * @return PermissionsManager
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return PermissionsManager
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSufix()
    {
        return $this->sufix;
    }

    /**
     * @param string $sufix
     * @return PermissionsManager
     */
    public function setSufix($sufix)
    {
        $this->sufix = $sufix;
        return $this;
    }

    /**
     * @param bool $permission
     * @param string $type
     * @return array
     */
    public function prosesPermissions($permission = false, $type = "user"){
        if($permission)
            $this->setPermissions($permission);
        $permission = decbin($this->getPermissions());
        while( strlen($permission) < 4 ){
            $permission = "0" . $permission;
        }
        $permission = str_split($permission);
        $ret = array();
        switch ($type){
            case 'frondend':
                $ret['APPROVE'] = $ret['DELETE'] = $ret['POST'] = $ret['VIEW'] = false;
                if($permission[0])
                    $ret['APPROVE'] = true;
                if($permission[1])
                    $ret['DELETE'] = true;
                if($permission[2])
                    $ret['POST'] = true;
                if($permission[3])
                    $ret['VIEW'] = true;
                break;
            case 'user':
            default:
                $retName = $this->getSufix().$this->getName();
                if($permission[0])
                    $ret[] = $retName.'APPROVE';
                if($permission[1])
                    $ret[] = $retName.'DELETE';
                if($permission[2])
                    $ret[] = $retName.'POST';
                if($permission[3])
                    $ret[] = $retName.'GET';
        }
        return $ret;
    }
    public function getInt(array $permission)
    {
        $binary = '';
        if(!isset($permission['APPROVE']) || !isset($permission['DELETE']) ||!isset($permission['POST']) ||!isset($permission['VIEW']) )
            return false;
        if ($permission['APPROVE'])
            $binary .= 1;
        else
            $binary .= 0;

        if ($permission['DELETE'])
            $binary .= 1;
        else
            $binary .= 0;

        if ($permission['POST'])
            $binary .= 1;
        else
            $binary .= 0;

        if ($permission['VIEW'])
            $binary .= 1;
        else
            $binary .= 0;

        return bindec($binary);
    }
    /**
     * get permissions value 0-15 and
     * returns a boolean, if this permissions
     * is valid for this endpoint
     *
     * @param $endpointPermission
     * @param $userPermission
     * @return bool
     */
    public function isAllow($endpointPermission,$userPermission){
        $endpointPermission = decbin($endpointPermission);
        while( strlen($endpointPermission) < 4 ){
            $endpointPermission = "0" . $endpointPermission;
        }
        $endpointPermission = str_split($endpointPermission);

        $userPermission = decbin($userPermission);
        while( strlen($userPermission) < 4 ){
            $userPermission = "0" . $userPermission;
        }
        $userPermission = str_split($userPermission);

        $allow = true;
        foreach($endpointPermission as $i=>$v)
            if($endpointPermission[$i] && !$userPermission[$i]  )
                $allow = false;
        return $allow;
    }



}