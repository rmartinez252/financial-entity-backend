<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/17/16
 * Time: 12:13 PM
 */

namespace AppBundle\Security;

use AppBundle\Entity\Company;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoder;
use AppBundle\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;

class Token
{


    /**
     * Token constructor.
     * @param $lexik_jwt_authentication JWTEncoderInterface
     */
    public function __construct(JWTEncoderInterface $lexik_jwt_authentication)
    {
        $this->lexik_jwt_authentication = $lexik_jwt_authentication;
    }

    /**
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    public function getNew(User $user,Company $company = null){
        $arrayToken = [
            'username' => $user->getUsername(),
            'idCompany' => $user->getIdCompany(),
            'companyName' => $user->getNameCompany()
        ];
        if($company)
            $arrayToken = [
                'username' => $user->getUsername(),
                'idCompany' => $company->getId(),
                'companyName' => $company->getName()
            ];
        return "Bearer ".$this->lexik_jwt_authentication->encode($arrayToken);
    }

}