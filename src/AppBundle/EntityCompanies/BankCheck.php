<?php

namespace AppBundle\EntityCompanies;

use Doctrine\ORM\Mapping as ORM;

/**
 * BankCheck
 *
 * @ORM\Table(name="bank_check")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BankCheckRepository")
 */
class BankCheck
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idEntry", type="integer")
     *
     */
    private $idEntry;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="idBank", type="integer")
     */
    private $idBank;

    /**
     * @var Entry
     * @ORM\OneToOne(targetEntity="AppBundle\EntityCompanies\Entry", inversedBy="bankCheck")
     * @ORM\JoinColumn(name="idEntry", referencedColumnName="id")
     */
    private $entry;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEntry
     *
     * @param integer $idEntry
     *
     * @return BankCheck
     */
    public function setIdEntry($idEntry)
    {
        $this->idEntry = $idEntry;

        return $this;
    }

    /**
     * Get idEntry
     *
     * @return int
     */
    public function getIdEntry()
    {
        return $this->idEntry;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return BankCheck
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return BankCheck
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set idBank
     *
     * @param integer $idBank
     *
     * @return BankCheck
     */
    public function setIdBank($idBank)
    {
        $this->idBank = $idBank;

        return $this;
    }

    /**
     * Get idBank
     *
     * @return int
     */
    public function getIdBank()
    {
        return $this->idBank;
    }

    /**
     * @return Entry
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * @param Entry $entry
     * @return BankCheck
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;
        return $this;
    }

}

