<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * AccountingEntryType
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="accounting_entry_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountingEntryTypeRepository")
 */
class AccountingEntryType
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var boolean
     *
     * @ORM\Column(name="system", type="boolean", nullable=true)
     */
    private $system;
    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="number", type="string", length=255,nullable=true)
     */
    private $number;
    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="description", type="string", length=255,nullable=true)
     */
    private $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\AccountingEntry", mappedBy="type")
     */
    private $accountingEntrys;

    /**
     * AccountingEntryType constructor.
     */
    public function __construct()
    {
        $this->accountingEntrys = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AccountingEntryType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getAccountingEntrys()
    {
        return $this->accountingEntrys;
    }

    /**
     * @param ArrayCollection $accountingEntrys
     * @return AccountingEntryType
     */
    public function setAccountingEntrys($accountingEntrys)
    {
        $this->accountingEntrys = $accountingEntrys;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSystem()
    {
        return $this->system;
    }

    /**
     * @param boolean $system
     * @return AccountingEntryType
     */
    public function setSystem($system)
    {
        $this->system = $system;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }



}

