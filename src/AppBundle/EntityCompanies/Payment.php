<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;


/**
 * Payment
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="payment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentRepository")
 */
class Payment
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var AccountingEntry
     * @ORM\OneToOne(targetEntity="AppBundle\EntityCompanies\AccountingEntry", inversedBy="payment", cascade={"persist"})
     * @ORM\JoinColumn(name="idEntry", referencedColumnName="id")
     */
    private $entry;

    /**
     * @var Client
     * @Serializer\Expose()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Client", inversedBy="payments", cascade={"persist"})
     * @ORM\JoinColumn(name="idDebtor", referencedColumnName="id")
     */
    private $debtor;

    /**
     * @var Operation
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Operation", inversedBy="payments", cascade={"persist"})
     * @ORM\JoinColumn(name="idOperation", referencedColumnName="id")
     */
    private $operation;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\InvoicePayment", mappedBy="payment", cascade={"persist"})
     */
    private $invoicePayments;

    /**
     * Payment constructor.
     */
    public function __construct()
    {
        $this->invoicePayments = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get Value
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("value")
     * @Serializer\Type("float")
     *
     * @return float
     */
    public function getValue()
    {
        $value = 0;
        if($this->entry)
            foreach($this->entry->getEntrys() as $entry)
                $value += $entry->getValue();
        else
            foreach($this->invoicePayments as $payment)
                $value += $payment->getValue();

        return number_format(floatval($value),2,".","");
    }



    /**
     * @return AccountingEntry
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * @param AccountingEntry $entry
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;
    }

    /**
     * @return ArrayCollection
     */
    public function getInvoicePayments()
    {
        return $this->invoicePayments;
    }

    /**
     * @param ArrayCollection $invoicePayments
     */
    public function setInvoicePayments($invoicePayments)
    {
        $this->invoicePayments = $invoicePayments;
    }

    /**
     * @param $invoicePayment InvoicePayment
     */
    public function addInvoicePayments($invoicePayment){
        $this->invoicePayments->add($invoicePayment);
    }

    /**
     * @return Client
     */
    public function getDebtor()
    {
        return $this->debtor;
    }

    /**
     * @param Client $debtor
     * @return Payment
     */
    public function setDebtor($debtor)
    {
        $this->debtor = $debtor;
        return $this;
    }

    /**
     * @return Operation
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param Operation $operation
     * @return Payment
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
        return $this;
    }
    /**
     * Get date_user
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("date_user")
     * @Serializer\Type("string")
     *
     *
     * @return string
     */
    public function getDate(){
        if($this->entry)
            return $this->entry->getDateUserFront();
        else
            return $this->operation->getEffectiveDateFront();
    }


}

