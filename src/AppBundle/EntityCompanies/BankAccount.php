<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity as Main;
use JMS\Serializer\Annotation as Serializer;

/**
 * BankAccount
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="bank_account")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BankAccountRepository")
 */
class BankAccount
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="accountNumber", type="string", length=255, unique=true)
     */
    private $accountNumber;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="nickname", type="string", length=255, nullable=true)
     */
    private $nickname;

    /**
     * @var int
     *
     * @ORM\Column(name="idBank", type="integer")
     */

    private $idBank;


    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="idAccountType", type="integer")
     */
    private $idAccountType;


    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\BankMove", mappedBy="bankAccount")
     *
     */
    private $bankMoves;
    /**
     * @var Account
     * @Serializer\Expose()
     *
     * @ORM\OneToOne(targetEntity="AppBundle\EntityCompanies\Account", inversedBy="bankAccount", cascade={"persist"})
     * @ORM\JoinColumn(name="idAccountingAccount", referencedColumnName="id")
     */
    private $accountingAccount;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return BankAccount
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     *
     * @return BankAccount
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }



    public function getIdBank()
    {
        return $this->idBank;
    }

    public function setIdBank($idBank)
    {
        if(is_object($idBank))
            $idBank = $idBank->getId();
        $this->idBank = $idBank;
        return $this;
    }

    public function getIdAccountType()
    {
        return $this->idAccountType;
    }
    public function setIdAccountType($idAccountType)
    {
        if(is_object($idAccountType))
            $idAccountType = $idAccountType->getId();
        $this->idAccountType = $idAccountType;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBankMoves()
    {
        return $this->bankMoves;
    }

    /**
     * @return Account
     */
    public function getAccountingAccount()
    {
        return $this->accountingAccount;
    }

    /**
     * @param Account $accountingAccount
     * @return BankAccount
     */
    public function setAccountingAccount($accountingAccount)
    {
        $this->accountingAccount = $accountingAccount;
        return $this;
    }



}

