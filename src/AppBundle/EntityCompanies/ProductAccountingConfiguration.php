<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * ProductAccountingConfiguration
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="product_accounting_configuration")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductAccountingConfigurationRepository")
 */
class ProductAccountingConfiguration
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccountingProduct"})
     * @ORM\Column(name="systemName", type="string", length=255, unique=true)
     */
    private $systemName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccountingProduct"})
     * @ORM\Column(name="showName", type="string", length=255, unique=true)
     */
    private $showName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccountingProduct"})
     * @ORM\Column(name="showDescription", type="string", length=255, nullable=true)
     */
    private $showDescription;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\ProductAccountingConfigurationProduct", mappedBy="productAccountingConfiguration", cascade={"persist"})
     */
    private $productAccountingConfigurationProducts;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     *
     * @return ProductAccountingConfiguration
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * Get systemName
     *
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * Set showName
     *
     * @param string $showName
     *
     * @return ProductAccountingConfiguration
     */
    public function setShowName($showName)
    {
        $this->showName = $showName;

        return $this;
    }

    /**
     * Get showName
     *
     * @return string
     */
    public function getShowName()
    {
        return $this->showName;
    }

    /**
     * Set showDescription
     *
     * @param string $showDescription
     *
     * @return ProductAccountingConfiguration
     */
    public function setShowDescription($showDescription)
    {
        $this->showDescription = $showDescription;

        return $this;
    }

    /**
     * Get showDescription
     *
     * @return string
     */
    public function getShowDescription()
    {
        return $this->showDescription;
    }
}

