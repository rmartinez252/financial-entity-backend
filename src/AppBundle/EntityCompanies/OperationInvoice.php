<?php

namespace AppBundle\EntityCompanies;

use AppBundle\Operations\OperationProcess;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;



/**
 * @Serializer\ExclusionPolicy("all")
 * OperationInvoice
 *
 * @ORM\Table(name="operation_invoice")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OperationInvoiceRepository")
 */
class OperationInvoice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @Serializer\Expose()
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var Client
     * @Serializer\Expose()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Client", inversedBy="operationInvoices", cascade={"persist"})
     * @ORM\JoinColumn(name="idClient", referencedColumnName="id")
     */
    private $debtor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateInvoice", type="datetime")
     */
    private $dateInvoice;


    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="tenor", type="integer")
     */
    private $tenor;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var bool
     * @Serializer\Expose()
     *
     * @ORM\Column(name="isPaid", type="boolean", nullable=true)
     */
    private $isPaid;

    /**
     * @var float
     * @Serializer\Expose()
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var Operation
     * @Serializer\Expose()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Operation", inversedBy="invoices", cascade={"persist"})
     * @ORM\JoinColumn(name="idOperation", referencedColumnName="id")
     */
    private $operation;



    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\InvoicePayment", mappedBy="invoice", cascade={"persist","refresh"})
     */
    private $invoicePayments;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\AccountingEntry", mappedBy="operationInvoice", cascade={"persist"})
     */
    private $lateFeeEntries;

    private $operationProcess;

    /**
     * OperationInvoice constructor.
     */
    public function __construct()
    {
        $this->invoicePayments = new ArrayCollection ();
        $this->lateFeeEntries = new ArrayCollection ();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return Client
     */
    public function getDebtor()
    {
        return $this->debtor;
    }

    /**
     * @param Client $debtor
     * @return OperationInvoice
     */
    public function setDebtor($debtor)
    {
        $this->debtor = $debtor;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateInvoice()
    {
        return $this->dateInvoice;
    }
    /**
     * Get effective date
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("date_invoice")
     * @Serializer\Groups({"AllOperation"})
     *
     *
     * @return string
     */
    public function getDateInvoiceFront()
    {
        return $this->dateInvoice->format("Y-m-d");
    }

    /**
     * @param \DateTime $dateInvoice
     * @return OperationInvoice
     */
    public function setDateInvoice($dateInvoice)
    {
        $this->dateInvoice = $dateInvoice;
        return $this;
    }

    /**
     * @return int
     */
    public function getTenor()
    {
        return $this->tenor;
    }

    /**     * @Serializer\Groups({"AllOperation"})

     * @param int $tenor
     * @return OperationInvoice
     */
    public function setTenor($tenor)
    {
        $this->tenor = $tenor;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return OperationInvoice
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * @param boolean $isPaid
     * @return OperationInvoice
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return OperationInvoice
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return Operation
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param Operation $operation
     * @return OperationInvoice
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getInvoicePayments()
    {
        return $this->invoicePayments;
    }

    /**
     * @param ArrayCollection $invoicePayments
     * @return OperationInvoice
     */
    public function setInvoicePayments($invoicePayments)
    {
        $this->invoicePayments = $invoicePayments;
        return $this;
    }

    /**
     * @param $invoicePayment InvoicePayment
     * @return $this
     */
    public function addInvoicePayments(InvoicePayment $invoicePayment){
        $this->invoicePayments->add($invoicePayment);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLateFeeEntries()
    {
        return $this->lateFeeEntries;
    }

    /**
     * @param ArrayCollection $lateFeeEntries
     * @return OperationInvoice
     */
    public function setLateFeeEntries($lateFeeEntries)
    {
        $this->lateFeeEntries = $lateFeeEntries;
        return $this;
    }

    /**
     * @param AccountingEntry $lateFeeEntry
     * @return OperationInvoice
     */
    public function addLateFeeEntry(AccountingEntry $lateFeeEntry)
    {
        $this->lateFeeEntries->add($lateFeeEntry);
        return $this;
    }

    /**
     * maturity date
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("maturityDate")
     * @Serializer\Type("string")
     *
     *
     * @return string
     */
    public function invoiceMaturityDate(){
        if(!$this->getDateInvoice())
            return false;
        $date = new \DateTime($this->getDateInvoice()->format("Y-m-d"));
        $date->add(new \DateInterval("P".$this->getTenor()."D"));
        return $date->format("Y-m-d");
    }
    /**
     * Commission Percentage
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("invoiceCommission")
     * @Serializer\Type("float")
     *
     *
     * @return bool|float
     */
    public function invoiceCommission(){
        if(!$this->getOperation()->getId())
            return false;
        $this->operationProcess = new OperationProcess($this->getOperation());
        return $this->operationProcess->getInvoiceCommissionP($this);

    }
    /**
     * Commission Amount
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("invoiceCommissionAmount")
     * @Serializer\Type("float")
     *
     *
     * @return bool|float
     */
    public function invoiceCommissionAmount(){
        if(!$commissionP = $this->invoiceCommission())
            return false;
        return $commissionP * $this->getValue();
    }






}

