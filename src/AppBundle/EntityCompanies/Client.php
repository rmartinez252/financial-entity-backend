<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Client
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @Serializer\Expose()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var int
     *
     * @ORM\Column(name="cuentaFF", type="integer",nullable=true)
     */
    private $cuentaFF;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="ruc", type="string", length=255,nullable=true)
     */
    private $ruc;
    /**
     * @var Address
     * @Serializer\Expose()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Address", inversedBy="clients", cascade={"persist"})
     * @ORM\JoinColumn(name="idAddress", referencedColumnName="id")
     */
    private $address;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;



    /**
     * @var Person
     * @Serializer\Expose()
     * @Serializer\Groups({"ClientPerson","ClientPersonNames"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Person", inversedBy="clients", cascade={"persist"})
     * @ORM\JoinColumn(name="idContactPerson", referencedColumnName="id")
     */
    private $contactPerson;


    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"FullClient"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Operation", mappedBy="client", cascade={"persist"})
     */
    private $operations;
    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"FullClient"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\OperationInvoice", mappedBy="debtor", cascade={"persist"})
     */
    private $operationInvoices;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Payment", mappedBy="debtor", cascade={"persist"})
     */
    private $payments;

    /**
     * Structure constructor.
     */
    public function __construct(){
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCuentaFF()
    {
        return $this->cuentaFF;
    }

    /**
     * @param int $cuentaFF
     * @return Client
     */
    public function setCuentaFF($cuentaFF)
    {
        $this->cuentaFF = $cuentaFF;
        return $this;
    }




    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * @param string $ruc
     * @return Client
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Client
     */
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Client
     */
    public function setPhone($phone){
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone(){
        return $this->phone;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }



    /**
     * @param Person $contactPerson
     * @return Client
     */
    public function setContactPerson(Person $contactPerson){
        $this->contactPerson = $contactPerson;
        return $this;
    }

    /**
     * @return Person
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Get idContactPerson
     *
     * @return int
     */
    public function getIdContactPerson(){
        return $this->contactPerson->getId();
    }


    public function toSystemLog(){
        return array(
            'name'=>$this->getName()
        );
    }

    /**
     * @return ArrayCollection
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * @param ArrayCollection $operations
     * @return Client
     */
    public function setOperations($operations)
    {
        $this->operations = $operations;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getOperationInvoices()
    {
        return $this->operationInvoices;
    }

    /**
     * @param ArrayCollection $operationInvoices
     * @return Client
     */
    public function setOperationInvoices($operationInvoices)
    {
        $this->operationInvoices = $operationInvoices;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param ArrayCollection $payments
     * @return Client
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;
        return $this;
    }




}

