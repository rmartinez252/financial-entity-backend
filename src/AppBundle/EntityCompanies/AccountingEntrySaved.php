<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;


/**
 * Payment
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="accounting_entry_saved")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountingEntrySavedRepository")
 */
class AccountingEntrySaved
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var AccountingEntry
     * @ORM\OneToOne(targetEntity="AppBundle\EntityCompanies\AccountingEntry", inversedBy="payment", cascade={"persist"})
     * @ORM\JoinColumn(name="idAccountingEntry", referencedColumnName="id", nullable=false)
     */
    private $accountingEntry;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return AccountingEntry
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("accountingEntryView")
     */
    public function getAccountingEntryView()
    {
        $accountingEntry = $this->accountingEntry;
        if($accountingEntry)
            $accountingEntry->unsetIds();
        return $accountingEntry;
    }

    /**
     * @return AccountingEntry
     */
    public function getAccountingEntry()
    {
        return $this->accountingEntry;
    }

    /**
     * @param AccountingEntry $accountingEntry
     * @return AccountingEntrySaved
     */
    public function setAccountingEntry($accountingEntry)
    {
        $this->accountingEntry = $accountingEntry;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AccountingEntrySaved
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return AccountingEntrySaved
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }





}

