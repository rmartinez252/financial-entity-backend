<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Process
 *
 * @ORM\Table(name="process")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProcessRepository")
 */
class Process
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\EntityCompanies\ProductAttribute")
     * @ORM\JoinTable(name="product_attribute_process",
     *      joinColumns={@ORM\JoinColumn(name="idProcess", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="idProductAttribute", referencedColumnName="id")}
     *      )
     */

    private $attributes;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\EntityCompanies\ProductAccountingConfiguration")
     * @ORM\JoinTable(name="product_accounting_configuration_process",
     *      joinColumns={@ORM\JoinColumn(name="idProcess", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="idAccountingConfiguration", referencedColumnName="id")}
     *      )
     */
    private $accountingConfigurations;


    /**
     * Process constructor.
     */
    public function __construct()
    {
        $this->accountingConfigurations = new ArrayCollection();
    }

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Product", mappedBy="process", cascade={"persist"})
     *
     */
    private $products;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Process
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param ArrayCollection $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return ArrayCollection
     */
    public function getAccountingConfigurations()
    {
        return $this->accountingConfigurations;
    }

    /**
     * @param ArrayCollection $accountingConfigurations
     */
    public function setAccountingConfigurations($accountingConfigurations)
    {
        $this->accountingConfigurations = $accountingConfigurations;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param ArrayCollection $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * Add Attribute
     *
     * @param ProductAttribute $attribute
     *
     * @return Process
     */
    public function addAttribute(ProductAttribute $attribute){

        $this->attributes->add($attribute);
        return $this;
    }

    /**
     * Remove Attribute
     *
     * @param ProductAttribute $attribute
     */
    public function removeAttribute(ProductAttribute $attribute){
        $this->attributes->removeElement($attribute);
    }

    /**
     * Add AccountingConfiguration
     *
     * @param ProductAccountingConfiguration $accountingConfiguration
     *
     * @return Process
     */
    public function addAccountingConfiguration(ProductAccountingConfiguration $accountingConfiguration){

        $this->accountingConfigurations->add($accountingConfiguration);
        return $this;
    }

    /**
     * Remove AccountingConfiguration
     *
     * @param ProductAccountingConfiguration $accountingConfiguration
     */
    public function removeAccountingConfiguration(ProductAccountingConfiguration $accountingConfiguration){
        $this->accountingConfigurations->removeElement($accountingConfiguration);
    }


}

