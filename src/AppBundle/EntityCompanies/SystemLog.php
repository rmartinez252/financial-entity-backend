<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity as Main;

/**
 * User
 *
 *
 * @ORM\Table(name="systemLog")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LogRepository")
 */
class SystemLog
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="actionType", type="string")
     *
     * @Assert\NotBlank()
     */
    private $actionType;

    /**
     * String 5-200
     * @var string
     * @ORM\Column(name="username", type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 5,
     *      max = 200,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private $username;

    /**
     * @var Main\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="userLogCompanies", cascade={"persist"})
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $user;
    /**
     * @var Main\Company
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Company", inversedBy="companyLogCompanies")
     * @ORM\JoinColumn(name="idCompany", referencedColumnName="id")
     */
    private $company;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date",type="datetime")
     */
    private $date;
    /**
     * @var string
     *
     * @ORM\Column(name="info", type="array",nullable=true)
     */
    private $info;

    /**
     * SystemLog constructor.
     * @param string $actionType
     * @param string $username
     * @param Main\User $user
     * @param Main\Company $company
     * @param \DateTime $date
     */
    public function __construct($actionType, $username, $user,$company, \DateTime $date = null)
    {
        $this->actionType = $actionType;
        $this->username = $username;
        $this->user = $user;
        $this->company = $company;
        if(!$date)
            $date = new \DateTime('now');
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SystemLog
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getActionType()
    {
        return $this->actionType;
    }

    /**
     * @param string $actionType
     * @return SystemLog
     */
    public function setActionType($actionType)
    {
        $this->actionType = $actionType;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return SystemLog
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return Main\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Main\User $user
     * @return SystemLog
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Main\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Main\Company $company
     * @return SystemLog
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return SystemLog
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return SystemLog
     */
    public function setInfo($info)
    {
        $this->info = $info;
        return $this;
    }



}

