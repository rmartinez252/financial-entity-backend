<?php

namespace AppBundle\EntityCompanies;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;


/**
 * ProductAccountingConfigurationProduct
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="product_accounting_configuration_product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductAccountingConfigurationProductRepository")
 */
class ProductAccountingConfigurationProduct
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccountingProduct"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="Account", type="object")
     */


    /**
     * @var Account
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccountingProduct"})
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Account", inversedBy="configurationProducts", cascade={"persist"})
     * @ORM\JoinColumn(name="idAccount", referencedColumnName="id")
     */
    private $account;

    /**
     * @var int
     *
     * @ORM\Column(name="nature", type="smallint")
     */
    private $nature;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Product", inversedBy="accountingConfigurations", cascade={"persist"})
     * @ORM\JoinColumn(name="idProduct", referencedColumnName="id")
     */
    private $product;

    /**
     * @var ProductAccountingConfiguration
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccountingProduct"})
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\ProductAccountingConfiguration", inversedBy="productAccountingConfigurationProducts", cascade={"persist"})
     * @ORM\JoinColumn(name="idProductAccountingConfiguration", referencedColumnName="id")
     */
    private $productAccountingConfiguration;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set account
     *
     * @param Account $account
     *
     * @return ProductAccountingConfigurationProduct
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set nature
     *
     * @param integer $nature
     *
     * @return ProductAccountingConfigurationProduct
     */
    public function setNature($nature)
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * Get nature
     *
     * @return int
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * @return ProductAccountingConfiguration
     */
    public function getProductAccountingConfiguration()
    {
        return $this->productAccountingConfiguration;
    }

    /**
     * @param ProductAccountingConfiguration $productAccountingConfiguration
     * @return ProductAccountingConfigurationProduct
     */
    public function setProductAccountingConfiguration($productAccountingConfiguration)
    {
        $this->productAccountingConfiguration = $productAccountingConfiguration;
        return $this;
    }



    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return ProductAccountingConfigurationProduct
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }



}

