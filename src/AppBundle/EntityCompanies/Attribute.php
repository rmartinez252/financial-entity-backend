<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
/**
 * Attribute
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="attribute")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttributeRepository")
 */
class Attribute
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\PersonAttribute", mappedBy="attribute")
     *
     */
    private $personAttributes;



    /**
     * Structure constructor.
     */
    public function __construct(){
        $this->personAttribute = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Attribute
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Attribute
     */
    public function setType($type){
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType(){
        return $this->type;
    }
    /**
     * @return ArrayCollection
     */
    public function getPersonAttributes(){
        return $this->personAttributes;
    }

    /**
     * @param ArrayCollection $personAttributes
     * @return Attribute
     */
    public function setPersonAttributes($personAttributes){
        $this->personAttributes = $personAttributes;
        return $this;
    }

    /**
     * Get name to string(From)
     * @return string
     */
    public function __toString() {
        return $this->name;
    }
    public function toStringSystemLog(){
        return array(
            "id" => $this->getId(),
            "name" => $this->getName(),
            "type" => $this->getType()
        );
    }
}

