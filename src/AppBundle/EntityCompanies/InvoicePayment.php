<?php

namespace AppBundle\EntityCompanies;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoicePayment
 *
 * @ORM\Table(name="invoice_payment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoicePaymentRepository")
 */
class InvoicePayment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Payment", inversedBy="invoicePayments", cascade={"persist"})
     * @ORM\JoinColumn(name="idPayment", referencedColumnName="id")
     */
    private $payment;

    /**
     * @var OperationInvoice
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\OperationInvoice", inversedBy="invoicePayments", cascade={"persist"})
     * @ORM\JoinColumn(name="idOperationInvoice", referencedColumnName="id")
     */
    private $invoice;
    /**
     * @var boolean
     *
     * @ORM\Column(name="isLateFee", type="boolean",nullable=true)
     */
    private $isLateFee;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return InvoicePayment
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set payment
     *
     * @param Payment $payment
     *
     * @return InvoicePayment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set invoice
     *
     * @param OperationInvoice $invoice
     *
     * @return InvoicePayment
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return OperationInvoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @return boolean
     */
    public function isLateFee()
    {
        return $this->isLateFee;
    }

    /**
     * @param boolean $isLateFee
     * @return InvoicePayment
     */
    public function setIsLateFee($isLateFee)
    {
        $this->isLateFee = $isLateFee;
        return $this;
    }


}

