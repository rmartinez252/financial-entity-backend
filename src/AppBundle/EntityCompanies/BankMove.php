<?php

namespace AppBundle\EntityCompanies;

use AppBundle\Repository\BankMoveTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity as Main;
use Faker\Provider\cs_CZ\DateTime;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BankMove
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="bank_move")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BankMoveRepository")
 */
class BankMove
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"BankMove1"})
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;
    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"BankMove1"})
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;
    /**
     * @var integer
     * @Serializer\Expose()
     * @Serializer\Groups({"BankMove1"})
     * @ORM\Column(name="idMoveType", type="integer")
     */
    private $idMoveType;
    /**
     * @var Main\BankMoveType
     */
    private $bankMoveType;
    /**
     * @var BankAccount
     * @Serializer\Expose()
     * @Serializer\Groups({"BankMove1"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\BankAccount", inversedBy="bankMoves", cascade={"persist"})
     * @ORM\JoinColumn(name="idAccount", referencedColumnName="id")
     */
    private $bankAccount;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"BankMove2"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Entry", mappedBy="bankMoveCredit", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="idEntryCredit", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     */
    private $entryCredit;
    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"BankMove2"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Entry", mappedBy="bankMoveDebit", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="idEntryDebit", referencedColumnName="id", nullable=true,onDelete="CASCADE")
     */
    private $entryDebit;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateUser", type="datetime", nullable=true)
     */
    private $dateUser;

    /**
     * BankMove constructor.
     */
    public function __construct()
    {
        $this->entryCredit = new ArrayCollection ;
        $this->entryDebit = new ArrayCollection ;
    }
//    /**
//     * @var BankMoveTypeRepository $bankMoveRepo
//     */
//    private $bankMoveRepo;
//
//    /**
//     * BankMove constructor.
//     * @param BankMoveTypeRepository $bankMoveRepo
//     */
//    public function __construct(BankMoveTypeRepository $bankMoveRepo)
//    {
//        $this->bankMoveRepo = 35;
//    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return BankMove
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return BankMove
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


    /**
     * @param Main\BankMoveType $bankMoveType
     * @return BankMove
     */
    public function setBankMoveType($bankMoveType)
    {
        $this->bankMoveType = $bankMoveType;
        $this->idMoveType = $this->bankMoveType->getId();
        return $this;
    }
//    private function setFirstBankMoveType(){
//        $this->bankMoveType = $this->bankMoveRepo->findOneBy(array("id"=>$this->getIdMoveType()));
//    }

    /**
     * @return Main\BankMoveType
     */
    public function getBankMoveType()
    {
        return $this->bankMoveType;
    }

    /**
     * @param BankAccount $bankAccount
     * @return BankMove
     */
    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = $bankAccount;
        return $this;
    }

    /**
     * @return BankAccount
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * @return int
     */
    public function getIdMoveType()
    {
        return $this->idMoveType;
    }

    /**
     * @param int $idMoveType
     * @return BankMove
     */
    public function setIdMoveType($idMoveType)
    {
        $this->idMoveType = $idMoveType;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getEntryCredit()
    {
        return $this->entryCredit;
    }

    /**
     * @param ArrayCollection $entryCredit
     * @return BankMove
     */
    public function setEntryCredit($entryCredit)
    {
        $this->entryCredit = $entryCredit;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getEntryDebit()
    {
        return $this->entryDebit;
    }

    /**
     * @param ArrayCollection $entryDebit
     * @return BankMove
     */
    public function setEntryDebit($entryDebit)
    {
        $this->entryDebit = $entryDebit;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUser()
    {
        return $this->dateUser;
    }

    /**
     * @param \DateTime $dateUser
     * @return BankMove
     */
    public function setDateUser($dateUser)
    {
        $this->dateUser = $dateUser;
        return $this;
    }


    /**
     * @return float
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("totalAmount")
     * @Serializer\Type("float")
     */
    public function getValue(){
        $total = 0;
        /**
         * @var Entry $entry
         */
        foreach ($this->entryCredit as $entry){
            $total += $entry->getValue();
        }
        foreach ($this->entryDebit as $entry){
            $total += $entry->getValue();
        }
        return $total;
    }
    /**
     * @return boolean
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("isDebit")
     * @Serializer\Type("boolean")
     */
    public function isDebit(){
        if($this->entryDebit->count())
            return true;
        else
            return false;
    }

    /**
     * @Serializer\PreSerialize
     */
    public function preSerializer(){
//        $this->setFirstBankMoveType();

    }
    /**
     * @return null|\DateTime
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("date")
     * @Serializer\Type("DateTime")
     */
    public function getDate(){
        /**
         * @var Entry $entry
         */
        foreach ($this->entryCredit as $entry){
            return $entry->getAccountingEntry()->getDateUser();
        }
        foreach ($this->entryDebit as $entry){
            return $entry->getAccountingEntry()->getDateUser();
        }
        return null;
    }



}

