<?php

namespace AppBundle\EntityCompanies;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Entry
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="entry")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EntryRepository")
 */
class Entry
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionDebit", type="string", length=255,nullable=true)
     */
    private $descriptionDebit;
    /**
     * @var string
     *
     * @ORM\Column(name="descriptionCredit", type="string", length=255,nullable=true)
     */
    private $descriptionCredit;

    /**
     * @var float
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccount","FullBankMove","BankMove1","FullAccountingEntrySave"})
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var AccountingEntry
     * @Serializer\Expose()
     * @Serializer\Groups({"FullBankMove","BankMove2"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\AccountingEntry", inversedBy="entrys", cascade={"persist"})
     * @ORM\JoinColumn(name="idAccountingEntry", referencedColumnName="id",onDelete="CASCADE", nullable=false)
     */
    private $accountingEntry;

    /**
     * @var Account
     * @Serializer\Expose()
     * @Serializer\Groups({"FullBankMove","BankMove2","FullAccountingEntrySave"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Account", inversedBy="entrysDebit", cascade={"persist"})
     * @ORM\JoinColumn(name="idAccountDebit", referencedColumnName="id",onDelete="CASCADE")
     */
    private $accountDebit;

    /**
     * @var Account
     * @Serializer\Expose()
     * @Serializer\Groups({"FullBankMove","BankMove2","FullAccountingEntrySave"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Account", inversedBy="entrysCredit", cascade={"persist"})
     * @ORM\JoinColumn(name="idAccountCredit", referencedColumnName="id",onDelete="CASCADE")
     */
    private $accountCredit;

    /**
     * @var BankCheck
     * @ORM\OneToOne(targetEntity="AppBundle\EntityCompanies\BankCheck", mappedBy="entry")
     */
    private $bankCheck;

    /**
     * @var BankMove
     * @ORM\ManyToOne(targetEntity="BankMove", inversedBy="entryCredit", cascade={"persist","remove"}))
     *
     */
    private $bankMoveCredit;
    /**
     * @var BankMove
     * @ORM\ManyToOne(targetEntity="BankMove", inversedBy="entryDebit", cascade={"persist","remove"})
     *
     */
    private $bankMoveDebit;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function unsetIds(){
        $this->id = null;
        $this->accountingEntry = null;
        return $this;
    }



    /**
     * Get idAccountDebit
     *
     * @return int
     */
    public function getIdAccountDebit()
    {
        if(!$this->accountDebit)
            return null;
        return $this->accountDebit->getId();
    }

    /**
     * Get idAccountCredit
     *
     * @return int
     */
    public function getIdAccountCredit()
    {
        if(!$this->accountCredit)
            return null;
        return $this->accountCredit->getId();
    }

    /**
     * Get idAccountingEntry
     *
     * @return int
     */
    public function getIdAccountingEntry(){
        if(!$this->accountingEntry)
            return null;
        return $this->accountingEntry->getId();
    }



    /**
     * Set description
     *
     * @param string $description
     *
     * @return Entry
     */
    public function setDescriptionDebit($description)
    {
        $this->descriptionDebit = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescriptionDebit()
    {
        return $this->descriptionDebit;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Entry
     */
    public function setDescriptionCredit($description)
    {
        $this->descriptionCredit = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescriptionCredit()
    {
        return $this->descriptionCredit;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return Entry
     */
    public function setValue($value)
    {
//        $this->value = number_format(floatval($value),2,".","");
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return round($this->value,2);
//        return $this->value;
    }

    /**
     * @return Account
     */
    public function getAccountDebit(){
        return $this->accountDebit;
    }

    /**
     * @param Account $accountDebit
     * @return Entry
     */
    public function setAccountDebit(Account $accountDebit){
        $this->accountDebit = $accountDebit;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccountCredit(){
        return $this->accountCredit;
    }

    /**
     * @param Account $accountCredit
     * @return Entry
     */
    public function setAccountCredit(Account $accountCredit){
        $this->accountCredit = $accountCredit;
        return $this;
    }

    /**
     * @return AccountingEntry
     */
    public function getAccountingEntry()
    {
        return $this->accountingEntry;
    }

    /**
     * @param AccountingEntry $accountingEntry
     * @return Entry
     */
    public function setAccountingEntry(AccountingEntry $accountingEntry)
    {
        $this->accountingEntry = $accountingEntry;
        return $this;
    }

    /**
     * @return BankCheck
     */
    public function getBankCheck()
    {
        return $this->bankCheck;
    }

    /**
     * @param BankCheck $bankCheck
     * @return Entry
     */
    public function setBankCheck($bankCheck)
    {
        $this->bankCheck = $bankCheck;
        return $this;
    }

    /**
     * @return BankMove
     */
    public function getBankMoveCredit()
    {
        return $this->bankMoveCredit;
    }

    /**
     * @param BankMove $bankMove
     * @return Entry
     */
    public function setBankMoveCredit($bankMove)
    {
        $this->bankMoveCredit = $bankMove;
        return $this;
    }

    /**
     * @return BankMove
     */
    public function getBankMoveDebit()
    {
        return $this->bankMoveDebit;
    }

    /**
     * @param BankMove $bankMove
     * @return Entry
     */
    public function setBankMoveDebit($bankMove)
    {
        $this->bankMoveDebit = $bankMove;
        return $this;
    }
//    /**
//     * Get total
//     * @Serializer\VirtualProperty
//     * @Serializer\SerializedName("idAccountingEntry")
//     * @Serializer\Type("int")
//     *
//     *
//     * @return int
//     */
//    public function getInitialAmount(){
//        return $this->accountingEntry->getId();
//    }


}

