<?php

namespace AppBundle\EntityCompanies;

use AppBundle\Operations\OperationProcess;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;


/**
 * OperationAttribute
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="operation_status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OperationStatusRepository")
 */
class OperationStatus
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"FullOperationStatus","FullOperation","FullOperationAttribute"})
     *
     * @ORM\Column(name="systemName", type="string", length=255)
     */
    private $systemName;

    /**
     * @var integer
     * @Serializer\Expose()
     * @Serializer\Groups({"FullOperationStatus","FullOperation","FullOperationAttribute"})
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"FullOperationStatus"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Operation", mappedBy="status", cascade={"persist","refresh"})
     */
    private $operations;


    /**
     * OperationAttribute constructor.
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return OperationStatus
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     * @return OperationStatus
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;
        return $this;
    }


    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return OperationStatus
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * @param ArrayCollection $operations
     * @return OperationStatus
     */
    public function setOperations($operations)
    {
        $this->operations = $operations;
        return $this;
    }




}

