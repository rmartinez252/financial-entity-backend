<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity as Main;
use JMS\Serializer\Annotation as Serializer;



/**
 * AccountingEntry
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="accounting_entry")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountingEntryRepository")
 */
class AccountingEntry
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="idBeneficiary", type="integer", nullable=true)
     */
    private $idBeneficiary;

    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="idModified", type="integer", nullable=true)
     */
    private $idModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateSystem", type="datetime", nullable=true)
     */
    private $dateSystem;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateUser", type="datetime", nullable=true)
     */
    private $dateUser;
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="idUser", type="integer")
     */
    private $idUser;


    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Entry", mappedBy="accountingEntry", cascade={"persist", "remove"})
     */
    private $entrys;

    /**
     * @var AccountingEntryType
     * @Serializer\Expose()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\AccountingEntryType", inversedBy="accountingEntrys")
     * @ORM\JoinColumn(name="idType", referencedColumnName="id", nullable=true)
     */
    private $type;

    /**
     * @var Operation
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Operation", inversedBy="entries", cascade={"persist"})
     * @ORM\JoinColumn(name="idOperation", referencedColumnName="id",nullable=true)
     */
    private $operation;

    /**
     * @var OperationInvoice
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\OperationInvoice", inversedBy="lateFeeEntries", cascade={"persist"})
     * @ORM\JoinColumn(name="idOperationInvoice", referencedColumnName="id",nullable=true)
     */
    private $operationInvoice;

    /**
     * @var Payment
     * @ORM\OneToOne(targetEntity="AppBundle\EntityCompanies\Payment", mappedBy="entry")
     */
    private $payment;

    /**
     * @var AccountingEntrySaved
     * @ORM\OneToOne(targetEntity="AppBundle\EntityCompanies\AccountingEntrySaved", mappedBy="accountingEntry")
     */
    private $accountingEntrySaved;


    public function __construct() {
        $this->entrys = new ArrayCollection();
        $this->dateSystem = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function unsetIds(){
        $entries = new ArrayCollection();
        foreach ($this->getEntrys() as $entry){

            $entries[] = $entry->unsetIds();
        }
        $this->setEntrys($entries);
        $this->id = null;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AccountingEntry
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idBeneficiary
     *
     * @param integer $idBeneficiary
     *
     * @return AccountingEntry
     */
    public function setIdBeneficiary($idBeneficiary)
    {
        $this->idBeneficiary = $idBeneficiary;

        return $this;
    }

    /**
     * Get idBeneficiary
     *
     * @return int
     */
    public function getIdBeneficiary()
    {
        return $this->idBeneficiary;
    }

    /**
     * Set idModified
     *
     * @param integer $idModified
     *
     * @return AccountingEntry
     */
    public function setIdModified($idModified)
    {
        $this->idModified = $idModified;

        return $this;
    }

    /**
     * Get idModified
     *
     * @return int
     */
    public function getIdModified()
    {
        return $this->idModified;
    }

    /**
     * Set dateUser
     *
     * @param \DateTime $date
     *
     * @return AccountingEntry
     */
    public function setDateUser($date)
    {
        $this->dateUser = $date;

        return $this;
    }

    /**
     * Get dateUser
     *
     * @return \DateTime
     */
    public function getDateUser()
    {
        return $this->dateUser;
    }

    /**
     * Get date user
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("date_user")
     * @Serializer\Groups({"AllOperation"})
     *
     *
     * @return string
     */
    public function getDateUserFront()
    {
        return $this->dateUser->format("Y-m-d");
    }

    /**
     * Get dateSystem
     *
     * @return \DateTime
     */
    public function getDateSystem()
    {
        return $this->dateSystem;
    }

    /**
     * Get date user
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("date_system")
     * @Serializer\Groups({"AllOperation"})
     *
     *
     * @return string
     */
    public function getDateSystemFront()
    {
        return $this->dateSystem->format("Y-m-d");
    }

    /**
     * Set dateSystem
     *
     * @param \DateTime $date
     *
     * @return AccountingEntry
     */
    public function setDateSystem($date)
    {
        $this->dateSystem = $date;

        return $this;
    }

    /**
     * Get idType
     *
     * @return int
     */
    public function getIdType()
    {
        if(!$this->type)
            return null;
        return $this->type->getId();
    }

    /**
     * @return ArrayCollection
     */
    public function getEntrys()
    {
        return $this->entrys;
    }

    /**
     * @param ArrayCollection $entrys
     * @return AccountingEntry
     */
    public function setEntrys($entrys){
        $this->entrys = $entrys;
        return $this;
    }

    /**
     * Add entry
     *
     * @param Entry $entry
     *
     * @return AccountingEntry
     */
    public function addEntry(Entry $entry){

        $this->entrys->add($entry);
        $entry->setAccountingEntry($this);
        return $this;
    }

    /**
     * Remove entry
     *
     * @param Entry $entry
     */
    public function removeEntry(Entry $entry){
        $this->entrys->removeElement($entry);
    }

    /**
     * Remove all user Entrys
     */
    public function removeAllEntry()
    {
        $this->entrys->clear();
    }

    /**
     * @return AccountingEntryType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param AccountingEntryType $type
     * @return AccountingEntry
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param int $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return Operation
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param Operation $operation
     * @return AccountingEntry
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
        return $this;
    }

    /**
     * @return OperationInvoice
     */
    public function getOperationInvoice()
    {
        return $this->operationInvoice;
    }

    /**
     * @param OperationInvoice $operationInvoice
     * @return AccountingEntry
     */
    public function setOperationInvoice($operationInvoice)
    {
        $this->operationInvoice = $operationInvoice;
        return $this;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     * @return AccountingEntry
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
        return $this;
    }


    /**
     * @return float
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("totalAmount")
     * @Serializer\Type("float")
     */
    public function getHoldValue(){
        $value = 0;
        if($entrys = $this->getEntrys())
            foreach ($entrys as $entry)
                $value += $entry->getValue();
        return $value;
    }

    /**
     * @return AccountingEntrySaved
     */
    public function getAccountingEntrySaved()
    {
        return $this->accountingEntrySaved;
    }

    /**
     * @param AccountingEntrySaved $accountingEntrySaved
     * @return AccountingEntry
     */
    public function setAccountingEntrySaved($accountingEntrySaved)
    {
        $this->accountingEntrySaved = $accountingEntrySaved;
        return $this;
    }







}

