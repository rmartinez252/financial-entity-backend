<?php

namespace AppBundle\EntityCompanies;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;


/**
 * ProductAttribute
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Table(name="product_attribute")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductAttributeRepository")
 */
class ProductAttribute
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct","FullOperation"})
     * @ORM\Column(name="systemName", type="string", length=255)
     */
    private $systemName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct","FullOperation"})
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct","FullOperation"})
     * @ORM\Column(name="showName", type="string", length=255)
     */
    private $showName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct"})
     * @ORM\Column(name="defaultValue", type="string", length=255)
     */
    private $defaultValue;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\ProductAttributeProduct", mappedBy="productAttribute", cascade={"persist"})
     */
    private $productAttributeProducts;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     *
     * @return ProductAttribute
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * Get systemName
     *
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ProductAttribute
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set showName
     *
     * @param string $showName
     *
     * @return ProductAttribute
     */
    public function setShowName($showName)
    {
        $this->showName = $showName;

        return $this;
    }

    /**
     * Get showName
     *
     * @return string
     */
    public function getShowName()
    {
        return $this->showName;
    }

    /**
     * Set defaultValue
     *
     * @param string $defaultValue
     *
     * @return ProductAttribute
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }
}

