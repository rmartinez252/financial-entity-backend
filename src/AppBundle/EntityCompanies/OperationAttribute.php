<?php

namespace AppBundle\EntityCompanies;

use AppBundle\Operations\OperationProcess;
use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;


/**
 * OperationAttribute
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="operation_attribute")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OperationAttributeRepository")
 */
class OperationAttribute
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ProductAttributeProduct
     * @Serializer\Expose()
     * @Serializer\Groups({"FullOperation","FullOperationAttribute"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\ProductAttributeProduct", inversedBy="operationAttributes", cascade={"persist"})
     * @ORM\JoinColumn(name="idProductAttributeProduct", referencedColumnName="id")
     */
    private $attributeProduct;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"FullOperation","FullOperationAttribute"})
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * @var Operation
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Operation", inversedBy="attributes", cascade={"persist"})
     * @ORM\JoinColumn(name="idOperation", referencedColumnName="id")
     */
    private $operation;

    private $operationProcess;

    /**
     * OperationAttribute constructor.
     */
    public function __construct()
    {
        if($this->operation)
            if($this->operation->getId())
                $this->operationProcess = new OperationProcess($this->operation);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set attributeProduct
     *
     * @param ProductAttributeProduct $attributeProduct
     *
     * @return OperationAttribute
     */
    public function setAttributeProduct($attributeProduct)
    {
        $this->attributeProduct = $attributeProduct;

        return $this;
    }

    /**
     * Get attributeProduct
     *
     * @return ProductAttributeProduct
     */
    public function getAttributeProduct()
    {
        return $this->attributeProduct;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return OperationAttribute
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return Operation
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param Operation $operation
     * @return OperationAttribute
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
        return $this;
    }
    /**
     * Get Maturity Date
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("calculateValue")
     * @Serializer\Groups({"AllOperation"})
     *
     *
     * @return string
     */
    public function getMaturityDate(){
        if($this->operationProcess)
            if($amounts = $this->operationProcess->getProcessAmounts())
                if(isset($amounts[$this->getAttributeProduct()->getProductAttribute()->getSystemName()."Value"]))
                    return $amounts[$this->getAttributeProduct()->getProductAttribute()->getSystemName()."Value"];
        return false;
    }
    /**
     * @Serializer\PreSerialize
     */
    public function preSerialize(){
        if($this->operation)
            if($this->operation->getId())
                $this->operationProcess = new OperationProcess($this->operation);
    }

}

