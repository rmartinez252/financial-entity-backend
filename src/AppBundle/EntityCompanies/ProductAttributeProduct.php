<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * ProductAttributeProduct
 *
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Table(name="product_attribute_product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductAttributeProductRepository")
 */
class ProductAttributeProduct
{

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct"})
     * @ORM\Column(name="isNullable", type="boolean")
     */
    private $isNullable;

    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct"})
     * @ORM\Column(name="isCalculation", type="boolean")
     */
    private $isCalculation;
    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct"})
     * @ORM\Column(name="isPercentage", type="boolean")
     */
    private $isPercentage;
    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct"})
     * @ORM\Column(name="isPositive", type="boolean")
     */
    private $isPositive;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Product", inversedBy="attributes", cascade={"persist"})
     * @ORM\JoinColumn(name="idProduct", referencedColumnName="id")
     */
    private $product;

    /**
     * @var ProductAttribute
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAttributeProduct","FullOperation"})
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\ProductAttribute", inversedBy="productAttributeProducts", cascade={"persist"})
     * @ORM\JoinColumn(name="idProductAttribute", referencedColumnName="id")
     */
    private $productAttribute;
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\OperationAttribute", mappedBy="attributeProduct", cascade={"persist"})
     */
    private $operationAttributes;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isNullable
     *
     * @param boolean $isNullable
     *
     * @return ProductAttributeProduct
     */
    public function setIsNullable($isNullable)
    {
        $this->isNullable = $isNullable;

        return $this;
    }

    /**
     * Get isNullable
     *
     * @return bool
     */
    public function getIsNullable()
    {
        return $this->isNullable;
    }

    /**
     * Set isCalculation
     *
     * @param boolean $isCalculation
     *
     * @return ProductAttributeProduct
     */
    public function setIsCalculation($isCalculation)
    {
        $this->isCalculation = $isCalculation;

        return $this;
    }

    /**
     * Get isCalculation
     *
     * @return bool
     */
    public function getIsCalculation()
    {
        return $this->isCalculation;
    }

    /**
     * @return boolean
     */
    public function isIsPercentage()
    {
        return $this->isPercentage;
    }

    /**
     * @param boolean $isPercentage
     * @return ProductAttributeProduct
     */
    public function setIsPercentage($isPercentage)
    {
        $this->isPercentage = $isPercentage;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsPositive()
    {
        return $this->isPositive;
    }

    /**
     * @param boolean $isPositive
     * @return ProductAttributeProduct
     */
    public function setIsPositive($isPositive)
    {
        $this->isPositive = $isPositive;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return ProductAttributeProduct
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return ProductAttribute
     */
    public function getProductAttribute()
    {
        return $this->productAttribute;
    }

    /**
     * @param ProductAttribute $productAttribute
     * @return ProductAttributeProduct
     */
    public function setProductAttribute($productAttribute)
    {
        $this->productAttribute = $productAttribute;
        return $this;
    }



}

