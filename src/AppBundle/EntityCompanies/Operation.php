<?php

namespace AppBundle\EntityCompanies;

use AppBundle\Operations\OperationProcess;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;


/**
 * @Serializer\ExclusionPolicy("all")
 * Operation
 *
 * @ORM\Table(name="operation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OperationRepository")
 */
class Operation
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Product
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Product", inversedBy="operations", cascade={"persist"})
     * @ORM\JoinColumn(name="idProduct", referencedColumnName="id")
     */
    private $product;
    /**
     * @var Client
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Client", inversedBy="operations", cascade={"persist","refresh"})
     * @ORM\JoinColumn(name="idClient", referencedColumnName="id")
     */
    private $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateSystem", type="datetime", nullable=true)
     */
    private $dateSystem;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="effectiveDate", type="datetime", nullable=true)
     */
    private $effectiveDate;

    /**
     * @var int
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation","FullOperation"})
     *
     * @ORM\Column(name="tenor", type="integer")
     */
    private $tenor;
    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\OperationInvoice", mappedBy="operation", cascade={"persist","refresh"})
     * @ORM\OrderBy({"dateInvoice" = "ASC"})
     */
    private $invoices;

    /**
     * @var OperationStatus
     * @Serializer\Expose()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\OperationStatus", inversedBy="operations", cascade={"persist"})
     * @ORM\JoinColumn(name="idOperationStatus", referencedColumnName="id")
     */
    private $status;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation","FullOperation"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\OperationAttribute", mappedBy="operation", cascade={"persist","refresh"})
     */
    private $attributes;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation","FullOperation"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\AccountingEntry", mappedBy="operation", cascade={"persist","refresh"})
     */
    private $entries;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation","FullOperation"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Payment", mappedBy="operation", cascade={"persist"})
     */
    private $payments;
    /**
     * @var OperationProcess
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation"})
     */
    private $operationProcess;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation","FullOperation"})
     *
     * @ORM\Column(name="description", type="string", length=255,nullable=true)
     */
    private $description;

    /**
     * Operation constructor.
     */
    public function __construct()
    {
        $this->attributes = new ArrayCollection ;
        $this->invoices = new ArrayCollection ;
        $this->payments = new ArrayCollection ;
        if($this->getId())
            $this->operationProcess = new OperationProcess($this);

    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return Operation
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @return Operation
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateSystem()
    {
        return $this->dateSystem;
    }

    /**
     * @param \DateTime $dateSystem
     * @return Operation
     */
    public function setDateSystem($dateSystem)
    {
        $this->dateSystem = $dateSystem;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }
    /**
     * Get effective date
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("effective_date")
     * @Serializer\Groups({"AllOperation"})
     *
     *
     * @return string
     */
    public function getEffectiveDateFront()
    {
        return $this->effectiveDate->format("Y-m-d");
    }

    /**
     * @param \DateTime $effectiveDate
     * @return Operation
     */
    public function setEffectiveDate($effectiveDate)
    {
        $this->effectiveDate = $effectiveDate;
        return $this;
    }



    /**
     * @return int
     */
    public function getTenor()
    {
        return $this->tenor;
    }

    /**
     * @param int $tenor
     * @return Operation
     */
    public function setTenor($tenor)
    {
        $this->tenor = $tenor;
        return $this;
    }


    /**
     * Set invoices
     *
     * @param ArrayCollection $invoices
     *
     * @return Operation
     */
    public function setInvoices($invoices)
    {
        $this->invoices = $invoices;

        return $this;
    }
    /**
     * @param $invoice OperationInvoice
     * @return Operation
     */
    public function addInvoice(OperationInvoice $invoice)
    {
        $this->invoices->add($invoice);
        return $this;
    }

    /**
     * Get invoices
     *
     * @return ArrayCollection
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * Set attributes
     *
     * @param ArrayCollection $attributes
     *
     * @return Operation
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Get attributes
     *
     * @return ArrayCollection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return ArrayCollection
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * @param ArrayCollection $entries
     * @return Operation
     */
    public function setEntries($entries)
    {
        $this->entries = $entries;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param ArrayCollection $payments
     * @return Operation
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;
        return $this;
    }
    /**
     * @param $payment Payment
     * @return Operation
     */
    public function addPayment(Payment $payment)
    {
        $this->payments->add($payment);
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Operation
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get total
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("initialAmount")
     * @Serializer\Type("float")
     * @Serializer\Groups({"AllOperation"})
     *
     *
     * @return float
     */
    public function getInitialAmount(){
        return $this->operationProcess->getTotalAmount();
    }
    /**
     * Get total
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("totalAmount")
     * @Serializer\Type("float")
     * @Serializer\Groups({"AllOperation"})
     *
     *
     * @return float
     */
    public function getTotal(){
        return $this->operationProcess->getTotalAmount(true);
    }

    /**
     * Get Maturity Date
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("maturityDate")
     * @Serializer\Groups({"AllOperation"})
     *
     *
     * @return string
     */
    public function getMaturityDate(){
        return $this->operationProcess->getMaturityDate()->format("Y-m-d");
    }

    /**
     * @Serializer\PreSerialize
     */
    public function preSerialize(){
        if($this->getId())
            $this->operationProcess = new OperationProcess($this);
    }

    /**
     * @return OperationStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param OperationStatus $status
     * @return Operation
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }


}

