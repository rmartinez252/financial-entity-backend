<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Account
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="account")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountRepository")
 */
class Account
{
    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="historicCode", type="string", length=255, nullable=true)
     */
    private $historicCode;

    /**
     * @var string
     * @Serializer\Expose()
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="l", type="integer")
     */
    private $l;

    /**
     * @var int
     * @Serializer\Expose()
     *
     * @ORM\Column(name="r", type="integer")
     */
    private $r;
    /**
     * it's "0" if the account have a creditor nature
     * ( increase the total amount when receive a credit) -> Liabilities
     * and "1" if the account have a debtor nature
     * ( increase the total amount when receive a debit) -> Assets ,
     * and "2" if the account is the net income
     * (  ) -> NetIncome,
     *
     * @var boolean
     * @Serializer\Expose()
     *
     * @ORM\Column(name="nature", type="integer")
     */
    private $nature;

    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccount"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Entry", mappedBy="accountDebit", cascade={"persist", "remove"})
     *
     */
    private $entrysDebit;
    /**
     * @var ArrayCollection
     * @Serializer\Expose()
     * @Serializer\Groups({"FullAccount"})
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Entry", mappedBy="accountCredit", cascade={"persist", "remove"})
     *
     */
    private $entrysCredit;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\ProductAccountingConfigurationProduct", mappedBy="account", cascade={"persist", "remove"})
     *
     */
    private $configurationProducts;

    /**
     * @var float
     * @Serializer\Expose()
     */
    private $amountCredit;

    /**
     * @var float
     * @Serializer\Expose()
     */
    private $amountDebit;

    /**
     * @var float
     * @Serializer\Expose()
     */
    private $totalAmount;
    /**
     * @var boolean
     * @Serializer\Expose()
     */
    private $holdValue;

    /**
     * @var BankAccount
     * @ORM\OneToOne(targetEntity="BankAccount", mappedBy="accountingAccount", cascade={"all"})
     *
     */
    private $bankAccount;

    /**
     * Structure constructor.
     */
    public function __construct(){
        $this->entrysDebit = new ArrayCollection();
        $this->entrysCredit = new ArrayCollection();

    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Account
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Account
     */
    public function setCode($code){
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode(){
        return $this->code;
    }

    /**
     * @return string
     */
    public function getHistoricCode()
    {
        return $this->historicCode;
    }

    /**
     * @param string $historicCode
     * @return Account
     */
    public function setHistoricCode($historicCode)
    {
        $this->historicCode = $historicCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Account
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;

    }


    /**
     * Set l
     *
     * @param integer $l
     *
     * @return Account
     */
    public function setL($l){
        $this->l = $l;
        return $this;
    }

    /**
     * Get l
     *
     * @return int
     */
    public function getL(){
        return $this->l;
    }

    /**
     * Set r
     *
     * @param integer $r
     *
     * @return Account
     */
    public function setR($r){
        $this->r = $r;
        return $this;
    }

    /**
     * Get r
     *
     * @return int
     */
    public function getR(){
        return $this->r;
    }

    /**
     * @return ArrayCollection
     */
    public function getEntrysDebit(){
        return $this->entrysDebit;
    }

    /**
     * @param ArrayCollection $entrysDebit
     * @return Account
     */
    public function setEntrysDebit($entrysDebit){
        $this->entrysDebit = $entrysDebit;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getEntrysCredit(){
        return $this->entrysCredit;
    }

    /**
     * @param ArrayCollection $entrysCredit
     * @return Account
     */
    public function setEntrysCredit($entrysCredit){
        $this->entrysCredit = $entrysCredit;
        return $this;
    }

    public function getAllEntries(){
        return new ArrayCollection(array_merge($this->getEntrysCredit()->toArray(), $this->getEntrysDebit()->toArray()));
    }
    /**
     * @return integer
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     *
     * @param integer $nature
     * @return Account
     */
    public function setNature($nature)
    {
        $this->nature = $nature;
        return $this;
    }




    /**
     * @Serializer\PreSerialize
     */
    public function preSerializer(){
        /**
         * @var Entry $entry
         */
        if($this->getL() == $this->getR()-1 && $this->getNature() != 2) {
            $amountDebit = $amountCredit = 0;

            if ($this->entrysDebit) {
                foreach ($this->entrysDebit as $entry) {
                    $amountDebit += $entry->getValue();
                }
            }

            if($this->entrysCredit){
                foreach ($this->entrysCredit  as $entry){
                    $amountCredit += $entry->getValue();
                }
            }
            $this->setAmountCredit($amountCredit);
            $this->setAmountDebit($amountDebit);
        }
        if(is_null($this->holdValue))
            $this->setHoldValue();
        $this->totalAmount = 0;
        if($this->nature == 1)
            $this->totalAmount += $this->amountDebit - $this->amountCredit ;
        else
            $this->totalAmount += $this->amountCredit - $this->amountDebit;
        $this->totalAmount =(float) number_format($this->totalAmount,2, '.', '');

    }

    /**
     * @return boolean
     */
    public function getHoldValue(){
        if(is_null($this->holdValue))
            return $this->holdValue = $this->getL()+1 == $this->getR();
        return $this->holdValue;
    }
    public function setHoldValue($holdValue = "internalValue"){
        if($holdValue == "internalValue")
            return $this->holdValue = $this->getL()+1 == $this->getR();
        return $this->holdValue = $holdValue;
    }



    /**
     * @param float $amountCredit
     * @return float
     */
    public function setAmountCredit($amountCredit)
    {
        $this->amountCredit = $amountCredit;
        return $this->amountCredit;
    }

    /**
     * @param float $amountDebit
     * @return float
     */
    public function setAmountDebit($amountDebit)
    {

        $this->amountDebit = $amountDebit;
        return $this->amountDebit ;

    }

    /**
     * @return float
     */
    public function getAmountDebit()
    {
        if(is_null($this->amountDebit))
            $this->preSerializer();
        return $this->amountDebit;
    }

    /**
     * @return float
     */
    public function getAmountCredit()
    {
        if(is_null($this->amountCredit))
            $this->preSerializer();
        return $this->amountCredit;
    }

    public function setParentTotalAmounts($children,$accounts){

        $debit = $credit = 0;
        foreach ($children as $c){
            if($c['node']->getL()+1 == $c['node']->getR() && $c['node']->getNature()!= 2){
                $debit += $c['node']->getAmountDebit();
                $credit += $c['node']->getAmountCredit();
            }elseif($c['node']->getNature() == 2){
                $values = $this->setNetIncomeAmount($accounts);
                $debit += $values['debit'];
                $credit += $values['credit'];
            }
        }
        $this->setAmountCredit($credit);
        $this->setAmountDebit($debit);

    }

    public function setNetIncomeAmount(array $accounts){
        /**
         * @var $c Account
         */
        $debit = $credit = 0;
        $left4 = $left5 = $left6 = $right4 = $right5 = $right6 = null;

        foreach ($accounts as $c){

            if($left4 && $left5 && $left6)
                break;
            if($c->getCode()=="4"){
                $left4 = $c->getL();
                $right4 = $c->getR();
            }elseif($c->getCode()=="5"){
                $left5 = $c->getL();
                $right5 = $c->getR();
            }elseif($c->getCode()=="6"){
                $left6 = $c->getL();
                $right6 = $c->getR();
            }
        }
        $accounts456 = array_filter($accounts, function ($i) use ($left4, $right4,$left5, $right5 ,$left6, $right6) {
            /**
             * @var $i Account
             */
            if (($i->getL() >= $left4 && $i->getR() <= $right4 ) || ($i->getL() >= $left5 && $i->getR() <= $right5 ) || ($i->getL() >= $left6 && $i->getR() <= $right6 )) {
                return true;
            }
            return false;
        });
        foreach ($accounts456 as $c){
            if($c->getL()+1 == $c->getR() && $c->getNature()!= 2){
                $debit += $c->getAmountDebit();
                $credit += $c->getAmountCredit();
            }
        }

        $values['debit'] = $debit;
        $values['credit'] = $credit;
        $this->setAmountCredit($credit);
        $this->setAmountDebit($debit);
        return $values;

    }
    /**
     * Add Entry Credit
     *
     * @param Entry $entry
     *
     * @return Account
     */
    public function addEntryCredit(Entry $entry){
        $this->entrysCredit->add($entry);
        $entry->setAccountCredit($this);
        return $this;
    }

    /**
     * Remove Entru Credit
     *
     * @param Entry $entry
     */
    public function removeEntryCredit(Entry $entry){
        $this->entrysCredit->removeElement($entry);
    }
    /**
     * Add Entry Debit
     *
     * @param Entry $entry
     *
     * @return Account
     */
    public function addEntryDebit(Entry $entry){
        $this->entrysDebit->add($entry);
        $entry->setAccountDebit($this);
        return $this;
    }

    /**
     * Remove Entru Debit
     *
     * @param Entry $entry
     */
    public function removeEntryDebit(Entry $entry){
        $this->entrysDebit->removeElement($entry);
    }

    /**
     * @return BankAccount
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * @param BankAccount $bankAccount
     * @return Account
     */
    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = $bankAccount;
        return $this;
    }

    /**
     * @return boolean
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("isBankAccount")
     * @Serializer\Type("float")
     */
    public function isBankAccount(){
        if($this->bankAccount)
            return true;
        return false;
    }








}

