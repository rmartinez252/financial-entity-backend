<?php

namespace AppBundle\EntityCompanies;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * Product
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation","FullProduct","FullAccountingProduct","FullAttributeProduct"})
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"FullProduct"})
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;
    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation","FullProduct"})
     *
     * @ORM\Column(name="abbreviation", type="string", length=255, nullable=true)
     */
    private $abbreviation;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"AllOperation","FullProduct"})
     * @ORM\Column(name="portfolio", type="string")
     */
    private $portfolio;
    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"FullProduct","FullAccountingProduct","FullAttributeProduct"})
     * @ORM\Column(name="needsInvoice", type="boolean")
     */
    private $needsInvoice;

    /**
     * @var Process
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\EntityCompanies\Process", inversedBy="products", cascade={"persist"})
     * @ORM\JoinColumn(name="idProcess", referencedColumnName="id")
     */
    private $process;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\ProductAttributeProduct", mappedBy="product", cascade={"persist"})
     */
    private $attributes;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\ProductAccountingConfigurationProduct", mappedBy="product", cascade={"persist"})
     */
    private $accountingConfigurations;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\EntityCompanies\Operation", mappedBy="product", cascade={"persist"})
     */
    private $operations;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->attributes = new ArrayCollection();
        $this->accountingConfigurations = new ArrayCollection();
        $this->operations = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * @param string $abbreviation
     * @return Product
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;
        return $this;
    }

    /**
     * @return Process
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param Process $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return ArrayCollection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param ArrayCollection $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }


    /**
     * @return ArrayCollection
     */
    public function getAccountingConfigurations()
    {
        return $this->accountingConfigurations;
    }

    /**
     * @param ArrayCollection $accountingConfigurations
     */
    public function setAccountingConfigurations($accountingConfigurations)
    {
        $this->accountingConfigurations = $accountingConfigurations;
    }

    /**
     * @param ProductAccountingConfigurationProduct $pac
     * @return $this
     */
    public function addAccountingConfiguration(ProductAccountingConfigurationProduct $pac){
        $this->accountingConfigurations->add($pac);
        return $this;

    }

    /**
     * @return ArrayCollection
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * @param ArrayCollection $operations
     */
    public function setOperations($operations)
    {
        $this->operations = $operations;
    }

    /**
     * @return string
     */
    public function getPortfolio()
    {
        return $this->portfolio;
    }

    /**
     * @param string $portfolio
     * @return Product
     */
    public function setPortfolio($portfolio)
    {
        $this->portfolio = $portfolio;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isNeedsInvoice()
    {
        return $this->needsInvoice;
    }

    /**
     * @param boolean $needsInvoice
     * @return Product
     */
    public function setNeedsInvoice($needsInvoice)
    {
        $this->needsInvoice = $needsInvoice;
        return $this;
    }





}

