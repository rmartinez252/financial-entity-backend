<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/24/16
 * Time: 9:08 AM
 */

namespace AppBundle\Form;


use AppBundle\Entity\Attribute;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonAttributeCompaniesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('attribute',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Attribute',
                'em'=>$options['em']
            ))
            ->add('person',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Person',
                'em'=>$options['em']
            ))
            ->add('value', TextType::class);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityCompanies\PersonAttribute',
            'is_edit' => true,
            'csrf_protection' => false,
            'em' => null,
        ));
    }
}