<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/24/16
 * Time: 9:08 AM
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descriptionDebit', TextType::class, [] )
            ->add('descriptionCredit', TextType::class, []  )
            ->add('value', NumberType::class,array(
                'required'=>true
            ))
            ->add('accountingEntry',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\AccountingEntry',
                'em'=>$options['em']
            ))
            ->add('accountDebit',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Account',
                'required'=>true,
                'em'=>$options['em']
            ))
            ->add('accountCredit',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Account',
                'required'=>true,
                'em'=>$options['em']
            ))
            ->add('bankMoveDebit',BankMoveEntryType::class,array(
                'required'=>false,
                'em'=>$options['em']
            ))
            ->add('bankMoveCredit',BankMoveEntryType::class,array(
                'required'=>false,
                'em'=>$options['em']
            ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityCompanies\Entry',
            'is_edit' => true,
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'em'=>null,
        ));
    }
}