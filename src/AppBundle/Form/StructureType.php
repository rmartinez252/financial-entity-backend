<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/9/16
 * Time: 2:40 PM
 */

namespace AppBundle\Form;


//use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
//use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StructureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                // readonly if we're in edit mode
            ])
            ->add('l',IntegerType::class)
            ->add('r',IntegerType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Structure',
            'is_edit' => false,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ));
    }
}