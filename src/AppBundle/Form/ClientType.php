<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/9/16
 * Time: 2:40 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name', TextType::class,[
                'label'=> 'Client Name',
                'description' => 'Company or person name'
            ])
            ->add('address',AddressType::class,
                array(
                    'em'=>$options['em']
                ))
            ->add('email', EmailType::class,[
                'label' =>'Email',
                'description' => ''
            ])
            ->add('phone', TextType::class,[
                'label' =>'Phone',
                'description' => ''
            ])
            ->add('contactPerson',PersonCompaniesType::class,
            array(
                'em'=>$options['em']
            ))
            ;
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityCompanies\Client',
            'is_edit' => true,
            'csrf_protection' => false,
            'em' => null,
        ));
    }
}