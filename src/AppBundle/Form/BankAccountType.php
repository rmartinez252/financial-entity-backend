<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/9/16
 * Time: 2:40 PM
 */

namespace AppBundle\Form;


use Nelmio\ApiDocBundle\Tests\Fixtures\Form\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BankAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('accountNumber', TextType::class)
            ->add('nickName', TextType::class)
            ->add('idBank',EntityType::class,array(
                'class'=>'AppBundle\Entity\Bank'
            ))
            ->add('idAccountType',EntityType::class,array(
                'class'=>'AppBundle\Entity\BankAccountType'
            ))
            ->add('accountingAccount',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Account',
                'em'=>$options['em']
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityCompanies\BankAccount',
            'is_edit' => false,
            'csrf_protection' => false,
            'em'=>null
        ));
    }
}