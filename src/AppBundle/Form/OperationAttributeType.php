<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/24/16
 * Time: 9:08 AM
 */

namespace AppBundle\Form;


use AppBundle\Entity\Attribute;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OperationAttributeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('attributeProduct',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\ProductAttributeProduct',
                'em'=>$options['em']
            ))

            ->add('value', TextType::class)
        ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityCompanies\OperationAttribute',
            'is_edit' => true,
            'csrf_protection' => false,
            'em' => null,
        ));
    }
}