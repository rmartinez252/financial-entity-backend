<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 3/7/17
 * Time: 3:33 PM
 */

namespace AppBundle\Form\Model;


use AppBundle\EntityCompanies\BankMove;
use Symfony\Component\Validator\Constraints as Assert;

class OperationPaymentAblModel
{

    /**
     * @var BankMove
     * @Assert\NotBlank()
     */

    private $bankMove;

    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */

    private $value;

    /**
     * @return BankMove
     */
    public function getBankMove()
    {
        return $this->bankMove;
    }

    /**
     * @param BankMove $bankMove
     * @return OperationPaymentAblModel
     */
    public function setBankMove($bankMove)
    {
        $this->bankMove = $bankMove;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return OperationPaymentAblModel
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }







}