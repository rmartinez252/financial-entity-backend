<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 2/27/17
 * Time: 12:43 PM
 */

namespace AppBundle\Form\Model;
use JMS\Serializer\Annotation as Serializer;


/**
 *
 * @Serializer\ExclusionPolicy("all")
 */

class AccountModel
{


    /**
     * @Type("integer")
     * @var integer
     * @Serializer\Expose()
     */
    private $isBankAccount;
    private $id;
    private $name;
    private $code;
    private $description;
    private $l;
    private $r;
    private $nature;
    private $amount_credit;
    private $amount_debit;
    private $total_amount;

}