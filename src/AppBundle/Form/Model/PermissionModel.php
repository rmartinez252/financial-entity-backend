<?php

namespace AppBundle\Form\Model;

use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Security\PermissionsManager;
use Symfony\Component\Validator\Constraints as Assert;

class PermissionModel
{
    /**
     * @Assert\NotBlank()
     */
    private $id;

    /**
     * @Assert\NotBlank()
     */
    private $permissions;



    /**
     * @return int
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param array $permissions
     * @return PermissionModel
     */
    public function setPermissions(array $permissions)
    {
        $permissionManager = new PermissionsManager();
        if($permissionManager->getInt($permissions))
            $this->permissions = $permissionManager->getInt($permissions);
        else
            $this->permissions = 0;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return PermissionModel
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }



}