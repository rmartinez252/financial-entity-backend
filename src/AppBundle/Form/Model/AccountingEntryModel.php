<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 12/16/16
 * Time: 3:40 PM
 */

namespace AppBundle\Form\Model;


use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Entry;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AccountingEntryModel
 * @package AppBundle\Form\Model
 * @Serializer\ExclusionPolicy("all")
 */
class AccountingEntryModel
{
    
    /**
     * @var int
     * @Serializer\Expose()
     *
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     *
     */
    private $description;

    /**
     * @var int
     * @Serializer\Expose()
     *
     */
    private $idBeneficiary;

    /**
     * @var int
     * @Serializer\Expose()
     *
     */
    private $idModified;

    /**
     * @var \DateTime
     * @Serializer\Expose()
     *
     */
    private $dateUser;
    /**
     * @var \DateTime
     * @Serializer\Expose()
     *
     */
    private $dateSystem;
    /**
     * @var int
     *
     */
    private $idUser;


   
    private $entrys;
    /**
     * @var array
     * @Serializer\Expose()
     */
    private $entrysShow;
    /**
     * @var \AppBundle\EntityCompanies\AccountingEntryType
     * @Serializer\Expose()
     */
    private $type;

    /**
     * AccountingEntryModel constructor.
     * @param AccountingEntry $accountingEntry
     */
    public function __construct($accountingEntry)
    {
        $this->id = $accountingEntry->getId();
        $this->description = $accountingEntry->getDescription();
        $this->idBeneficiary = $accountingEntry->getIdBeneficiary();
        $this->idModified = $accountingEntry->getIdModified();
        $this->dateUser = $accountingEntry->getDateUserFront();
        $this->dateSystem = $accountingEntry->getDateSystemFront();
        $this->idUser = $accountingEntry->getIdUser();
        $this->entrys = $accountingEntry->getEntrys();
        $this->type = $accountingEntry->getType();
        /**
         * @var Entry $entry
         */
        foreach ($this->entrys as $entry){
            $this->entrysShow[$entry->getIdAccountDebit()]['descriptionDebit'] = $entry->getDescriptionDebit();
            $this->entrysShow[$entry->getIdAccountCredit()]['descriptionCredit'] = $entry->getDescriptionCredit();

            if(!isset($this->entrysShow[$entry->getIdAccountDebit()]['debit'])){
                $this->entrysShow[$entry->getIdAccountDebit()]['debit'] = 0;
        }
            $this->entrysShow[$entry->getIdAccountDebit()]['debit'] += $entry->getValue();
            $this->entrysShow[$entry->getIdAccountDebit()]['account'] = $entry->getAccountDebit();

            if(!isset($this->entrysShow[$entry->getIdAccountCredit()]['credit'])){
                $this->entrysShow[$entry->getIdAccountCredit()]['credit'] = 0;

            }

            $this->entrysShow[$entry->getIdAccountCredit()]['credit'] += $entry->getValue();
            $this->entrysShow[$entry->getIdAccountCredit()]['account'] = $entry->getAccountCredit();
        }
        foreach ($this->entrysShow as $account => $entryShow){
            if(isset($entryShow['debit']) && isset($entryShow['credit']))
            if($entryShow['debit'] && $entryShow['credit']){
                if($entryShow['debit'] >= $entryShow['credit']){
                    $this->entrysShow[$account]['debit'] = $entryShow['debit'] - $entryShow['credit'];
                    $this->entrysShow[$account]['credit'] = 0;
                }elseif($entryShow['debit'] < $entryShow['credit']){
                    $this->entrysShow[$account]['credit'] = $entryShow['credit'] - $entryShow['debit'];
                    $this->entrysShow[$account]['debit'] = 0;
                }
            }
        }
    }


}