<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 2/27/17
 * Time: 12:46 PM
 */


namespace AppBundle\Form\Model;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;


/**
 *
 * @Serializer\ExclusionPolicy("all")
 */
class AccountStructureModel
{
    /**
     * @Type("integer")
     * @var integer
     * @Serializer\Expose()
     */
    private $level;
    /**
     * @Type("boolean")
     * @var boolean
     * @Serializer\SerializedName("node[holdvalue]")
     * @Serializer\Expose()
     */
    private $holdValue;
    /**
     * @Type("boolean")
     * @var boolean
     * @Serializer\SerializedName("node[isBankAccount]")
     * @Serializer\Expose()
     */

    private $isBankAccount;
    /**
     * @Type("integer")
     * @var integer
     * @Serializer\SerializedName("node[id]")
     * @Serializer\Expose()
     */
    private $id;
    /**
     * @Type("string")
     * @var string
     * @Serializer\SerializedName("node[name]")
     * @Serializer\Expose()
     */
    private $name;
    /**
     * @Type("string")
     * @var string
     * @Serializer\SerializedName("node[code]")
     * @Serializer\Expose()
     */
    private $code;
    /**
     * @Type("string")
     * @var string
     * @Serializer\SerializedName("node[description]")
     * @Serializer\Expose()
     */
    private $description;
    /**
     * @Type("integer")
     * @var integer
     * @Serializer\SerializedName("node[l]")
     * @Serializer\Expose()
     */
    private $l;
    /**
     * @Type("integer")
     * @var integer
     * @Serializer\SerializedName("node[r]")
     * @Serializer\Expose()
     */
    private $r;
    /**
     * @Type("boolean")
     * @var boolean
     * @Serializer\SerializedName("node[nature]")
     * @Serializer\Expose()
     */
    private $nature;
    /**
     * @Type("float")
     * @var float
     * @Serializer\SerializedName("node[amount_credit]")
     * @Serializer\Expose()
     */
    private $amount_credit;
    /**
     * @Type("float")
     * @var float
     * @Serializer\SerializedName("node[amount_debit]")
     * @Serializer\Expose()
     */
    private $amount_debit;
    /**
     * @Type("float")
     * @var float
     * @Serializer\SerializedName("node[total_amount]")
     * @Serializer\Expose()
     */
    private $total_amount;


}