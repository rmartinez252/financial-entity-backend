<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 3/7/17
 * Time: 3:33 PM
 */

namespace AppBundle\Form\Model;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AblDateModel
 * @package AppBundle\Form\Model
 * @Serializer\ExclusionPolicy("all")
 */

class AblDateModel
{

    /**
     * @var \DateTime
     *
     */
    private $payment1;
    /**
     * @var \DateTime
     *
     */
    private $payment2;
    /**
     * @var \DateTime
     *
     */
    private $payment3;

    public function __construct($initialDate,$day1 = 90, $day3 = 92, $day2 = 92)
    {
        $initialDate = \DateTimeImmutable::createFromFormat("Y-m-d", $initialDate);

        $interval = new \DateInterval("P".$day1."D");
        $this->payment1 = $initialDate->add($interval);
//        var_dump($initialDate);
//        var_dump($this->payment1);
        while($this->payment1->format("N") >= 6){
            $day1++;
            $day2--;

            $interval = new \DateInterval("P".$day1."D");
            $this->payment1 = $initialDate->add($interval);
        }

        $interval = new \DateInterval("P".$day2."D");
        $this->payment2 = $this->payment1->add($interval);
        while($this->payment2->format("N") >= 6){
            $day2++;
            $day3--;

            $interval = new \DateInterval("P".$day2."D");
            $this->payment2 = $this->payment1->add($interval);
        }
        $interval = new \DateInterval("P".$day3."D");
        $this->payment3 = $this->payment2->add($interval);
        while($this->payment3->format("N") >= 6){
            $day3++;

            $interval = new \DateInterval("P".$day3."D");
            $this->payment3 = $this->payment2->add($interval);
        }
//        var_dump($initialDate);
//        var_dump($this->payment1);
//        var_dump($this->payment2);
//        var_dump($this->payment3);
//        exit;
    }

    /**
     * @return \DateTime
     */
    public function getPayment1()
    {
        return $this->payment1;
    }

    /**
     * @param \DateTime $payment1
     * @return AblDateModel
     */
    public function setPayment1($payment1)
    {
        $this->payment1 = $payment1;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPayment2()
    {
        return $this->payment2;
    }

    /**
     * @param \DateTime $payment2
     * @return AblDateModel
     */
    public function setPayment2($payment2)
    {
        $this->payment2 = $payment2;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPayment3()
    {
        return $this->payment3;
    }

    /**
     * @param \DateTime $payment3
     * @return AblDateModel
     */
    public function setPayment3($payment3)
    {
        $this->payment3 = $payment3;
        return $this;
    }


    /**
     * @return \DateTime
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("payment1")
     */
    public function getPayment1ToString()
    {
        return $this->payment1->format("Y-m-d");
    }
    /**
     * @return \DateTime
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("payment2")
     */
    public function getPayment2ToString()
    {
        return $this->payment2->format("Y-m-d");
    }
    /**
     * @return \DateTime
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("payment3")
     */
    public function getPayment3ToString()
    {
        return $this->payment3->format("Y-m-d");
    }



}