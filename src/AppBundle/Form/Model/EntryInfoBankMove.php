<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 3/7/17
 * Time: 4:13 PM
 */

namespace AppBundle\Form\Model;


use AppBundle\EntityCompanies\Account;

class EntryInfoBankMove
{

    /**
     * @var string
     */
    private $description;
    /**
     * @var float
     */
    private $value;

    /**
     * @var  Account
     */
    private $account;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return EntryInfoBankMove
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return EntryInfoBankMove
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return EntryInfoBankMove
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }

}