<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 12/16/16
 * Time: 3:40 PM
 */

namespace AppBundle\Form\Model\Reports;


use AppBundle\EntityCompanies\Account;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Entry;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AccountingEntryModel
 * @package AppBundle\Form\Model
 * @Serializer\ExclusionPolicy("all")
 */
class GeneralLedgerModel
{

    /**
     * @var int
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var string
     * @Serializer\Expose()
     */
    private $code;

    /**
     * @var string
     * @Serializer\Expose()
     */
    private $historicCode;

    /**
     * @var string
     * @Serializer\Expose()
     *
     */
    private $description;


    /**
     * it's true if the account have a debtor nature
     * ( increase the total amount when receive a debit) -> Assets ,
     * and false if the account have a creditor nature
     * ( increase the total amount when receive a credit) -> Liabilities
     *
     * @var boolean
     * @Serializer\Expose()
     *
     */
    private $nature;

    /**
     * @var ArrayCollection
     *
     *
     */
    private $entrysDebit;
    /**
     * @var ArrayCollection
     *
     *
     */
    private $entrysCredit;

    /**
     * @var float
     * @Serializer\Expose()
     */
    private $startAmountCredit;

    /**
     * @var float
     * @Serializer\Expose()
     */
    private $startAmountDebit;

    /**
     * @var float
     * @Serializer\Expose()
     */
    private $startTotalAmount;
    /**
     * @var array
     * @Serializer\Expose()
     */
    private $entries;

    /**
     * GeneralLedgerModel constructor.
     * @param Account $account
     * @param $dateStart
     * @param $dateEnd
     */
    public function __construct($account,$dateStart,$dateEnd)
    {
        /**
         * @var Entry $entry
         */
        $this->id = $account->getId();
        $this->name = $account->getName();
        $this->code = $account->getCode();
        $this->historicCode = $account->getHistoricCode();
        $this->description = $account->getDescription();
        $this->nature = $account->getNature();
        $this->entrysDebit = $account->getEntrysDebit();
        $this->entrysCredit = $account->getEntrysCredit();


        if($dateStart && $dateEnd){
            if($dateEnd < $dateStart){
                $safeDate = $dateEnd;
                $dateEnd = $dateStart;
                $dateStart = $safeDate;
            }
        }

        /** @var $dateStart \DateTime */
        /** @var $dateEnd \DateTime */
        if ($dateStart){
            $dateStart = \DateTime::createFromFormat("Y-m-d", $dateStart);
            $dateStart->setTime(0,0,0);
            $dateStart = $dateStart->format('U');
        }
        if ($dateEnd){
            $dateEnd = \DateTime::createFromFormat("Y-m-d", $dateEnd);
            $dateEnd->setTime(23,59,59);
            $dateEnd = $dateEnd->format('U');
        }




        $arrayEntries = array();
        if($account->getL() == $account->getR()-1) {
            $startAmountDebit = $startAmountCredit = 0;

            if ($this->entrysDebit) {
                foreach ($this->entrysDebit as $entry) {
                    if($dateStart) {
                        if ($entry->getAccountingEntry()->getDateUser()->format('U') < $dateStart)
                            $startAmountDebit += $entry->getValue();
                    }/*else
                        $this->startAmountDebit += $entry->getValue();*/
                    if($dateStart)
                        if ($entry->getAccountingEntry()->getDateUser()->format('U') < $dateStart)
                            continue;
                    if($dateEnd)
                        if($entry->getAccountingEntry()->getDateUser()->format('U') > $dateEnd  )
                            continue;
                    $arrayEntries[$entry->getAccountingEntry()->getDateUser()->format("Y-m-d")][$entry->getId()]['type'] = "debit";
                    $arrayEntries[$entry->getAccountingEntry()->getDateUser()->format("Y-m-d")][$entry->getId()]['entry'] = $entry;
                }
            }
            if($this->entrysCredit){
                foreach ($this->entrysCredit  as $entry){
                    if($dateStart) {
                        if ($entry->getAccountingEntry()->getDateUser()->format('U') <= $dateStart)
                            $startAmountCredit += $entry->getValue();
                    }/*else
                        $this->startAmountCredit += $entry->getValue();*/
                    if($dateStart)
                        if ($entry->getAccountingEntry()->getDateUser()->format('U') < $dateStart)
                            continue;
                    if($dateEnd)
                        if($entry->getAccountingEntry()->getDateUser()->format('U') > $dateEnd )
                            continue;
                    $arrayEntries[$entry->getAccountingEntry()->getDateUser()->format("Y-m-d")][$entry->getId()]['type'] = "credit";
                    $arrayEntries[$entry->getAccountingEntry()->getDateUser()->format("Y-m-d")][$entry->getId()]['entry'] = $entry;
                }
            }

            $this->startAmountCredit = $account->setAmountCredit($startAmountCredit);
            $this->startAmountDebit = $account->setAmountDebit($startAmountDebit);

        }
        krsort($arrayEntries);
        foreach ($arrayEntries as $date => $ents){
            foreach ($ents as $ent){
                $entry = $ent['entry'];
                if(isset($this->entries[$date][$entry->getAccountingEntry()->getId()])){
                    if($this->entries[$date][$entry->getAccountingEntry()->getId()]['type'] == $ent['type'])
                        $this->entries[$date][$entry->getAccountingEntry()->getId()]['value'] += $entry->getValue();
                    else{
                        $this->entries[$date][$entry->getAccountingEntry()->getId()]['value'] -= $entry->getValue();
                        if($this->entries[$date][$entry->getAccountingEntry()->getId()]['value'] <= 0){
                            $this->entries[$date][$entry->getAccountingEntry()->getId()]['type'] = $ent['type'];
                            $this->entries[$date][$entry->getAccountingEntry()->getId()]['value'] *= -1;
                        }
                    }

                }else{
                    $this->entries[$date][$entry->getAccountingEntry()->getId()]['type'] = $ent['type'];
                    $this->entries[$date][$entry->getAccountingEntry()->getId()]['description'] = $entry->getAccountingEntry()->getDescription();
                    $this->entries[$date][$entry->getAccountingEntry()->getId()]['value'] = $entry->getValue();
                    $this->entries[$date][$entry->getAccountingEntry()->getId()]['idAccountingEntry'] = $entry->getAccountingEntry()->getId();
                }
            }
        }
        if($this->nature)
            $this->startTotalAmount = $this->startAmountDebit - $this->startAmountCredit ;
        else
            $this->startTotalAmount = $this->startAmountCredit - $this->startAmountDebit;
        $this->startTotalAmount = (float) number_format($this->startTotalAmount,2, '.', '');

    }

    /**
     * @return array
     */
    public function getEntries()
    {
        return $this->entries;
    }



}