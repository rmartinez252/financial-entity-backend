<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 3/7/17
 * Time: 3:33 PM
 */

namespace AppBundle\Form\Model;


use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Client;
use AppBundle\EntityCompanies\Operation;
use Symfony\Component\Validator\Constraints as Assert;

class OperationPaymentModel
{

    /**
     * @var BankMove
     * @Assert\NotBlank()
     * @Assert\Type("object")
     */
    private $bankMove;

    /**
     * @var Client
     * @Assert\NotBlank()
     * @Assert\Type("object")
     *
     */
    private $debtor;
    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    private $value;

    /**
     * @var boolean
     * @Assert\Type("Bool")
     */
    private $lateFeeFirst;
    
    /**
     * @var boolean
     * @Assert\Type("Bool")
     */
    private $strict;

    /**
     * @var boolean
     * @Assert\Type("Bool")
     */
    private $payAll;

    /**
     * @var array
     */
    private $options;

    /**
     * @return BankMove
     */
    public function getBankMove()
    {
        return $this->bankMove;
    }

    /**
     * @param BankMove $bankMove
     * @return OperationPaymentModel
     */
    public function setBankMove($bankMove)
    {
        if($bankMove->getIdMoveType())
            $this->bankMove = $bankMove;
        return $this;
    }

    /**
     * @return Client
     */
    public function getDebtor()
    {
        return $this->debtor;
    }

    /**
     * @param Client $debtor
     * @return OperationPaymentModel
     */
    public function setDebtor($debtor)
    {
        $this->debtor = $debtor;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return OperationPaymentModel
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isLateFeeFirst()
    {
        return $this->lateFeeFirst;
    }

    /**
     * @param boolean $lateFeeFirst
     * @return OperationPaymentModel
     */
    public function setLateFeeFirst($lateFeeFirst)
    {
//        if($lateFeeFirst === null)
//            $lateFeeFirst = true;
        $this->lateFeeFirst = $lateFeeFirst;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isStrict()
    {
        return $this->strict;
    }

    /**
     * @param boolean $strict
     * @return OperationPaymentModel
     */
    public function setStrict($strict)
    {
        $this->strict = $strict;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isPayAll()
    {
        return $this->payAll;
    }

    /**
     * @param boolean $payAll
     * @return OperationPaymentModel
     */
    public function setPayAll($payAll)
    {
        $this->payAll = $payAll;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        $this->options['lateFeeFirst'] =$this->isLateFeeFirst();
        $this->options['strict'] =$this->isStrict();
        $this->options['payAll'] =$this->isPayAll();
        return $this->options;
    }

    





}