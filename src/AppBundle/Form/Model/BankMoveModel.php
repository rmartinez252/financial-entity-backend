<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 3/7/17
 * Time: 3:33 PM
 */

namespace AppBundle\Form\Model;


use AppBundle\Entity\User;
use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\BankMove;
use AppBundle\EntityCompanies\Entry;
use Doctrine\Common\Collections\ArrayCollection;

class BankMoveModel extends BankMove
{

    /**
     * @var ArrayCollection
     *
     */
    private $entries;
    /**
     * @var User
     */
    private $user;

    /**
     * BankMoveModel constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * @return ArrayCollection
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * @param ArrayCollection $entries
     * @return BankMoveModel
     */
    public function setEntries($entries)
    {
        $this->entries = $entries;
        return $this;
    }
    public function createMove(){
        $accountingEntry = new AccountingEntry();
        $accountingEntry->setDateUser($this->getDateUser());
        $accountingEntry->setIdUser($this->user->getId());
        $accountingEntry->setDescription($this->getDescription());
        $move = new BankMove();
        $move->setBankAccount($this->getBankAccount());
        $move->setBankMoveType($this->getBankMoveType());
        $move->setNumber($this->getNumber());
        $move->setDescription($this->getDescription());
        $move->setDateUser($this->getDateUser());
        $arrayEntries = new ArrayCollection();
        /**
         * @var $infoEntry EntryInfoBankMove
         */
        if(!$this->getBankMoveType()->isNature()){
            foreach($this->entries as $infoEntry){
                $entry = new Entry();
                $entry->setAccountDebit($infoEntry->getAccount());
                $entry->setAccountCredit($this->getBankAccount()->getAccountingAccount());
                $entry->setBankMoveCredit($move);

                $entry->setDescriptionCredit($infoEntry->getDescription());
                $entry->setDescriptionDebit($infoEntry->getDescription());
                $entry->setValue($infoEntry->getValue());

                $entry->setAccountingEntry($accountingEntry);
                $arrayEntries->add( $entry);
            }
            $move->setEntryCredit($arrayEntries);

        }else{
            foreach($this->entries as $infoEntry){
                $entry = new Entry();
                $entry->setAccountDebit($this->getBankAccount()->getAccountingAccount());
                $entry->setAccountCredit($infoEntry->getAccount());
                $entry->setBankMoveDebit($move);

                $entry->setDescriptionCredit($infoEntry->getDescription());
                $entry->setDescriptionDebit($infoEntry->getDescription());
                $entry->setValue($infoEntry->getValue());

                $entry->setAccountingEntry($accountingEntry);
                $arrayEntries->add( $entry);
            }
            $move->setEntryDebit($arrayEntries);
        }
        return $move;
    }
    public function updateMove(BankMove $move){
        $newMove = $this->createMove();
        if($newMove->getDescription())
            $move->setDescription($newMove->getDescription());
        if($newMove->getDateUser())
            $move->setDateUser($newMove->getDateUser());
        
        if($newMove->getBankMoveType())
            $move->setBankMoveType($newMove->getBankMoveType());
        
        if($newMove->getNumber())
            $move->setNumber($newMove->getNumber());
        
        if($newMove->getEntryCredit())
            $move->setEntryCredit($newMove->getEntryCredit());
        
        if($newMove->getEntryDebit())
            $move->setEntryDebit($newMove->getEntryDebit());
        
        if($newMove->getBankAccount())
            $move->setBankAccount($newMove->getBankAccount());
        return $move;
        
    }


}