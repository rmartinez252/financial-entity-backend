<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/24/16
 * Time: 9:08 AM
 */

namespace AppBundle\Form;


use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubPermissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('VIEW', CheckboxType::class )
            ->add('POST', CheckboxType::class )
            ->add('DELETE', CheckboxType::class )
            ->add('APPROVE', CheckboxType::class );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'is_edit' => true,
            'csrf_protection' => false,
        ));
    }
}