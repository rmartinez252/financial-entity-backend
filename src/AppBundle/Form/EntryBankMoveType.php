<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/24/16
 * Time: 9:08 AM
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntryBankMoveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, []  )
            ->add('value', NumberType::class)
            ->add('account',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Account',
                'required'=>true,
                'em'=>$options['em']
            ))
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Form\Model\EntryInfoBankMove',
            'is_edit' => true,
            'csrf_protection' => false,
            'em'=>null,
        ));
    }
}