<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/9/16
 * Time: 2:40 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OperationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('product',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Product',
                'required'=>true,
                'em'=>$options['em']
            ))
            ->add('client',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Client',
                'em'=>$options['em']
            ))
            ->add('description', TextType::class)
            ->add('effectiveDate', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ])
            ->add('tenor', NumberType::class)
            ->add('invoices', CollectionType::class, array(
                'entry_type' => OperationInvoiceType::class,
                'allow_add' => true,
                'required'=>false,
                'by_reference' => true,
                'entry_options'=>array("em"=>$options['em'])
            ))
            ->add('attributes', CollectionType::class, array(
                'entry_type' => OperationAttributeType::class,
                'allow_add' => true,
                'required'=>true,
                'by_reference' => true,
                'entry_options'=>array("em"=>$options['em'])
            ));
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityCompanies\Operation',
            'is_edit' => false,
            'csrf_protection' => false,
            'em' => null
        ));
    }
}