<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/24/16
 * Time: 9:08 AM
 */

namespace AppBundle\Form;


use AppBundle\Entity\Attribute;

use AppBundle\Repository\ClientRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OperationPaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('debtor',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\Client',
                'required'=>true,
                'em'=>$options['em'],
                'query_builder' => function (ClientRepository $er) use($options) {
                    return $er->findDebtorQuery($options['idOperation']);
                }
            ))
            ->add('value', NumberType::class,array(
                'required'=>true,
            ))
            ->add('bankMove',BankMoveOperationPaymentType::class,array(
                'required'=>true
            ))
            ->add('lateFeeFirst',CheckboxType::class,array(
                'required'=>false
            ))
            ->add('strict',CheckboxType::class,array(
                'required'=>false
            ))
            ->add('payAll',CheckboxType::class,array(
                'required'=>false
            ))
        ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Form\Model\OperationPaymentModel',
            'is_edit' => false,
            'csrf_protection' => false,
            'em' => null,
            'idOperation'=>null
        ));
    }
}