<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/9/16
 * Time: 2:40 PM
 */

namespace AppBundle\Form;


//use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
//use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class AccountingEntryType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, [])
            ->add('dateUser', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ])->add('type',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\AccountingEntryType',
                'required'=>false,
                'em'=>$options['em']
            ))
            ->add('entrys', CollectionType::class, array(
                'entry_type' => EntryType::class,
                'allow_add' => true,
                'required'=>true,
                'by_reference' => false,
                'entry_options'=>array('em'=>$options['em']),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityCompanies\AccountingEntry',
            'is_edit' => false,
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'em' => null,
        ));
    }
}