<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/31/16
 * Time: 9:15 AM
 */

namespace AppBundle\Form\Api;


use Symfony\Component\Form\Form;

class FormResponse
{

    private $formFactory;
    private $formFactory2;
    private $formFactory3;
    private $formFactory4;
    private $formFactory5;
    private $formFactory6;
    private $formFactory7;
    private $formFactory8;
    /**
     * @var null
     */
    private $data;

    /**
     * FormResponse constructor.
     * @param  Form $formFactory
     */
    public function __construct($formFactory,$data = null)
    {
//        $this->formFactory = $formFactory->all();
//        $this->formFactory2 = $formFactory->getData();
        $this->formFactory5 = $formFactory->getConfig()->getViewTransformers();
        $this->formFactory4 = $formFactory->isValid();
//        $this->formFactory5 = $formFactory->getViewData();
//        $this->formFactory6 = $formFactory->all();
//        $this->formFactory7 = $formFactory->all();
//        $this->formFactory8 = $formFactory->all();
        $this->data = $data;
    }
    public function setForm($formFactory){
        $this->formFactory = $formFactory;
    }
    public function getForm($formAll = null ){
        if(!$formAll)
            $formAll = $this->formFactory4;
        $form = false;

        foreach($formAll as $t => $v ){
//            return $v;
            if($v->children)
                $form[$t] = $this->getForm($v->children);
            elseif(isset($v->vars['prototype']))
                $form[$t]['prototype'] = $this->getForm($v->vars['prototype']);
            else{
                $form[$t]['fieldName']=$v->vars['full_name'];
                $form[$t]['label']=$v->vars['label'];
                $form[$t]['required']=$v->vars['required'];
                $form[$t]['description']=$v->vars['description'];
            }

        }

        return $form;


    }




}