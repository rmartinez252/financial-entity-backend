<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/9/16
 * Time: 2:40 PM
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BankMoveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', TextType::class,array(
                'required'=>false
            ))
            ->add('description', TextType::class,array(
                'required'=>false
            ))
            ->add('dateUser', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd'
            ])
            ->add('bankMoveType',EntityType::class,array(
                'class'=>'AppBundle\Entity\BankMoveType'
            ))
            ->add('bankAccount',EntityType::class,array(
                'class'=>'AppBundle\EntityCompanies\BankAccount',
                'em'=>$options['em']
            ))
            ->add('entries', CollectionType::class, array(
                'entry_type' => EntryBankMoveType::class,
                'allow_add' => true,
                'by_reference' => false,
                'entry_options'=>array('em'=>$options['em']),
            ));

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Form\Model\BankMoveModel',
            'is_edit' => false,
            'csrf_protection' => false,
            'em' => null,
        ));
    }
}