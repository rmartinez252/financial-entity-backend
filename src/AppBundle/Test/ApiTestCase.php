<?php

namespace AppBundle\Test;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyEndpoint;
use AppBundle\Entity\Endpoint;
use AppBundle\Entity\Programmer;
use AppBundle\Entity\Project;
use AppBundle\Entity\Structure;
use AppBundle\Entity\StructureEndpoint;
use AppBundle\Entity\User;
use AppBundle\Entity\UserCompanyEndpoint;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AppBundle\DataFixtures\ORM\LoadFixtures;


class ApiTestCase extends KernelTestCase
{
    private static $staticClient;

    /**
     * @var array
     */
    private static $history = array();

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ConsoleOutput
     */
    private $output;

    /**
     * @var FormatterHelper
     */
    private $formatterHelper;

    private $responseAsserter;

    public static function setUpBeforeClass()
    {
        $handler = HandlerStack::create();
        $handler->push(Middleware::history(self::$history));
        $handler->push(Middleware::mapRequest(function(RequestInterface $request) {
            $path = $request->getUri()->getPath();
            if (strpos($path, '/app_test.php') !== 0) {
                $path = '/app_test.php' . $path;
            }
            $uri = $request->getUri()->withPath($path);
            return $request->withUri($uri);
        }));
        $baseUrl = getenv('TEST_BASE_URL');
        if (!$baseUrl) {
            static::fail('No TEST_BASE_URL environmental variable set in phpunit.xml.');
        }
        self::$staticClient = new Client([
            'base_uri' => $baseUrl,
            'http_errors' => false,
            'handler' => $handler
        ]);

        self::bootKernel();
    }

    protected function setUp()
    {
        $this->client = self::$staticClient;
        // reset the history
        self::$history = array();
        $this->purgeDatabase();
        $fixtures =  new LoadFixtures();
        $fixtures->setContainer(self::$kernel->getContainer());
        $fixtures->load();

    }

    /**
     * Clean up Kernel usage in this test.
     */
    protected function tearDown()
    {
        // purposefully not calling parent class, which shuts down the kernel
    }

    protected function onNotSuccessfulTest(Exception $e)
    {
        if ($lastResponse = $this->getLastResponse()) {
            $this->printDebug('');
            $this->printDebug('<error>Failure!</error> when making the following request:');
            $this->printLastRequestUrl();
            $this->printDebug('');

            $this->debugResponse($lastResponse);
        }

        throw $e;
    }

    private function purgeDatabase()
    {
        $purger = new ORMPurger($this->getService('doctrine')->getManager());
        $purger->purge();
    }

    protected function getService($id)
    {
        return self::$kernel->getContainer()
            ->get($id);
    }

    protected function printLastRequestUrl()
    {
        $lastRequest = $this->getLastRequest();

        if ($lastRequest) {
            $this->printDebug(sprintf('<comment>%s</comment>: <info>%s</info>', $lastRequest->getMethod(), $lastRequest->getUri()));
        } else {
            $this->printDebug('No request was made.');
        }
    }

    protected function debugResponse(ResponseInterface $response)
    {
        foreach ($response->getHeaders() as $name => $values) {
            $this->printDebug(sprintf('%s: %s', $name, implode(', ', $values)));
        }
        $body = (string) $response->getBody();

        $contentType = $response->getHeader('Content-Type');
        $contentType = $contentType[0];
        if ($contentType == 'application/json' || strpos($contentType, '+json') !== false) {
            $data = json_decode($body);
            if ($data === null) {
                // invalid JSON!
                $this->printDebug($body);
            } else {
                // valid JSON, print it pretty
                $this->printDebug(json_encode($data, JSON_PRETTY_PRINT));
            }
        } else {
            // the response is HTML - see if we should print all of it or some of it
            $isValidHtml = strpos($body, '</body>') !== false;

            if ($isValidHtml) {
                $this->printDebug('');
                $crawler = new Crawler($body);

                // very specific to Symfony's error page
                $isError = $crawler->filter('#traces-0')->count() > 0
                    || strpos($body, 'looks like something went wrong') !== false;
                if ($isError) {
                    $this->printDebug('There was an Error!!!!');
                    $this->printDebug('');
                } else {
                    $this->printDebug('HTML Summary (h1 and h2):');
                }

                // finds the h1 and h2 tags and prints them only
                foreach ($crawler->filter('h1, h2')->extract(array('_text')) as $header) {
                    // avoid these meaningless headers
                    if (strpos($header, 'Stack Trace') !== false) {
                        continue;
                    }
                    if (strpos($header, 'Logs') !== false) {
                        continue;
                    }

                    // remove line breaks so the message looks nice
                    $header = str_replace("\n", ' ', trim($header));
                    // trim any excess whitespace "foo   bar" => "foo bar"
                    $header = preg_replace('/(\s)+/', ' ', $header);

                    if ($isError) {
                        $this->printErrorBlock($header);
                    } else {
                        $this->printDebug($header);
                    }
                }

                $profilerUrl = $response->getHeader('X-Debug-Token-Link');
                if ($profilerUrl) {
                    $fullProfilerUrl = $response->getHeader('Host').$profilerUrl[0];
                    $this->printDebug('');
                    $this->printDebug(sprintf(
                        'Profiler URL: <comment>%s</comment>',
                        $fullProfilerUrl
                    ));
                }

                // an extra line for spacing
                $this->printDebug('');
            } else {
                $this->printDebug($body);
            }
        }
    }

    /**
     * Print a message out - useful for debugging
     *
     * @param $string
     */
    protected function printDebug($string)
    {
        if ($this->output === null) {
            $this->output = new ConsoleOutput();
        }

        $this->output->writeln($string);
    }

    /**
     * Print a debugging message out in a big red block
     *
     * @param $string
     */
    protected function printErrorBlock($string)
    {
        if ($this->formatterHelper === null) {
            $this->formatterHelper = new FormatterHelper();
        }
        $output = $this->formatterHelper->formatBlock($string, 'bg=red;fg=white', true);

        $this->printDebug($output);
    }

    /**
     * @return RequestInterface
     */
    private function getLastRequest()
    {
        if (!self::$history || empty(self::$history)) {
            return null;
        }

        $history = self::$history;

        $last = array_pop($history);

        return $last['request'];
    }

    /**
     * @return ResponseInterface
     */
    private function getLastResponse()
    {
        if (!self::$history || empty(self::$history)) {
            return null;
        }

        $history = self::$history;

        $last = array_pop($history);

        return $last['response'];
    }

    protected function createUser($username, $plainPassword = 'foo',Company $company = NULL){
        $user = new User();
        $user->setUsername($username);
        $password = $this->getService('security.password_encoder')
            ->encodePassword($user, $plainPassword);
        $user->setPassword($password);
        if($company)
            $user->setCompany($company);
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
        return $user;
    }
    protected function createUserAndPermissions($username, $plainPassword = 'foo',Company $company)
    {
        $user = $this->createUser($username,$plainPassword,$company);
        $em = $this->getEntityManager();
        foreach($company->getEndpoints() as $ce){
            $user_company_endpoint = new UserCompanyEndpoint();
            $user_company_endpoint->setUser($user);
            $user_company_endpoint->setCompanyEndpoint($ce);
            $user_company_endpoint->setPermission($ce->getPermission());
            $em->persist($user_company_endpoint);
            $em->flush();
        }

    }
    protected function createCompany($companyName,$direction='dir',$email='foo@bar.com',$phone='3056761728')
    {
        $company = new Company();
        $company->setName($companyName);
        $company->setDirection($direction);
        $company->setEmail($email);
        $company->setPhone($phone);
        $em = $this->getEntityManager();
        $em->persist($company);
        $em->flush();
        $endpoints = $this->getEntityManager()->getRepository('AppBundle:Endpoint')->findAll();
        foreach ($endpoints as $ep){
            $company_endpoint = new CompanyEndpoint();
            $company_endpoint->setCompany($company);
            $company_endpoint->setEndpoint($ep);
//            $company_endpoint->setIdCompany($company->getId());
            $company_endpoint->setIdEndpoint($ep->getId());
            $company_endpoint->setPermission($ep->getPermission());
            $company->addEndpoint($company_endpoint);
            $em->persist($company_endpoint);
            $em->flush();
        }
        return $company;
    }
    protected function createStructure($name, $l,$r,$endpoint = false,$description = null)
    {
        $structure = new Structure();
        $structure->setName($name);
        $structure->setDescription($description);
        $structure->setL($l);
        $structure->setR($r);


        $em = $this->getEntityManager();
        $em->persist($structure);
        $em->flush();
        if($endpoint) {
            if (is_array($endpoint)) {
                foreach ($endpoint as $ep) {
                    $structure_endpoint = new StructureEndpoint();
                    $structure_endpoint->setEndpoint($this->getEnpointByName($ep));
                    $structure_endpoint->setStructure($structure);
                    $structure_endpoint->setPermission(15);
                    $structure_endpoint->setShowStructure(true);
                    $em->persist($structure_endpoint);
                    $em->flush();
                }
            }
        }
        return $structure;
    }
    protected function createEndpoint($name, $permission =15)
    {
        $endpoint = new Endpoint();
        $endpoint->setUriName($name);
        $endpoint->setPermission($permission);
        $endpoint->setDescription('Description '.$name);
        $em = $this->getEntityManager();
        $em->persist($endpoint);
        $em->flush();
        return $endpoint;
    }
    protected function createAttributeType($name,$type = 'text'){
        $attribute = new Attribute();
        $attribute->setName($name);
        $attribute->setType($type);
        $em = $this->getEntityManager();
        $em->persist($attribute);
        $em->flush();
        return $attribute;
    }
    protected function  getEnpointByName($name){
        return $this->getEntityManager()->getRepository('AppBundle:Endpoint')->findOneBy(array("uriName"=>$name));
    }
    protected function getAuthorizedHeaders($username, $headers = array())
    {
        $user = $this->getEntityManager()->getRepository("AppBundle:User")->findOneBy(array("username"=>$username));
//        $token = $this->getService('lexik_jwt_authentication.encoder')
//            ->encode(['username' => $username]);

        $token = $this->getService('token_manager')->getNew($user);

//        $headers['AuthorizationTk'] = 'Bearer '.$token;
        $headers['AuthorizationTk'] = $token;

        return $headers;
    }

    /**
     * @return ResponseAsserter
     */
    protected function asserter()
    {
        if ($this->responseAsserter === null) {
            $this->responseAsserter = new ResponseAsserter();
        }

        return $this->responseAsserter;
    }

    /**
     * @param bool $iscompany
     * @return EntityManager
     */
    protected function getEntityManager($iscompany = false)
    {
        if($iscompany)
            return $this->getService('doctrine.orm.company01_entity_manager');

        return $this->getService('doctrine.orm.default_entity_manager');
    }


    /**
     * Call this when you want to compare URLs in a test
     *
     * (since the returned URL's will have /app_test.php in front)
     *
     * @param string $uri
     * @return string
     */
    protected function adjustUri($uri)
    {
        return '/app_test.php'.$uri;
    }

}
