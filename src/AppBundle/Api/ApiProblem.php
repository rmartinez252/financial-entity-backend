<?php

namespace AppBundle\Api;
use Symfony\Component\HttpFoundation\Response;

/**
 * A wrapper for holding data to be used for a application/problem+json response
 */
class ApiProblem
{
    const TYPE_VALIDATION_ERROR = 'validation_error';
    const TYPE_VALIDATION_ERROR2 = 'validation_error2';
    const TYPE_VALIDATION_ERROR3 = 'validation_error3';
    const TYPE_VALIDATION_SYSTEM_TYPE= 'system_type_validation';
    const TYPE_CLIENT_WITH_OPERATION = 'client_with_operation';
    const TYPE_INVALID_REQUEST_BODY_FORMAT = 'invalid_body_format';
    const TYPE_INVALID_REQUEST_DATA = 'invalid_data';
    const TYPE_ATTRIBUTES_DATA = 'invalid_attributes';
    const TYPE_OPERATION_PROCESS_ERROR = 'operation_process';
    const TYPE_VALIDATION_OVERPAY = 'validation_error_overpay';
    const TYPE_PAYMENT_ERROR = 'payment_error';
    const TYPE_NO_OPERATION = 'operation_error';
    const TYPE_NO_OPERATION_ID_PORTFOLIO = 'operation_error';
    const TYPE_NO_CONTENT_IN_THIS_SEARCH= 'no_content_error';
    const TYPE_NO_CLIENT_FOUND= 'no_client_in_company';

    private static $titles = array(
        self::TYPE_VALIDATION_ERROR => 'There was a validation error',
        self::TYPE_VALIDATION_ERROR2 => 'There was a validation error 2',
        self::TYPE_VALIDATION_ERROR3 => 'There was a validation error 3',
        self::TYPE_VALIDATION_SYSTEM_TYPE => 'System Type cant be Modified',
        self::TYPE_CLIENT_WITH_OPERATION => 'This client has operations and cannot be deleted',
        self::TYPE_INVALID_REQUEST_BODY_FORMAT => 'Invalid JSON format sent',
        self::TYPE_INVALID_REQUEST_DATA => 'Invalid Data sent',
        self::TYPE_ATTRIBUTES_DATA => 'You miss one needed attribute',
        self::TYPE_OPERATION_PROCESS_ERROR => 'Operation process error',
        self::TYPE_VALIDATION_OVERPAY => 'Payment process error, the value of the payment is superior of the total amount from this debtor',
        self::TYPE_PAYMENT_ERROR => 'Payment process error, some way we have a negative value',
        self::TYPE_NO_OPERATION => 'We don\'t have operations whit this conditions',
        self::TYPE_NO_OPERATION_ID_PORTFOLIO => 'We don\'t have this operation id in this portfolio',
        self::TYPE_NO_CONTENT_IN_THIS_SEARCH => 'No entries whit this conditions',
        self::TYPE_NO_CLIENT_FOUND => 'No cliente related in this company call'
    );

    private $statusCode;

    private $type;

    private $title;

    private $extraData = array();

    public function __construct($statusCode, $type = null)
    {
        $this->statusCode = $statusCode;

        if ($type === null) {
            // no type? The default is about:blank and the title should
            // be the standard status code message
            $type = 'about:blank';
            $title = isset(Response::$statusTexts[$statusCode])
                ? Response::$statusTexts[$statusCode]
                : 'Unknown status code :(';
        } else {
            if (!isset(self::$titles[$type])) {
                throw new \InvalidArgumentException('No title for type '.$type);
            }

            $title = self::$titles[$type];
        }

        $this->type = $type;
        $this->title = $title;
    }

    public function toArray()
    {
        return array_merge(
            $this->extraData,
            array(
                'status' => $this->statusCode,
                'type' => $this->type,
                'title' => $this->title,
            )
        );
    }

    public function set($name, $value)
    {
        $this->extraData[$name] = $value;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getTitle()
    {
        return $this->title;
    }
}
