<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 11/26/16
 * Time: 4:59 PM
 */

namespace Tests\AppBundle\Controller;


use AppBundle\Controller\AccountingEntryController;
use AppBundle\Test\ApiTestCase;


class AccountingEntryControllerTest extends ApiTestCase
{
    public function testAccountingEntryPOST(){
        $data = array(
            'description' => 'prueba de Entrys',
            'dateUser' => "2016-06-05",
            'entrys' => array(
                array(
                    "descriptionDebit"=>"primer entry esc Debit",
                    "descriptionCredit"=>"primer entry esc Credit",
                    "value"=>"500",
//                    "accountingEntry"=>"",
                    "accountDebit"=>"1",
                    "accountCredit"=>"167"
                ),
                array(
                    "descriptionDebit"=>"segundo entry Debit",
                    "descriptionCredit"=>"segundo entry Credit",
                    "value"=>"350",
//                    "accountingEntry"=>"",
                    "accountDebit"=>"1",
                    "accountCredit"=>"167"
                )
            )
        );

        $response = $this->client->post('/accountingentries', [
            'body' => json_encode($data,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
//        dump(json_encode($data,JSON_FORCE_OBJECT));
//        die;
        $this->debugResponse($response);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('application/hateoas+json', $response->getHeader('Content-Type')[0]);
        $this->assertTrue($response->hasHeader('Location'));
//        $this->assertStringEndsWith('/api/programmers/ObjectOrienter', $response->getHeader('Location')[0]);
    }

    public function testAccountingEntryGET(){
        $data = array(
            'description' => 'prueba de Entrys',
            'dateUser' => "2016-06-05 12:55:00",
            'entrys' => array(
                array(
                    "descriptionDebit"=>"primer entry esc Debit",
                    "descriptionCredit"=>"primer entry esc Credit",
                    "value"=>"500",
//                    "accountingEntry"=>"",
                    "accountDebit"=>"1",
                    "accountCredit"=>"167"
                ),
                array(
                    "descriptionDebit"=>"segundo entry Debit",
                    "descriptionCredit"=>"segundo entry Credit",
                    "value"=>"350",
//                    "accountingEntry"=>"",
                    "accountDebit"=>"1",
                    "accountCredit"=>"167"
                )
            )
        );

        $response = $this->client->post('/accountingentries', [
            'body' => json_encode($data,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $response = $this->client->get('/accountingentries/'.json_decode($response->getBody(true), true)['id'], [
            'body' => json_encode($data,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
//        dump(json_encode($data,JSON_FORCE_OBJECT));
//        die;



        $this->debugResponse($response);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAccountingEntryByDateGET(){
        $data = array(
            'description' => 'prueba de Entrys',
            'dateUser' => "2016-06-05 12:55:00",
            'entrys' => array(
                array(
                    "descriptionDebit"=>"primer entry esc Debit",
                    "descriptionCredit"=>"primer entry esc Credit",
                    "value"=>"500",
//                    "accountingEntry"=>"",
                    "accountDebit"=>"1",
                    "accountCredit"=>"167"
                ),
                array(
                    "descriptionDebit"=>"segundo entry Debit",
                    "descriptionCredit"=>"segundo entry Credit",
                    "value"=>"350",
//                    "accountingEntry"=>"",
                    "accountDebit"=>"1",
                    "accountCredit"=>"167"
                )
            )
        );

        $this->client->post('/accountingentries', [
            'body' => json_encode($data,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $data2['end'] = "2016-06-05 12:55:00" ;
        $response = $this->client->get('/accountingentries/byDate', [
            'body' => json_encode($data2,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
//        dump(json_encode($data,JSON_FORCE_OBJECT));
//        die;



        $this->debugResponse($response);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
