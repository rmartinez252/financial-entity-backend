<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 11/15/16
 * Time: 1:13 PM
 */

namespace Tests\AppBundle\Controller;


use AppBundle\Test\ApiTestCase;

class AccountControllerTest extends ApiTestCase
{
    public function testAccountPOST(){
        $data = array(
            'id' => '14277',
            'name' => 'NuevaCuneta',
            'dir' => '',
           );

        $response = $this->client->post('/accounts', [
            'body' => json_encode($data,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
//        dump(json_encode($data,JSON_FORCE_OBJECT));
//        die;
        $this->debugResponse($response);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('application/hateoas+json', $response->getHeader('Content-Type')[0]);
        $this->assertTrue($response->hasHeader('Location'));
//        $this->assertStringEndsWith('/api/programmers/ObjectOrienter', $response->getHeader('Location')[0]);
    }
}
