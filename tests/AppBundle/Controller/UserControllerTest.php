<?php

namespace Tests\AppBundle\Controller;

use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Entry;
use AppBundle\Test\ApiTestCase;
use Doctrine\Common\Collections\ArrayCollection;

class UserControllerTest extends ApiTestCase
{
    protected function setUp()
    {

        parent::setUp();
    }

    public function testUserPOST()
    {
        $data = array(
            'username' => 'ObjectOrienter',
        );

        // 1) Create a programmer resource        return $response;

        $response = $this->client->post('/userstest', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
//        $this->debugResponse($response);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('application/hateoas+json', $response->getHeader('Content-Type')[0]);
        $this->assertTrue($response->hasHeader('Location'));
//        $this->assertStringEndsWith('/api/programmers/ObjectOrienter', $response->getHeader('Location')[0]);
        $finishedData = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('username', $finishedData);
        $this->assertEquals('ObjectOrienter', $finishedData['username']);
    }

    public function testUserPOSTReal1(){
        $at1 = array(
            'attribute'=>$this->createAttributeType('phone')->getId(),
            'value'=>'adsd'
        );
        $at2 =  array(
            'attribute'=>$this->createAttributeType('phone2')->getId(),
            'value'=>'adsd2'
        );
        $attrs[] = $at1;
        $attrs[] = $at2;
        $data = array(
            'username' => 'Superman',
            'password' => 'clarkkent',
            'person' =>
                array(
                'name'=>'Ronald',
                'lastname'=>'Martinez',
                'attributes' => $attrs
                )

            );

        $response = $this->client->post('/users', [
            'body' => json_encode($data,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
//        dump(json_encode($data,JSON_FORCE_OBJECT));
//        die;
        $this->debugResponse($response);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('application/hateoas+json', $response->getHeader('Content-Type')[0]);
        $this->assertTrue($response->hasHeader('Location'));
//        $this->assertStringEndsWith('/api/programmers/ObjectOrienter', $response->getHeader('Location')[0]);
        $finishedData = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('username', $finishedData);
        $this->assertEquals('Superman', $finishedData['username']);
    }

    public function testUserPATH()
    {
        $at1 = array(
            'attribute'=>$this->createAttributeType('phone')->getId(),
            'value'=>'adsd'
        );
        $at2 =  array(
            'attribute'=>$this->createAttributeType('phone2')->getId(),
            'value'=>'adsd2'
        );
        $attrs[] = $at1;
        $attrs[] = $at2;
        $data = array(
            'username' => 'Superman',
            'password' => 'clarkkent',
            'person' =>
                array(
                'name'=>'Ronald',
                'lastname'=>'Martinez',
                'attributes' => $attrs
                )

            );
        $response = $this->client->post('/users', [
            'body' => json_encode($data,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
//        $this->debugResponse($response);
        $finishedDataB = json_decode($response->getBody(true), true);
        $at3 = array(
            'attribute'=>$this->createAttributeType('phone')->getId(),
            'value'=>'adsd33'
        );
        $dataA = array(
            'username' => 'Acuaman',
            'person' =>
                array(
                    'name'=>'Rafael',
                    'lastname'=>'Simosa',
                    'attributes' => array('0'=>$at3)
                )
        );
        $response = $this->client->patch('/users/'.$finishedDataB['id'], [
            'body' => json_encode($dataA,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
//        $this->debugResponse($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/hateoas+json', $response->getHeader('Content-Type')[0]);
        $this->assertTrue($response->hasHeader('Location'));
        $finishedDataA = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('username', $finishedDataA);
        $this->assertEquals('Superman', $finishedDataB['username']);
        $this->assertEquals('Acuaman', $finishedDataA['username']);
    }

    public function testUserGET(){
        $response = $this->client->get('/users', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->debugResponse($response);

    }

    public function testUserGETOne(){
        $response = $this->client->get('/userstest/cyberlord', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $dataid = json_decode($response->getBody(), true);
        var_dump($dataid[0]['id']);
        $response = $this->client->get('/users/'.$dataid[0]['id'], [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);

    }

    public function testUserDELETE(){
        $this->createUser('holamundo');

        $response = $this->client->get('/userstest/holamundo', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $dataid = json_decode($response->getBody(), true);
        $response = $this->client->delete('/users/'.$dataid[0]['id'], [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
    }

    public function testUserDELETEBadRequest(){
        $this->createUser('holamundo');

        $response = $this->client->get('/userstest/holamundo', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $dataid = json_decode($response->getBody(), true);
        $response = $this->client->delete('/users/'.$dataid[0]['id'].'0', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(400, $response->getStatusCode());
//        $this->debugResponse($response);
    }

    public function testUserPermissionGET(){
        $response = $this->client->get('/permissions', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->debugResponse($response);

    }
    public function testUserPermissionPOST(){
        $response = $this->client->get('/userstest/cyberlord', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $dataid = json_decode($response->getBody(), true);
        $response = $this->client->get('/permissions', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $permissions = json_decode($response->getBody(), true);
//        $permissions = $permissions;

        $response = $this->client->post('/permissions/'.$dataid[0]['id'], [
            'body' => json_encode($permissions,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(201, $response->getStatusCode());
        $this->debugResponse($response);


        /*
         * "Permissions": [

    ]
*/
    }



//    public function testUserGETForm(){
//        $response = $this->client->get('/form/user', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
//
//    }
//
//    public function testUserGETUpdateForm(){
//        $at1 = array(
//            'attribute'=>$this->createAttributeType('phone')->getId(),
//            'value'=>'adsd'
//        );
//        $at2 =  array(
//            'attribute'=>$this->createAttributeType('phone2')->getId(),
//            'value'=>'adsd2'
//        );
//        $attrs[] = $at1;
//        $attrs[] = $at2;
//        $data = array(
//            'username' => 'Superman',
//            'password' => 'clarkkent',
//            'person' =>
//                array(
//                    'name'=>'Ronald',
//                    'lastname'=>'Martinez',
//                    'attributes' => $attrs
//                )
//        );
//        $response = $this->client->post('/users', [
//            'body' => json_encode($data,JSON_FORCE_OBJECT),
//            'headers' => $this->getAuthorizedHeaders('admin')
//        ]);
//        $response = $this->client->get('/userstest/Superman', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $dataid = json_decode($response->getBody(), true);
//        $response = $this->client->get('/users/form/'.$dataid[0]['id'], [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
//
//    }


    public function newEntry($id){
        $em = $this->getEntityManager(true);
//        $emUser = $this->getEntityManager();
        $entry =  new AccountingEntry();
//        $user = $emUser->getRepository("AppBundle:User")->findOneBy(array("id"=>$id));

        $entrys = new ArrayCollection();
        for ($i=1;$i>5;$i++){
            $ent = new Entry();
            $ent->setDescription('hola');
            $ent->setValue(50);
            $entrys[] = $ent;
        }
        $entry->setIdUser($id);
        $entry->setDescription('mundo');
        $entry->setIdBeneficiary(1);
        $entry->setIdModified(1);
        $entry->setDate(new \DateTime('now'));
        $entry->setEntrys($entrys);
        $em->persist($entry);
        $em->flush();
        return $entry;
    }
    public function testEntry()
    {

        $response = $this->client->get('/userstest/cyberlord', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $dataid = json_decode($response->getBody(), true);
        var_dump($dataid[0]['id']);
        $this->newEntry($dataid[0]['id']);
        var_dump($this->newEntry($dataid[0]['id'])->getIdUser());
    }
//    public function testGETProgrammer()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//
//        $response = $this->client->get('/api/programmers/UnitTester', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertiesExist($response, array(
//            'nickname',
//            'avatarNumber',
//            'powerLevel',
//            'tagLine'
//        ));
//        $this->asserter()->assertResponsePropertyEquals($response, 'nickname', 'UnitTester');
//        $this->asserter()->assertResponsePropertyEquals(
//            $response,
//            '_links.self',
//            $this->adjustUri('/api/programmers/UnitTester')
//        );
//    }
//
//    public function testFollowProgrammerBattlesLink()
//    {
//        $programmer = $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//        $project = $this->createProject('cool_project');
//
//        /** @var BattleManager $battleManager */
//        $battleManager = $this->getService('battle.battle_manager');
//        $battleManager->battle($programmer, $project);
//        $battleManager->battle($programmer, $project);
//        $battleManager->battle($programmer, $project);
//
//        $response = $this->client->get('/api/programmers/UnitTester', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $url = $this->asserter()
//            ->readResponseProperty($response, '_links.battles');
//        $response = $this->client->get($url, [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->asserter()->assertResponsePropertyExists($response, 'items');
//        $this->debugResponse($response);
//    }
//
//    public function testGETProgrammerDeep()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//
//        $response = $this->client->get('/api/programmers/UnitTester?deep=1', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertiesExist($response, array(
//            'user.username'
//        ));
//    }
//
//    public function testGETProgrammersCollection()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//        $this->createProgrammer(array(
//            'nickname' => 'CowboyCoder',
//            'avatarNumber' => 5,
//        ));
//
//        $response = $this->client->get('/api/programmers', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyIsArray($response, 'items');
//        $this->asserter()->assertResponsePropertyCount($response, 'items', 2);
//        $this->asserter()->assertResponsePropertyEquals($response, 'items[1].nickname', 'CowboyCoder');
//    }
//
//    public function testGETProgrammersCollectionPagination()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'willnotmatch',
//            'avatarNumber' => 5,
//        ));
//
//        for ($i = 0; $i < 25; $i++) {
//            $this->createProgrammer(array(
//                'nickname' => 'Programmer'.$i,
//                'avatarNumber' => 3,
//            ));
//        }
//
//        // page 1
//        $response = $this->client->get('/api/programmers?filter=programmer', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals(
//            $response,
//            'items[5].nickname',
//            'Programmer5'
//        );
//
//        $this->asserter()->assertResponsePropertyEquals($response, 'count', 10);
//        $this->asserter()->assertResponsePropertyEquals($response, 'total', 25);
//        $this->asserter()->assertResponsePropertyExists($response, '_links.next');
//
//        // page 2
//        $nextLink = $this->asserter()->readResponseProperty($response, '_links.next');
//        $response = $this->client->get($nextLink, [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals(
//            $response,
//            'items[5].nickname',
//            'Programmer15'
//        );
//        $this->asserter()->assertResponsePropertyEquals($response, 'count', 10);
//
//        $lastLink = $this->asserter()->readResponseProperty($response, '_links.last');
//        $response = $this->client->get($lastLink, [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals(
//            $response,
//            'items[4].nickname',
//            'Programmer24'
//        );
//
//        $this->asserter()->assertResponsePropertyDoesNotExist($response, 'items[5].name');
//        $this->asserter()->assertResponsePropertyEquals($response, 'count', 5);
//    }
//
//    public function testPUTProgrammer()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'CowboyCoder',
//            'avatarNumber' => 5,
//            'tagLine' => 'foo',
//        ));
//
//        $data = array(
//            'nickname' => 'CowgirlCoder',
//            'avatarNumber' => 2,
//            'tagLine' => 'foo',
//        );
//        $response = $this->client->put('/api/programmers/CowboyCoder', [
//            'body' => json_encode($data),
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals($response, 'avatarNumber', 2);
//        // the nickname is immutable on edit
//        $this->asserter()->assertResponsePropertyEquals($response, 'nickname', 'CowboyCoder');
//    }
//
//    public function testPATCHProgrammer()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'CowboyCoder',
//            'avatarNumber' => 5,
//            'tagLine' => 'foo',
//        ));
//
//        $data = array(
//            'tagLine' => 'bar',
//        );
//        $response = $this->client->patch('/api/programmers/CowboyCoder', [
//            'body' => json_encode($data),
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals($response, 'avatarNumber', 5);
//        $this->asserter()->assertResponsePropertyEquals($response, 'tagLine', 'bar');
//    }
//
//    public function testDELETEProgrammer()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//
//        $response = $this->client->delete('/api/programmers/UnitTester', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(204, $response->getStatusCode());
//    }
//
//    public function testValidationErrors()
//    {
//        $data = array(
//            'avatarNumber' => 2,
//            'tagLine' => 'I\'m from a test!'
//        );
//
//        // 1) Create a programmer resource
//        $response = $this->client->post('/api/programmers', [
//            'body' => json_encode($data),
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//
//        $this->assertEquals(400, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertiesExist($response, array(
//            'type',
//            'title',
//            'errors',
//        ));
//        $this->asserter()->assertResponsePropertyExists($response, 'errors.nickname');
//        $this->asserter()->assertResponsePropertyEquals($response, 'errors.nickname[0]', 'Please enter a clever nickname');
//        $this->asserter()->assertResponsePropertyDoesNotExist($response, 'errors.avatarNumber');
//        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type')[0]);
//    }
//
//    public function testInvalidJson()
//    {
//        $invalidBody = <<<EOF
//{
//    "nickname": "JohnnyRobot",
//    "avatarNumber" : "2
//    "tagLine": "I'm from a test!"
//}
//EOF;
//
//        $response = $this->client->post('/api/programmers', [
//            'body' => $invalidBody,
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//
//        $this->assertEquals(400, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyContains($response, 'type', 'invalid_body_format');
//    }
//
//    public function test404Exception()
//    {
//        $response = $this->client->get('/api/programmers/fake', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//
//        $this->assertEquals(404, $response->getStatusCode());
//        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type')[0]);
//        $this->asserter()->assertResponsePropertyEquals($response, 'type', 'about:blank');
//        $this->asserter()->assertResponsePropertyEquals($response, 'title', 'Not Found');
//        $this->asserter()->assertResponsePropertyEquals($response, 'detail', 'No programmer found with nickname "fake"');
//    }
//
//    public function testRequiresAuthentication()
//    {
//        $response = $this->client->post('/api/programmers', [
//            'body' => '[]'
//            // do not send auth!
//        ]);
//        $this->assertEquals(401, $response->getStatusCode());
//    }
//
//    public function testBadToken()
//    {
//        $response = $this->client->post('/api/programmers', [
//            'body' => '[]',
//            'headers' => [
//                'Authorization' => 'Bearer WRONG'
//            ]
//        ]);
//        $this->assertEquals(401, $response->getStatusCode());
//        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type')[0]);
//    }
//
//    public function testEditTagline()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//            'tagLine' => 'The original UnitTester'
//        ));
//
//        $response = $this->client->put('/api/programmers/UnitTester/tagline', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord'),
//            'body' => 'New Tag Line'
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->assertEquals('New Tag Line', (string) $response->getBody());
//    }
//
//    public function testPowerUp()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//            'powerLevel' => 10
//        ));
//
//        $response = $this->client->post('/api/programmers/UnitTester/powerup', [
//            'headers' => $this->getAuthorizedHeaders('cyberlord')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $powerLevel = $this->asserter()
//            ->readResponseProperty($response, 'powerLevel');
//        $this->assertNotEquals(10, $powerLevel, 'The level should change');
//    }
}
