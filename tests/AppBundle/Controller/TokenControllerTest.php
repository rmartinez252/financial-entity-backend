<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Test\ApiTestCase;

class TokenControllerTest extends ApiTestCase
{
    public function testPOSTCreateToken()
    {
        $this->createUser('cyber', 'I<3Pizza');

        $response = $this->client->post('/tokens', [
            'auth' => ['cyber', 'I<3Pizza']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyExists(
            $response,
            'token'
        );
//        $this->debugResponse($response);
    }

    public function testPOSTTokenInvalidCredentials()
    {
        $this->createUser('cyber', 'I<3Pizza');

        $response = $this->client->post('/tokens', [
            'auth' => ['cyber', 'IH8Pizza']
        ]);
        $this->assertEquals(401, $response->getStatusCode());
        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type')[0]);
        $this->asserter()->assertResponsePropertyEquals($response, 'type', 'about:blank');
        $this->asserter()->assertResponsePropertyEquals($response, 'title', 'Unauthorized');
        $this->asserter()->assertResponsePropertyEquals($response, 'detail', 'Invalid credentials.');
    }
}
