<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Test\ApiTestCase;

class StructureTest extends ApiTestCase
{

    protected function setUp()
    {
        parent::setUp();

        $this->createEndpoint('ACCOUNTING_ENTRY');
        $this->createEndpoint('ACCOUNTING_REPORT');
        $this->createEndpoint('ACCOUNTING_PARAMETER_ACCOUNT');
        $this->createEndpoint('ACCOUNTING_PARAMETER_PRODUCT');
        $this->createUser('weaverryan');
        $datas[] = array(
            'name' => 'Accounting',
            'l' => 1,
            'r' => 20
        );
        $datas[] = array(
            'name' => 'Accounting Entry',
            'l' => 2,
            'r' => 3
        );
        $datas[] = array(
            'name' => 'Search',
            'l' => 4,
            'r' => 5
        );
        $datas[] = array(
            'name' => 'Reports',
            'l' => 6,
            'r' => 13
        );
        $datas[] = array(
            'name' => 'Report 01',
            'l' => 7,
            'r' => 8
        );
        $datas[] = array(
            'name' => 'Report 02',
            'l' => 9,
            'r' => 10
        );
        $datas[] = array(
            'name' => 'Report 03',
            'l' => 11,
            'r' => 12
        );
        $datas[] = array(
            'name' => 'Parameterization',
            'l' => 14,
            'r' => 19
        );
        $datas[] = array(
            'name' => 'Products',
            'l' => 15,
            'r' => 16
        );
        $datas[] = array(
            'name' => 'Accounts',
            'l' => 17,
            'r' => 18
        );
        foreach ($datas as $data){
//            $this->client->post('/structurestest', [
//                'body' => json_encode($data),
//                'headers' => $this->getAuthorizedHeaders('weaverryan')
//            ]);
            $endpoints = false;
            if(isset($data['endpoint']))
                $endpoints =$data['endpoint'];

            $this->createStructure($data['name'],$data['l'],$data['r'],$endpoints);
        }
    }

    public function testStructureGetAll()
    {

        $response = $this->client->get('/structures', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->debugResponse($response);
    }
    public function testStructureGetMain()
    {

        $response = $this->client->get('/structures/showMain', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
    }
    public function testStructureGetSons()
    {
        $response = $this->client->get('/structurestest/Accounting', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);

        $dataid = json_decode($response->getBody(), true);

        $response = $this->client->get('/structures/'.$dataid[1]['node']['id'].'/sons', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
    }
    public function testStructureGetParents()
    {
        $response = $this->client->get('/structurestest/Reports', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);

        $dataid = json_decode($response->getBody(), true);

        $response = $this->client->get('/structures/'.$dataid[6]['node']['id'].'/parents', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
    }
    public function testStructureGetFamily()
    {
        $response = $this->client->get('/structurestest/Reports', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);

        $dataid = json_decode($response->getBody(), true);

        $response = $this->client->get('/structures/'.$dataid[6]['node']['id'].'/family', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
        $this->assertEquals(200, $response->getStatusCode());


//        $this->debugResponse($response);
    }
    public function testStructureGetOne()
    {
        $response = $this->client->get('/structurestest/Reports', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);

        $dataid = json_decode($response->getBody(), true);
        $response = $this->client->get('/structures/'.$dataid[6]['node']['id'], [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
    }
    public function testStructureAddOneSubLeft()
    {
        $response = $this->client->get('/structurestest/Accounting', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);

        $dataid = json_decode($response->getBody(), true);
        $data = array(
            'name' => 'TESTT',
            'id' => $dataid[1]['node']['id'],
        );

        $response = $this->client->post('/structures', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
//        $this->assertEquals(201, $response->getStatusCode());
        $response = $this->client->get('/structures', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
//        $this->debugResponse($response);
    }
    public function testStructureAddOneSubRight()
    {
        $response = $this->client->get('/structurestest/Accounting', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);

        $dataid = json_decode($response->getBody(), true);
        $data = array(
            'name' => 'TESTT',
            'id' => $dataid[1]['node']['id'],
            'dir' => 'subr',
        );

        $response = $this->client->post('/structures', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
//        $this->assertEquals(201, $response->getStatusCode());
        $response = $this->client->get('/structures', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
//        $this->debugResponse($response);
    }
    public function testStructureAddOneDer()
    {
        $response = $this->client->get('/structurestest/Accounting', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);

        $dataid = json_decode($response->getBody(), true);
        $data = array(
            'name' => 'TESTT',
            'id' => $dataid[1]['node']['id'],
            'dir' => 'right'
        );
        $response = $this->client->post('/structures', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
//        $this->assertEquals(201, $response->getStatusCode());
        $response = $this->client->get('/structures', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
//        $this->debugResponse($response);
    }
    public function testStructureAddOneIzq()
    {
        $response = $this->client->get('/structurestest/Accounting', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);

        $dataid = json_decode($response->getBody(), true);
        $data = array(
            'name' => 'TESTT',
            'id' => $dataid[1]['node']['id'],
            'dir' => 'left'
        );
        $response = $this->client->post('/structures', [
            'body' => json_encode($data),
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
//        $this->assertEquals(201, $response->getStatusCode());
        $response = $this->client->get('/structures', [

            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
//        $this->debugResponse($response);
    }
    public function testStructureDelete()
    {
        $response = $this->client->get('/structurestest/Accounting', [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
        $dataid = json_decode($response->getBody(), true);
        $response = $this->client->delete('/structures/'.$dataid[1]['node']['id'], [
            'headers' => $this->getAuthorizedHeaders('weaverryan')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
    }

//    public function testGETProgrammer()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//
//        $response = $this->client->get('/api/programmers/UnitTester', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertiesExist($response, array(
//            'nickname',
//            'avatarNumber',
//            'powerLevel',
//            'tagLine'
//        ));
//        $this->asserter()->assertResponsePropertyEquals($response, 'nickname', 'UnitTester');
//        $this->asserter()->assertResponsePropertyEquals(
//            $response,
//            '_links.self',
//            $this->adjustUri('/api/programmers/UnitTester')
//        );
//    }
//
//    public function testFollowProgrammerBattlesLink()
//    {
//        $programmer = $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//        $project = $this->createProject('cool_project');
//
//        /** @var BattleManager $battleManager */
//        $battleManager = $this->getService('battle.battle_manager');
//        $battleManager->battle($programmer, $project);
//        $battleManager->battle($programmer, $project);
//        $battleManager->battle($programmer, $project);
//
//        $response = $this->client->get('/api/programmers/UnitTester', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $url = $this->asserter()
//            ->readResponseProperty($response, '_links.battles');
//        $response = $this->client->get($url, [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->asserter()->assertResponsePropertyExists($response, 'items');
//        $this->debugResponse($response);
//    }
//
//    public function testGETProgrammerDeep()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//
//        $response = $this->client->get('/api/programmers/UnitTester?deep=1', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertiesExist($response, array(
//            'user.username'
//        ));
//    }
//
//    public function testGETProgrammersCollection()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//        $this->createProgrammer(array(
//            'nickname' => 'CowboyCoder',
//            'avatarNumber' => 5,
//        ));
//
//        $response = $this->client->get('/api/programmers', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyIsArray($response, 'items');
//        $this->asserter()->assertResponsePropertyCount($response, 'items', 2);
//        $this->asserter()->assertResponsePropertyEquals($response, 'items[1].nickname', 'CowboyCoder');
//    }
//
//    public function testGETProgrammersCollectionPagination()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'willnotmatch',
//            'avatarNumber' => 5,
//        ));
//
//        for ($i = 0; $i < 25; $i++) {
//            $this->createProgrammer(array(
//                'nickname' => 'Programmer'.$i,
//                'avatarNumber' => 3,
//            ));
//        }
//
//        // page 1
//        $response = $this->client->get('/api/programmers?filter=programmer', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals(
//            $response,
//            'items[5].nickname',
//            'Programmer5'
//        );
//
//        $this->asserter()->assertResponsePropertyEquals($response, 'count', 10);
//        $this->asserter()->assertResponsePropertyEquals($response, 'total', 25);
//        $this->asserter()->assertResponsePropertyExists($response, '_links.next');
//
//        // page 2
//        $nextLink = $this->asserter()->readResponseProperty($response, '_links.next');
//        $response = $this->client->get($nextLink, [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals(
//            $response,
//            'items[5].nickname',
//            'Programmer15'
//        );
//        $this->asserter()->assertResponsePropertyEquals($response, 'count', 10);
//
//        $lastLink = $this->asserter()->readResponseProperty($response, '_links.last');
//        $response = $this->client->get($lastLink, [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals(
//            $response,
//            'items[4].nickname',
//            'Programmer24'
//        );
//
//        $this->asserter()->assertResponsePropertyDoesNotExist($response, 'items[5].name');
//        $this->asserter()->assertResponsePropertyEquals($response, 'count', 5);
//    }
//
//    public function testPUTProgrammer()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'CowboyCoder',
//            'avatarNumber' => 5,
//            'tagLine' => 'foo',
//        ));
//
//        $data = array(
//            'nickname' => 'CowgirlCoder',
//            'avatarNumber' => 2,
//            'tagLine' => 'foo',
//        );
//        $response = $this->client->put('/api/programmers/CowboyCoder', [
//            'body' => json_encode($data),
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals($response, 'avatarNumber', 2);
//        // the nickname is immutable on edit
//        $this->asserter()->assertResponsePropertyEquals($response, 'nickname', 'CowboyCoder');
//    }
//
//    public function testPATCHProgrammer()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'CowboyCoder',
//            'avatarNumber' => 5,
//            'tagLine' => 'foo',
//        ));
//
//        $data = array(
//            'tagLine' => 'bar',
//        );
//        $response = $this->client->patch('/api/programmers/CowboyCoder', [
//            'body' => json_encode($data),
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyEquals($response, 'avatarNumber', 5);
//        $this->asserter()->assertResponsePropertyEquals($response, 'tagLine', 'bar');
//    }
//
//    public function testDELETEProgrammer()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//        ));
//
//        $response = $this->client->delete('/api/programmers/UnitTester', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(204, $response->getStatusCode());
//    }
//
//    public function testValidationErrors()
//    {
//        $data = array(
//            'avatarNumber' => 2,
//            'tagLine' => 'I\'m from a test!'
//        );
//
//        // 1) Create a programmer resource
//        $response = $this->client->post('/api/programmers', [
//            'body' => json_encode($data),
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//
//        $this->assertEquals(400, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertiesExist($response, array(
//            'type',
//            'title',
//            'errors',
//        ));
//        $this->asserter()->assertResponsePropertyExists($response, 'errors.nickname');
//        $this->asserter()->assertResponsePropertyEquals($response, 'errors.nickname[0]', 'Please enter a clever nickname');
//        $this->asserter()->assertResponsePropertyDoesNotExist($response, 'errors.avatarNumber');
//        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type')[0]);
//    }
//
//    public function testInvalidJson()
//    {
//        $invalidBody = <<<EOF
//{
//    "nickname": "JohnnyRobot",
//    "avatarNumber" : "2
//    "tagLine": "I'm from a test!"
//}
//EOF;
//
//        $response = $this->client->post('/api/programmers', [
//            'body' => $invalidBody,
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//
//        $this->assertEquals(400, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyContains($response, 'type', 'invalid_body_format');
//    }
//
//    public function test404Exception()
//    {
//        $response = $this->client->get('/api/programmers/fake', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//
//        $this->assertEquals(404, $response->getStatusCode());
//        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type')[0]);
//        $this->asserter()->assertResponsePropertyEquals($response, 'type', 'about:blank');
//        $this->asserter()->assertResponsePropertyEquals($response, 'title', 'Not Found');
//        $this->asserter()->assertResponsePropertyEquals($response, 'detail', 'No programmer found with nickname "fake"');
//    }
//
//    public function testRequiresAuthentication()
//    {
//        $response = $this->client->post('/api/programmers', [
//            'body' => '[]'
//            // do not send auth!
//        ]);
//        $this->assertEquals(401, $response->getStatusCode());
//    }
//
//    public function testBadToken()
//    {
//        $response = $this->client->post('/api/programmers', [
//            'body' => '[]',
//            'headers' => [
//                'Authorization' => 'Bearer WRONG'
//            ]
//        ]);
//        $this->assertEquals(401, $response->getStatusCode());
//        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type')[0]);
//    }
//
//    public function testEditTagline()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//            'tagLine' => 'The original UnitTester'
//        ));
//
//        $response = $this->client->put('/api/programmers/UnitTester/tagline', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan'),
//            'body' => 'New Tag Line'
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $this->assertEquals('New Tag Line', (string) $response->getBody());
//    }
//
//    public function testPowerUp()
//    {
//        $this->createProgrammer(array(
//            'nickname' => 'UnitTester',
//            'avatarNumber' => 3,
//            'powerLevel' => 10
//        ));
//
//        $response = $this->client->post('/api/programmers/UnitTester/powerup', [
//            'headers' => $this->getAuthorizedHeaders('weaverryan')
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//        $powerLevel = $this->asserter()
//            ->readResponseProperty($response, 'powerLevel');
//        $this->assertNotEquals(10, $powerLevel, 'The level should change');
//    }
}
