<?php
/**
 * Created by PhpStorm.
 * User: cyberlord
 * Date: 10/20/16
 * Time: 8:59 AM
 */

namespace Tests\AppBundle\Controller;


use AppBundle\EntityCompanies\AccountingEntry;
use AppBundle\EntityCompanies\Entry;
use AppBundle\Test\ApiTestCase;
use Doctrine\Common\Collections\ArrayCollection;

class LoginControllerTest extends ApiTestCase
{
    protected function setUp()
    {
        parent::setUp();

    }

    public function testLoginPOST()
    {

        $response = $this->client->post('/login', [
            'auth' => ['cyberlord', 'foo']
        ]);
        $this->assertEquals(201, $response->getStatusCode());
//        $this->asserter()->assertResponsePropertyExists(
//            $response,
//            'token'
//        );
        $this->debugResponse($response);
    }

}
