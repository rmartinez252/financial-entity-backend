<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Test\ApiTestCase;

class ClientControllerTest extends ApiTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->createEndpoint('ACCOUNTING_ENTRY');
        $this->createEndpoint('ACCOUNTING_REPORT');
        $this->createEndpoint('ACCOUNTING_PARAMETER_ACCOUNT');
        $this->createEndpoint('ACCOUNTING_PARAMETER_PRODUCT');
        $this->createUser('admin');
        $this->createUserAndPermissions('cyberlord','foo',$this->createCompany('TCG Capital'));
        $datas[] = array(
            'name' => 'Accounting',
            'l' => 1,
            'r' => 20
        );
        $datas[] = array(
            'name' => 'Accounting Entry',
            'l' => 2,
            'r' => 3
        );
        $datas[] = array(
            'name' => 'Search',
            'l' => 4,
            'r' => 5
        );
        $datas[] = array(
            'name' => 'Reports',
            'l' => 6,
            'r' => 13
        );
        $datas[] = array(
            'name' => 'Report 01',
            'l' => 7,
            'r' => 8
        );
        $datas[] = array(
            'name' => 'Report 02',
            'l' => 9,
            'r' => 10
        );
        $datas[] = array(
            'name' => 'Report 03',
            'l' => 11,
            'r' => 12,
            'endpoint' => array('ACCOUNTING_REPORT')
        );
        $datas[] = array(
            'name' => 'Parameterization',
            'l' => 14,
            'r' => 19
        );
        $datas[] = array(
            'name' => 'Products',
            'l' => 15,
            'r' => 16,
            'endpoint' => array('ACCOUNTING_PARAMETER_PRODUCT')
        );
        $datas[] = array(
            'name' => 'Accounts',
            'l' => 17,
            'r' => 18
        );
        foreach ($datas as $data){
            $endpoints = false;
            if(isset($data['endpoint']))
                $endpoints =$data['endpoint'];
            $this->createStructure($data['name'],$data['l'],$data['r'],$endpoints);
        }
    }

    public function testClientPOSTReal1(){
        $at1 = array(
            'attribute'=>$this->createAttributeType('phone')->getId(),
            'value'=>'adsd'
        );
        $at2 =  array(
            'attribute'=>$this->createAttributeType('phone2')->getId(),
            'value'=>'adsd2'
        );
        $attrs[] = $at1;
        $attrs[] = $at2;
        $data = array(
            'name' => 'Acme',
            'direction' => "Warner's Brother's",
            'email' => "AllIsB@d.today",
            'phone' => "(305)6761728",
            'contactPerson' =>
                array(
                    'name'=>'bucks',
                    'lastname'=>'bunny',
                    'attributes' => $attrs
                )

        );
        $response = $this->client->post('/clients', [
            'body' => json_encode($data,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('application/hateoas+json', $response->getHeader('Content-Type')[0]);
        $this->assertTrue($response->hasHeader('Location'));
        $finishedData = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('name', $finishedData);
        $this->assertEquals('Acme', $finishedData['name']);
//        $this->debugResponse($response);
    }

    public function testClientPATH(){

        $new = $this->testClientPOSTReal1();
        $response = $this->client->get('/clients', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $finishedDataB = json_decode($response->getBody(true), true);
        $at3 = array(
            'attribute'=>$this->createAttributeType('phone')->getId(),
            'value'=>'adsd33'
        );
        $dataA = array(
            'direction' => 'BadGuys',
            'contactPerson' =>
                array(
                    'name'=>'Sam',
                    'lastname'=>'Yosemite',
                    'attributes' => array('0'=>$at3)
                )
        );
        $response = $this->client->patch('/clients/'.$finishedDataB['clients'][0]['id'], [
            'body' => json_encode($dataA,JSON_FORCE_OBJECT),
            'headers' => $this->getAuthorizedHeaders('admin')
        ]);
//        $this->debugResponse($response);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('application/hateoas+json', $response->getHeader('Content-Type')[0]);
        $this->assertTrue($response->hasHeader('Location'));
        $finishedDataA = json_decode($response->getBody(true), true);
        $this->assertEquals("Warner's Brother's", $finishedDataB['clients'][0]['direction']);
        $this->assertEquals('BadGuys', $finishedDataA['direction']);
    }

    public function testClientGET(){
        $new = $this->testClientPOSTReal1();
        $response = $this->client->get('/clients', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);

    }

    public function testClientGETOne(){
        $new = $this->testClientPOSTReal1();
        $response = $this->client->get('/clients', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $dataId = json_decode($response->getBody(), true);
        $response = $this->client->get('/clients/'.$dataId['clients'][0]['id'], [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->debugResponse($response);

    }

    public function testClientDELETE(){
        $new = $this->testClientPOSTReal1();
        $response = $this->client->get('/clients', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $dataId = json_decode($response->getBody(), true);
        $response = $this->client->delete('/clients/'.$dataId['clients'][0]['id'], [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->debugResponse($response);
    }

    public function testClientDELETEBadRequest(){
        $new = $this->testClientPOSTReal1();
        $response = $this->client->get('/clients', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $dataId = json_decode($response->getBody(), true);
        $response = $this->client->delete('/clients/'.$dataId['clients'][0]['id'].'0', [
            'headers' => $this->getAuthorizedHeaders('cyberlord')
        ]);
        $this->assertEquals(400, $response->getStatusCode());
        $this->debugResponse($response);
    }
}
