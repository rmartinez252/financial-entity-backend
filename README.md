# Financial Entity Back End
====
### A Symfony project created on October 9, 2016, 12:59 pm.

### The main function 

- REST API
- Maintain the accounting accounts of multiple companies 
- Maintain the transaction between this accounts
- CRUD for Operations (Factoring Operations)
- Run Accruals Daily and Monthly on this Operations
- User profile to manage the permissions   
 
### Operation Classes
The classes that I use to create and maintain the Operations in the system are allocated in `src/AppBundle/Operations` 

3 Abstract classes 
    
- `Process.php`
    - `Funding.php`
    - `Lending.php`   
  
Plus one extra class for each type of operation in the system

- `AssetBaseLoadProcess.php`
- `Factoring01Process.php`
- `FactoringHistoricProcess.php`
- `ReverseFactoring01Process.php`
- `ReverseFactoring02Process.php`

And One class that work as entry point to use this classes

- `OperationProcess.php`


For any location in the system tha we need to access this process, we call this last class and all the process is manage inside this classes, you can see an example `src\AppBundle\Controller\OperationController.php` in line `85`



    
    
   
    
    
    
    
